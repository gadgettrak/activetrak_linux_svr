<div id="reports">

	
	
	<div id="map_canvas_details"></div><br/>

	<?php if(count($deviceConnections)) {?>

	<p> Showing <?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?> Connections
	</p>

	<table class="tabledata" style="margin-top:20px;">
		<thead>
			<tr>
				<th class="ui-corner-tl mobile_hidden"></th>
				<th>Date</th>
				<?php if($device->productid == 8) { ?>
				<th class="mobile_hidden">Location</th>
				<?php }?>
				<th class="mobile_hidden">IP Address</th>
				<th class="mobile_hidden">Latitude</th>
				<th class="mobile_hidden">Longitude</th>
				<th class="mobile_only"></th>
				<th class="ui-corner-tr mobile_hidden"></th>
			</tr>
		</thead>

		<tbody>
			<?php 
			//$index = $paginator->rowStartNum;
			$index = 1;
			foreach($deviceConnections as $connection) {
			?>
				<tr <?php if($connection->isValidLocation()) {?>id="dr_<?=$index?>"<?php }?>>
					<td style="width:28px;" class="mobile_hidden"><?php if($connection->isValidLocation()) {?><img class="mobile_hidden" src="/_gfx/gmap-icons/<?=$index?>.png" alt=""/><?php }?></td>
					<td><?=$connection->timestamp?></td>
					<?php if($device->productid == 8) { ?>
					<td class="mobile_hidden"><?=$connection->loc_street?> <?=$connection->loc_city?> <?=$connection->loc_state?></td>
					<? }?>
					<td class="mobile_hidden"><a title="IP Address details :: <?=$connection->public_ip?>" class="ip-link" href="/ip?ip=<?=$connection->public_ip?>"><?=$connection->public_ip?></a></td>
					<td class="mobile_hidden"><?=$connection->getLat()?></td>
					<td class="mobile_hidden"><?=$connection->getLon()?></td>
					<td class="mobile_only"><a href="http://maps.google.com/maps?q=<?=$connection->getLat()?>,+<?=$connection->getLon()?>(Approximate+Device+Location)&iwloc=A&hl=en">View Map</a></td>
					<td style="width:100px;" class="mobile_hidden"><a class="delete-connection-link" href="/connection/delete/?rid=<?=$index?>&id=<?=$connection->connectionid?>&deviceKey=<?=$device->devicekey?>&tab=reports">Delete</a></td>
				</tr>
			<?php
				if($connection->isValidLocation()) {
					$index++;
				}
			}
			?>
		</tbody>
	</table>

	<!-- PAGING CONTROLS -->
	<form method="GET" action="/device/view" id="devices">
	<div class="pagingControls">
		<input type="hidden" name="key" value="<?=$device->devicekey?>" />
		<input type="hidden" name="tab" value="reports" />
		<span>
			Goto: <input type="text" name="pageNum" id="pageNum" value="<?=$paginator->pageNum?>" size="3"/> of <?=$paginator->numPages?> pages
		</span>
		<span>
			Show rows:
			<select name="pageSize" id="pageSize">
				<option value="10" <?php if($paginator->pageSize == 10) {print(" selected=\"selected\" ");}?>>10</option>
				<option value="25" <?php if($paginator->pageSize == 25) {print(" selected=\"selected\" ");}?>>25</option>
				<option value="50" <?php if($paginator->pageSize == 50) {print(" selected=\"selected\" ");}?>>50</option>
				<option value="100" <?php if($paginator->pageSize == 100) {print(" selected=\"selected\" ");}?>>100</option>
				<option value="200" <?php if($paginator->pageSize == 200) {print(" selected=\"selected\" ");}?>>200</option>
			</select>
		</span>
		<span>
			<?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?>
		</span>
		<span>
			<button id="prevButton" <?php if($paginator->pageNum <= 1) {print("disabled=\"disabled\"");}?>>&laquo;&laquo;</button>
			<button id="nextButton" <?php if($paginator->pageNum >= $paginator->numPages) {print("disabled=\"disabled\"");}?>>&raquo;&raquo;</button>
		</span>
	</div>
	</form>

	<p style="text-align: right;"><a class="delete-all-connections" href="/connection/deleteAll?deviceKey=<?=$device->devicekey?>" title="Delete All Tracking Data">Delete all</a></p>

	<?php } else {?>

	<p>This device has no historical tracking data.</p>
	<p>Please enable "save tracking data" when activating tracking for this device.</p>

	<?php } ?>

</div>

