<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
$isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
?>

<?php if(!$isiPad) {?>
<meta name="viewport" content="width=device-width"/>
<?php } ?>

<title>GadgetTrak - Web Control Panel</title>
<link href="/_style/main.css?v=1.1" rel="stylesheet" type="text/css" />
<link href="/_style/custom-theme2/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<link href="/_style/iPhone.css" rel="stylesheet" type="text/css" media="only screen and (max-device-width: 320px)"/>

<script type="text/javascript" src="/_js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/_js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/_js/jquery.tools.min.js"></script>
<script type="text/javascript" src="/_js/core.js"></script>

<script type="text/javascript">
	$(function() {

		//If user isn't authenticated, then redirect to login if any ajax request is made...
		$.ajaxSetup({
			error:function(xhr, e) {
				if (xhr.status == 403) {
					window.location = "/login/";
				}
			}
		});

		//Bind modals to links
		$(".lbOn").live("click", function() {
			var $link = $(this);
			var $d = $("<div/>").attr("id", "genDialog");
			$("body").append($d);
			$d.load($link.attr("href"), function() {
				$d.dialog({
					modal: true,
					width:500,
					title:$link.attr("title"),
					resizable:true,
					buttons: {
						'Cancel':function() {$d.dialog('destroy');}
					}

				});
				$d.dialog("resize", "auto");
			});
			return false;
		});

		//Stripe all data tables
		$(".tabledata tr:even").addClass("row-even");
		//$(".tabledata tr").hover(function() {$(this).addClass("row-over");}, function() {$(this).removeClass("row-over");});
	});
</script>


</head>

<body>

	<div id="header">
		<div class="container">
			<div class="logo"><a href="/"><img src="/_gfx/gt-logo.gif" alt="GadgetTrak" /></a></div>
			<div class="right">
				<h1>Control Panel</h1>
				<div id="nav">
					<ul>
						<li><a href="/">Home</a></li>
						<li class="mobile_hidden"><a href="http://www.gadgettrak.com/support/">Support</a></li>
						<?if ($_SESSION['loggedin']){print "<li><a href=\"/logout.php\">Logout " . $_SESSION[user]->fname . "</a></li>"; }?>
					</ul>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>

	<div class="container">
		<div id="content">


