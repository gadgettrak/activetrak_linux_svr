<div id="tabs">
	<ul>
		<li><a href="#details">Device Information</a></li>
	</ul>

	<div id="details">

		<div id="deviceActions">
			<a class="lbOn editDevice mobile_hidden" title="Edit Device" href="/device/edit?src=details&key=<?=$device->devicekey?>">Edit</a><br/>
			<a class="lbOn deleteDevice mobile_hidden" title="Delete Device" href="/device/delete?&deviceKey=<?=$device->devicekey?>">Delete</a>
		</div>

		<div id="detailsTable">
		<table class="deviceDetails">
			<tbody>
			<tr>
				<th>Description</th>
				<td><?=$device->description?></td>
			</tr>
			<tr>
				<th>Label</th>
				<td><?=$device->labelcode?></td>
			</tr>
			<tr>
				<th>Type</th>
				<td><?=$device->type?></td>
			</tr>
			<tr>
				<th>Manufacturer</th>
				<td><?=$device->manufacturer?></td>
			</tr>
			<tr>
				<th>Model</th>
				<td><?=$device->model?></td>
			</tr>
			<tr>
				<th>Color</th>
				<td><?=$device->color?></td>
			</tr>
			<tr>
				<th>Serial number</th>
				<td><?=$device->serial?></td>
			</tr>

			</tbody>
		</table>
		</div>

		<div class="clear"></div>

	</div>

</div>



