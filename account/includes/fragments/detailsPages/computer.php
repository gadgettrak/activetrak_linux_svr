<div id="tabs">
	<ul>
		<li><a href="#details">Device Information</a></li>
		<li><a href="#reports">Tracking Reports</a></li>
	</ul>

	<div id="details">

		<div id="deviceActions">
			<div class="tracking-status-icon si_<?=$device->devicekey?>"></div>
			<a title="Change Device Tracking Status" class="tracking-link" href="status.php?s=<?=$device->theft_status?>&id=<?=$device->devicekey?>"><?=$tracking?></a><br/>
			<a class="lbOn editDevice mobile_hidden" title="Edit Device" href="/device/edit?src=details&key=<?=$device->devicekey?>">Edit</a><br/>
			<a class="lbOn deleteDevice mobile_hidden" title="Delete Device" href="/device/delete?&deviceKey=<?=$device->devicekey?>">Delete</a>
		</div>

		<div id="detailsTable">
		<table class="deviceDetails">
			<tbody>
			<tr>
				<th>Description</th>
				<td><?=$device->description?></td>
			</tr>
			<tr>
				<th>MAC address</th>
				<td><?=$device->macAddress?></td>
			</tr>
			<tr>
				<th>Manufacturer</th>
				<td><?=$device->manufacturer?></td>
			</tr>
			<tr>
				<th>Model</th>
				<td><?=$device->model?></td>
			</tr>
			<tr>
				<th>Serial number</th>
				<td><?=$device->serial?></td>
			</tr>
			<tr>
				<th>License Key</th>
				<td><?=$device->key?></td>
			</tr>
			<tr>
				<th>Save tracking data</th>
				<td>
					<?=(($device->saveTracking == "Y")?"Yes":"NO")?>
				</td>
			</tr>
			<!-- <?=$device->length?> -->
			<?php if($device->length != -1) {?>
			<tr>
				<th>License expires in</th>
				<td>
					<?php
						$activatedDate = strtotime($device->activated);
						$expireDate = getdate(strtotime("+" . $device->length . " months", $activatedDate));
						$nowDate = getdate(strtotime("now"));
						$begin = array ('year' => $nowDate['year'], 'month' => $nowDate['mon'], 'day' => $nowDate['mday']);
						$end = array ('year' => $expireDate['year'], 'month' => $expireDate['mon'], 'day' => $expireDate['mday']);
						$timeRemaining = Utils::dateDifference($begin, $end);
					?>
					<? if($timeRemaining['years']){print("$timeRemaining[years] years");}?> <? if($timeRemaining['months']){print("$timeRemaining[months] months");}?> <? if($timeRemaining['days']){print("$timeRemaining[days] days");}?>
					<?php
					if(!$timeRemaining || (
						$timeRemaining['years'] == 0 &&
						$timeRemaining['months'] == 0 &&
						$timeRemaining['days'] == 0)) {
						print("<strong>This license is expired</strong>");
					}
					?>
				</td>
			</tr>
			<?php } ?>
			<tr>
				<th>Download software</th>
				<td><a href="http://www.gadgettrak.com/downloads?v=current&p=mac">Mac</a> / <a href="http://www.gadgettrak.com/downloads?v=current&p=win">PC</a></td>
			</tr>
			</tbody>
		</table>
		</div>
		<div class="clear"></div>
	</div>

	<?php include("fragments/reports.php"); ?>

</div>