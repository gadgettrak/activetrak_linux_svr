<div id="tabs">
	<ul>
		<li><a href="#details">Device Information</a></li>
		<li><a href="#reports">Tracking Reports</a></li>
	</ul>

	<div id="details">

		<div id="deviceActions">
			<div class="tracking-status-icon si_<?=$device->devicekey?>"></div>
			<a title="Change Device Tracking Status" class="tracking-link" href="status.php?s=<?=$device->theft_status?>&id=<?=$device->devicekey?>"><?=$tracking?></a><br/>
			<a class="lbOn editDevice mobile_hidden" title="Edit Device" href="/device/edit?src=details&key=<?=$device->devicekey?>">Edit</a><br/>
			<a class="lbOn deleteDevice mobile_hidden" title="Delete Device" href="/device/delete?&deviceKey=<?=$device->devicekey?>">Delete</a>
		</div>

		<div id="detailsTable">

		<table class="deviceDetails">
			<tbody>
			<tr>
				<th>Description</th>
				<td><?=$device->description?></td>
			</tr>
			<tr>
				<th>Color</th>
				<td><?=$device->color?></td>
			</tr>
			<tr>
				<th>Model</th>
				<td><?=$device->model?></td>
			</tr>
			<tr>
				<th>Serial number</th>
				<td><?=$device->serial?></td>
			</tr>
			</tbody>
		</table>

		<?php if((float) $device->version < 2) {?>
		<p class="notification">There's a new version of GadgetTrak available for your iPhone! Please click <a href="http://itunes.apple.com/us/app/gadgettrak/id288927565?mt=8">here</a> for more information.</p>
		<?php }?>

		</div>
		<div class="clear"></div>
	</div>

	<?php include("fragments/reports.php"); ?>


	<script type="text/javascript">
		$(function() {
			$(".notification").effect("pulsate", { times:3 }, 600);
		});
	</script>

</div>