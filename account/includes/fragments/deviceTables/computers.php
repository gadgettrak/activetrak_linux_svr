<?php

require_once("classes/Utils.php");

?>

<table class="tabledata">
	<thead>
	<tr>
		<th></th>
		<th>Description</th>
		<th>OS</th>
		<th>MAC address</th>
		<th>Manufacturer</th>
		<th>Model</th>
		<!--
		<th>License Key</th>
		<th>Time until expiration</th>
		-->
		<th>Tracking</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($deviceResults->computers as $deviceResult) {?>

	<?
		//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
		$classPName = preg_replace('/:/','_', $deviceResult->devicekey);
		$activatedDate = strtotime($deviceResult->activated);
		$expireDate = getdate(strtotime("+" . $deviceResult->length . " months", $activatedDate));
		$nowDate = getdate(strtotime("now"));
		$begin = array ('year' => $nowDate['year'], 'month' => $nowDate['mon'], 'day' => $nowDate['mday']);
		$end = array ('year' => $expireDate['year'], 'month' => $expireDate['mon'], 'day' => $expireDate['mday']);
		$timeRemaining = Utils::date_difference($begin, $end);
	?>

	<tr class="dr_<?=$classPName?> <?php if($deviceResult->theft_status=="Y"){ print("tracking-active");}?>">
		<td class="icon"><img src="/_gfx/icon-laptop.png" alt="GadgetTrak Device" title="GadgetTrak Device"/></td>
		<td><?=$deviceResult->description?></td>
		<td><?=$deviceResult->os?></td>
		<td><?=$deviceResult->macAddress?></td>
		<td><?=$deviceResult->manufacturer?></td>
		<td><?=$deviceResult->model?></td>
		<!--
		<td><?=$deviceResult->key?></td>
		<td>
			<? if($timeRemaining['years']){print("$timeRemaining[years] years");}?> <? if($timeRemaining['months']){print("$timeRemaining[months] months");}?> <? if($timeRemaining['days']){print("$timeRemaining[days] days");}?>
			<?php
			if(!$timeRemaining || (
				$timeRemaining['years'] == 0 &&
				$timeRemaining['months'] == 0 &&
				$timeRemaining['days'] == 0)) {
				print("<strong>This license is expired</strong>");
			}
			?>
		</td>
		-->
		<td><a title="Change Device Tracking Status" class="tracking-link" href="status.php?s=<?=$deviceResult->theft_status?>&id=<?=$deviceResult->devicekey?>"><?=$tracking?></a></td>
		<td>
			<!--
			<a class="lbOn" title="Edit Device" href="/device/edit?did=<?=$deviceResult->devicekey?>">Edit</a> &nbsp;
			<a class="lbOn" title="Delete Device" href="/device/delete?&deviceKey=<?=$deviceResult->devicekey?>">Delete</a>
			-->
			<a title="viewDetails" href="/device/view?key=<?=$deviceResult->devicekey?>">View</a>
		</td>
	</tr>


	<?php }?>
	</tbody>
</table>