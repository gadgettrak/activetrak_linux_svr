<table class="tabledata">
	<thead>
	<tr>
		<th></th>
		<th>Description</th>
		<th>Device Type</th>
		<th>Manufacturer</th>
		<th>Model</th>
		<th>Color</th>
		<th>Tracking</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($deviceResults->usbDevices as $deviceResult) {?>
		<?
			//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
			$classPName = preg_replace('/:/','_', $deviceResult->devicekey);
		?>
		<tr class="dr_<?=$classPName?> <?php if($deviceResult->theft_status=="Y"){ print("tracking-active");}?>">
			<td class="icon"><img src="/_gfx/icon-usb.png" alt="GadgetTrak Device" title="GadgetTrak Device"/></td>
			<td><?=$deviceResult->description?></td>
			<td><?=$deviceResult->type?></td>
			<td><?=$deviceResult->manufacturer?></td>
			<td><?=$deviceResult->model?></td>
			<td><?=$deviceResult->color?></td>
			<td><a title="Change Device Tracking Status" class="tracking-link" href="status.php?s=<?=$deviceResult->theft_status?>&id=<?=$deviceResult->devicekey?>"><?=$tracking?></a></td>
			<td><a title="viewDetails" href="/device/view?key=<?=$deviceResult->devicekey?>">View</a></td>
		</tr>
	<?php }?>
	</tbody>
</table>