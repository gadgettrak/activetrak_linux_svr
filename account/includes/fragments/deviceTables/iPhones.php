<table class="tabledata">
	<thead>
	<tr>
		<th></th>
		<th>Description</th>
		<th>Serial Number</th>
		<th>Color</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($deviceResults->iPhones as $deviceResult) {?>
	<?
		//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
		$classPName = preg_replace('/:/','_', $deviceResult->devicekey);
	?>
	<tr class="dr_<?=$classPName?> <?php if($deviceResult->theft_status=="Y"){ print("tracking-active");}?>">
		<td class="icon"><img src="/_gfx/icon-iphone.png" alt="GadgetTrak Device" title="GadgetTrak Device"/></td>
		<td><?=$deviceResult->description?></td>
		<td><?=$deviceResult->serial?></td>
		<td><?=$deviceResult->color?></td>
		<td><a title="viewDetails" href="/device/view?key=<?=$deviceResult->devicekey?>">View</a></td>
	</tr>

<? }?>
	</tbody>
</table>