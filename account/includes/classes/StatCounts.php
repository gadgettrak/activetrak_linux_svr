<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class StatCounts
{
	public $laptopPhotos;
	public $iOsPhotos;
	public $devicesTracked;
	public $devicesTotal;
	public $responseTime;

	public function __construct() {}
}