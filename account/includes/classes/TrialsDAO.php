<?php
/**
 * TrialsDAO
 *
 * A DAO class for this model implementing methods called
 * from the manager class
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/BaseDAO.php");
require_once("classes/Trials.php");
require_once("classes/TrialUserResult.php");

class TrialsDAO extends BaseDAO
{

	/**
	 * Saves a new Trials to the database
	 * @param Trials $trials 
	 * @return Trials 
	 */
	public function create($trials)
	{
		$q = "INSERT INTO trials (
			`userId`,
			`date`,
			`licenseId` 
		) VALUES (
			'$trials->userId',
			'$trials->date',
			'$trials->licenseId' 
		)";
		$this->execute($q);
		$trials->userId = mysql_insert_id();
		return $trials;
	}

	/**
	 * Updates a Trials in the database
	 * @param Trials $trials 
	 * @return Trials 
	 */
	public function update($trials)
	{
		$q = "UPDATE trials SET
			`userId` = '$trials->userId',
			`date` = '$trials->date',
			`licenseId` = '$trials->licenseId' 
		WHERE
			`userId` = '$trials->userId'";
		return $this->execute($q);
	}

	/**
	 * Deletes a Trials in the database
	 * @param Trials $trials 
	 */
	public function delete($trials)
	{
		$q = "DELETE from trials WHERE `userId` = '$trials->userId'";
		return $this->execute($q);
	}

	/**
	 * Retrieves a Trials in the database
	 * @param String userId 
	 */
	public function get($userId)
	{
		$q = "SELECT * from trials WHERE `userId` = '$userId'";

		$result = $this->queryUniqueObject($q);
		if($result) {
			return Trials::newFromDatabase($result);
		} else {
			return null;
		}
	}

	public function getTrialUserResults($filters = null, $sorts = null, &$paginator = null)
	{
		$query = "
			SELECT trials.date AS trialsDate,
				users.userid, 
				users.email, 
				users.datecreated AS userCreateDate, 
				license.id AS licenseId, 
				license.`key`, 
				license.activated, 
				devices.devicekey, 
				devices.datecreated AS deviceCreateDate
			FROM trials INNER JOIN users ON trials.userId = users.userid
				 INNER JOIN license ON license.id = trials.licenseId
				 LEFT JOIN devices ON license.deviceId = devices.deviceid";

		if($filters == null) {
			$filters = new QueryFilterList();
		}

		$query .= $filters->toString() . " ";

		if($sorts != null) {

			$sortField = str_replace("___", ".", $sorts['field']);
			$sortString = (strpos($sortField, "."))?$sortField:"`$sortField`";

			$direction = ($sorts['direction'] == "true")?"ASC":"DESC";

			$query .= 'ORDER BY ' . $sortString . " " . $direction . ' ';
		}

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($query));
			$paginator->updateCounts();
			$query .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}

		$results = $this->query($query);
		$trialUserResults = array();

		while ($row = mysql_fetch_object($results)) {
			array_push($trialUserResults, TrialUserResult::newFromDatabase($row));
		}
		
		return $trialUserResults;
	}

}

?>