<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/AdminLicenseDAO.php");
require_once("classes/LicenseManager.php");

class AdminLicenseManager extends LicenseManager {

	public function __construct()
	{
		$this->dao = new AdminLicenseDAO();
	}

	public function findLicenses($filters = null, $sorts = null, &$paginator = null) {
		return $this->dao->findLicenses($filters, $sorts, $paginator);
	}
}


?>