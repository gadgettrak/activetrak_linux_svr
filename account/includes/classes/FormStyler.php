<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("HTML/QuickForm/Renderer/Default.php");

class FormStyler
{
	
	public $form;
	public $renderer;
	
	function __construct($name = '')
	{
		$this->form = new HTML_QuickForm($name, null, null, null, null, true);
		$this->renderer =& new HTML_QuickForm_Renderer_Default();

// set new element template
$elementTpl = <<< ELEMENT
<tr>
	<th>
		<label>{label}</label>
	</th>
	<td>
		<!-- BEGIN error --><span class="error">{error}</span><br /><!-- END error -->
		{element}<!-- BEGIN required --><span class="required">*</span><!-- END required -->
	</td>
</tr>
ELEMENT;

$formTpl = <<< FORM

<form{attributes}>
	<table class="formTable">
	{content}
	</table>
</form>

FORM;

		$this->renderer->setElementTemplate($elementTpl);
		$this->renderer->setFormTemplate($formTpl);
	}

	public function display() {
		$this->form->accept($this->renderer);
		$this->form->validate();
		print($this->renderer->toHtml());
	}

}

?>