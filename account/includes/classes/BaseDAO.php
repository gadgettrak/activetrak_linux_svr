<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DB.php");

class BaseDAO extends DB
{

	private $lastResult;
    private $defaultDebug = false;

	//function __construct()
	//{
	//	parent::__construct();
	//}

	public function getResultsAsObjectList(/*mysql resource*/ $results)
	{
		$values = array();
		while ($row = mysql_fetch_assoc($results)) {
			//Cast array to object
			array_push($values, (object) $row);
		}
		return $values;
	}

	public function query($query, $debug = -1)
	{
		$this->lastResult = mysql_query($query) or $this->debugAndDie($query);
		$this->debug($debug, $query, $this->lastResult);
		return $this->lastResult;
	}
	
	public function execute($query, $debug = -1)
	{
		$result = mysql_query($query) or $this->debugAndDie($query);
		$this->debug($debug, $query);
		return $result;
	}
	
	public function executeNoDie($query, $debug = -1)
	{
		$result = mysql_query($query);
		$this->debug($debug, $query);
		return $result;
	}
	
	public function numRows($result = NULL)
	{
		if ($result == NULL) {
			return 0;
		} else {
			return mysql_num_rows($result);
		}
	}
	
	public function queryUniqueObject($query, $debug = -1)
	{
		$query = "$query LIMIT 1";
		$this->nbQueries++;
		$result = mysql_query($query) or $this->debugAndDie($query);
		$this->debug($debug, $query, $result);
		return mysql_fetch_object($result);
	}
	
	public function debugAndDie($query)
	{
		$this->debugQuery($query, "Error");
		die("<p>" . mysql_error() . "</p>");
	}

	public function debug($debug, $query, $result = NULL)
	{
		if ($debug === -1 && $this->defaultDebug === false) {
			return;
		}
		if ($debug === false) {
			return;
		}
		$reason = ($debug === -1 ? "Default Debug" : "Debug");
		$this->debugQuery($query, $reason);
		if ($result == NULL) {
			//print("<p>Number of affected rows: " . mysql_affected_rows() . "</p>");
		} else {
			$this->debugResult($result);
		}
	}

	public function debugQuery($query, $reason = "Debug")
	{
		//print("<p>$reason:" . htmlentities($query) . "</p>");
	}
	
	public function debugResult($result)
	{
		//print("<table border=\"1\"><thead>");
		$numFields = mysql_num_fields($result);
		// BEGIN HEADER
		$tables    = array();
		$nbTables  = -1;
		$lastTable = "";
		$fields    = array();
		$nbFields  = -1;
		while ($column = mysql_fetch_field($result)) {
			if ($column->table != $lastTable) {
				$nbTables++;
				$tables[$nbTables] = array("name" => $column->table, "count" => 1);
			} else
				$tables[$nbTables]["count"]++;
				$lastTable = $column->table;
				$nbFields++;
				$fields[$nbFields] = $column->name;
			}
			for ($i = 0; $i <= $nbTables; $i++) {
				//print("<th colspan=".$tables[$i]["count"].">".$tables[$i]["name"]."</th>");
			}
			//print("</thead>");
			//print("<tbody>");
			for ($i = 0; $i <= $nbFields; $i++) {
				//print("<th>".$fields[$i]."</th>");
			}
			while ($row = mysql_fetch_array($result)) {
				//print("<tr>");
				for ($i = 0; $i < $numFields; $i++) {
					//print("<td>".htmlentities($row[$i])."</td>");
				}
				//print("</tr>");
			}
		//print("</table>");;
		$this->resetFetch($result);
	}

    public function resetFetch($result)
    {
      if (mysql_num_rows($result) > 0)
        mysql_data_seek($result, 0);
    }
}