<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/AdminDeviceDAO.php");
require_once("classes/DeviceManager.php");

class AdminDeviceManager extends DeviceManager {

	public function __construct()
	{
		$this->dao = new AdminDeviceDAO();
	}

	public function findDevices($filters = null, $sorts = null, &$paginator = null) {
		return $this->dao->findDevices($filters, $sorts, $paginator);
	}

}


?>