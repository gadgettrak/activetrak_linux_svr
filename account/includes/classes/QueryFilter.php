<?php

class QueryFilter {

	public $key;
	public $value;
	public $operator;

	public function __construct($key, $value, $operator = "=")
	{
		$this->key = $key;
		$this->value = $value;
		$this->operator = $operator;
	}

	public function toString()
	{
		if($this->operator == "ISNULL") {
			return "`$this->key` IS NULL";

		} else if($this->operator == "NOTNULL") {
			return "`$this->key` IS NOT NULL";

		} else {
			return "`$this->key` $this->operator '$this->value'";
		}
	}
}

?>
