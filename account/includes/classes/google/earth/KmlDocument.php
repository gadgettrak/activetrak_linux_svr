<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once 'classes/DeviceConnection.php';
require_once 'classes/DeviceResult.php';

class KmlDocument
{

	public $docNode;
	public $document;
	public $parNode;

	public function __construct()
	{
		$this->document = new DOMDocument('1.0', 'UTF-8');

		$node = $this->document->createElementNS('http://earth.google.com/kml/2.1', 'kml');
		$this->parNode = $this->document->appendChild($node);

		$dnode = $this->document->createElement('Document');
		$this->docNode = $this->parNode->appendChild($dnode);
	}

	public function addDeviceConnectionPlacemark($deviceConnection, $device)
	{

		$node = $this->document->createElement('Placemark');
		$placeNode = $this->docNode->appendChild($node);
		$placeNode->setAttribute('id', 'placemark' . $deviceConnection->connectionid);

		//NAME
		$nameNode = $this->document->createElement('name', htmlentities($device->description));
		$placeNode->appendChild($nameNode);


		$pointNode = $this->document->createElement('Point');
		$placeNode->appendChild($pointNode);

		$coorStr = $deviceConnection->ip_lon . ','  . $deviceConnection->ip_lat;
		$coorNode = $this->document->createElement('coordinates', $coorStr);
		$pointNode->appendChild($coorNode);

		//DESCRIPTION
		//$descNode = $this->document->createElement('description', $row['address']);
		//$placeNode->appendChild($descNode);

	}

	public function addDeviceResultPlacemark($deviceResult)
	{

		$node = $this->document->createElement('Placemark');
		$placeNode = $this->docNode->appendChild($node);
		$placeNode->setAttribute('id', 'placemark' . $deviceResult->lastConnection->connectionid);

		//NAME
		$nameNode = $this->document->createElement('name', htmlentities($deviceResult->description));
		$placeNode->appendChild($nameNode);

		$pointNode = $this->document->createElement('Point');
		$placeNode->appendChild($pointNode);

		$coorStr = $deviceResult->lastConnection->ip_lon . ','  . $deviceResult->lastConnection->ip_lat;

		$coorNode = $this->document->createElement('coordinates', $coorStr);
		$pointNode->appendChild($coorNode);

		//DESCRIPTION
		//$descNode = $this->document->createElement('description', $row['address']);
		//$placeNode->appendChild($descNode);
	}

	public function getXML()
	{
		return $this->document->saveXML();
	}

}

?>
