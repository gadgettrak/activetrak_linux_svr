<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/HttpUtils.php");

class DeviceMapDAO extends BaseDAO
{
	/**
	 *
	 * @param <type> $deviceMap
	 * @return <type>
	 */
	public function saveNewDeviceMap($deviceMap)
	{
		$newDeviceMapQuery = "INSERT INTO devicemap (userid, devicekey) VALUES ('$deviceMap->userid', '$deviceMap->devicekey')";
		$this->executeNoDie($newDeviceMapQuery);
		$deviceMap->mapid = mysql_insert_id();
		return $deviceMap;
	}

	/**
	 *
	 * @param <type> $key
	 * @return <type>
	 */
	public function deleteDeviceMapByKey($key)
	{
		HttpUtils::cleanInput($key);
		return $this->execute("DELETE FROM devicemap WHERE devicekey='$key'");
	}

	/**
	 *
	 * @param <type> $key
	 * @return <type>
	 */
	public function getDeviceMapByKey($key)
	{
		HttpUtils::cleanInput($key);
		$query = "SELECT * FROM devicemap WHERE devicekey='$key'";
		$result = $this->queryUniqueObject($query);
		if($result) {
			return DeviceMap::newFromDatabase($result);
		} else {
			return null;
		}
	}

}