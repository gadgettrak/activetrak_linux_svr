<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceConnectionDAO.php");

class DeviceConnectionManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new DeviceConnectionDAO();
	}

	/**
	 *
	 * @param string $devicekey
	 * @return mixed
	 */
	public function getDeviceConnections($devicekey, &$paginator = null)
	{
		return $this->dao->getDeviceConnections($devicekey, $paginator);
	}
	
	/**
	 *
	 * @param DeviceConnection $deviceConnection
	 * @return DeviceConnection
	 */
	public function saveNewDeviceConnection($deviceConnection)
	{
		return $this->dao->saveNewDeviceConnection($deviceConnection);
	}

	/**
	 *
	 * @param string $devicekey
	 * @return DeviceConnection
	 */
	public function getMostRecentLocationByDeviceKey($devicekey)
	{
		return $this->dao->getMostRecentLocationByDeviceKey($devicekey);
	}

	/**
	 *
	 * @param int $connectionId
	 * @param int $userId
	 * @return bool
	 */
	public function deleteDeviceConnection($connectionId, $userId)
	{
		return $this->dao->deleteDeviceConnection($connectionId, $userId);
	}

	/**
	 *
	 * @param int $deviceKey the device key to delete connections for
	 * @param int $userId the owner of the device
	 * @return bool
	 */
	public function deleteDeviceConnections($deviceKey, $userId)
	{
		return $this->dao->deleteDeviceConnections($deviceKey, $userId);
	}

}

?>