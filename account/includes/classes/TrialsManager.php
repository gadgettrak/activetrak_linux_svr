<?php
/**
 * Trials Manager
 *
 * A manager class used to interact with this Model's DAO
 *
 * Example usage:
 * $trialsManager = new TrialsManager();
 * $trials = new Trials();
 * $trialsManager->save($trials);
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/TrialsDAO.php");

class TrialsManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new TrialsDAO();
	}

	/**
	 * Creates a new Trials 
	 * @param Trials $trials 
	 * @return Trials 
	 */
	public function create($trials)
	{
		return $this->dao->create($trials);
	}

	/**
	 * Updates a Trials 
	 * @param Trials $trials 
	 * @return Trials 
	 */
	public function update($trials)
	{
		return $this->dao->update($trials);
	}

	/**
	 * Deletes a Trials	 * @param Trials $trials 
	 * @return bool
	 */
	public function delete($trials)
	{
		return $this->dao->delete($trials);
	}

	/**
	 * Retrieves a Trials in the database
	 * @param String userId 
	 * @return Trials 
	 */
	public function get($userId)
	{
		return $this->dao->get($userId);
	}

	public function getTrialUserResults($filters = null, $sorts = null, &$paginator = null) {
		return $this->dao->getTrialUserResults($filters, $sorts, $paginator);
	}

}

?>

