<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class Device
{
	public $deviceid;
	public $type;
	public $model;
	public $manufacturer;
	public $os;
	public $theft_status;
	public $serial;
	public $color;
	public $description;
	public $devicekey;
	public $macAddress;
	public $labelcode;
	public $datecreated;
	public $pro;
	public $productid;
	public $saveTracking;
	public $version;
	public $devicePassword;

	public function __construct($deviceid = null, $type = null, $model = null, $manufacturer = null, $os = null, $theft_status = null, $serial = null, $color = null, $description = null, $devicekey = null, $macAddress = null, $labelcode = null, $datecreated = null, $pro = null, $productid = null, $saveTracking = null, $version = null, $devicePassword = null)
	{
		$this->deviceid = $deviceid;
		$this->type = $type;
		$this->model = $model;
		$this->manufacturer = $manufacturer;
		$this->os = strtolower($os);
		$this->theft_status = $theft_status;
		$this->serial = $serial;
		$this->color = $color;
		$this->description = $description;
		$this->devicekey = $devicekey;
		$this->macAddress = $macAddress;
		$this->labelcode = $labelcode;
		$this->datecreated = $datecreated;
		$this->pro = $pro;
		$this->productid = $productid;
		$this->saveTracking = $saveTracking;
		$this->version = $version;
		$this->devicePassword = $devicePassword;
	}

	/**
	 *
	 * @param MySQLResource $result
	 * @return Device
	 */
	public static function newFromDatabase($result)
	{
		return new Device(
			$result->deviceid,
			$result->type,
			$result->model,
			$result->manufacturer,
			$result->os,
			$result->theft_status,
			$result->serial,
			$result->color,
			$result->description,
			$result->devicekey,
			$result->macAddress,
			$result->labelcode,
			$result->datecreated,
			$result->pro,
			$result->productid,
			$result->saveTracking,
			$result->version,
			$result->devicePassword
		);
	}

	/**
	 *
	 */
	public function cleanAttributes()
	{
		HttpUtils::cleanInput($this->type);
		HttpUtils::cleanInput($this->model);
		HttpUtils::cleanInput($this->manufacturer);
		HttpUtils::cleanInput($this->os);
		HttpUtils::cleanInput($this->theft_status);
		HttpUtils::cleanInput($this->serial);
		HttpUtils::cleanInput($this->color);
		HttpUtils::cleanInput($this->description);
		HttpUtils::cleanInput($this->devicekey);
		HttpUtils::cleanInput($this->macAddress);
		HttpUtils::cleanInput($this->labelcode);
	}

	/**
	 *
	 * @return <type>
	 */
	public function toString()
	{
		return "deviceid=" . $this->deviceid . ", devicekey=" . $this->devicekey . ", macAddress=" . $this->macAddress . ", productid=" . $this->productid;
	}

}