<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class ExpiredLicenseResult
{


/*
license.length,
license.deviceId,
license.key,
license.activated,
users.email,
DATE_ADD(DATE(license.activated), INTERVAL license.length MONTH) as expireDate,
devices.devicekey,
devices.description,
devices.deviceid
*/


	public $length;
	public $deviceId;
	public $key;
	public $activated;
	public $email;
	public $expireDate;
	public $deviceKey;
	public $deviceDescription;

	public function __construct($length = null, $deviceId = null, $key = null, $activated = null, $email = null, $expireDate = null, $deviceKey = null, $deviceDescription = null)
	{
		$this->length = $length;
		$this->deviceId = $deviceId;
		$this->key = $key;
		$this->activated = $activated;
		$this->email = $email;
		$this->expireDate = $expireDate;
		$this->deviceKey = $deviceKey;
		$this->deviceDescription = $deviceDescription;
	}

	/**
	 *
	 * @param MySQLResource $result
	 * @return Device
	 */
	public static function newFromDatabase($result)
	{
		return new ExpiredLicenseResult(
			$result->length,
			$result->deviceId,
			$result->key,
			$result->activated,
			$result->email,
			$result->expireDate,
			$result->deviceKey,
			$result->description
		);
	}

}