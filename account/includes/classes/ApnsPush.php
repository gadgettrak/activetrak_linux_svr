<?php

// Report all PHP errors
error_reporting(-1);

class ApnsPush {

    function push($message) {
    // Instanciate a new ApnsPHP_Push object
    $push = new ApnsPHP_Push(
        ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
        'PushNotificationMerge.pem'
    );

    // Set the Root Certificate Autority to verify the Apple remote peer
    //$push->setRootCertificationAuthority('E:\Work\Gadget Trak\gt_laptop_ios\gt_laptop_ios\account\includes\AudioMergKeyAndCert.pem');

    // Connect to the Apple Push Notification Service
    $push->connect();
    
    // Add the message to the message queue
    $push->add($message);
    
    // Send all messages in the message queue
    $push->send();

    // Disconnect from the Apple Push Notification Service
    $push->disconnect();

    // Examine the error message container
    $aErrorQueue = $push->getErrors();
    if (!empty($aErrorQueue)) {
        var_dump($aErrorQueue);
    }
    
}
}
?>
