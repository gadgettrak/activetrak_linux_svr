<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/AdminDeviceConnectionDAO.php");
require_once("classes/DeviceConnectionManager.php");

class AdminDeviceConnectionManager extends DeviceConnectionManager {

	public function __construct()
	{
		$this->dao = new AdminDeviceConnectionDAO();
	}

	public function findDeviceConnections($filters = null, $sorts = null, &$paginator = null, $table = null)
	{
		return $this->dao->findDeviceConnections($filters, $sorts, $paginator, $table);
	}

	public function getAllConnections($table = null, $limit = null)
	{
		return $this->dao->getAllConnections($table, $limit);
	}

	public function getJsonConnections($table = null, $limit = null)
	{
		return $this->dao->getJsonConnections($table, $limit);
	}

}


?>