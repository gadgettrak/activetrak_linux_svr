<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class License
{
	public $id;
	public $userId;
	public $deviceId;
	public $productId;
	public $resellerId;
	public $length;
	public $licenses;
	public $key;
	public $used;
	public $activated;
	public $nfr;
	public $os;
	public $renewals;
	
	public function __construct($id = null, $userId = null, $deviceId = null, $productId = null, $resellerId = null, $length = null, $licenses = null, $key = null, $used = null, $activated = null, $nfr = null, $os = null, $renewals = null)
	{
		$this->id = $id;
		$this->userId = $userId;
		$this->deviceId = $deviceId;
		$this->productId = $productId;
		$this->resellerId = $resellerId;
		$this->length = $length;
		$this->licenses = $licenses;
		$this->key = $key;
		$this->used = $used;
		$this->activated = $activated;
		$this->nfr = $nfr;
		$this->os = $os;
		$this->renewals = $renewals;
	}

	/**
	 * determines if the license has expired
	 * @return bool
	 */
	public function isExpired($today = null)
	{

		$today = ($today)?$today:strtotime(gmdate("Y-m-d H:i:s"));
		$expiresDate = $this->getExpirationDate();
		if ($today <= $expiresDate || !strtotime($this->activated) || strtotime($this->activated) < 0) {
			 return false;
		} else {
			 return true;
		}
	}

	/**
	 * Gets the expiration date of the license
	 * @return string unix timestamp
	 */
	public function getExpirationDate()
	{
		return strtotime("+$this->length months", strtotime($this->activated));
	}

	public static function newFromDatabase($result)
	{
		return new License(
			$result->id,
			$result->userId,
			$result->deviceId,
			$result->productId,
			$result->resellerId,
			$result->length,
			$result->licenses,
			$result->key,
			$result->used,
			$result->activated,
			$result->nfr,
			$result->os,
			$result->renewals
		);
	}

}