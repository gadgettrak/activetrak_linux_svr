<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once('Mail.php');
require_once('Mail/mime.php');
require_once('classes/Logger.php');

class Mailer
{


	public static function getSMTP()
	{
		return Mail::factory('smtp', array("host"=>"intGTmail"));
		//return Mail::factory('smtp', array("host"=>"mail.alien109.com"));
	
	}

	/**
	 *
	 * @param <type> $message
	 * @param <type> $textPath
	 * @param <type> $htmlPath
	 */
	public static function setMultipartBody(&$message, $textPath, $htmlPath)
	{
		ob_start();
		include($textPath);
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include($htmlPath);
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);
	}

	/**
	 *
	 * @param User $user
	 * @param License $license
	 */
	public static function sendExpiredLicenseNotice($user, $license)
	{

		$message = new Mail_mime();

		ob_start();
		include("templates/email/license-expired-text.php");
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include("templates/email/license-expired-html.php");
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);

		$headers['From'] = 'accounts@gadgettrak.com';
		$headers['Subject'] = 'License Expired Notice';

		$body = $message->get();
		$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"License Expired Notice");
		$headers = $message->headers($extraheaders);

		$smtp = Mailer::getSMTP();
		$smtp->send($user->email, $headers, $body);
	}

	public static function sendExpiredLicenseDeviceNotice($user, $license, $device)
	{

		if(CONFIG_APP_LOGGING) Logger::logToFile($user->userid . ", license:" . $license->id . ", device:" . $device->deviceid);


		$message = new Mail_mime();

		ob_start();
		include("templates/email/license-expired-device-text.php");
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include("templates/email/license-expired-device-html.php");
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);

		$headers['From'] = 'accounts@gadgettrak.com';
		$headers['Subject'] = 'License Expired Notice';

		$body = $message->get();
		$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"License Expired Notice");
		$headers = $message->headers($extraheaders);

		$smtp = Mailer::getSMTP();
		$smtp->send($user->email, $headers, $body);

	}

	public static function sendDeviceRegistrationNotice($user, $license, $device)
	{

		$message = new Mail_mime();

		ob_start();
		include("templates/email/new-device-text.php");
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include("templates/email/new-device-html.php");
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);

		$headers['From'] = 'accounts@gadgettrak.com';
		$headers['Subject'] = 'Device Registration Notice';

		$body = $message->get();
		$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"Device Registration Notice");
		$headers = $message->headers($extraheaders);

		$smtp = Mailer::getSMTP();
		$smtp->send($user->email, $headers, $body);

	}

	public static function sendNewUserVerifyNotice($user)
	{

		$message = new Mail_mime();

		ob_start();
		include("templates/email/new-user-text.php");
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include("templates/email/new-user-html.php");
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);

		$headers['From'] = 'accounts@gadgettrak.com';
		$headers['Subject'] = 'Account Activation and Email Verification';

		$body = $message->get();
		$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"Account Activation and Email Verification");
		$headers = $message->headers($extraheaders);

		$smtp = Mailer::getSMTP();
		$smtp->send($user->email, $headers, $body);

	}

	public static function sendNewTrialUserVerifyNotice($user)
	{

		$message = new Mail_mime();

		ob_start();
		include("templates/email/new-trial-user-text.php");
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include("templates/email/new-trial-user-html.php");
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);

		$headers['From'] = 'accounts@gadgettrak.com';
		$headers['Subject'] = 'GadgetTrak Account Activation';

		$body = $message->get();
		$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"GadgetTrak Account  Activation");
		$headers = $message->headers($extraheaders);

		$smtp = Mailer::getSMTP();
		return $smtp->send($user->email, $headers, $body);

	}

	public static function sendNewTrialKey($user, $trialKey)
	{

		$message = new Mail_mime();

		ob_start();
		include("templates/email/trial-key-text.php");
		$text = ob_get_contents();
		ob_end_clean();

		ob_start();
		include("templates/email/trial-key-html.php");
		$html = ob_get_contents();
		ob_end_clean();

		$message->setTXTBody($text);
		$message->setHTMLBody($html);

		$headers['From'] = 'accounts@gadgettrak.com';
		$headers['Subject'] = 'GadgetTrak Laptop Security Trial License';

		$body = $message->get();
		$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"GadgetTrak Laptop Security Trial License");
		$headers = $message->headers($extraheaders);

		$smtp = Mailer::getSMTP();
		return $smtp->send($user->email, $headers, $body);

	}

}

?>
