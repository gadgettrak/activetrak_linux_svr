<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class TrialUserResult
{
public $trialsDate;
public $userid;
public $email;
public $userCreateDate;
public $licenseId;
public $key;
public $activated;
public $devicekey;
public $deviceCreateDate;

	public function __construct($trialsDate = null, $userid = null, $email = null, $userCreateDate = null, $licenseId = null, $key = null, $activated = null, $devicekey = null, $deviceCreateDate = null)
	{
		$this->trialsDate = $trialsDate;
		$this->userid = $userid;
		$this->email = $email;
		$this->userCreateDate = $userCreateDate;
		$this->licenseId = $licenseId;
		$this->key = $key;
		$this->activated = $activated;
		$this->devicekey = $devicekey;
		$this->deviceCreateDate = $deviceCreateDate;
	}

	/**
	 *
	 * @param MySQLResource $result
	 * @return TrialUserResult
	 */
	public static function newFromDatabase($result)
	{
		return new TrialUserResult(
			$result->trialsDate,
			$result->userid,
			$result->email,
			$result->userCreateDate,
			$result->licenseId,
			$result->key,
			$result->activated,
			$result->devicekey,
			$result->deviceCreateDate
		);
	}

}