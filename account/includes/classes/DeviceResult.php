<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class DeviceResult
{
	public $deviceid;
	public $type;
	public $model;
	public $manufacturer;
	public $os;
	public $theft_status;
	public $serial;
	public $color;
	public $description;
	public $devicekey;
	public $macAddress;
	public $labelcode;
	public $datecreated;
	public $pro;
	public $productid;
	public $saveTracking;
	public $version;
	public $key;
	public $activated;
	public $productname;
	public $lastConnection;

	public function __construct($deviceid = null,
		$type = null,
		$model = null,
		$manufacturer = null,
		$os = null,
		$theft_status = null,
		$serial = null,
		$color = null,
		$description = null,
		$devicekey = null,
		$macAddress = null,
		$labelcode = null,
		$datecreated = null,
		$pro = null,
		$productid = null,
		$saveTracking = null,
		$key = null,
		$activated = null,
		$length = null,
		$productname = null,
		$version = null
	)
	{
		$this->deviceid = $deviceid;
		$this->type = $type;
		$this->model = $model;
		$this->manufacturer = $manufacturer;
		$this->os = $os;
		$this->theft_status = $theft_status;
		$this->serial = $serial;
		$this->color = $color;
		$this->description = $description;
		$this->devicekey = $devicekey;
		$this->macAddress = $macAddress;
		$this->labelcode = $labelcode;
		$this->datecreated = $datecreated;
		$this->pro = $pro;
		$this->productid = $productid;
		$this->saveTracking = $saveTracking;
		$this->key = $key;
		$this->activated = $activated;
		$this->length = $length;
		$this->productname = $productname;
		$this->version = $version;
	}

	/**
	 *
	 * @param MySQLResource $result
	 * @return Device
	 */
	public static function newFromDatabase($result)
	{
		return new DeviceResult(
			$result->deviceid,
			$result->type,
			$result->model,
			$result->manufacturer,
			$result->os,
			$result->theft_status,
			$result->serial,
			$result->color,
			$result->description,
			$result->devicekey,
			$result->macAddress,
			$result->labelcode,
			$result->datecreated,
			$result->pro,
			$result->productid,
			$result->saveTracking,
			$result->key,
			$result->activated,
			$result->length,
			$result->productname,
			$result->version
		);
	}

	public function toString()
	{
		return "deviceid=" . $this->deviceid . ", devicekey=" . $this->devicekey . ", macAddress=" . $this->macAddress . ", productid=" . $this->productid;
	}

}