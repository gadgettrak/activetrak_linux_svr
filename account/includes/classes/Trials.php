<?php
/**
 * Trials 
 *
 * An object model, representing a table in the database
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

class Trials 
{

	public $userId;
	public $date;
	public $licenseId;

	public function __construct($userId = null, $date = null, $licenseId = null)
	{
		$this->userId = $userId;
		$this->date = $date;
		$this->licenseId = $licenseId;
	}

	/**
	 * Creates a new instance of a Trials from a database result
	 * @param MySQLResource $result
	 * @return Trials 
	 */
	public static function newFromDatabase($result)
	{
		return new Trials(
			$result->userId,
			$result->date,
			$result->licenseId 
		);
	}

	public function toString()
	{
		return "userId=" . $this->userId . ", " . "date=" . $this->date . ", " . "licenseId=" . $this->licenseId;
	}

}

?>