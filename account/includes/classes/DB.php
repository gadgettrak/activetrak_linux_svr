<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class DB {

	private $host     = CONFIG_DB_HOST;
	private $database = CONFIG_DB_NAME;
	private $username = CONFIG_DB_USERNAME;
	private $password = CONFIG_DB_PASSWORD;

	private $defaultDebug = false;
	private $link;

	function __construct($host = "", $database = "", $username = "", $password = "")
	{
		if (!empty($host)){$this->host = $host;}
		if (!empty($database)){$this->database = $database;}
		if (!empty($username)){$this->username = $username;}
		if (!empty($password)){$this->password = $password;}
		$this->link = mysql_connect($this->host, $this->username, $this->password) or $this->error();
		$this->link = mysql_select_db($this->database) or $this->error();
	}

	public function error() 
	{ 
		return mysql_error($this->link); 
	}

	public function close() 
	{ 
		return mysql_close($this->link); 
	}
}

?>
