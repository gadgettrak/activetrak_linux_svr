<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class Logger
{
	/**
	 *
	 * @param <type> $message
	 * @param <type> $level
	 */
	public static function logToFile($message, $level = "debug")
	{
		error_log("V3TESTING ::  " . $message, 0);
	}
}

?>
