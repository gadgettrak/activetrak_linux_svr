<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */


require_once("classes/DeviceResult.php");

class DeviceResults
{
	public $devices;
	public $activeDevices = 0;

	public function __construct()
	{
		$this->devices = array();
	}

	/**
	 *
	 * @param DeviceResult $deviceResult
	 */
	public function addResult($deviceResult)
	{
		array_push($this->devices, $deviceResult);
		if($deviceResult->theft_status == 'Y') {
			$this->activeDevices++;
		}
	}

}