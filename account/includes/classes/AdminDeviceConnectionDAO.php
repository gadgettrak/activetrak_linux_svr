<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceConnectionDAO.php");
require_once("classes/HttpUtils.php");

class AdminDeviceConnectionDAO extends DeviceConnectionDAO
{

	public function findDeviceConnections($filters = null, $sorts = null, &$paginator = null, $table = null)
	{

		$q = ($table == "connectionsArchive")?"SELECT * FROM connectionsArchive ":"SELECT * FROM connections ";

		if($filters == null) {
			$filters = new QueryFilterList();
		}

		$q .= $filters->toString() . " ";

		if($sorts != null) {
			$direction = ($sorts['direction'] == "true")?"ASC":"DESC";
			$q .= 'ORDER BY ' . $sorts['field'] . " " . $direction . ' ';
		}

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($q));
			$paginator->updateCounts();
			$q .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}

		$results = $this->query($q);

		$connections = array();

		while ($row = mysql_fetch_object($results)) {
			array_push($connections, DeviceConnection::newFromDatabase($row));
		}


		return $connections;
	}

	public function getAllConnections($table = null, $limit = null)
	{

		if($table == "archive") {
			$q = "SELECT timestamp, ip_lat, ip_lon, public_ip FROM connectionsArchive WHERE ip_lat IS NOT NULL AND ip_lat<>'' ";
		} else if($table == "new") {
			$q = "SELECT timestamp, ip_lat, ip_lon, public_ip FROM connections WHERE ip_lat IS NOT NULL AND ip_lat<>'' ";
		} else {
			$q = "SELECT DISTINCT timestamp, ip_lat, ip_lon, public_ip FROM connections WHERE ip_lat IS NOT NULL AND ip_lat<>'' UNION ALL SELECT DISTINCT timestamp, ip_lat, ip_lon, public_ip FROM connectionsArchive WHERE ip_lat IS NOT NULL AND ip_lat<>'' ";
		}

		$q .= " ORDER BY timestamp ";

		if($limit) {
			$q .= " LIMIT $limit";
		}
		
		$results = $this->query($q);
		return  $this->getResultsAsObjectList($results);

	}


}

?>