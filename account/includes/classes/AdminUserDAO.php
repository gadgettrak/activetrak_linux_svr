<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/UserDAO.php");
require_once("classes/HttpUtils.php");

class AdminUserDAO extends UserDAO
{

	public function findUsersppp($email = null, $userId = null) {
		HttpUtils::cleanInput($email);
		HttpUtils::cleanInput($userId);

		$q = "SELECT * FROM users";

		if((isset($email) && $email != '') || (isset($userId) && $userId != '')) {
			$params = array();
			$q .= " WHERE ";

			if(isset($email) && $email != '') {
				array_push($params, " email LIKE '%$email%' ");
			}

			if(isset($userId) && $userId != '') {
				array_push($params, " userid LIKE '%$userId%' ");
			}

			$q .= implode(' AND ', $params);

			$result = $this->query($q);
			return $this->getResultsAsObjectList($result);

		} else {
			return array();
		}
	}

	public function findUsers($filters = null, $sorts = null, &$paginator = null)
	{
		$q = "SELECT * FROM users";

		if($filters == null) {
			$filters = new QueryFilterList();
		}

		$q .= $filters->toString() . " ";

		if($sorts != null) {
			$direction = ($sorts['direction'] == "true")?"ASC":"DESC";
			$q .= 'ORDER BY ' . $sorts['field'] . " " . $direction . ' ';
		}

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($q));
			$paginator->updateCounts();
			$q .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}

		$results = $this->query($q);
		$errors = array();
		while ($row = mysql_fetch_object($results)) {
			array_push($errors, User::newFromDatabase($row));
		}
		return $errors;
	}


	public function getUserById($userId) {
		HttpUtils::cleanInput($userId);

		$query = "SELECT * FROM users WHERE userid='$userId'";
		$result = $this->queryUniqueObject($query);
		if($result) {
			return User::newFromDatabase($result);
		} else {
			return null;
		}
	}
	public function getUserByEmail($email) {
		HttpUtils::cleanInput($email);

		$query = "SELECT * FROM users WHERE email='$email'";
		$result = $this->queryUniqueObject($query);
		if($result) {
			return User::newFromDatabase($result);
		} else {
			return null;
		}
	}




}

?>