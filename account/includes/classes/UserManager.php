<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/UserDAO.php");

class UserManager {

	protected $dao;

	public function __construct()
	{
		$this->dao = new UserDAO();
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @return User
	 */
	public function getUserByUserCredentials($credentials)
	{
		return $this->dao->getUserByCredentials($credentials);
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @return User
	 */
	public function getAdminByUserCredentials($credentials)
	{
		return $this->dao->getAdminByUserCredentials($credentials);
	}

	/**
	 *
	 * @param <type> $email
	 * @return <type>
	 */
	public function getUserByEmail($email) {
		return $this->dao->getUserByEmail($email);
	}

	/**
	 * Saves a new User to the database
	 * @param User $user
	 * @return User
	 */
	public function saveNewUser($user)
	{
		return $this->dao->saveNewUser($user);
	}

	/**
	 *
	 * @param <type> $userName
	 * @return bool
	 */
	public function userNameIsAvailable($userName)
	{
		return $this->dao->userNameIsAvailable($userName);
	}

	/**
	 *
	 * @param <type> $key
	 * @return <type>
	 */
	public function isValidEmailKey($key)
	{
		return $this->dao->isValidEmailKey($key);
	}

	/**
	 *
	 * @param <type> $email
	 * @return <type>
	 */
	public function isValidEmail($email)
	{
		return $this->dao->isValidEmail($email);
	}

	public function updateUser($user)
	{
		$this->dao->updateUser($user);
	}

	public function validateUser($user)
	{
		$this->dao->validateUser($user);
	}


}


?>