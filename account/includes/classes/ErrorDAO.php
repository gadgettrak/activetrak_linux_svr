<?php
/**
 * errorDAO
 *
 * A DAO class for this model implementing methods called
 * from the manager class
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/BaseDAO.php");
require_once("classes/Error.php");
require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");
require_once("classes/Paginator.php");

class ErrorDAO extends BaseDAO
{

	/**
	 * Saves a new error to the database
	 * @param error $error
	 * @return error
	 */
	public function create($error)
	{
		$q = "INSERT INTO error (
			`date`,
			`errorType`,
			`count`,
			`fname`,
			`lname`,
			`email`,
			`deviceKey`,
			`product`,
			`password`
		) VALUES (
			'$error->date',
			'$error->errorType',
			'$error->count',
			'$error->fname',
			'$error->lname',
			'$error->email',
			'$error->deviceKey',
			'$error->product',
			'$error->password'
		)";
		$this->execute($q);
		$error->id = mysql_insert_id();
		return $error;
	}

	public function createIfDeviceKeyUnique($error)
	{
		$result = -1;

		if($error->deviceKey) {
			$result = $this->updateDateByDeviceKey($error);
		}
		
		if($result < 1) {
			$error = $this->create($error);
		}

		return $error;
	}

	/**
	 * Updates a error in the database
	 * @param error $error
	 * @return error
	 */
	public function updateDateByDeviceKey($error)
	{
		$q = "UPDATE error SET
			`date` = '$error->date',
			`count` = (error.count + 1),
			`password` = '$error->password'
		WHERE
			`deviceKey` = '$error->deviceKey'";
		$this->execute($q);
		return mysql_affected_rows();
	}

	/**
	 * Updates a error in the database
	 * @param error $error
	 * @return error
	 */
	public function update($error)
	{
		$q = "UPDATE error SET
			`date` = '$error->date',
			`errorType` = '$error->errorType',
			`count` = '$error->count',
			`fname` = '$error->fname',
			`lname` = '$error->lname',
			`email` = '$error->email',
			`deviceKey` = '$error->deviceKey',
			`product` = '$error->product',
			`password` = '$error->password'
		WHERE
			`id` = '$error->id'";
		return $this->execute($q);
	}

	/**
	 * Deletes a error in the database
	 * @param error $error
	 */
	public function delete($error)
	{
		$q = "DELETE from error WHERE `id` = '$error->id'";
		return $this->execute($q);
	}

	/**
	 * Retrieves a error in the database
	 * @param String id 
	 */
	public function get($id)
	{
		$q = "SELECT * from error WHERE `id` = '$id'";
		$result = $this->queryUniqueObject($q);
		if($result) {
			return error::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param int $userId
	 * @return DeviceResults
	 */
	public function getAllErrors($filters = null, $sorts = null, &$paginator = null)
	{
		$q = "SELECT * FROM error";

		if($filters == null) {
			$filters = new QueryFilterList();
		}
		
		$q .= $filters->toString() . " ";

		if($sorts != null) {
			$direction = ($sorts['direction'] == "true")?"ASC":"DESC";
			$q .= 'ORDER BY ' . $sorts['field'] . " " . $direction . ' ';
		}

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($q));
			$paginator->updateCounts();
			$q .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}

		$results = $this->query($q);
		$errors = array();
		while ($row = mysql_fetch_object($results)) {
			array_push($errors, Error::newFromDatabase($row));
		}
		return $errors;
	}

}

?>