<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceDAO.php");
require_once("classes/HttpUtils.php");

class AdminDeviceDAO extends DeviceDAO
{

	public function findDevices($filters = null, $sorts = null, &$paginator = null)
	{
		$q = "SELECT * FROM devices";

		if($filters == null) {
			$filters = new QueryFilterList();
		}

		$q .= $filters->toString() . " ";

		if($sorts != null) {
			$direction = ($sorts['direction'] == "true")?"ASC":"DESC";
			$q .= 'ORDER BY ' . $sorts['field'] . " " . $direction . ' ';
		}

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($q));
			$paginator->updateCounts();
			$q .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}

		$results = $this->query($q);
		$errors = array();
		while ($row = mysql_fetch_object($results)) {
			array_push($errors, Device::newFromDatabase($row));
		}
		return $errors;
	}

}

?>