<?php

/**
 * error
 *
 * An object model, representing a table in the database
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class Error 
{

	public $id;
	public $date;
	public $errorType;
	public $count;
	public $fname;
	public $lname;
	public $email;
	public $deviceKey;
	public $product;
	public $meta;

	public function __construct($id = null, $date = null, $errorType = null, $count = null, $fname = null, $lname = null, $email = null, $deviceKey = null, $product = null, $password = null)
	{
		$this->id = $id;
		$this->date = $date;
		$this->errorType = $errorType;
		$this->count = $count;
		$this->fname = $fname;
		$this->lname = $lname;
		$this->email = $email;
		$this->deviceKey = $deviceKey;
		$this->product = $product;
		$this->password = $password;
	}

	/**
	 * Creates a new instance of a error from a database result
	 * @param MySQLResource $result
	 * @return error
	 */
	public static function newFromDatabase($result)
	{
		return new error(
			$result->id,
			$result->date,
			$result->errorType,
			$result->count,
			$result->fname,
			$result->lname,
			$result->email,
			$result->deviceKey,
			$result->product,
			$result->password
		);
	}

	public function toString()
	{
		return "id=" . $this->id . ", " . "date=" . $this->date . ", " . "errorType=" . $this->errorType . ", " . "count=" . $this->count . ", " . "fname=" . $this->fname . ", " . "lname=" . $this->lname . ", " . "email=" . $this->email . ", " . "deviceKey=" . $this->deviceKey . ", " . "product=" . $this->product;
	}

}

?>