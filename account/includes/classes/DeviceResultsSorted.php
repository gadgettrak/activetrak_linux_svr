<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceResult.php");

class DeviceResultsSorted
{

	public $labels;
	public $usbDevices;
	public $iPhones;
	public $computers;
	public $activeDevices = 0;

	/*
	<?if($device->productid == 0) {?>
		<img src="/_gfx/icon-label.gif" alt="GadgetTrak Label" title="GadgetTrak Label" />
	<?} else if($device->productid == 1) {?>
		<img src="/_gfx/icon-usb.png" alt="GadgetTrak USB" title="GadgetTrak USB"/>
	<?} else if($device->productid == 8) {?>
		<img src="/_gfx/icon-iphone.png" alt="GadgetTrak iPhone" title="GadgetTrak iPhone"/>
	<?} else if($device->productid == 4 || $device->productid == 7 || $device->productid == 10 || $device->productid == 12 || $device->productid == 13 || $device->productid == 14 || $device->productid == 15) {?>
		<img src="/_gfx/icon-laptop.png" alt="GadgetTrak for Laptops" title="GadgetTrak for Laptops"/>
	<?}?>

	*/

	public function __construct()
	{
		$this->labels = array();
		$this->usbDevices = array();
		$this->iPhones = array();
		$this->computers = array();
	}

	/**
	 *
	 * @param DeviceResult $deviceResult
	 */
	public function addResult($deviceResult)
	{

		if($deviceResult->productid == 0) {
			array_push($this->labels, $deviceResult);
		} else if($deviceResult->productid == 1) {
			array_push($this->usbDevices, $deviceResult);
		} else if($deviceResult->productid == 8) {
			array_push($this->iPhones, $deviceResult);
		} else if($deviceResult->productid == 4 || $deviceResult->productid == 7 || $deviceResult->productid == 10 || $deviceResult->productid == 12 || $deviceResult->productid == 13 || $deviceResult->productid == 14 || $deviceResult->productid == 15) {
			array_push($this->computers, $deviceResult);
		}

		if($deviceResult->theft_status == 'Y') {
			$this->activeDevices++;
		}

	}


}