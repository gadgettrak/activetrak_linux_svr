<?php

require_once 'QueryFilter.php';

class QueryFilterList {

	/**
	 *
	 * @var array
	 */
	public $filters;

	public function __construct()
	{
		$this->filters = array();
	}

	/**
	 *
	 * @param QueryFilter $filter
	 */
	public function addFilter($key, $value, $operator)
	{
		$key = addslashes($key);
		$value = addslashes($value);
		$operator = addslashes($operator);
		array_push($this->filters, new QueryFilter($key, $value, $operator));
	}

	public function toString()
	{
		if(!count($this->filters)) {
			return " ";
		}

		$temp = array();
		foreach($this->filters as $filter)
		{
			array_push($temp, $filter->toString());
		}
		$filterString = " WHERE " . implode(" AND ", $temp);

		return $filterString;
	}

	public function hasFilters()
	{
		return (count($this->filters) > 0);
	}

	public function hasFilter($key) {

		if(count($this->filters) < 1) {
			return false;
		}

		/* @var $filter QueryFilter */
		foreach($this->filters as $filter) {
			if($filter->key == $key) {
				return true;
			}
		}
		return false;
	}

}

?>
