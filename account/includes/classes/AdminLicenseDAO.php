<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/License.php");
require_once("classes/LicenseDAO.php");
require_once("classes/HttpUtils.php");

class AdminLicenseDAO extends LicenseDAO
{

	public function findLicenses($filters = null, $sorts = null, &$paginator = null)
	{
		$q = "SELECT * FROM license";

		if($filters == null) {
			$filters = new QueryFilterList();
		}

		$q .= $filters->toString() . " ";

		if($sorts != null) {
			$direction = ($sorts['direction'] == "true")?"ASC":"DESC";
			$q .= 'ORDER BY `' . $sorts['field'] . "` " . $direction . ' ';
		}

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($q));
			$paginator->updateCounts();
			$q .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}

		$results = $this->query($q);
		$errors = array();
		while ($row = mysql_fetch_object($results)) {
			array_push($errors, License::newFromDatabase($row));
		}
		return $errors;
	}

}

?>