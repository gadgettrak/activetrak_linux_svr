<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/License.php");
require_once("classes/HttpUtils.php");
require_once("classes/ExpiredLicenseResult.php");
require_once("classes/TrialsManager.php");
require_once("classes/Trials.php");

class LicenseDAO extends BaseDAO
{

	/**
	 *
	 * @param <type> $licenseKey
	 * @return License
	 */
	public function getLicenseByKey($licenseKey)
	{

		HttpUtils::cleanInput($licenseKey);

		$result = $this->queryUniqueObject("SELECT * FROM license WHERE `key`='$licenseKey'");
		if($result) {
			return new License(
				$result->id,
				$result->userId,
				$result->deviceId,
				$result->productId,
				$result->resellerId,
				$result->length,
				$result->licenses,
				$result->key,
				$result->used,
				$result->activated,
				$result->nfr,
				$result->os,
				$result->renewals
			);
		} else {
			return null;
		}

	}

	/**
	 *
	 * @param <type> $deviceId
	 * @return License
	 */
	public function getLicenseByDeviceId($deviceId)
	{
		HttpUtils::cleanInput($deviceId);
		$result = $this->queryUniqueObject("SELECT * FROM license WHERE `deviceId`='$deviceId'");
		if($result) {
			return new License(
				$result->id,
				$result->userId,
				$result->deviceId,
				$result->productId,
				$result->resellerId,
				$result->length,
				$result->licenses,
				$result->key,
				$result->used,
				$result->activated,
				$result->nfr,
				$result->os,
				$result->renewals
			);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param <type> $license
	 */
	public function updateLicense($license)
	{

		$q = "UPDATE license SET";

		if($license->userId != null) {
			$q .= " userId = '$license->userId', ";
		}

		if($license->deviceId != null) {
			$q .= " deviceId = '$license->deviceId', ";
		}

		$q .= "	
				productId = '$license->productId',
				resellerId = '$license->resellerId',
				length = '$license->length',
				licenses = '$license->licenses',
				`key` = '$license->key',
				used = '$license->used',
				activated = '$license->activated',
				nfr = '$license->nfr',
				os = '$license->os',
				renewals = '$license->renewals'
			WHERE
				id=$license->id";

		$this->execute($q);
	}

	/**
	 *
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function isValidKey($licenseKey)
	{
		$result = $this->query("SELECT id FROM license WHERE `key`='$licenseKey'");
		if($this->numRows($result)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param <type> $userId
	 * @param <type> $deviceId
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function mapLicenseToUserAndDevice($userId, $deviceId, $licenseKey)
	{
		$q = "UPDATE license SET userId='$userId', deviceId='$deviceId', activated=NOW() WHERE `key`='$licenseKey'";
		return $this->execute($q);
	}

	/**
	 *
	 * @param <type> $licenseKey
	 * @return <type> 
	 */
	public function resetLicense($licenseKey)
	{
		$q = "UPDATE license SET userId=null, deviceId=null, used='N' WHERE `key`='$licenseKey'";
		return $this->execute($q);
	}

	/**
	 *
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function resetLicenseByDeviceId($deviceId)
	{
		$q = "UPDATE license SET userId=null, deviceId=null, used='N' WHERE `deviceId`='$deviceId'";
		return $this->execute($q);
	}

	public function getRemainingUSBLicenses($userId)
	{
		$q = "Select SUM(number_licenses) as licenseCount FROM licenses WHERE productid='1' AND userid='$userId'";
		$result = $this->queryUniqueObject($q);
		return (int)$result->licenseCount;
	}

	public function getExpiredLicensesResults($userId)
	{
		$query = "SELECT
			license.length,
			license.deviceId,
			license.key,
			license.activated,
			users.email,
			DATE_ADD(DATE(license.activated), INTERVAL license.length MONTH) as expireDate,
			devices.devicekey,
			devices.description,
			devices.deviceid
		FROM
			license
		INNER JOIN
			users
		ON
			users.userid=license.userId
		INNER JOIN
			devices
		ON
			devices.deviceid=license.deviceId
		WHERE
			DATE_ADD(DATE(license.activated), INTERVAL license.length MONTH) < DATE(NOW())
		AND
			license.activated != '0000-00-00 00:00:00'
		AND
			license.length<>'-1'
		AND
			users.userid = '$userId'";

		$result = $this->query($query);
		
		$expiredResults = array();

		while ($row = mysql_fetch_object($result)) {
			array_push($expiredResults, ExpiredLicenseResult::newFromDatabase($row));
		}

		return $expiredResults;
	}

	public function saveNewLicense($license)
	{
		$query = "INSERT INTO license (`productid`, `length`, `licenses`, `key`, `nfr`)
			VALUES ('$license->productId','$license->length','$license->licenses','$license->key','$license->nfr')";

		$this->executeNoDie($query);
		$license->id = mysql_insert_id();
		return $license;
	}

	public function createTrialKey()
	{
		$numFound = 1;
		$count = 0;
		while($numFound > 0) {
			$count++;
			if($count > 4000) {
				die("Could not create key...");
			}
			$guid = "TR1" . strtoupper(substr(sha1(uniqid("", true)), 0, 12));
			$result = $this->query("SELECT * FROM license WHERE `key` = '$guid'");
			$numFound = $this->numRows($result);
		}

		$license = new License();
		$license->productId = 14;
		$license->length = 1;
		$license->licenses = 1;
		$license->key = $guid;
		$license->nfr = 1;
		$this->saveNewLicense($license);
		return $license;
	}

	public function createRandomKey($prefix = null, $length = "12", $productId = "14", $nfr = "0", $keyLength = 10, $import = false)
	{
		$numFound = 1;
		$count = 0;

		$allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		while($numFound > 0) {
			$count++;
			if($count > 2000) {
				return false;
			}

			$guid = $prefix;

			for($i = 0; $i < $keyLength; $i++) {
				$rand = rand(0, strlen($allowedChars) - 1);
				$guid .= substr($allowedChars, $rand, 1);
			}

			//$guid = $prefix . strtoupper(substr((sha1(uniqid("", true)) . sha1(uniqid("", true)) . sha1(uniqid("", true))), 0, 12));
			$result = $this->query("SELECT * FROM license WHERE `key` = '$guid'");
			$numFound = $this->numRows($result);
		}

		$license = new License();
		$license->productId = $productId;
		$license->length = $length;
		$license->licenses = 1;
		$license->key = $guid;
		$license->nfr = $nfr;

		if($import) {
			$this->saveNewLicense($license);
		}

		return $license;
	}

}