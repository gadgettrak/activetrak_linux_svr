<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class DeviceConnection {

	public $connectionid;
	public $devicekey;
	public $timestamp;
	public $public_ip;
	public $internal_ip;
	public $stolen;
	public $username;
	public $computer_name;
	public $ip_country;
	public $ip_state;
	public $ip_city;
	public $postal_code;
	public $ip_lon;
	public $ip_lat;
	public $agent;
	public $os;
	public $serial;
	public $loc_state;
	public $loc_city;
	public $loc_street;
	public $did;

	public function __construct($connectionid = null, $devicekey = null, $timestamp = null, $public_ip = null, $internal_ip = null, $stolen = null, $username = null, $computer_name = null, $ip_country = null, $ip_state = null, $ip_city = null, $postal_code = null, $ip_lon = null, $ip_lat = null, $agent = null, $os = null, $serial = null, $loc_state = null, $loc_city = null, $loc_street = null, $did = null)
	{
		$this->connectionid = $connectionid;
		$this->devicekey = $devicekey;
		$this->timestamp = $timestamp;
		$this->public_ip = $public_ip;
		$this->internal_ip = $internal_ip;
		$this->stolen = $stolen;
		$this->username = $username;
		$this->computer_name = $computer_name;
		$this->ip_country = $ip_country;
		$this->ip_state = $ip_state;
		$this->ip_city = $ip_city;
		$this->postal_code = $postal_code;
		$this->ip_lon = (float)$ip_lon;
		$this->ip_lat = (float)$ip_lat;
		$this->agent = $agent;
		$this->os = $os;
		$this->serial = $serial;
		$this->loc_state = $loc_state;
		$this->loc_city = $loc_city;
		$this->loc_street = $loc_street;
		$this->did = $did;
	}

	/**
	 *
	 * @param MySQLResource $result
	 * @return DeviceConnection
	 */
	public static function newFromDatabase($result)
	{
		return new DeviceConnection(
			$result->connectionid,
			$result->devicekey,
			$result->timestamp,
			$result->public_ip,
			$result->internal_ip,
			$result->stolen,
			$result->username,
			$result->computer_name,
			$result->ip_country,
			$result->ip_state,
			$result->ip_city,
			$result->postal_code,
			$result->ip_lon,
			$result->ip_lat,
			$result->agent,
			$result->os,
			$result->serial,
			$result->loc_state,
			$result->loc_city,
			$result->loc_street,
			$result->did
		);
	}

	public function isValidLocation()
	{
		return ($this->ip_lon != 0 && $this->ip_lat != 0);
	}

	public function getLat()
	{
		return ($this->ip_lat != 0)?$this->ip_lat:"n/a";
	}

	public function getLon()
	{
		return ($this->ip_lon != 0)?$this->ip_lon:"n/a";
	}

}
	
?>
