<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/DeviceConnection.php");
require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class DeviceConnectionDAO extends BaseDAO
{
	/**
	 *
	 * @param string $devicekey
	 * @return mixed
	 */
	public function getDeviceConnections($devicekey, &$paginator = null)
	{
		HttpUtils::cleanInput($deviceKey);
		$q = "SELECT * FROM connections WHERE devicekey='$devicekey' ORDER BY timestamp DESC ";

		if($paginator != null) {
			$paginator->totalRecords = $this->numRows($this->query($q));
			$paginator->updateCounts();
			$q .= 'LIMIT ' . ($paginator->pageNum - 1) * $paginator->pageSize . ',' . $paginator->pageSize;
		}
		
		$results = $this->query($q);
		$values = array();
		while ($row = mysql_fetch_object($results)) {
			array_push($values, DeviceConnection::newFromDatabase($row));
		}
		return $values;
	}

	/**
	 *
	 * @param strong $devicekey
	 * @return DeviceConnection
	 */
	public function getMostRecentLocationByDeviceKey($devicekey)
	{
		$q = "SELECT connections.ip_lon,
			connections.ip_lat,
			connections.timestamp
		FROM connections
		WHERE connections.devicekey = '$devicekey'
		AND
			connections.ip_lon <> '0.000000'
		AND
			connections.ip_lon <> ''
		AND
			connections.ip_lat <> '0.000000'
		AND
			connections.ip_lat <> ''
		ORDER BY connections.timestamp DESC";
		$result = $this->queryUniqueObject($q);

		if($result) {
			return DeviceConnection::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param DeviceConnection $deviceConnection
	 * @return DeviceConnection
	 */
	public function saveNewDeviceConnection($deviceConnection)
	{
		//TODO should sanitize objects attributes here...

		$newDeviceConnectionQuery = "
			INSERT INTO connections (
				`devicekey`,
				`timestamp`,
				`public_ip`,
				`internal_ip`,
				`stolen`,
				`username`,
				`computer_name`,
				`ip_country`,
				`ip_state`,
				`ip_city`,
				`postal_code`,
				`ip_lon`,
				`ip_lat`,
				`agent`,
				`os`,
				`serial`,
				`loc_state`,
				`loc_city`,
				`loc_street`,
				`did`
			) VALUES (
				'$deviceConnection->devicekey',
				'$deviceConnection->timestamp',
				'$deviceConnection->public_ip',
				'$deviceConnection->internal_ip',
				'$deviceConnection->stolen',
				'$deviceConnection->username',
				'$deviceConnection->computer_name',
				'$deviceConnection->ip_country',
				'$deviceConnection->ip_state',
				'$deviceConnection->ip_city',
				'$deviceConnection->postal_code',
				'$deviceConnection->ip_lon',
				'$deviceConnection->ip_lat',
				'$deviceConnection->agent',
				'$deviceConnection->os',
				'$deviceConnection->serial',
				'$deviceConnection->loc_state',
				'$deviceConnection->loc_city',
				'$deviceConnection->loc_street',
				'$deviceConnection->did'
		)";

		$this->executeNoDie($newDeviceConnectionQuery);
		$deviceConnection->connectionid = mysql_insert_id();
		return $deviceConnection;
	}

	/**
	 *
	 * @param int $connectionId
	 * @param int $userId
	 * @return bool
	 */
	public function deleteDeviceConnection($connectionId, $userId)
	{
		HttpUtils::cleanInput($connectionId);
		HttpUtils::cleanInput($userId);

		$q = "DELETE connections.* FROM connections
				INNER JOIN devicemap ON connections.devicekey = devicemap.devicekey
				WHERE connections.connectionid='$connectionId'
					AND devicemap.userid='$userId'";
		return $this->query($q);
	}

	/**
	 *
	 * @param int $deviceId the device key to delete connections for
	 * @param int $userId the owner of the device
	 * @return bool
	 */
	public function deleteDeviceConnections($deviceKey, $userId)
	{
		HttpUtils::cleanInput($deviceKey);
		HttpUtils::cleanInput($userId);

		$q = "DELETE connections.* FROM connections
				INNER JOIN devicemap ON connections.devicekey = devicemap.devicekey
				WHERE connections.devicekey='$deviceKey'
					AND devicemap.userid='$userId'";
		return $this->query($q);
	}

}