<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utils
 *
 * @author msweet
 */
class Utils {

	public static function smoothdate ($year, $month, $day)
	{
		return sprintf ('%04d', $year) . sprintf ('%02d', $month) . sprintf ('%02d', $day);
	}

	public static function dateDifference($first, $second)
	{
		$month_lengths = array (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

		$retval = FALSE;

		if (    checkdate($first['month'], $first['day'], $first['year']) &&
				checkdate($second['month'], $second['day'], $second['year'])
			)
		{
			$start = self::smoothdate ($first['year'], $first['month'], $first['day']);
			$target = self::smoothdate ($second['year'], $second['month'], $second['day']);

			if ($start <= $target)
			{
				$add_year = 0;
				while (self::smoothdate ($first['year']+ 1, $first['month'], $first['day']) <= $target)
				{
					$add_year++;
					$first['year']++;
				}

				$add_month = 0;
				while (self::smoothdate ($first['year'], $first['month'] + 1, $first['day']) <= $target)
				{
					$add_month++;
					$first['month']++;

					if ($first['month'] > 12)
					{
						$first['year']++;
						$first['month'] = 1;
					}
				}

				$add_day = 0;
				while (self::smoothdate ($first['year'], $first['month'], $first['day'] + 1) <= $target)
				{
					if (($first['year'] % 100 == 0) && ($first['year'] % 400 == 0))
					{
						$month_lengths[1] = 29;
					}
					else
					{
						if ($first['year'] % 4 == 0)
						{
							$month_lengths[1] = 29;
						}
					}

					$add_day++;
					$first['day']++;
					if ($first['day'] > $month_lengths[$first['month'] - 1])
					{
						$first['month']++;
						$first['day'] = 1;

						if ($first['month'] > 12)
						{
							$first['month'] = 1;
						}
					}

				}

				$retval = array ('years' => $add_year, 'months' => $add_month, 'days' => $add_day);
			}
		}
		return $retval;
	}

	public static function createOptionsList($optionIds, $id)
	{
		$optionMap = array(
			"eq" => array("label" => "Equal to", "value" => "="),
			"ne" => array("label" => "Not equal to", "value" => "<>"),
			"lt" => array("label" => "Less Than", "value" => "<"),
			"gt" => array("label" => "Greater Than", "value" => ">"),
			"lk" => array("label" => "Like", "value" => "LIKE"),
			"in" => array("label" => "Is NULL", "value" => "ISNULL"),
			"nn" => array("label" => "Is Not NULL", "value" => "NOTNULL"),
		);
		foreach($optionIds as $optionId) {
			$option = $optionMap[$optionId];
			$selected = ($_GET[$id] == $option['value'])?"selected=\"selected\"":"";
			print("<option value=\"$option[value]\" $selected>$option[label]</option>");
		}
	}

	public static function UrlSearchParam($key, $value) {
		return $key . "_o==&" . $key . "_a=on&" . $key . "_v=" . $value;
	}

}
?>
