<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/AdminUserDAO.php");
require_once("classes/UserManager.php");

class AdminUserManager extends UserManager {

	public function __construct()
	{
		$this->dao = new AdminUserDAO();
	}

	public function findUsers($filters = null, $sorts = null, &$paginator = null) {
		return $this->dao->findUsers($filters, $sorts, $paginator);
	}

	public function getUserById($userId = null) {
		return $this->dao->getUserById($userId);
	}

	public function getUserByEmail($email = null) {
		return $this->dao->getUserByEmail($email);
	}

}


?>