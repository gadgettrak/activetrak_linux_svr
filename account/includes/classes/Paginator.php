<?php

class Paginator {

	public $pageNum;
	public $pageSize;
	public $numRecords;
	public $totalRecords;
	public $numPages;

	public $rowStartNum;
	public $rowEndNum;

    public function __construct($pageNum = 1, $pageSize = 20) {
		$this->pageNum = $pageNum;
		$this->pageSize = $pageSize;
	}

	public function updateCounts()
	{
		
		$this->numPages = ceil($this->totalRecords/$this->pageSize);

		$this->rowStartNum = ($this->pageNum - 1) * $this->pageSize + 1;

		$this->rowEndNum = (($this->rowStartNum + $this->pageSize) < $this->totalRecords)?$this->rowStartNum + $this->pageSize - 1:$this->totalRecords;

		if ($this->pageNum > $this->numPages) {
		   $this->pageNum = $this->numPages;
		}

		if ($this->pageNum < 1) {
		   $this->pageNum = 1;
		}
	}
}

?>
