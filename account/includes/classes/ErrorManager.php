<?php
/**
 * Error Manager
 *
 * A manager class used to interact with this Model's DAO
 *
 * Example usage:
 * $errorManager = new ErrorManager();
 * $error = new Error();
 * $errorManager->save($error);
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/ErrorDAO.php");

class ErrorManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new ErrorDAO();
	}

	/**
	 * Creates a new Error
	 * @param Error $error
	 * @return Error
	 */
	public function create($error)
	{
		return $this->dao->create($error);
	}

	/**
	 * Updates a Error
	 * @param Error $error
	 * @return Error
	 */
	public function update($error)
	{
		return $this->dao->update($error);
	}

	public function createIfDeviceKeyUnique($error)
	{
		return $this->dao->createIfDeviceKeyUnique($error);
	}

	/**
	 * Deletes a Error	 * @param Error $error
	 * @return bool
	 */
	public function delete($error)
	{
		return $this->dao->delete($error);
	}

	/**
	 * Retrieves a Error in the database
	 * @param String id 
	 * @return Error
	 */
	public function get($id)
	{
		return $this->dao->get($id);
	}

	public function getAllErrors($filters = null, $sorts = null, &$paginator = null)
	{
		return $this->dao->getAllErrors($filters, $sorts, $paginator);
	}

}

?>