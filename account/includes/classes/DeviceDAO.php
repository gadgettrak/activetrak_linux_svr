<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/Device.php");
require_once("classes/DeviceResult.php");
require_once("classes/DeviceResults.php");
require_once("classes/DeviceConnection.php");
require_once("classes/DeviceConnectionManager.php");
require_once("classes/DeviceResultsSorted.php");
require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class DeviceDAO extends BaseDAO
{
	/**
	 *
	 * @param string $macAddress
	 * @return bool
	 */
	public function isDuplicateDevice($macAddress)
	{
		HttpUtils::cleanInput($macAddress);
		$result = $this->query("SELECT deviceid FROM devices WHERE macAddress='$macAddress'");
		if($this->numRows($result)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param string $deviceKey
	 * @return Device
	 */
	public function getDeviceByKey($deviceKey, $userId = null)
	{
		HttpUtils::cleanInput($deviceKey);
		if($userId) {
			$q = "SELECT devices.*, devicemap.userid FROM devices, devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid='$userId' AND devices.devicekey='$deviceKey'";
		} else {
			$q = "SELECT * FROM devices WHERE devicekey='$deviceKey'";
		}
		$result = $this->queryUniqueObject($q);
		if($result) {
			return Device::newFromDatabase($result);
		} else {
			return null;
		}
	}

	public function getDeviceById($deviceId)
	{
		$q = "SELECT * FROM devices WHERE deviceid='$deviceId'";
		$result = $this->queryUniqueObject($q);
		if($result) {
			return Device::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param Device $device
	 * @return Device
	 */
	public function saveNewDevice($device)
	{
		$device->cleanAttributes();
		$newDeviceQuery = "INSERT INTO devices (type, model, manufacturer, os, theft_status, serial, color, description, devicekey, macAddress, labelcode, datecreated, pro, productid, saveTracking) VALUES ('$device->type', '$device->model', '$device->manufacturer', '$device->os', '$device->theft_status', '$device->serial', '$device->color', '$device->description', '$device->devicekey', '$device->macAddress', '$device->labelcode', '$device->datecreated', '$device->pro', '$device->productid', '$device->saveTracking')";
		$this->executeNoDie($newDeviceQuery);
		$device->deviceid = mysql_insert_id();
		return $device;
	}

	/**
	 *
	 * @param Device $device
	 * @return bool
	 */
	public function updateDevice($device)
	{
		$device->cleanAttributes();
		$q = "UPDATE devices SET
			`type` = '$device->type',
			`model` = '$device->model',
			`manufacturer` = '$device->manufacturer',
			`os` = '$device->os',
			`theft_status` = '$device->theft_status',
			`serial` = '$device->serial',
			`color` = '$device->color',
			`description` = '$device->description',
			`devicekey` = '$device->devicekey',
			`macAddress` = '$device->macAddress',
			`labelcode` = '$device->labelcode',
			`datecreated` = '$device->datecreated',
			`pro` = '$device->pro',
			`productid` = '$device->productid',
			`saveTracking` = '$device->saveTracking'
		WHERE
			deviceid='$device->deviceid'";
		return $this->execute($q);
	}

	/**
	 *
	 * @param int $id
	 * @return bool
	 */
	public function deleteDeviceById($id)
	{
		HttpUtils::cleanInput($id);
		return $this->execute("DELETE FROM devices WHERE deviceid='$id'");
	}

	/**
	 *
	 * @param string $deviceKey
	 * @param int $userId
	 * @return DeviceResult
	 */
	public function getDeviceResultByKey($deviceKey, $userId = null)
	{

		HttpUtils::cleanInput($userId);
		HttpUtils::cleanInput($deviceKey);
		$q = "SELECT license.activated,
			license.length,
			devices.type,
			devices.model,
			devices.manufacturer,
			devices.os,
			devices.theft_status,
			devices.serial,
			devices.color,
			devices.deviceid,
			devices.description,
			devices.devicekey,
			devices.labelcode,
			devices.datecreated,
			devices.productid,
			devices.saveTracking,
			products.productname,
			license.`key`,
			devices.macAddress,
			devices.version,
			license.id
			FROM devicemap INNER JOIN devices ON devices.devicekey = devicemap.devicekey
			LEFT OUTER JOIN products ON products.productid = devices.productid
			LEFT OUTER JOIN license ON license.deviceId = devices.deviceid
			WHERE devicemap.userid = '$userId' AND devices.devicekey='$deviceKey'";

		$results = $this->queryUniqueObject($q);
		$deviceResult = DeviceResult::newFromDatabase($results);
		return $deviceResult;
	}

	/**
	 *
	 * @param string $deviceId
	 * @return DeviceResult
	 */
	public function getDeviceResultByid($deviceId)
	{

		HttpUtils::cleanInput($deviceId);
		$q = "SELECT license.activated,
			license.length,
			devices.type,
			devices.model,
			devices.manufacturer,
			devices.os,
			devices.theft_status,
			devices.serial,
			devices.color,
			devices.deviceid,
			devices.description,
			devices.devicekey,
			devices.labelcode,
			devices.datecreated,
			devices.productid,
			devices.saveTracking,
			products.productname,
			license.`key`,
			devices.macAddress,
			devices.version,
			license.id
			FROM devicemap INNER JOIN devices ON devices.devicekey = devicemap.devicekey
			LEFT OUTER JOIN products ON products.productid = devices.productid
			LEFT OUTER JOIN license ON license.deviceId = devices.deviceid
			WHERE devices.deviceid='$deviceId'";

		$results = $this->queryUniqueObject($q);
		$deviceResult = DeviceResult::newFromDatabase($results);
		return $deviceResult;
	}

	/**
	 *
	 * @param int $userId
	 * @return resource
	 */
	private function getUserDeviceResults($userId) {
		HttpUtils::cleanInput($userId);
		$q = "SELECT license.activated,
			license.length,
			devices.type,
			devices.model,
			devices.manufacturer,
			devices.os,
			devices.theft_status,
			devices.serial,
			devices.color,
			devices.deviceid,
			devices.description,
			devices.devicekey,
			devices.labelcode,
			devices.datecreated,
			devices.productid,
			devices.saveTracking,
			products.productname,
			license.`key`,
			devices.macAddress,
			license.id,
			devices.version
			FROM devicemap INNER JOIN devices ON devices.devicekey = devicemap.devicekey
			LEFT OUTER JOIN products ON products.productid = devices.productid
			LEFT OUTER JOIN license ON license.deviceId = devices.deviceid
			WHERE devicemap.userid = '$userId'
			ORDER BY devices.description, devices.productid DESC";

		return $this->query($q);
	}

	/**
	 *
	 * @param int $userId
	 * @return DeviceResultsSorted
	 */
	public function getSortedUserDevices($userId)
	{
		$results = $this->getUserDeviceResults($userId);
		$deviceResults = new DeviceResultsSorted();
		while ($row = mysql_fetch_object($results)) {
			$deviceResult = DeviceResult::newFromDatabase($row);
			$deviceResults->addResult($deviceResult);
		}
		return $deviceResults;
	}

	/**
	 *
	 * @param int $userId
	 * @return DeviceResults
	 */
	public function getUserDevices($userId)
	{
		$deviceConnectionManager = new DeviceConnectionManager();
		$results = $this->getUserDeviceResults($userId);
		$deviceResults = new DeviceResults();
		while ($row = mysql_fetch_object($results)) {
			$deviceResult = DeviceResult::newFromDatabase($row);
			$deviceConnection = $deviceConnectionManager->getMostRecentLocationByDeviceKey($deviceResult->devicekey);
			$deviceResult->lastConnection = $deviceConnection;
			$deviceResults->addResult($deviceResult);
		}
		return $deviceResults;
	}

	/**
	 *
	 * @param int $userId
	 * @return int
	 */
	public function getTotalActiveDevices($userId)
	{
		HttpUtils::cleanInput($userId);
		$q = "SELECT devices.devicekey FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey WHERE devicemap.userid='$userId' AND devices.theft_status='Y'";
		$result = $this->query($q);
		return $this->numRows($result);
	}

}