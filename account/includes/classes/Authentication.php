<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/UserManager.php");
require_once("classes/UserCredentials.php");
require_once("classes/User.php");

class Authentication
{

	/**
	 *
	 * @param string $redirect
	 */
	public static function authenticateSession($redirect = true)
	{
		session_start();
		if (!$_SESSION['loggedin']) {
			if($redirect) {
				header("Location: /login/");
			} else {
				header("HTTP/1.0 403 Forbidden");
				die();
			}
		}
	}

	/**
	 *
	 * @param string $redirect
	 */
	public static function authenticateAdminSession($redirect = true)
	{
		session_start();
		if ($_SESSION['loggedin'] !== true || $_SESSION['isAdmin'] !== true) {
			if($redirect) {
				header("Location: /__admin/login.php");
			} else {
				header("HTTP/1.0 403 Forbidden");
				die();
			}
		}
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @param string $redirectUrl
	 */
	public static function loginUser($credentials, $redirectUrl)
	{
		session_start();
		$userManager = new UserManager();
		$user = $userManager->getUserByUserCredentials($credentials);
		if(!$user) {
			header("Location: /login/?error=UserNotFound");
		} else {
			$_SESSION['user'] = $user;
			$_SESSION['userid'] = $user->userid;
			$_SESSION['loggedin'] = true;
			header("Location: $redirectUrl");
		}
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @param string $redirectUrl
	 */
	public static function loginAdmin($credentials, $redirectUrl)
	{
		session_start();
		$userManager = new UserManager();
		$user = $userManager->getAdminByUserCredentials($credentials);
		if(!$user) {
			header("Location: /__admin/login.php?error=UserNotFound");
		} else {
			$_SESSION['user'] = $user;
			$_SESSION['isAdmin'] = true;
			$_SESSION['loggedin'] = true;
			header("Location: $redirectUrl");
		}
	}

	public static function logoutUser()
	{
		session_start();
		$_SESSION = array();
		if (isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time()-42000, '/');
		}
		session_destroy();
		header("Location: /login/");
	}

}

?>