<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceDAO.php");

class DeviceManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new DeviceDAO();
	}

	/**
	 * 
	 * @param string $macAddress
	 * @return bool
	 */
	public function isDuplicateDevice($macAddress)
	{
		return $this->dao->isDuplicateDevice($macAddress);
	}

	/**
	 *
	 * @param string $deviceKey
	 * @return Device
	 */
	public function getDeviceByKey($deviceKey, $userId = null)
	{
		return $this->dao->getDeviceByKey($deviceKey, $userId);
	}

	public function getDeviceById($deviceId)
	{
		return $this->dao->getDeviceById($deviceId);
	}

	/**
	 *
	 * @param string $deviceKey
	 * @param int $userId
	 * @return DeviceResult
	 */
	public function getDeviceResultByKey($deviceKey, $userId = null)
	{
		return $this->dao->getDeviceResultByKey($deviceKey, $userId);
	}

	/**
	 *
	 * @param string $deviceId
	 * @return DeviceResult
	 */
	public function getDeviceResultByid($deviceId)
	{
		return $this->dao->getDeviceResultById($deviceId);
	}

	/**
	 *
	 * @param Device $device
	 * @return Device
	 */
	public function addNewDevice($device)
	{
		return $this->dao->saveNewDevice($device);
	}

	/**
	 *
	 * @param Device $device
	 * @return bool
	 */
	public function updateDevice($device)
	{
		return $this->dao->updateDevice($device);
	}

	/**
	 *
	 * @param int $id
	 * @return bool
	 */
	public function deleteDeviceById($id)
	{
		return $this->dao->deleteDeviceById($id);
	}

	/**
	 *
	 * @param int $userId
	 * @return mixed
	 */
	public function getSortedUserDevices($userId)
	{
		return $this->dao->getSortedUserDevices($userId);
	}

	/**
	 *
	 * @param int $userId
	 * @return mixed
	 */
	public function getUserDevices($userId)
	{
		return $this->dao->getUserDevices($userId);
	}

	public function getTotalActiveDevices($userId)
	{
		return $this->dao->getTotalActiveDevices($userId);
	}

}


?>