<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/HttpResponseCodes.php");

class HttpUtils {

	/**
	 *
	 * @param <type> $code 
	 */
	public static function setHttpStatus($code)
	{
		header('HTTP/1.0 ' . HttpResponseCodes::$CODES["STATUS_$code"]);
		if(CONFIG_APP_DEBUG) {
			print('HTTP/1.0 ' . HttpResponseCodes::$CODES["STATUS_$code"]);
		}
	}

	/**
	 *
	 * @return <type>
	 */
	public static function getXMLPayload()
	{
		$raw_data = file_get_contents('php://input');
		try {
			$xml = @new SimpleXMLElement($raw_data);
			return $xml;
		} catch (Exception $e) {
			print($e->getMessage() . " | " . $e->getCode());
			return null;
		}
	}

	/**
	 *
	 */
	public static function requirePostRequest()
	{
		if ( $_SERVER['REQUEST_METHOD'] !== 'POST')
		{
			self::setHttpStatus(400);
			die();
		}
	}

	/**
	 *
	 * @param <type> $var
	 */
	public static function cleanInput(&$var)
	{
		//first strip slashes to prevent double escaping
		$var = stripslashes($var);
		$var = mysql_escape_string($var);
	}

	public static function cleanOutput(&$var)
	{
		return htmlspecialchars(stripslashes($var));
	}

	/**
	 *
	 * @param <type> $array
	 */
	public static function cleanRequestParamsArray(&$array)
	{
		$array = array_map('mysql_escape_string', $array);
	}

}