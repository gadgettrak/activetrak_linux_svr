<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/HttpUtils.php");
require_once("classes/StatCounts.php");

class Stats
{

	private $dao;

	public function __construct()
	{
		$this->dao = new BaseDAO();
	}

	public function getNumRegisteredDevices($fromDate = null) {
		$q = "SELECT devicekey FROM devices WHERE datecreated >= '$fromDate'";
		$result = $this->dao->execute($q);
		return $this->dao->numRows($result);
	}

	public function getActivatedLicenses($fromDate = null) {
		$q = "SELECT id FROM license WHERE activated >= '$fromDate'";
		$result = $this->dao->execute($q);
		return $this->dao->numRows($result);
	}

	public function getRegisteredDevicesByMonthSummary() {
		$q = "select count(devicekey) as count, MONTH(datecreated) as month, YEAR(datecreated) as year from devices WHERE YEAR(datecreated)>='2001' GROUP BY YEAR(datecreated), MONTH(datecreated) ORDER BY datecreated ASC";
		$result = $this->dao->execute($q);
		$deviceMonths = $this->dao->getResultsAsObjectList($result);

		$values = array();
		$labels = array();
		foreach($deviceMonths as $deviceMonth) {
			array_push($values, (int)$deviceMonth->count);
			array_push($labels, $deviceMonth->month . "/" . substr($deviceMonth->year, 2));
		}

		return array('values'=>$values, 'labels'=>$labels);
	}

	public function getRegisteredLaptopsByMonthSummary() {
		$q = "select count(devicekey) as count, MONTH(datecreated) as month, YEAR(datecreated) as year from devices WHERE YEAR(datecreated)>='2001' AND productid IN ('4', '7', '9', '10', '11', '12', '13', '14', '15', '16', '17') GROUP BY YEAR(datecreated), MONTH(datecreated) ORDER BY datecreated ASC";
		$result = $this->dao->execute($q);
		$deviceMonths = $this->dao->getResultsAsObjectList($result);

		$values = array();
		$labels = array();
		foreach($deviceMonths as $deviceMonth) {
			array_push($values, (int)$deviceMonth->count);
			array_push($labels, $deviceMonth->month . "/" . substr($deviceMonth->year, 2));
		}

		return array('values'=>$values, 'labels'=>$labels);
	}

	public function getActivatedLicensesByMonthSummary() {
		$q = "select count(id) as count, MONTH(activated) as month, YEAR(activated) as year from license WHERE used='Y' AND YEAR(activated)>='2001' group by YEAR(activated), MONTH(activated) ORDER BY activated ASC";
		$result = $this->dao->execute($q);
		$licenseMonths = $this->dao->getResultsAsObjectList($result);

		$values = array();
		$labels = array();
		foreach($licenseMonths as $licenseMonth) {
			array_push($values, (int)$licenseMonth->count);
			array_push($labels, $licenseMonth->month . "/" . substr($licenseMonth->year, 2));
		}

		return array('values'=>$values, 'labels'=>$labels);
	}

	//select count(*), MONTH(activated), YEAR(activated) from license group by YEAR(activated), MONTH(activated) ORDER BY activated DESC;
	//select count(*), MONTH(datecreated), YEAR(datecreated) from devices group by YEAR(datecreated), MONTH(datecreated) ORDER BY datecreated DESC;

	public function getRegisteredDevicesByDaySummary() {
		$q = "select count(devicekey) as count, DAY(datecreated) as day, MONTH(datecreated) as month, YEAR(datecreated) as year from devices WHERE YEAR(datecreated)>='2008' AND MONTH(datecreated)>='6' GROUP BY YEAR(datecreated), MONTH(datecreated), DAY(datecreated) ORDER BY datecreated ASC";
		$result = $this->dao->execute($q);
		$deviceMonths = $this->dao->getResultsAsObjectList($result);

		$values = array();
		$labels = array();
		$i = 0;
		foreach($deviceMonths as $deviceMonth) {
			array_push($values, (int)$deviceMonth->count);
			if($i%1 == 0) {
				array_push($labels, $deviceMonth->month . "/" . $deviceMonth->day . "/" . substr($deviceMonth->year, 2));

			} else {
				array_push($labels, " ");
			}

			$i++;

		}

		return array('values'=>$values, 'labels'=>$labels);
	}

	public function getLatestDeviceStats() {

		$iPhoneQuery = "
			SELECT
				COUNT(devicekey) as count,
				DAY(datecreated) as day,
				MONTH(datecreated) as month,
				YEAR(datecreated) as year,
				datecreated
			FROM
				devices
			WHERE
				YEAR(datecreated)>='2001'
			AND
				productid IN ('8')
			GROUP BY
				YEAR(datecreated),
				MONTH(datecreated),
				DAY(datecreated)
			ORDER BY
				datecreated ASC
			";


		$laptopQuery = "
			SELECT
				COUNT(devicekey) as count,
				DAY(datecreated) as day,
				MONTH(datecreated) as month,
				YEAR(datecreated) as year,
				datecreated
			FROM
				devices
			WHERE
				YEAR(datecreated)>='2001'
			AND
				productid IN ('4', '7', '9', '10', '11', '12', '13', '14', '15', '16', '17')
			GROUP BY
				YEAR(datecreated),
				MONTH(datecreated),
				DAY(datecreated)
			ORDER BY
				datecreated ASC
			";

		$result1 = $this->dao->execute($iPhoneQuery);
		$iPhoneResults = $this->processDeviceResults($result1);

		$result2 = $this->dao->execute($laptopQuery);
		$laptopResults = $this->processDeviceResults($result2);

		$startDate = new DateTime(date("Y-m-d", ($iPhoneResults["min"] < $laptopResults["min"])?$iPhoneResults["min"]:$laptopResults["min"]));
		$endDate = new DateTime(date("Y-m-d", ($iPhoneResults["max"] > $laptopResults["max"])?$iPhoneResults["max"]:$laptopResults["max"]));

		$iphoneData = array();
		while ($startDate <= $endDate) {
			$key = $startDate->format("Y-m-d");
			$val = (isset($iPhoneResults["values"][$key])?$iPhoneResults["values"][$key]:"0");
			array_push($iphoneData, intval($val));
			$startDate->modify('+1 day');
		}
		
		$startDate = new DateTime(date("Y-m-d", ($iPhoneResults["min"] < $laptopResults["min"])?$iPhoneResults["min"]:$laptopResults["min"]));
		$endDate = new DateTime(date("Y-m-d", ($iPhoneResults["max"] > $laptopResults["max"])?$iPhoneResults["max"]:$laptopResults["max"]));

		$laptopData = array();
		while ($startDate <= $endDate) {

			$key = $startDate->format("Y-m-d");
			$val = (isset($laptopResults["values"][$key])?$laptopResults["values"][$key]:"0");
			array_push($laptopData, intval($val));
			$startDate->modify('+1 day');
		}

		$startDate = new DateTime(date("Y-m-d", ($iPhoneResults["min"] < $laptopResults["min"])?$iPhoneResults["min"]:$laptopResults["min"]));

		return array("startDate"=>$startDate->format("Y/m/d"), "endDate"=>$endDate->format("Y/m/d"), "laptopData"=>$laptopData, "iPhoneData"=>$iphoneData);
	}

	public function getLatestUserStats() {

		$query = "
			SELECT
				COUNT(userid) as count,
				DAY(datecreated) as day,
				MONTH(datecreated) as month,
				YEAR(datecreated) as year,
				datecreated
			FROM
				users
			WHERE
				YEAR(datecreated)>='2001'
			GROUP BY
				YEAR(datecreated),
				MONTH(datecreated),
				DAY(datecreated)
			ORDER BY
				datecreated ASC
			";


		$result1 = $this->dao->execute($query);
		$results = $this->processDeviceResults($result1);


		$startDate = new DateTime(date("Y-m-d", $results["min"]));
		$endDate = new DateTime(date("Y-m-d", $results["max"]));

		$data = array();
		while ($startDate <= $endDate) {
			$key = $startDate->format("Y-m-d");
			$val = (isset($results["values"][$key])?$results["values"][$key]:"0");
			array_push($data, intval($val));
			$startDate->modify('+1 day');
		}

		$startDate = new DateTime(date("Y-m-d", $results["min"]));


		return array("startDate"=>$startDate->format("Y/m/d"), "endDate"=>$endDate->format("Y/m/d"), "data"=>$data);
	}


	private function processDeviceResults($results) {
		$values = array();
		$min;
		$max;
		while ($row = mysql_fetch_assoc($results)) {
			$row = (object) $row;
			$key = date("Y-m-d", strtotime($row->datecreated));
			$values[$key] = $row->count;
			//update min
			if(isset($min)) {
				$min = ($min < strtotime($key))?$min:strtotime($key);
			} else {
				$min = strtotime($key);
			}
			//update max
			if(isset($max)) {
				$max = ($max > strtotime($key))?$max:strtotime($key);
			} else {
				$max = strtotime($key);
			}
		}
		return array("min"=>$min, "max"=>$max, "values"=>$values);
	}

	public function getStatCounts()
	{

		$statCounts = new StatCounts();

		$query = "SELECT COUNT(*) as count from users";
		$result = $this->dao->queryUniqueObject($query);
		
		$statCounts->devicesTotal = $result->count;

		$query = "SELECT * FROM stats";
		$results = $this->dao->query($query);
		$stats = $this->dao->getResultsAsObjectList($results);

		foreach($stats as $stat) {

			switch($stat->type) {
				case "iPhonePhoto":
					$statCounts->iOsPhotos = $stat->count;
					break;

				case "trackingRequests":
					$statCounts->devicesTracked = $stat->count;
					break;

				case "laptopPhotos":
					$statCounts->laptopPhotos = $stat->count;
					break;
			}
		}
		return $statCounts;
	}


}