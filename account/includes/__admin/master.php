<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-us" />
<title>GadgetTrak Web Admin - <?=$pagetitle?></title>
<link rel="stylesheet" href="/__admin/css/reset.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/__admin/css/layout.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/__admin/css/main.css" type="text/css" media="screen, projection" />

<link href="/_style/custom-theme2/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/_js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/_js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/_js/jquery.tools.min.js"></script>
<script type="text/javascript" src="/_js/highcharts.js"></script>

<script type="text/javascript">
	//Bind modals to links
	$(".modalEdit").live("click", function() {
		var $link = $(this);
		var $d = $("<div/>").attr("id", "genDialog");
		$("body").append($d);
		$d.load($link.attr("href"), function() {
			$d.dialog({
				modal: true,
				width:500,
				title:$link.attr("title"),
				resizable:true,
				buttons: {
					'Cancel':function() {$d.dialog('destroy');}
				}
			});
			$d.dialog("resize", "auto");
		});
		return false;
	});

$(function() {

	function enableInputs(prefix, ids, isOn) {
		$.each(ids, function() {
			var el = $("#" + prefix + "_" + this);
			if(isOn) {
				el.removeAttr("disabled");
			} else {
				el.attr("disabled", "disabled");
			}
		//console.log(this, prefix);
		});
	}

	function callEnable($check, $row) {
		var isOn = $check.is(":checked");
		var prefix = $check.attr("id").replace(/_a$/, "");
		enableInputs(prefix, ["o", "v"], isOn);
		if(isOn) {
			$row.addClass("activeFilter");
		} else {
			$row.removeClass("activeFilter");
		}
	}

	$("#filters tr").each(function() {
		var $this = $(this);
		var $check = $this.find(":checkbox");
		if(!$check.length) {
			return false;
		}
		callEnable($check, $this);
		$check.click(function() {
			callEnable($check, $this);
		});
		return true;
	});

	$("#filter-header").click(function() {
		$("#filters").toggle();
		$("#filter-header").toggleClass("closed");
		return false;
	})

	$("#prevButton").click(function() {
		$("#pageNum").val(parseInt($("#pageNum").val()) - 1);
		$("#searchForm").submit();
		return false;
	});

	$("#nextButton").click(function() {
		$("#pageNum").val(parseInt($("#pageNum").val()) + 1);
		$("#searchForm").submit();
		return false;
	});

	$("#pageSize").change(function() {
		$("#pageNum").val(1);
		$("#searchForm").submit();
	});

	$("pageNum").change(function() {
		$("#searchForm").submit();

	});

	$("table.data tbody tr:even").addClass("even");
	$("table.data tbody tr:odd").addClass("odd");

	$("table.sortable thead th").click(function() {
		var sfn = $(this).attr("id").replace(/_head/, "");
		var sfc = $("#sortField").val();
		var sdn = ($("#sortDirection").val() == "true");
		if(sfn == sfc) {
			sdn = !sdn;
		} else {
			sdn = true;
		}
		$("#sortField").val(sfn);
		$("#sortDirection").val(sdn);
		$("#searchForm").submit();
		return false;
	});

	var sfc = $("#sortField").val();
	var sdn = $("#sortDirection").val();

	$("#" + sfc + "_head").each(function() {
		$(this).addClass("sorted");
		$(this).addClass((sdn == "false")?"sort_desc":"sort_asc");
	});

	$("table.data tr").hover(function() {$(this).addClass("row-over");}, function() {$(this).removeClass("row-over");});

	$("table.sortable th:not('.sorted')").hover(function() {
		$(this).addClass("sort-hover");
	},
	function() {
		$(this).removeClass("sort-hover");
	});
	
});

</script>

</head>

<body>
	<div id="wrapper">
		<div id="header">
			<?php require_once("__admin/admin-header.php");?>
		</div>
		<div id="middle">
			<div id="container">
				<div id="content">
					<h2><?=$pageHeader?></h2>
					<?=$pagemaincontent?>
				</div>
			</div>
			<div class="sidebar" id="sideLeft">
				<?php require_once("__admin/admin-menu.php");?>
			</div>
		</div>
	</div>
	<div id="footer">
		<?php require_once("__admin/admin-footer.php");?>
	</div>
</body>

</html>