<?php

require_once 'classes/Utils.php';
//$filterVars should be a global set in the search/index.php file

?>

<table class="form">
	<tbody id="filters">
		<?php foreach($filterVars as $filterVar) {?>
		<?php
			$varName = $filterVar . "_v";
		?>
		<tr>
			<td><input type="checkbox" name="<?=$filterVar?>_a" id="<?=$filterVar?>_a" <?if(isset($_GET["{$filterVar}_a"])) { print "checked=\"checked\"";}?>/></td>
			<td><label for="<?=$filterVar?>_a"><?=$filterVar?></label></td>
			<td>
				<select name="<?=$filterVar?>_o" id="<?=$filterVar?>_o">
					<?php Utils::createOptionsList(array("eq", "ne", "lk", "in", "nn"), $filterVar . "_o");?>
				</select>
			</td>
			<td><input type="text" name="<?=$filterVar?>_v" id="<?=$filterVar?>_v" value="<?=$$varName?>"></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<p><input type="submit" value="Filter Results"/></p>