<?php

require_once 'classes/Utils.php';

?>

<table class="form">
	<tbody id="filters">

		<!-- EMAIL -->
		<tr>
			<td><input type="checkbox" name="email_a" id="email_a" <?if(isset($_GET["email_a"])) { print "checked=\"checked\"";}?>/></td>
			<td><label for="email_a">Email</label></td>
			<td>
				<select name="email_o" id="email_o">
					<?php Utils::createOptionsList(array("eq", "ne", "lk"), "email_o");?>
				</select>
			</td>
			<td><input type="text" name="email_v" id="email_v" value="<?=$email_v?>"></td>
		</tr>

		<!-- DEVICE KEY -->
		<tr>
			<td><input type="checkbox" name="deviceKey_a" id="deviceKey_a" <?if(isset($_GET["deviceKey_a"])) { print "checked=\"checked\"";}?>/></td>
			<td><label for="deviceKey_a">Device Key</label></td>
			<td>
				<select name="deviceKey_o" id="deviceKey_o">
					<?php Utils::createOptionsList(array("eq", "ne", "lk"), "deviceKey_o");?>
				</select>
			</td>
			<td><input type="text" name="deviceKey_v" id="deviceKey_v" value="<?=$deviceKey_v?>"></td>
		</tr>

		<!-- PASSWORD -->
		<tr>
			<td><input type="checkbox" name="password_a" id="password_a" <?if(isset($_GET["password_a"])) { print "checked=\"checked\"";}?>/></td>
			<td><label for="password_a">Password</label></td>
			<td>
				<select name="password_o" id="password_o">
					<?php Utils::createOptionsList(array("eq", "ne", "lk"), "password_o");?>
				</select>
			</td>
			<td><input type="text" name="password_v" id="password_v" value="<?=$password_v?>"></td>
		</tr>

	</tbody>
</table>

<p><input type="submit" value="Filter Results"/></p>