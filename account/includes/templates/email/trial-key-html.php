<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>GadgetTrak Laptop Security  License</h3>
			<p>Thank you for choosing GadgetTrak to protect your laptop!<p>
			<p>Please follow the instructions below to get started.</p>

			<h4>Step 1: Download</h4>
			<p>Download the latest version of GadgetTrak : <a href="http://www.gadgettrak.com/downloads/?p=mac&v=current">Mac OS X</a> | <a href="http://www.gadgettrak.com/downloads/?p=win&v=current">Windows</a><p>

			<h4>Step 2: Install</h4>
			<p>Once you have downloaded the software, please install the application and register using the trial license key below.</p>

			<p style="padding:20px;width:300px;border-radius:8px;background-color:#f0f0f0;">License Key: <strong><?=$trialKey?></strong></p>

			<p>If you need further assistance, please refer to our <a href="http://www.gadgettrak.com/downloads/manual.pdf">user guide</a> or contact <a href="mailto:support@gadgettrak.com">support@gadgettrak.com</a> for help</p>

		</td>
	</tr>
</table>
</body>
</html>
