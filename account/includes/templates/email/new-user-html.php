<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>New User Registration</h3>
			<p>Thank you for registering at GadgetTrak.com!</p>
			<p>The next step to enable your account is to verify your email address. Please follow the link below.</p>
			<p><a href="https://account.gadgettrak.com/verify.php?key=<?=$user->email_key?>">https://account.gadgettrak.com/verify.php?key=<?=$user->email_key?></a></p>
			<br/><br/>
			<p>If you have any problems verifying your email address, please contact <a href="mailto:support@gadgettrak.com">support@gadgettrak.com</a></p>
		</td>
	</tr>
</table>
</body>
</html>