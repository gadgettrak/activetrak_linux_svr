<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>GadgetTrak Account Activation</h3>
			<p>Thanks for signing up for the GadgetTrak Laptop Security.</p>
			<p>To activate your account and receive your trial license key, simply click on the link below:</p>
			<p><a href="https://account.gadgettrak.com/trial/verify/?key=<?=$user->email_key?>&email=<?=$user->email?>">https://account.gadgettrak.com/trial/verify/?key=<?=$user->email_key?>&email=<?=$user->email?></a></p>
			
			<br/><br/>

			<p>If you have any trouble activating your account or trial, please contact us at <a href="mailto:support@gadgettrak.com">support@gadgettrak.com</a></p>
		</td>
	</tr>
</table>
</body>
</html>
