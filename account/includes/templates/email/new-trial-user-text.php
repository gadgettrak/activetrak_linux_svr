GadgetTrak Account & Trial Activation
--------------------------------------------------------------------------------

Thanks for signing up for the GadgetTrak Laptop Security trial.

To activate your account and receive your trial license key, simply click on the link below:
https://account.gadgettrak.com/trial/verify/?key=<?=$user->email_key?>&email=<?=$user->email?>

If you have any trouble activating your account or trial,
please contact us at support@gadgettrak.com