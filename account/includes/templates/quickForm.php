<?php

require_once 'HTML/QuickForm/Renderer/Default.php';

HTML_QuickForm_Renderer_Default::setFormTemplate("
<form{attributes}>
	<ul>
	{content}
	</ul>
</form>
");


HTML_QuickForm_Renderer_Default::setElementTemplate("
<li>
	<!-- BEGIN required --><span style='color: #ff0000'>*</span><!-- END required -->
	<label for=''>{label}</label>
	<!-- BEGIN error --><span style='color: #ff0000'>{error}</span><br /><!-- END error -->
	{element}
</li>
");


?>
