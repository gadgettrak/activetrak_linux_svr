<?php

require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/devices.php");

require_once("$_SERVER[DOCUMENT_ROOT]/classes/Connection.php");

authenticateSession();

$did=addslashes($_GET['did']);
$connections  = Connection::getConnectionsByDeviceId($did);

$title="Gadget Theft Tracking System - " . $_SESSION['name'];

include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>

<script type="text/javascript" src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAdPyIs1t_9TewX-elA4HvcBS-efayPAMWN6TozPxXbi10vYjwTRTHN7AZ5egYnr1rt-KjFz7HXIPO8A"></script>
<script src="/_js/jq.thickbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="/_style/thickbox.css" type="text/css" />
<script type="text/javascript">

function deleterow(id) { 
	$.ajax({
		type: "POST",
		url: "delete.php",
		data: "cid=" + id,
		success: function(html){
			$("#linkage").html(html);
		}
	});
	$("#row_"+id).remove(); 
} 
</script>

<?php cookieCrumbNav(array("myDevices", "iPhoneDevices", "iPhoneReports"));?>
<h2 style="clear:both;">Device Connection Report</h2>
<p>Note: Location data collected is based on information provided by the device, accuracy may vary depending on the device, signal strength, available GPS and other factors. You should NEVER attempt to recover a stolen device on your own, the information provided should be shared with law enforcement, we can also assist law enforcement with additional information.</p>

<table class="tabledata">
	<thead>
	<tr>
		<th>Time</th>
		<th>Address</th>
		<th>Map</th>
		<th>Latitude/Longitude</th>
		<th>IP Address</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
<?php foreach($connections as $connection) {?>
	<?
		//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
		$classPName = preg_replace('/:/','_', $device->devicekey);
	?>
	<tr id="row_<?=$connection->connectionid?>">
		<td><?=$connection->timestamp?></td>
		<td>
			<? if ($connection->loc_street == '' &&  $connection->loc_city == '' &&  $connection->loc_state == ''){?>
				n/a
			<?} else {?>
				<?=$connection->loc_street?> <?=$connection->loc_city?> <?=$connection->loc_state?>
			<?}?>
		</td>
		<td>
			<? if ($connection->ip_lat != '' &&  $connection->ip_lon != '' ){?>
				<a class="thickbox" href="map.php?lat=<?=$connection->ip_lat ?>&amp;lon=<?=$connection->ip_lon ?>&KeepThis=true&TB_iframe=true&height=625&width=600">Show Map</a><br />
			<?} else {?>
				n/a
			<?}?>
		</td>
		<td>
			<? if ($connection->ip_lat != '' &&  $connection->ip_lon != '' ){?>
				GPS:<?=$connection->ip_lat ?>,<?= $connection->ip_lon ?>
			<?} else {?>
				n/a
			<?}?>
		</td>
		<td>
			<a class="thickbox" href="http://ip.gadgettrak.com?ip=<?= $connection->public_ip ?>&KeepThis=true&TB_iframe=true&height=600&width=600"><?= $connection->public_ip ?></a>
		</td>
   		<td><a href="#" onClick="deleterow(<?=$connection->connectionid ?>);"> Delete </a></td>	
	</tr>
<? }?>
	</tbody>
</table>

<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>