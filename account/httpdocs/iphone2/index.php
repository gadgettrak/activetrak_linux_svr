<?php

require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/devices.php");

require_once("$_SERVER[DOCUMENT_ROOT]/classes/Device.php");

authenticateSession();

$devices  = Device::getIPhoneDevicesByUserId($_SESSION['user']->id);

$title="Gadget Theft Tracking System - " . $_SESSION['name'];

include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>

<script type="text/javascript">
$(function() {
	//bind all tracking links
	$('.tracking-link').trackingLink();
});
</script>

<?php cookieCrumbNav(array("myDevices", "iPhoneDevices"));?>
<h2 style="clear:both;">GadgetTrak iPhone Control Panel</h2>

<table class="tabledata">
	<thead>
	<tr>
		<th>Serial Number</th>
		<th>Description</th>
		<th>Reports</th>
		<th>Tracking</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
<?php foreach($devices as $device) {?>

	<?
		//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
		$classPName = preg_replace('/:/','_', $device->devicekey);
	?>
	<tr class="dr_<?=$classPName?> <?php if($device->theft_status=="Y"){ print("tracking-active");}?>">
		<td><?=$device->serial?></td>
		<td><?=$device->description?></td>
		<td><a href="report/?did=<?=$device->devicekey?>">View Report</a></td>
		<td><a title="Change Device Theft Status" class="tracking-link" href="status.php?s=<?=$device->theft_status?>&id=<?=$device->devicekey?>"><?=$tracking?></a></td>
		<td>
			<a class="lbOn" title="Edit Device" href="/edit.php?src=iphone&did=<?=$device->devicekey?>">Edit</a>
		</td>

	</tr>

<? }?>
	</tbody>
</table>

<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>