<?
ini_set( 
  'include_path', 
  ini_get( 'include_path' ) . PATH_SEPARATOR . "/home/kwestin/pear/php"
); 


session_start();

if (!$_SESSION['loggedin']){
	header("Location: login.php?error=notloggedin");
}

include("database.php");
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');

$form = new HTML_QuickForm('register');
$userid=$_SESSION['userid'];

function createForm() {

	global $userid, $form, $conn;
	$did=$_GET['did'];
	
	//get the device information and make sure the person logged in is the owner
	$devicesql="SELECT * FROM devices , devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid AND devices.devicekey='$did'";
	$deviceresult = mysql_query($devicesql,$conn);
	$devicedata=mysql_fetch_array($deviceresult);
	$devicedata = cleanOutput($devicedata);
	
	
	$devicetypes= array (
		$devicedata['type'] => $devicedata['type'],
		"#"=>" - Please Select -",
		"Cell Phone"=>"Cell Phone",
		"PDA"=>"PDA", 
		"Apple Mac"=>"Apple Mac",
		"Computer" => "Computer",
		"Other" => "Other"
	);
	
	if($devicedata['productid'] == 0) {
	
		$form->addElement('select', 'type', 'Device Type:', $devicetypes);
		$form->addElement('text', 'manufacturer', 'Manufacturer:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['manufacturer']));
		$form->addElement('text', 'model', 'Model:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['model']));
		$form->addElement('text', 'serial', 'Serial Number:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['serial']));

	} else {
	
		$form->addElement('html', "<tr><td>Device Type:</td><td>$devicedata[type]</td></tr>");
		$form->addElement('html', "<tr><td>Manufacturer:</td><td>$devicedata[manufacturer]</td></tr>");
		$form->addElement('html', "<tr><td>Model:</td><td>$devicedata[model]</td></tr>");
		$form->addElement('html', "<tr><td>Serial Number:</td><td>$devicedata[serial]</td></tr>");
	
		$form -> addElement ('hidden', 'type', $devicedata[type]);
		$form -> addElement ('hidden', 'manufacturer', $devicedata[manufacturer]);
		$form -> addElement ('hidden', 'model', $devicedata[model]);
		$form -> addElement ('hidden', 'serial', $devicedata[serial]);

	}
	
	$form->addElement('text', 'labelcode', 'GadgetTrak Label Code:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['labelcode']));
	$form -> addElement ('hidden', 'did', $did);
	$form -> addElement ('hidden', 'src', $_REQUEST['src']);
	$form->addElement('submit', null, 'Save', array('class' => "submit"));			
}				

function updateDevice($data) {
	global $conn;
	$data = cleanInput($data);
	$userid=addslashes($_SESSION['userid']);
	$type=addslashes($data['type']);
	$manuf=addslashes($data['manufacturer']);
	$model=addslashes($data['model']);
	$serial=addslashes($data['serial']);
	$devicekey=addslashes($data['did']);
	$labelcode=addslashes($data['labelcode']);
	$q ="UPDATE devices SET type='$type', serial='$serial' , manufacturer='$manuf', model='$model', labelcode='$labelcode' WHERE devicekey='$devicekey'";
	mysql_query($q,$conn) or die(mysql_error()); 
}

function processdata($data){
	updateDevice($data);
	if($_REQUEST['src'] == 'dash') {
		header("Location: index.php");
	} else {
		header("Location: labels.php");
	}
}

if ($form->validate()) {
	$form->process('processdata');
} else {
?>

<div style="width:450px;margin:0 auto;">
<? createForm();
	$form->display();
?>
</div>
<?} ?>