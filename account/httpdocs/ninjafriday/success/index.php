<?php include("fragments/header.php"); ?>

<h2>GadgetTrak free 1 year  registration</h2>

<p>Thank you for registering for your free license!</p>
<p>You will soon receive an email with instructions on how to verify your account. After verifying your account you will receive your license key.</p>

<?php include("fragments/footer.php");?>
