<?php

require_once("bootstrapper.php");
require_once("classes/TabelessFormRenderer.php");
require_once("classes/UserCredentials.php");
require_once("classes/LicenseManager.php");
require_once("classes/TrialsManager.php");
require_once("classes/Trials.php");
require_once("classes/Mailer.php");
require_once ('HTML/QuickForm.php');
require_once('classes/UserManager.php');

function processdata($data) {

	global $error;

	$userManager = new UserManager();
	$licenseManager = new LicenseManager();
	$trialsManager = new TrialsManager();

	$credentials = new UserCredentials($data['email'], sha1($data['password']));
	$user = $userManager->getUserByUserCredentials($credentials);

	if(!$user) {
		$error = "UserNotFound";
	}

	if(!$error) {
		$trial = $trialsManager->get($user->userid);
		if($trial) {
			$error = "TrialAlreadyUsed";
		}
	}

	if(!$error) {

		$license = $licenseManager->createTrialKey($user);
		$trial = new Trials($user->userid, gmdate("Y-m-d H:i:s"), $license->id);
		$trialsManager->create($trial);
		Mailer::sendNewTrialKey($user, $license->key);
		header("HTTP/1.1 303 See Other");
		header("Location: /trial/key/?key=" . $license->key);
	}

}

$renderer =& new HTML_QuickForm_Renderer_Tableless();
$loginForm = new HTML_QuickForm();

$loginForm->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$loginForm->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$loginForm->addElement('hidden', 'action', 'create');
$loginForm->addElement('submit', null, 'Sign In', array('class' => "submit"));

$loginForm->addRule('email', 'Please enter your email address', 'required', null, 'client');
$loginForm->addRule('email', 'Please enter a valid email', 'email', null, 'client');
$loginForm->addRule('password', 'Please enter a password', 'required', null, 'client');
$loginForm->setRequiredNote('');

$loginForm->accept($renderer);

if ($loginForm->validate()) {
	$loginForm->process('processdata');
}

?>

<?php include("fragments/header.php"); ?>

<h2>GadgetTrak free trial login</h2>
<p class="form-switch">Need an account? <a href="/trial">Register here</a></p>

<div id="trialForm" class="user-forms">

<?php if($error == "UserNotFound") { ?>
	<p class="error">The username or password you entered is incorrect.</p>
<?php }?>

<?php if($error == "TrialAlreadyUsed") { ?>
	<p class="error">Sorry, there's only one trial allowed per user.</p>
<?php } ?>

<?php
	$loginForm->accept($renderer);
	echo $renderer->toHtml();
?>

</div>

<?php include("fragments/footer.php");?>