<?php
	$trialKey = htmlspecialchars($_GET['key']);
?>
<?php include("fragments/header.php"); ?>

<h2>GadgetTrak Laptop Security 1 Year License</h2>
<p>Thank you for  registering out GadgetTrak!<p>
<p>Please follow the instructions below to get started.</p>

<h4>Step 1: Download</h4>
<p>Download the latest version of GadgetTrak : <a href="http://www.gadgettrak.com/downloads/?p=mac&v=current">Mac OS X</a> | <a href="http://www.gadgettrak.com/downloads/?p=win&v=current">Windows</a><p>

<h4>Step 2: Install</h4>
<p>Once you have downloaded the software, please install the application and register using the trial license key below.</p>

<p style="padding:20px;width:300px;border-radius:8px;background-color:#f0f0f0;">License Key: <strong><?=$trialKey?></strong></p>

<p>If you need further assistance, please refer to our <a href="http://www.gadgettrak.com/downloads/manual.pdf">user guide</a> or contact <a href="mailto:support@gadgettrak.com">support@gadgettrak.com</a> for help</p>

<?php include("fragments/footer.php");?>
