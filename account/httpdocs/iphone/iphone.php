<? 
session_start();
if (!$_SESSION['loggedin']){
header("Location: login.php?error=notloggedin");
}


include("database.php");
$userid=$_SESSION['userid'];


$q = "SELECT * FROM devices , devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid AND productid=8";

$deviceresult = mysql_query($q,$conn);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GadgetTrak iPhone &amp; iPod Touch Control Panel</title>
</head>

<body>

<h4> iPhone </h4>
<table width="700" class="tabledata" border="1">
<tr> 
<th> Manufacturer</th>
<th>Model </th>
<th>Serial Number</th>
<th>Tracking</th> 
<th>Actions </th>
<th>Reports</th>
</tr>
<?

$rownum=0;
while ($row = mysql_fetch_array($deviceresult)){
	if($rownum%2 != 0) {
	$td="<td class=\"grow\">";
	}else {
	$td="<td>";
	}
	
	if ($row['theft_status']== 'Y'){
	$tracking="Active";
	}else{
	$tracking="Disabled";

	}
	
	
print "<tr>";

 print $td . $row['manufacturer']."</td>"; 
 print $td . $row['model']."</td>"; 
  print $td . $row['serial']."</td>"; 
 print $td . "<a href=\"status.php?s=".$row['theft_status'] . "&id=". $row['devicekey'] ."\" class=\"lbOn\">" . $tracking . "</a></td>";
 print $td . "<a title=\"Edit Device\" class=\"lbOn\" href=\"edit.php?did=". $row['devicekey'] . "\"> <img  border=\"0\" src=\"/_gfx/edit.png\" /> </a>&nbsp; | &nbsp; <a class=\"lbOn\" onclick=\"javascript:confirm('You are about to delete a device, all information pertaining to this device will be removed from the database. This cannot be undone. Click OK to continue with deletion.'); \" title=\"Delete Device\"href=\"delete.php?did=" . $row['devicekey'] ."\"> <img  border=\"0\" src=\"/_gfx/delete.png\" /></a></td>"; 
 print $td;
  print "<a href=\"report.php?did=".$row['devicekey']."\" >View Report</a></td>";
 print "</td></tr>";

$rownum++;

}
?>
</table>
</body>
</html>
