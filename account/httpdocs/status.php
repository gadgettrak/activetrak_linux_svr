<?
ini_set("display_errors", 1);

require_once ('HTML/QuickForm.php');
require_once("bootstrapper.php");
require_once("classes/User.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/PushAlias.php");
require_once("classes/PushAliasManager.php");
//require_once('classes/urbanairship/urbanairship.php');
require_once('classes/ApnsPHP/Autoload.php');

Authentication::authenticateSession(false);

error_log("test test test");
$deviceKey = $_REQUEST['id'];
$event = $_REQUEST['event'];

$deviceManager = new DeviceManager();
$device = $deviceManager->getDeviceByKey($deviceKey, $_SESSION['user']->userid);
$pushAliasManager = new PushAliasManager();

?>

<?php
error_log("EVENT");
error_log($event);
error_log(array_shift( $_REQUEST));
if($event == "tracking") { 
	error_log("tracking");
	if(isset($_GET['track']) && isset($_GET['id'])) {
       
		$pushAlias = $pushAliasManager->get($deviceKey);

		if($pushAlias) {
            // Instantiate a new Message with a single recipient
            $message = new ApnsPHP_Message($pushAlias->deviceToken);
            
            $message->setCustomProperty('track', $_GET["track"]);
            if($_GET["track"] == "Y") {
                $message->setText("Meeting Reminder");
			}  
            $apns_push = new ApnsPush();
            
			try {
				$apns_push->push($message);
				$result['push_notification'] = true;
			} catch (Exception $e) {
				error_log('Caught exception: ' . $e->getMessage());
			}
		}

		$device->theft_status = $_GET['track'];
		error_log("----device : " + $_GET['saveTracking']);
        if ($device->theft_status == 'Y') {
            $device->saveTracking = $_GET['saveTracking'];
        }
		else {
            $device->saveTracking = 'N';
        }
		$deviceManager->updateDevice($device);

		$result = array();
		$result['success'] = true;
		$result['tracking_status'] = $device->theft_status;
		$result['saveTracking'] = $device->saveTracking;
		$result['total_active'] = $deviceManager->getTotalActiveDevices($_SESSION['user']->userid);

		error_log(json_encode($result));
		//prevent further output
		exit();
	}
} else {

	?>

	<p style="text-align:center;"><img src="/_gfx/tracking-icon.png" alt="tracking" /></p>

	<?php

	if($_GET['s']=='N'){?>
		<p>You are about to enable tracking for this device. Once tracking is enabled, you will recieve tracking reports approximately every 30 minutes.</p>
			
		
		<p style="text-align:center; margin:20px 0;"><strong>Save Tracking Data (<a class="tracking-tip" title="By saving tracking data, you will be able to view historical tracking information. Data older than 30 days is automatically deleted." href="#">?</a>)</strong><br/>
			<label for="saveYes">Yes</label><input type="radio" name="saveData" value="Y" id="saveYes" checked="checked">&nbsp;&nbsp;
			<label for="saveNo">No</label><input type="radio" name="saveData" value="N" id="saveNo">
		</p>
		<!--<p style="text-align:center;"><strong><a class='services-link'  href="/status.php?event=tracking&id=<?=$_GET['id']?>&track=Y"> Enable Tracking </a></strong> </p>-->
		<p style="text-align:center;"><strong><a class='services-link' href="#" onclick='statusEver("event=tracking&id=<?=$_GET['id']?>&track=Y&saveTracking=",true);return false;'> Enable Tracking </a></strong> </p>
	<?php }else if($_GET['s']=='Y'){ ?>
		<p>You are about to disable tracking for this device. Once disabled, you will stop receiving tracking reports.</p>
		<!--<p style="text-align:center;"><strong><a class='services-link' href="/status.php?event=tracking&id=<?=$_GET['id']?>&track=N"> Disable Tracking </a></strong> </p>-->
		<p style="text-align:center;"><strong><a class='services-link' href="#" onclick='statusEver("event=tracking&id=<?=$_GET['id']?>&track=N",false);return false;'> Disable Tracking </a></strong> </p>
	<?php }
}
?>

<script type="text/javascript">
	$(".tracking-tip[title]").tooltip({effect: 'bouncy'});
	
</script>
