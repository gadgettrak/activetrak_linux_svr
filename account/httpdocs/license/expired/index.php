<?php

require_once("bootstrapper.php");
require_once("classes/LicenseManager.php");

Authentication::authenticateSession();
$licenseManager = new LicenseManager();
$expiredLicenses = $licenseManager->getExpiredLicensesResults($_SESSION['user']->userid);

?>

<?php include("fragments/header.php"); ?>

<h2>Expired Licenses</h2>

<p>The following licenses have expired. The devices that are registered with them are no longer protected. We recommend your renew your licenses as soon as possible</p>

<?php if(count($expiredLicenses)) { ?>

<table class="tabledata" id="expiredLicenses">
	<thead>
	<tr>
		<th>Device Description</th>
		<th>License Key</th>
		<th>License Length</th>
		<th>Activation Date</th>
		<th>Expire Date</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($expiredLicenses as $expiredLicense) {?>
	<tr>
		<td><?=$expiredLicense->deviceDescription?></td>
		<td><?=$expiredLicense->key?></td>
		<td><?=$expiredLicense->length?></td>
		<td><?=gmdate("Y-m-d", strtotime($expiredLicense->activated))?></td>
		<td><?=$expiredLicense->expireDate?></td>
		<td><a href="https://secure.esellerate.net/secure/prefill.aspx?s=STR2046114780&Cmd=Buy&_CartItem0.SkuRefNum=SKU07270055655&_Custom.Data0=<?=$expiredLicense->key?>&page=MultiCart.htm" class="btn-red30"><span>Renew License</span></a></td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<p>Renewals will be reflected in your account within 24 hours.</p>

<?php } ?>

<?php include("fragments/footer.php"); ?>