<?php require_once("$_SERVER[DOCUMENT_ROOT]/_functions/navigation.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$title?></title>

<link href="/_style/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/_style/custom-theme/jquery-ui-1.8.custom.css" type="text/css" />

<script type="text/javascript" src="/_js/jq.js"></script>
<script type="text/javascript" src="/_js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="/_js/core.js"></script>

<script type="text/javascript">
$(function() {

	//If user isn't authenticated, then redirect to login if any ajax request is made...
	$.ajaxSetup({
		error:function(xhr, e) {
			if (xhr.status == 403) {
				window.location = "/login.php";
			}
		}
	});
	
	//Bind modals to links
	$(".lbOn").live("click", function() {
		var $link = $(this);
		var $d = $("<div/>").attr("id", "genDialog");
		$("body").append($d);
		$d.load($link.attr("href"), function() {
			$d.dialog({
				modal: true,
				width:500,
				title:$link.attr("title"),
				resizable:true,
				buttons: {
					'Cancel':function() {$d.dialog('destroy');}
				}

			});
			$d.dialog("resize", "auto");
		});
		return false;
	});
	
	//Stripe all data tables
	$(".tabledata tr:even").addClass("row-even");
	$(".tabledata tr").hover(function() {$(this).addClass("row-over");}, function() {$(this).removeClass("row-over");});
});

</script>

<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-232419-4";
urchinTracker();
</script>

</head>

<body>
<div id="header">
	<div class="container">
		<div class="logo"><a href="/"><img src="_gfx/gt-logo.gif" alt="GadgetTrak" /></a></div>
		<div class="right">
			<h1>Control Panel</h1>
			<div id="nav">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="http://www.gadgettrak.com/support/">Support</a></li>
					<li><a href="http://www.gadgettrak.com/legal/">Legal</a></li>
					<?if ($_SESSION['loggedin']){print "<li><a href=\"/logout.php\">Logout " . $_SESSION[user]->fname . "</a></li>"; }?>
				</ul>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="container">
<!--<div id="nav"><ul><li><a href="http://www.gadgettrak.com">Home</a></li><li><a href="http://www.gadgettrak.com/support/">Support</a></li>
<li><a href="http://www.gadgettrak.com/legal/">Legal</a></li><?if ($_SESSION['loggedin']){print "<li><a href=\"/logout.php\">Logout " . $_SESSION[user]->fname . "</a></li>"; }?></ul>
</div>-->
<!--<h1 class="nh"><a href="/"><img src="/_gfx/gt_logo.png" alt="GadgetTrak"/></a></h1>-->
<div id="content">