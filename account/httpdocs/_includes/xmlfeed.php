<?php
function GetXMLTree ($xmldata)
{
// we want to know if an error occurs
 ini_set ('track_errors', '1');

 $xmlreaderror = FALSE;

 $parser = xml_parser_create ('ISO-8859-1');
 xml_parser_set_option ($parser, XML_OPTION_SKIP_WHITE, 1);
 xml_parser_set_option ($parser, XML_OPTION_CASE_FOLDING, 0);
 if (!xml_parse_into_struct ($parser, $xmldata, $vals, $index))
 {
  $xmlreaderror = TRUE;
  MyErrorLog ("XML parse error occured: ".xml_error_string (xml_get_error_code ($parser)).", data was ".$xmldata."\n", 0);
 }
 xml_parser_free ($parser);

 if (!$xmlreaderror)
 {
  $result = array ();

  $i = 0;
  if (isset ($vals [$i]['attributes']))
   foreach (array_keys ($vals [$i]['attributes']) as $attkey)
    $attributes [$attkey] = $vals [$i]['attributes'][$attkey];

  $result [$vals [$i]['tag']] = array_merge ($attributes, GetChildren ($vals, $i, 'open'));
 }

 ini_set ('track_errors', '0');
 return $result;
}

function GetChildren ($vals, &$i, $type)
{
 if ($type == 'complete')
 {
  if (isset ($vals [$i]['value']))
   return ($vals [$i]['value']);
  else
   return '';
 }


 $children = array ();     // Contains node data

  /* Loop through children */
 while (($type = $vals [++$i]['type']) != 'close')
 {
 // are there any attributes to deal with?
  $att = FALSE;
  if (isset ($vals [$i]['attributes']))
  {
  // build up an array of each attribute as there can be more than one
   $attributes = array ();
   foreach (array_keys ($vals [$i]['attributes']) as $attkey)
    $attributes [$attkey] = $vals [$i]['attributes'][$attkey];
   $att = TRUE;
  }

 // first check if we already have one and need to create an array as this is the second or subsequent one
  if (isset ($children [$vals [$i]['tag']]))
  {
   // this is the case where there is just one thing (a "complete") with one or more attribute
   // so it is only the attribute there is no value so we replace the null value with the attribute array
   if (($children [$vals [$i]['tag']] == '') && $att)
   {
    unset ($children [$vals [$i]['tag']]);
    $children [$vals [$i]['tag']] = $attributes;
   }
   // there is one of these things already and it is already an array
   elseif (is_array ($children [$vals [$i]['tag']]))
   {
    // first check is it an array because there is more than one or just because it is an array of child elements?
    $temp = array_keys ($children [$vals [$i]['tag']]);
    // this means it is an array of elements rather than there is more than one of this thing
    // so we must bump up the whole lot
    if (is_string ($temp [0]))
    {
     $a = $children [$vals [$i]['tag']];
     unset ($children [$vals [$i]['tag']]);
     // if there are any attributes then we merge them into the result too
     if ($att)
     {
      $children [$vals [$i]['tag']][0] = array_merge ($a, $attributes);
     }
     else
     {
      $children [$vals [$i]['tag']][0] = $a;
     }
    }
   }
   // this is where we bump up to an array there is just the value, no array
   else
   {
    $a = $children [$vals [$i]['tag']];
    unset ($children [$vals [$i]['tag']]);
    if ($att)
    {
     $children [$vals [$i]['tag']]['value'] = $a;
     $children [$vals [$i]['tag']] = array_merge ($children [$vals [$i]['tag']], $attributes);
    }
    else
    {
     $children [$vals [$i]['tag']][0] = $a;
    }

   }

   // this is all cases: we now move on to get the children of this element (if any)
   if ($att)
   {
    unset ($a);
    $a ['value'] = GetChildren ($vals, $i, $type);
    $a = array_merge ($a, $attributes);
    $children [$vals [$i]['tag']][] = $a;
//    $children [$vals [$i]['tag']][] = array_merge (GetChildren ($vals, $i, $type), $attributes);
   }
   else
   {
    $children [$vals [$i]['tag']][] = GetChildren ($vals, $i, $type);
   }
  }
 // we do not have one already, we do not need to bump it up and create an array
  else
  {
   if ($att)
   {
    $children [$vals [$i]['tag']][0]['value'] = GetChildren ($vals, $i, $type);
    $children [$vals [$i]['tag']] = array_merge ($children [$vals [$i]['tag']][0], $attributes);
   }
   else
   {
    $children [$vals [$i]['tag']] = GetChildren ($vals, $i, $type);
   }
  }

 }
 return $children;
}





?>
