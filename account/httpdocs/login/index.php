<?php

require_once("bootstrapper.php");
require_once("classes/TabelessFormRenderer.php");
require_once("classes/UserCredentials.php");
require_once ('HTML/QuickForm.php');

$form = new HTML_QuickForm();

$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$form->addElement('submit', null, 'Sign In', array('class' => "submit"));

$form->addRule('password', 'Please enter your password', 'required', null, 'client');
$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');

$form->setRequiredNote('');

$renderer =& new HTML_QuickForm_Renderer_Tableless();
$form->accept($renderer);

function processdata($data) {
	$credentials = new UserCredentials($data['email'], sha1($data['password']));
	Authentication::loginUser($credentials, "/");
}

if ($form->validate()) {
	$form->process('processdata');
}

include("fragments/header.php");

?>

<h2>Sign in</h2>

<div id="loginForm" class="user-forms">

<?php if($_REQUEST['error'] == "UserNotFound") {?>

	<p class="error2">The username or password you entered is incorrect.</p>

<?php }?>
<? 
	$form->accept($renderer);
	echo $renderer->toHtml();
?>

<p><a href="/user/create/">Create Account</a> | <a href="/user/forgotpassword/">Forgot Password?</a></p>

</div>

<?php include("fragments/footer.php"); ?>