<?php
	include("fragments/header.php");
?>

<style type="text/css">

.selector {
	margin:50px 0
}

.selector h2 {
	text-align:center;
	font-size:24px;
	border:0;
	color:#666;
}

.selector ul {
	list-style:none; 
	padding:0;
	margin:0;
}

.selector li {
	margin:0 0 20px;
	padding:0;
	float:left;
	width:186px;
	height:187px;
	cursor:pointer;
}

.selector li a {
	display:block;
	position:relative;
	width:186px;
	height:234px;
	text-decoration: none;
}

.selector li span {
	position:absolute;
	display:block;
	width:187px;
	height:204px;
	padding:0;
	margin:0;
	text-indent: -9999px;
}

li#macos span {
	background:transparent url('/_gfx/ps-macos.png') no-repeat 0 0;
}

li#windows span {
	background:transparent url('/_gfx/ps-windows.png') no-repeat 0 0;
}

li#iphone span {
	background:transparent url('/_gfx/ps-ios.png') no-repeat 0 0;
}

li#blackberry span {
	background:transparent url('/_gfx/ps-blackberry.png') no-repeat 0 0;
}

li#android span {
	background:transparent url('/_gfx/ps-android.png') no-repeat 0 0;
}

li#macos.active span.hover {
	background:transparent url('/_gfx/ps-macos.png') no-repeat 0 100%;
}

li#windows.active span.hover {
	background:transparent url('/_gfx/ps-windows.png') no-repeat 0 100%;
}

li#iphone.active span.hover {
	background:transparent url('/_gfx/ps-ios.png') no-repeat 0 100%;
}

li#blackberry.active span.hover {
	background:transparent url('/_gfx/ps-blackberry.png') no-repeat 0 100%;
}

li#android.active span.hover {
	background:transparent url('/_gfx/ps-android.png') no-repeat 0 100%;
}

.selector li p.btn {
	display:block;
	position:absolute;
	bottom:10px;
	width:187px;
	height:30px;
	padding:0;
	margin:0;
	background:transparent url('/_gfx/btn-login2.png') no-repeat 0 100%;
}

</style>

<script type="text/javascript">

$(function() {
	var ua = navigator.userAgent.toLowerCase();
	if(!ua.match(/iphone|ipad|ipod/)) {
		$(".selector li").hover(function() {
			$(this).addClass("active").find("a").append($("<span class='hover'>&nbsp;</span>").hide().stop().fadeIn(400));
			var btn = $("<p class='btn'>&nbsp;</p>")
			$(this).addClass("active").find("a").append(btn);
			btn.animate({opacity:0}, 0).animate({bottom:0, opacity:1}, 100);
		}, function() {
			var self = $(this);
			self.find(".btn").stop().animate({bottom:10, opacity:0}, 200, function() {
				$(this).remove();
			});
			$(".selector span.hover").stop().fadeOut(400, function() {
				$(this).remove();
				self.removeClass("active");
			});
		});   
	}
});

</script>

<div class="selector">
	<h2>Select your device to log in</h2>
	<ul>
		<li id="macos" rel="laptop"><a href="/login/"><span></span></a></li>
		<li id="windows" rel="laptop"><a href="/login/"><span></span></a></li>
		<li id="iphone" rel="laptop"><a href="/login/"><span></span></a></li>
		<li id="blackberry" rel="mobile"><a href="https://cp.activetrak.com"><span></span></a></li>
		<li id="android" rel="mobile"><a href="https://cp.activetrak.com"><span></span></a></li>
	</ul>
	<br class="clear"/>
</div>

<?php
include("fragments/footer.php");
?>