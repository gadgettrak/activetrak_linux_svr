<?php

require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/devices.php");

require_once("$_SERVER[DOCUMENT_ROOT]/classes/Device.php");

authenticateSession();

$devices  = Device::getUSBDevicesByUserId($_SESSION['user']->id);

$userid=$_SESSION['user']->id;

$credits = mysql_evaluate("Select SUM(number_licenses) FROM licenses WHERE productid=1 AND userid='$userid'");
$usedcredits = mysql_evaluate("Select COUNT(pro) FROM devices, devicemap, users WHERE devices.pro='Y' AND devicemap.devicekey = devices.devicekey AND devices.productid=1 AND users.userid=devicemap.userid AND users.userid='$userid' ");
$currentCredits=$credits - $usedcredits;

$q = "SELECT * FROM devices , devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid AND productid=1";
$deviceresult = mysql_query($q,$conn);

$title="Gadget Theft Tracking System - " . $_SESSION['name'];

include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>

<script type="text/javascript">
$(function() {
	//bind all tracking links
	$('.tracking-link').trackingLink();
});
</script>

<div>
	<div style="float:right;" id="tools">
		<a class="icon-link icon-link-pdf" href="gettingstarted.pdf" target="_blank">Getting Started</a> | <a href="/downloads/OS_X_GT2.zip">Download Mac OS X Beta Agent </a>
	</div>
	<div style="float:left;">
		<?php cookieCrumbNav(array("myDevices", "usbDevices"));?>
	</div>
</div>
<h2 style="clear:both;">GadgetTrak USB Control Panel</h2>

<div style="margin-bottom:10px;">
	<div style="float:right;">
		<?if ($currentCredits > 0 ) {?>
			Unused Device Licenses: <?=$currentCredits?>
		<? }?>
	</div>
	<?if($usedcredits < $credits){?>
	<div style="float:left;">
		<a href="deviceadd.php" class="lbOn icon-link icon-link-add" title="Add Device">Add Device</a>
	</div>
	<?}?>
	<div style="clear:both;"></div>
</div>

<table class="tabledata">
	<thead>
	<tr>
		<th>Device Type</th>
		<th>Manufacturer</th>
		<th>Model</th>
		<th>Agent Files</th>
		<th>Reports</th>
		<th>Tracking</th> 
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
<?php foreach($devices as $device) {?>

	<?
		//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
		$classPName = preg_replace('/:/','_', $device->devicekey);
	?>
	<tr class="dr_<?=$classPName?> <?php if($device->theft_status=="Y"){ print("tracking-active");}?>">
		<td><?=$device->type?></td>
		<td><?=$device->manufacturer?></td>
		<td><?=$device->model?></td>
		<td><a title="Download Agent" href="agentdownload.php?dkey=<?=$device->devicekey?>">Download</a></td>
		<td>
			<? if($device->pro == 'Y' ){?>
				<a href="report.php?did=<?=$device->devicekey?>">View Report</a>
			<?} else {?>
				n/a
			<?}?>
		</td>
		<td><a title="Change Device Theft Status" class="tracking-link" href="status.php?s=<?=$device->theft_status?>&id=<?=$device->devicekey?>"><?=$tracking?></a></td>
		<td>
			<a class="lbOn" title="Edit Device"   href="edit.php?did=<?=$device->devicekey?>">Edit</a>&nbsp;|&nbsp;<a class="lbOn" title="Delete Device" href="delete.php?did=<?=$device->devicekey?>">Delete</a>
		</td>

	</tr>

<? }?>
	</tbody>
</table>

<br /><br />
<p> For each device that is tracked, you need to download the specific agent files for that device.  </p>
<img src="/_gfx/root_usb.png" style="float:right;margin-top:20px;margin-left:20px;" />
<h2>Tips</h2>
<h4>Installation</h4>
<p> To install the agent files, you need to unzip them and then copy the directories into THE ROOT of your USB device. If the
files are put in directory it will not work. Each device has their own custom download. </p>
<h4>Tracking Activation  </h4>
<p>When a device goes missing (or for testing) you will need to manually activate device tracking. This is done by clicking on the &quot;disabled&quot; link in the &quot;Tracking&quot; column above. When the link value says &quot;Active&quot; then the device is being tracked and as soon as the device is able to communicate with the server you will receive an email. </p>
<br clear="both" />
<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>