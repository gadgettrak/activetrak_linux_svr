<?php

require_once("bootstrapper.php");
require_once("classes/TabelessFormRenderer.php");
require_once('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');
require_once('classes/Country.php');
require_once('classes/User.php');
require_once('classes/UserManager.php');
require_once('classes/HttpUtils.php');

function keycheck($key) {
	global $userManager;
	return $userManager->isValidEmailKey($key);
}

function emailexists($element_name,$element_value) {
	global $userManager;
	$asdf = $userManager->isValidEmail($element_value);
	if($asdf) {
		return true;
	} else {
		return false;
	}
}

function processdata1($data) {
	sendemail($data);
}

function sendemail($data) {

	global $userManager;
	$user = $userManager->getUserByEmail($data['email']);

	$message = new Mail_mime();

	ob_start();
	include("templates/email/password-reset-text.php");
	$text = ob_get_contents();
	ob_end_clean();

	ob_start();
	include("templates/email/password-reset-html.php");
	$html = ob_get_contents();
	ob_end_clean();

	$headers['From'] = 'accounts@gadgettrak.com';
	$headers['Subject'] = 'GadgetTrak.com Password Reset';

	$message->setTXTBody($text);
	$message->setHTMLBody($html);

	$body = $message->get();
	$extraheaders = array("To"=>$user->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"GadgetTrak Theft Recovery Info");
	$headers = $message->headers($extraheaders);

	$smtp = Mail::factory('smtp', array("host"=>"intGTmail"));

	$success = $smtp->send($user->email, $headers, $body);

	if($success) {
		print "<p> An email has been sent to you to verify your password change request. Please click on the link in the email, you will then be able to enter a new password for your account. </p>";
	} else {
		print("There was an error trying to send email. Please contact support@gadgettrak.com");
	}
}

$userManager = new UserManager();

$form = new HTML_QuickForm();

$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addElement('submit', null, 'Submit', array('class' => "submit"));
$form->registerRule('emailexists','function','emailexists');
$form->addRule('email','The email you entered does not exist in our database', 'emailexists');
$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');

$renderer =& new HTML_QuickForm_Renderer_Tableless();
$form->accept($renderer);
$form->setRequiredNote('');

include("fragments/header.php");

?>

<h2>Change Password</h2>
<p>Please enter your email address below.</p>
<div id="loginForm" class="user-forms">

<?php
if ($form->validate()) {
	$form->process('processdata1');
} else {
	$form->accept($renderer);
	echo $renderer->toHtml();
}
?>
<p>
	<a href="/user/create/">Create Account</a> | <a href="/">Login</a>
</p>
</div>

<?php include("fragments/footer.php"); ?>