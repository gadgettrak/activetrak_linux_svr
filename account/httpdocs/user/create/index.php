<?php
require_once("bootstrapper.php");
require_once("classes/TabelessFormRenderer.php");
require_once('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');
require_once('classes/Country.php');
require_once('classes/User.php');
require_once('classes/UserManager.php');
require_once('classes/HttpUtils.php');
require_once('classes/Mailer.php');

$form = new HTML_QuickForm();

//$form->addElement('text', 'fname', 'First Name:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'lname', 'Last Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'emailConfirm', 'Confirm Email:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('html', '<tr><td></td><td><p style=\"color:#fff;font-size:10px;\">Hotmail accounts cannot be used. If you use a free email provider such as Yahoo, please check your spam box as an email verification link will be sent. We recommend using a Gmail account.</p></td></tr>');
//$form->addElement('text', 'phone', 'Phone:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'company', 'Company:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'address1', 'Address1:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'address2', 'Address2:', array('size' => 30, 'maxlength' => 255));

//$form->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('select', 'country', 'Country:', Country::$countries);
$form->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$form->addElement('password', 'passwordConfirm', 'Confirm Password:', array('size' => 30, 'maxlength' => 255));

$form->addElement('advcheckbox','terms','', 'I agree to the <a target="_blank" href="http://www.gadgettrak.com/legal/terms.php">terms and conditions</a>',array('class'=>'poop'),'yes');

$form->addElement('submit', null, 'Create Account', array('class' => "submit"));

//validation
$form->addRule('fname', 'Please enter your first name', 'required', null, 'client');
$form->addRule('lname', 'Please enter your last name', 'required', null, 'client');

$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');
$form->addRule(array('email','emailConfirm'), 'Emails do not match', 'compare', null, 'client');

//$form->addRule('phone', 'Please enter your phone number', 'required', null, 'client');
//$form->addRule('address1', 'Please enter your address', 'required', null, 'client');
//$form->addRule('city', 'Please enter your city', 'required', null, 'client');
//$form->addRule('state', 'Please enter your state', 'required', null, 'client');
//$form->addRule('postal', 'Please enter your postal code', 'required', null, 'client');
//$form->addRule('country', '', 'required', null, 'client');
//$form->addRule('country', 'Please select a country', 'minlength', 2, 'client');
$form->addRule('password', 'Please enter a password', 'required', null, 'client');
$form->addRule(array('password','passwordConfirm'), 'Passwords do not match', 'compare', null, 'client');

$form->registerRule('usernameTaken','function','usernameTaken');
$form->addRule('email','The email you have selected already exists', 'usernameTaken');
$form->addRule('terms', 'In order to use this service you must agree the terms and conditions', 'required', null, 'client');

$form->setRequiredNote('');

$renderer =& new HTML_QuickForm_Renderer_Tableless();
$form->accept($renderer);

function usernameTaken($element_name, $element_value) {
	$userManager = new UserManager();
	return $userManager->userNameIsAvailable($element_value);
}

function processdata() {
	//Clean inputs
	HttpUtils::cleanRequestParamsArray($_POST);
	$userManager = new UserManager();
	$phpdate = gmdate("Y-m-d H:i:s");
	$emailKey = sha1($_POST['email'] . rand());
	$user = new User(
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		$_POST['email'],
		null,
		null,
		0,
		$emailKey,
		null,
		$phpdate,
		null,
		sha1($_POST['password']),
		0,
		0
	);
	$user = $userManager->saveNewUser($user);
	Mailer::sendNewUserVerifyNotice($user);
	return $user;
}

include("fragments/header.php");

?>

<h2>GadgetTrak Account Registration</h2>

<div id="registerForm" class="user-forms">

<?php
if ($form->validate()) {
	$user = $form->process('processdata');
?>

<p><?=$user->fname?> <?=$user->lname?>,</p>
<p>Thank you for registering with GadgetTrak. You will receive an email shortly with instructions on how to activate your online account.</p>

<?php
} else {
	$form->accept($renderer);
	echo $renderer->toHtml();
}
?>

<p>Already registered? <a href="/">Sign In</a></p>

</div>

<?php include("fragments/footer.php");?>