<?

include($_SERVER['DOCUMENT_ROOT']. "/database.php");
include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');


$serialnumber=$_GET['serialnumber'];
$countries= countryList();

$form= new HTML_QuickForm('iphonereg');
$form->addElement('text', 'serial', 'Device Serial:', array('size' => 30, 'maxlength' => 200, 'value' => $serialnumber));
//$form->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 200));
//$form->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 200));
$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 200));
$form->addElement('html', '<tr><td></td><td>Please use an email that is not sent to your iPhone, we reccomend Gmail or other email provider.</td></tr>');

$form->addElement('select', 'country', 'Country:', $countries);
$form->addElement('password', 'password', 'Create Account Password:', array('size' => 30, 'maxlength' => 200));

$form->addElement('html','<tr><td style="margin:0;padding;0"></td><td style="margin:0;padding;0"><a target="_blank" href="http://www.gadgettheft.com/legal/terms.php"/>Terms &amp; Conditions</a></td></tr>');
$form->addElement('advcheckbox','terms','I Agree', '',null,'yes');



$form->addElement('submit', null, 'Submit', array('class' => "submit"));

$form->addElement('html', '<tr><td></td><td><a target=\"_blank\" href="http://www.gadgettheft.com/legal/privacy.php"> Privacy Policy</a></td></tr>');
//$form->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
//$form->addRule('lastname', 'Please enter your last name', 'required', null, 'client');

$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');

$form->addRule('country', '', 'required', null, 'client');
$form->addRule('country', 'Please enter select a country', 'minlength', 2, 'client');
$form->addRule('password', 'Please choose a password', 'required', null, 'client');

$form->registerRule('usernameTaken','function','usernameTaken');
$form->registerRule('serialTaken','function','serialTaken');
$form->addRule('serial', '', 'required', null, 'client');
$form->addRule('serial','The serial number you have entered is not valid or already in use', 'serialTaken'); 
$form->addRule('email','The email you have selected already exists', 'usernameTaken'); 
$form->addRule('terms', 'In order to use this service you must agree the terms and conditions', 'required', null, 'client');



function emailver($key, $email){

$to=$email;
$bdy=<<<PTEXT
Thank you for registering your iPhone/iPod Touch with GadgetTrak, your device is now registered with us.  The next step to enable your account is to verify your email address, all you need to do is click on the link below:
https://account.gadgettheft.com/verify.php?key={$key}

PTEXT;

$headers['From'] = 'info@gadgettrak.com';
//$headers['Bcc']="kwestin@gadgettrak.com";
$headers['Subject'] = 'GadgetTrak Email Verification';
$message = &Mail::factory('mail');

$sentmessage=$message->send($to, $headers, $bdy);


}

function usernameTaken($element_name,$element_value) {
global $conn;
global $test;
if(!get_magic_quotes_gpc()){
      $usermail = addslashes($element_value);
   }
   $q = "SELECT email FROM users WHERE email = '$usermail'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) > 0){
   return false;
   }else{
   return true;
   }
   
 
}

function serialTaken($element_name,$element_value) {
global $conn;
global $test;
if(!get_magic_quotes_gpc()){
      $serial = addslashes($element_value);
   }
   $q = "SELECT serial FROM devices WHERE serial  = '$serial'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) > 0){
   return false;
   }else{
   return true;
   }
   
 
}

function processdata($data){
global $conn;

$password=sha1($data['password']); //encrypt password
$emailkey=sha1($data['email']); //create encyrpted mail key

//insert user data
$accountq = "INSERT INTO users (
   				fname, 
  				lname, 
				country,
   				email,
				ipaddress,
   				email_key, 
  			 	password) 
   
   VALUES('"
   			.addslashes($data['firstname'])."','"
   			.addslashes($data['lastname'])."','"
			.addslashes($data['country'])."','"
   			.addslashes($data['email'])."','"
			.$_SERVER['REMOTE_ADDR']."','"
			.$emailkey."','"
			.$password."')";
  
  

   
mysql_query($accountq,$conn) or die(mysql_error()); 

emailver($emailkey, $data['email']);


//insert license data


$accountid = mysql_insert_id();//get userid for last insert

$licenseq = "INSERT INTO licenses (
   				userid, 
  				number_licenses, 
				productid,
				country) VALUES('"
   			.addslashes($accountid)."','"
   			.'1'."','"
			.'8'."','"
			
			.addslashes($data['country'])."')";

mysql_query($licenseq,$conn) or die(mysql_error()); 

$devicekey=sha1(uniqid($userid,true)); // create a device key for insertion in both device and devicemap table

$deviceq = "INSERT INTO devices (
   				type, 	
				model,
				manufacturer,
				serial,
				devicekey,	
				productid) VALUES('"
			."iPhone - iPod Touch"."','"
			."iPhone - iPod Touch"."','"
			."Apple"."','"
			.addslashes($data['serial'])."','"
			.$devicekey."','"
			.'8'."')";

mysql_query($deviceq, $conn) or die(mysql_error()); 

$deviceid = mysql_insert_id();



$devicemapq = "INSERT INTO devicemap (
   				userid, 
				devicekey) VALUES('"
			.$accountid."','"
			.$devicekey."')";

mysql_query($devicemapq, $conn) or die(mysql_error()); 	


session_start(); 
$_SESSION['userid']=$accountid;
$_SESSION['loggedin'] = true;
	
}



if ($form->validate()) {// if the form validates?

$form->process('processdata');

?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>GadgetTrak iPhone/iPod Touch Registration</title>
    <link media="screen" href="/_style/iphone.css" type= "text/css" rel="stylesheet">
    <meta name = "viewport" content = "user-scalable=no, width=device-width">
    <script type="text/javascript">
    addEventListener('load', function() { setTimeout(hideAddressBar, 0); }, false);
    function hideAddressBar() { window.scrollTo(0, 1); }
    </script>
    </head>
    <body>
    <div id="header"><img src="/_gfx/iphone_logo.gif" /></div>
    <div id="content">
    <h1> iPhone/iPod Touch Registration </h1>
    <p>Thank you, your device has been registered in our system, you will receive an email from us to verify your email address.
    To test the software go ahead and go to the <a href="/iphone/"> control panel page and activate tracking</a>. 
    </body>
    </div>
    </html>

<?}else{ ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>GadgetTrak iPhone/iPod Touch Registration</title>
    <link media="screen" href="/_style/iphone.css" type= "text/css" rel="stylesheet">
    <meta name = "viewport" content = "user-scalable=no, width=device-width">
    <script type="text/javascript">
    addEventListener('load', function() { setTimeout(hideAddressBar, 0); }, false);
    function hideAddressBar() { window.scrollTo(0, 1); }
    </script>
    </head>
    <body>
 <div id="header"><img src="/_gfx/iphone_logo.gif" /></div>
    <div id="content">
    <h1> iPhone/iPod Touch Registration</h1>
    <?$form->display();?>
    </body>
    </div>
    </html>

<?}?>
