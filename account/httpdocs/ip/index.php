<?php
//// IP validation functions
function iplongtostring($ip) {
	$ip=floatval($ip); // otherwise it is capped at 127.255.255.255
	$a=($ip>>24)&255;
	$b=($ip>>16)&255;
	$c=($ip>>8)&255;
	$d=$ip&255;
	return "$a.$b.$c.$d";
}

function dot2dec($ip) {
	$ip=explode('.',$ip);
	# 16777216 = 256**3 and
	# 65536 = 256**2 precalculated to save time.
	return $ip[0]*16777216 + $ip[1]*65536 + $ip[2]*256 + $ip[3];
}

// ip address is converted to decimal notation and back
// if the two results does not match, the ip adress contained something illegal (even catches letters in the adress)
function isIpValid($ip){
	return (iplongtostring(dot2dec($ip))==$ip);
}


function getLocateAdvanced($ip){
	$license_key="9ksgnctrfzP4";

	$url = "http://maxmind.com:8010/f?l=" . $license_key . "&i=" . $ip;
	$ch = curl_init();
	$timeout = 7; // set to zero for no timeout
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$file_contents = curl_exec($ch);
	curl_close($ch);

	//echo $file_contents;

	$geo = explode(",",$file_contents);
	$locationArray['country'] = $geo[0];
	$locationArray['state'] = $geo[1];
	$locationArray['city'] = $geo[2];
	$locationArray['postal'] = $geo[3];
	$locationArray['lat'] = $geo[4];
	$locationArray['lon'] = $geo[5];
	$locationArray['metro'] = $geo[6];
	$locationArray['area'] = $geo[7];
	$locationArray['isp'] = $geo[8] . " ".$geo[9];
	$locationArray['agent']=$_SERVER['HTTP_USER_AGENT'];
	return $locationArray;
}

$iplookup = $_GET['ip'];

if(!isipValid($iplookup)) {die("not a valid ip");}

$locationArray=getLocateAdvanced($iplookup);
$command = "whois ". $iplookup;
exec($command , $whoisdata);
unset($whoisdata[0]);
unset($whoisdata[1]);

?>

<table class="deviceDetails ipDetails">
	<tbody>
		<tr>
			<th>Country</th>
			<td><?=$locationArray['country']?></td>
		</tr>

		<tr>
			<th>State</th>
			<td><?=$locationArray['state']?></td>
		</tr>

		<tr>
			<th>City</th>
			<td><?=$locationArray['city']?></td>
		</tr>

		<tr>
			<th>Postal</th>
			<td><?=$locationArray['postal']?></td>
		</tr>

		<tr>
			<th>Metro</th>
			<td><?=$locationArray['metro']?></td>
		</tr>

		<tr>
			<th>Area</th>
			<td><?=$locationArray['area']?></td>
		</tr>

		<tr>
			<th>Organization</th>
			<td><?=$locationArray['isp']?></td>
		</tr>

		<tr>
			<th>Organization Information</th>
			<td>
				<pre>
<?php foreach ($whoisdata as $value) {?>
<?= $value ?>

<? } ?>
				</pre>
			</td>
		</tr>

	</tbody>
</table>