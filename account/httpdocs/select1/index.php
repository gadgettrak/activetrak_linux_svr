<?php
	include("fragments/header.php");
?>

<style type="text/css">

.selector ul {
	list-style:none;
}

.selector li {
	float:left;
	margin:0;
	padding:0;
}

li.laptop h3 {
	background:transparent url('/_gfx/devices.png') no-repeat 0 0;
}

li.mobile h3 {
	background:transparent url('/_gfx/devices.png') no-repeat -460px 0;
}

li.laptop:hover h3 {
	background:transparent url('/_gfx/devices.png') no-repeat 0 -197px;
}

li.mobile:hover h3 {
	background:transparent url('/_gfx/devices.png') no-repeat -460px -197px;
}

.selector h3 {
	border:0;
	width:420px;
}

.selector h3 a  {
	display:inline-block;
	width:420px;
	height:197px;
	text-decoration: none;
}

.selector li p{
	text-align:center;
}

.selector h2 {
	text-align:center;
	font-size:24px;
	border:0;
}

.selector a.login-btn {
	display:inline-block;
	line-height:35px;
	background:transparent url('/_gfx/btn-login.png') no-repeat 0 0;
	color:#fff;
	text-decoration:none;
	font-weight:bold;
	font-size:14px;
	width:177px;
}

.selector li:hover a.login-btn {
	background:transparent url('/_gfx/btn-login.png') no-repeat 0 -35px;
	color:#ccc;
}
</style>

<div class="selector">
	<h2>Select your platform</h2>
	<ul>
		<li class="laptop">
			<h3>
				<a href="/login/">&nbsp;</a>
			</h3>
			<p>
				<a href="/login/" class="login-btn">Log in here</a>
			</p>
		</li>
		<li class="mobile">
			<h3>
				<a href="https://cp.activetrak.com">&nbsp;</a>
			</h3>
			<p>
				<a href="https://cp.activetrak.com" class="login-btn">Log in here</a>
			</p>
		</li>
	</ul>
	<br class="clear"/>
</div>

<?php
include("fragments/footer.php");
?>