<?php

require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/devices.php");

require_once("$_SERVER[DOCUMENT_ROOT]/classes/Device.php");

authenticateSession();

$devices  = Device::getLaptopDevicesByUserId($_SESSION['user']->id);

$title="Gadget Theft Tracking System - " . $_SESSION['name'];

include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>

<script type="text/javascript">
$(function() {
	//bind all tracking links
	$('.tracking-link').trackingLink();
});
</script>

<div>
	<div style="float:right;" id="tools">
		Download software: <a href="http://www.gadgettrak.com/downloads?v=current&p=mac">Mac</a> / <a href="http://www.gadgettrak.com/downloads?v=current&p=win">PC</a>
	</div>
	<div style="float:left;">
		<?php cookieCrumbNav(array("myDevices", "laptopDevices"));?>
	</div>
</div>
<h2 style="clear:both;">GadgetTrak Laptop Control Panel</h2>

<table class="tabledata">
	<thead>
	<tr>
		<th>OS</th>
		<th>MAC address</th>
		<th>Manufacturer</th>
		<th>Model</th>
		<th>License Key</th>
		<th>Time until expiration</th>
		<th>Tracking</th> 
		<th>Actions</th>
	</tr>
	</thead>
<tbody>
	
<?php foreach($devices as $device) {?>
	<?
		//Since some items use a MAC address as there key, we need to strip bad chars out for the class name
		$classPName = preg_replace('/:/','_', $device->devicekey);
		
		$activatedDate = strtotime($device->activated);
		
		$expireDate = getdate(strtotime("+" . $device->length . " months", $activatedDate));
		
		$nowDate = getdate(strtotime("now"));
		$begin = array ('year' => $nowDate['year'], 'month' => $nowDate['mon'], 'day' => $nowDate['mday']);
		$end = array ('year' => $expireDate['year'], 'month' => $expireDate['mon'], 'day' => $expireDate['mday']);
		$timeRemaining = date_difference ($begin, $end);
		
	?>
	<tr class="dr_<?=$classPName?> <?php if($device->theft_status=="Y"){ print("tracking-active");}?>">
		<td><?=$device->os?></td>
		<td><?=$device->macAddress?></td>
		<td><?=$device->manufacturer?></td>
		<td><?=$device->model?></td>
		<td><?=$device->key?></td>
		<td>
		
<? if($timeRemaining['years']){print("$timeRemaining[years] years");}?> <? if($timeRemaining['months']){print("$timeRemaining[months] months");}?> <? if($timeRemaining['days']){print("$timeRemaining[days] days");}?>

<?php
if(!$timeRemaining || (
	$timeRemaining['years'] == 0 &&
	$timeRemaining['months'] == 0 &&
	$timeRemaining['days'] == 0)) {
	print("<strong>This license is expired</strong>");
}

?>	
		</td>
		<td><a title="Change Device Theft Status" class="tracking-link" href="status.php?s=<?=$device->theft_status?>&id=<?=$device->devicekey?>"><?=$tracking?></a></td>
		<td>
			<a class="lbOn" title="Edit Device"   href="edit.php?did=<?=$device->devicekey?>">Edit</a>
		</td>
	</tr>
<? }?>
	</tbody>
</table>

<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>