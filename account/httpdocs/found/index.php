<?

require_once("bootstrapper.php");
require_once("classes/BaseDAO.php");
require_once('classes/Country.php');

require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');

$form = new HTML_QuickForm('report');

$bodyonload="javascript:document.forms[0][0].focus();";

$invalidwarn="<p style=\"text-align:left;color:#c00; \">The code you have entered appears to be invalid. Please make sure that you have typed the code in correctly and submit again. If you are sure the label is correct, please contact <a href=\"mailto:support@gadgettrak.com\"> support@gadgettrak.com </a> for assistance</p>";



if($_GET['gtcode']) {

	$gtcode=addslashes($_GET['gtcode']);
	$labelQuery = "SELECT
		devices.deviceid,
		devices.type,
		devices.model,
		devices.manufacturer,
		devices.devicekey,
		devices.macAddress,
		devices.labelcode,
		devices.datecreated,
		devices.pro,
		devices.productid,
		devices.saveTracking,
		devices.version,
		users.email,
		devices.description,
		devices.color,
		devices.serial,
		devices.theft_status,
		devices.os
	FROM devices
	INNER JOIN devicemap ON devices.devicekey = devicemap.devicekey
	INNER JOIN users ON users.userid = devicemap.userid
	WHERE devices.labelcode='$gtcode'";

	$dao = new BaseDAO();
	
	$deviceResults = $dao->queryUniqueObject($labelQuery);
	if ($deviceResults) {
		$validlbl = true;
	} else {
		$validlbl = false;
	}
}

function processdata($data) {

	$gtcode = addslashes($data['device']);
	$dao = new BaseDAO();

	$labelQuery = "SELECT
		devices.deviceid,
		devices.type,
		devices.model,
		devices.manufacturer,
		devices.devicekey,
		devices.macAddress,
		devices.labelcode,
		devices.datecreated,
		devices.pro,
		devices.productid,
		devices.saveTracking,
		devices.version,
		users.email,
		devices.description,
		devices.color,
		devices.serial,
		devices.theft_status,
		devices.os
	FROM devices
	INNER JOIN devicemap ON devices.devicekey = devicemap.devicekey
	INNER JOIN users ON users.userid = devicemap.userid
	WHERE devices.labelcode='$gtcode'";

	$deviceResults = $dao->queryUniqueObject($labelQuery);

	$emailmsg="Someone has found your device:\n";
	$emailmsg.="Manufacturer: " . $deviceResults->manufacturer . "\n";
	$emailmsg.="Type: " . $deviceResults->type . "\n";
	$emailmsg.="Model: " . $deviceResults->model . "\n";
	$emailmsg.="Label Code: " . $deviceResults->labelcode . "\n";

	$emailmsg.="\n\n=========================\n";
	$emailmsg.="Information Submitted \n=========================\n\n";
	$emailmsg.="Name: " . $data['firstname'] . " " . $data['lastname'] . "\n";
	$emailmsg.="Email: " . $data['email'] . "\n " ;
	$emailmsg.="Phone: " . $data['phone'] . "\n ";
	$emailmsg.="Address: " .$data['address'] . "\n ";
	$emailmsg.="City: " . $data['city'] . "\n ";
	$emailmsg.="State/Region: " . $data['state'] . "\n ";
	$emailmsg.="Country: " . $data['country'] . "\n ";
	$emailmsg.="Comment: " . $data['comments'] . "\n ";
	$emailmsg.="IP Address: " . $_SERVER['REMOTE_ADDR'];

	$message = new Mail_mime();
	$message->setTXTBody($emailmsg);

	$headers['From'] = 'accounts@gadgettrak.com';
	$headers['Subject'] = "GadgetTrak Contact - Device Label: ". $deviceResults->labelcode;

	$body = $message->get();
	$extraheaders = array("To"=>$deviceResults->email, "From"=>"accounts@gadgettrak.com", "Subject"=>"GadgetTrak Contact - Device Label: ". $deviceResults->labelcode);
	$headers = $message->headers($extraheaders);

	$smtp = Mail::factory('smtp', array("host"=>"intGTmail"));
	$smtp->send($deviceResults->email, $headers, $body);
}

if ($validlbl) {

	$form->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('text', 'phone', 'Phone:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));
	$form->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255));
	$form->addElement('select', 'country', 'Country:', Country::$countries);
	$form->addElement ('hidden', 'device', $gtcode);//get code
	$form->addElement('textarea','comments','Comments:','wrap="soft"');
	$form->addElement('submit', null, 'Submit', array('class' => "submit"));

	$message ="<h2>Device Found!</h2>";
	$message .= "<p>The <strong>" . $devicedata->manufacturer . " " .  $devicedata->model ." "  .$devicedata->type;
	$message .= "</strong> has been found in our system. ";

	$message .= "The form below will allow you to communicate with the owner of the device. </p> <p> All of the fields listed here are  <strong>optional</strong>, except the comments field. If you wish to provide the owner information about this device anonymously you may do so. </p><br />";
}

$title="Found Device";

include("fragments/header.php");

?>

<style type="text/css">

#gtlabel {
	position:absolute;
	bottom:12px;
	text-align:center;
	width:408px;
}

#gtlabel input {
	font-size:28px;
	font-weight:bold;
	vertical-align: middle;
	font-family: Helvetica, Arial, Verdana, sans-serif;
}

#gtlbl_code {
	width:170px;
}

#lblcontainer {
	background-image: url(/_gfx/bg-label.png);
	background-repeat: no-repeat;
	margin:20px auto;
	width:408px;
	height:208px;
	text-align:center;
	position:relative;
}

</style>

<!-- default -->
<?if( $validlbl == false && !($form->isSubmitted())){?>
<h2>Found a gadget with a GadgetTrak label? Want to return it to its owner?</h2>
<p>Please enter in the code that appears on the label in the form below.</p>

<?php
	if($validlbl == false && isset($_GET['gtcode'])) {
		print $invalidwarn;
	}
?>

<div id="lblcontainer">
	<form name="labelcode" action="<?=$_SERVER["REQUEST_URI"]?>" method="get">
	<div id="gtlabel">
		<input name="gtcode" type="text" size="7" maxlength="7" value="<?=($gtcode)?$gtcode:"GT"?>" id="gtlbl_code" />
		<input name="Submit" class="submit" type="submit" value="Submit" />
	</div>
	</form>
</div>


<?}else{?>

<div style="width:500px;margin:0 auto;">

<?
	if ($form->validate() && $form->issubmitted()) {// if the form validates?
	$form->process('processdata');
	print "<h2>Thank you!</h2><p> The information has been sent to the device owner. If you have questions or issues regarding returning the device, please feel free to contact us at <a href=\"support@gadgettrak.com \">support@gadgettrak.com</a></p>";
	} else {
	print $message;
	$form->display();
	}
?>
</div>
<?}?>

<?
include("fragments/footer.php");
?>
