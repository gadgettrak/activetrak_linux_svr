<?
ini_set( 
  'include_path', 
  ini_get( 'include_path' ) . PATH_SEPARATOR . "/home/kwestin/pear/php"
); 


include($_SERVER['DOCUMENT_ROOT']. "/database.php");
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');
$form = new HTML_QuickForm('report');


$bodyonload="javascript:document.forms[0][0].focus();";

$invalidwarn="<p style=\"text-align:left;color:#c00; \">The code you have entered does appears to be invalid. Please make sure that you have typed the code in correctly and submit again. If you are sure the label is correct, please contact <a href=\"mailto:support@gadgettrak.com\"> support@gadgettrak.com </a> for assistance</p>";

if($_GET['gtcode']){
// when the code is submitted check to see if it exists if not return error 
$gtcode=addslashes($_GET['gtcode']);


$q="SELECT 
users.email, devices.devicekey, devices.*
FROM users, devices, devicemap
WHERE users.userid = devicemap.userid
AND devicemap.devicekey = devices.devicekey
AND devices.labelcode='$gtcode'";

$deviceresult = mysql_query($q,$conn) or die ("mysql error");


$numrows=mysql_num_rows($deviceresult);
$devicedata = mysql_fetch_array($deviceresult);

	if ($numrows > 0){
	$validlbl = true;
	}else{
	$validlbl = false;	
	}

}

function processdata($data){
global $conn;

$ddata_sql="SELECT * FROM  users, devices, devicemap WHERE
devices.labelcode='" .$data['device'] ."' and devices.devicekey=devicemap.devicekey AND devicemap.userid=users.userid";
$devicedata_result = mysql_query($ddata_sql,$conn) or die ("mysql error");
$devicedata_array =mysql_fetch_array($devicedata_result);

$emailmsg="Someone has found your device:\n";
$emailmsg.="Manufacturer: " .$devicedata_array['manufacturer'] . "\n";
$emailmsg.="Type: " .$devicedata_array['type'] . "\n";
$emailmsg.="Model: " .$devicedata_array['model'] . "\n";
$emailmsg.="Label Code: " .$devicedata_array['labelcode'] . "\n";

$emailmsg.="\n\n=========================\n";
$emailmsg.="Information Submitted \n=========================\n\n";
$emailmsg.="Name: ". $data['firstname'] . " " . $data['lastname'] . "\n";
$emailmsg.="Email: " . $data['email'] . "\n " ;
$emailmsg.="Phone: " . $data['phone'] . "\n ";
$emailmsg.="Address: " .$data['address'] . "\n ";
$emailmsg.="City: ". $data['city'] . "\n ";
$emailmsg.="State/Region: " . $data['state'] . "\n ";
$emailmsg.="Country: " . $data['country'] . "\n ";
$emailmsg.="Comment: ". $data['comments'] . "\n ";
$emailmsg.="IP Address: " . $_SERVER['REMOTE_ADDR'];

$subject="GadgetTrak Contact - Device Label: ". $devicedata_array['labelcode'];
$from = "info@gadgettrak.com";
$headers = "From: $from\r\nReply-to: $from\r\n";
$headers .="Bcc:kwestin@gmail.com \r\n";

mail($devicedata_array['email'], $subject, $emailmsg, $headers);



}


if ($validlbl){


include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");

$countries= countryList();

$form->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'phone', 'Phone:', array('size' => 30, 'maxlength' => 255));
$form->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));
$form->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255));
$form->addElement('select', 'country', 'Country:', $countries);
$form->addElement ('hidden', 'device', $gtcode);//get code

$form->addElement('textarea','comments','Comments:','wrap="soft"');
$form->addElement('submit', null, 'Submit', array('class' => "submit"));
//$form->addRule('comments', 'Please enter a comment', 'required', null, 'client');

$message ="<h2>Device Found!</h2>";
$message .= "<p>The <strong>" . $devicedata['manufacturer'] . " " .  $devicedata['model'] ." "  .$devicedata['type'];
$message .= "</strong> has been found in our system. ";

$message .= "The form below will allow you to communicate with the owner of the device. </p> <p> All of the fields listed here are  <strong>optional</strong>, except the comments field. If you wish to provide the owner information about this device anonymously you may do so. </p><br />";
}
$title="Found Device";
include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>
<style type="text/css">
#gtlabel{
	text-align:center;
	background-image: url(/_gfx/gadgettraklbl.png);
	background-repeat: no-repeat;
	width:200px;height:100px;margin:0 auto;
}
#gtlbl_code{position:relative;top:65px;font-weight:bold;}
#lblcontainer{margin:0 auto;width:200px;text-align:center;}
</style>

<!-- default -->
<?if( $validlbl == false && !($form->isSubmitted())){?>
<h2> Found a gadget? Want to return it to its owner?  </h2>
<p> If you have found a device with the GadgetTrak label? Please enter in the code that appears on the label in the form below. </p>

<? 
	if($validlbl == false && isset($_GET['gtcode'])){
	print $invalidwarn;
	}
?>
<div id="lblcontainer">
<form name="labelcode" action="<?=$_SERVER["REQUEST_URI"]?>" method="get">
<div id="gtlabel">
<input name="gtcode" type="text" size="7" maxlength="7" value="GT" id="gtlbl_code" />
</div><br />

<input name="Submit" class="submit" type="submit" value="Submit" />
</form>
</div>
<br /><br /><br /><br /><br /><br /><br /><br />
<!-- end default -->
<?}else{?>

<div style="width:500px;margin:0 auto;">

<?

	if ($form->validate() && $form->issubmitted()) {// if the form validates?
	$form->process('processdata');
	print "<h2>Thank you </h2><p> The information has been sent to the device owner. If you have questions or issues regarding returning the device, please feel free to contact us at <a href=\"support@gadgettrak.com \">support@gadgettrak.com</a></p>";
	}else{
	print $message;
	$form->display();
	}
?>
</div>
<?}?>



<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>
