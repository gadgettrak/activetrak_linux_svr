<?php include("fragments/header.php"); ?>

<h1>Migration and maintenance in progress.</h1>

<p>Our systems are currently unavailable as we migrate and update our servers. We should be back up shortly with an all new control panel!</p>

<!-- <?=$_SERVER['REMOTE_ADDR']?> -->

<?php include("fragments/footer.php"); ?>
