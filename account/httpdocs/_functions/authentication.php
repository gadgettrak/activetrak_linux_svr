<?php

require_once("$_SERVER[DOCUMENT_ROOT]/classes/User.php");
require_once("$_SERVER[DOCUMENT_ROOT]/database.php");

session_start();

function getUser($email, $password) {

	global $conn;

	if(!get_magic_quotes_gpc()){
		$email = addslashes($email);
		$password = sha1(addslashes($password));
	} else {
		$password = sha1($password);
	}
	
	$q = "SELECT * FROM users WHERE email = '$email' and password='$password'";
	$result = mysql_query($q, $conn);

	if (mysql_num_rows($result) > 0) {
		$userData = mysql_fetch_object($result);
		return new User($userData->userid, $userData->email, $userData->fname, $userData->lname);
	} else {
		return false;
	}
}

function loginUser($email, $password, $redirectUrl) {
	$user = getUser($email, $password);
	if(!$user) {
		header("Location: login.php?error=UserNotFound");
	} else {
		$_SESSION['user'] = $user;
		$_SESSION['loggedin'] = true;
		
		//To prevent older pages from breaking, until they get updated.
		$_SESSION['userid'] = $user->id;
		
		header("Location: $redirectUrl");
	}
}

function authenticateSession($redirect=true) {
	if (!$_SESSION['loggedin']) {
		if($redirect) {
			header("Location: login.php");
		} else {
			header("HTTP/1.0 403 Forbidden");
			die();
		}
	}
}

?>