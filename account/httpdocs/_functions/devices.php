<?php

function createTrackingLinks($device) {

	if(
		$device->productid == 1 ||
		$device->productid == 4 ||
		$device->productid == 7 ||
		$device->productid == 8 ||
		$device->productid == 9 ||
		$device->productid == 12 ||
		$device->productid == 13 ||
		$device->productid == 14 ||
		$device->productid == 15
	) {
		print(createTrackingLink($device));
	}
		
}

function createActionLinks($device) {

/*
|         1 | GadgetTrak USB                            | 
|         2 | PhoneBAK                                  | 
|         3 | GadgetTrak for Windows Mobile             | 
|         4 | GadgetTrak MacTrak                        | 
|         5 | GadgetTrak Tags                           | 
|         6 | GadgetTrak for Blackberry                 | 
|         7 | GadgetTrak for Windows (12 Months)        | 
|         8 | iPhone - iPod Touch                       | 
|         9 | GadgetTrak Search and Destroy (12 Months) | 
|        10 | GadgetTrak for Windows (36 Months)        | 
|        11 | GadgetTrak Search and Destroy (36 Months) | 
|        12 | GadgetTrak - Windows (1-year license)     | 
|        13 | GadgetTrak - Windows (3-year license)     | 
|        14 | GadgetTrak - Mac (1-year license)         | 
|        15 | GadgetTrak - Mac (3-year license)         |
*/

	switch ($device->productid) {
	
		case 0:
			print("<a class='lbOn' title='Edit Label' href='label-edit.php?src=dash&did=$device->devicekey'>Edit</a>");
			break;
		case 1:
			//print(createTrackingLink($device) . "");
			print("<a class='lbOn' title='Edit Device' href='edit.php?src=dash&did=" . $device->devicekey . "'>Edit</a>");
			break;
		case 2:
			print("<a href='http://www.gadgettrak.com/downloads/PhoneBAK_Mobilephone.zip'>Download</a> | <a href='labels.php'>Add/Edit Labels</a>");
			break;
		case 3:
			print("<a href='http://64.22.138.131/GadgetTrak.cab'>Download</a> | <a href='labels.php'>Add/Edit Labels</a>");
			break;
		case 4:
			print("<a href='http://www.gadgettrak.com/downloads/MacTrak.dmg'>Download</a>");
			break;
		case 5:
			print("<a href='labels.php'>Add/Edit Labels</a>");
			break;
		case 6:
			print("<a href='http://www.gadgettrak.com/downloads/GadgetTrakBB.zip'>Download</a> | <a href='labels.php'>Add/Edit Labels</a>");
			break;
		case 7:
			print("<a href='http://www.gadgettrak.com/downloads/GadgetTrak_Windows.zip'>Download</a> | <a href='labels.php'>Add/Edit Labels</a>");
			break;
		case 8:
			print("<a class='lbOn' title='Edit Device' href='edit.php?src=dash&did=" . $device->devicekey . "'>Edit</a>");
			break;
		case 9:
			print("<a href='http://www.gadgettrak.com/downloads/GadgetTrak_Windows.zip'>Download</a> | <a href='labels.php'>Add/Edit Labels</a>");
			break;
		case 10:
			print("<a href='http://www.gadgettrak.com/downloads/GadgetTrak_Windows.zip'>Download</a> | <a href='/destroy'>Control Panel</a>");
			break;
		case 11:
			print("<a href='http://www.gadgettrak.com/downloads/GadgetTrak_Windows.zip'>Download</a> | <a href='/destroy'>Control Panel</a>");
			break;
		case 12:
			print("<a class='lbOn' title='Edit Device' href='edit.php?src=dash&did=" . $device->devicekey . "'>Edit</a>");
			break;
		case 13:
			print("<a class='lbOn' title='Edit Device' href='edit.php?src=dash&did=" . $device->devicekey . "'>Edit</a>");
			break;
		case 14:
			print("<a class='lbOn' title='Edit Device' href='edit.php?src=dash&did=" . $device->devicekey . "'>Edit</a>");
			break;
		case 15:
			print("<a class='lbOn' title='Edit Device' href='edit.php?src=dash&did=" . $device->devicekey . "'>Edit</a>");
			break;	
	}

}

function createTrackingLink($device) {
	$tracking = ($device->theft_status == 'Y')?"Active":"Disabled";
	$checked = ($device->theft_status == 'Y')?"checked='checked'":"";
	print("<a title='Change Device Theft Status' class='tracking-link' href='/status.php?s=$device->theft_status&id=$device->devicekey'>$tracking</a>");
}

function smoothdate ($year, $month, $day)
{
    return sprintf ('%04d', $year) . sprintf ('%02d', $month) . sprintf ('%02d', $day);
}


/*
    function date_difference calculates the difference between two dates in
    years, months, and days.  There is a ColdFusion funtion called, I
    believe, date_diff() which performs a similar function.
    
    It does not make use of 32-bit unix timestamps, so it will work for dates
    outside the range 1970-01-01 through 2038-01-19.  This function works by
    taking the earlier date finding the maximum number of times it can
    increment the years, months, and days (in that order) before reaching
    the second date.  The function does take yeap years into account, but does
    not take into account the 10 days removed from the calendar (specifically
    October 5 through October 14, 1582) by Pope Gregory to fix calendar drift.
    
    As input, it requires two associative arrays of the form:
    array (    'year' => year_value,
            'month' => month_value.
            'day' => day_value)
    
    The first input array is the earlier date, the second the later date.  It
    will check to see that the two dates are well-formed, and that the first
    date is earlier than the second.
    
    If the function can successfully calculate the difference, it will return
    an array of the form:
    array (    'years' => number_of_years_different,
            'months' => number_of_months_different,
            'days' => number_of_days_different)
            
    If the function cannot calculate the difference, it will return FALSE.
    
*/

function date_difference ($first, $second)
{
    $month_lengths = array (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    $retval = FALSE;

    if (    checkdate($first['month'], $first['day'], $first['year']) &&
            checkdate($second['month'], $second['day'], $second['year'])
        )
    {
        $start = smoothdate ($first['year'], $first['month'], $first['day']);
        $target = smoothdate ($second['year'], $second['month'], $second['day']);
                            
        if ($start <= $target)
        {
            $add_year = 0;
            while (smoothdate ($first['year']+ 1, $first['month'], $first['day']) <= $target)
            {
                $add_year++;
                $first['year']++;
            }
                                                                                                            
            $add_month = 0;
            while (smoothdate ($first['year'], $first['month'] + 1, $first['day']) <= $target)
            {
                $add_month++;
                $first['month']++;
                
                if ($first['month'] > 12)
                {
                    $first['year']++;
                    $first['month'] = 1;
                }
            }
                                                                                                                                                                            
            $add_day = 0;
            while (smoothdate ($first['year'], $first['month'], $first['day'] + 1) <= $target)
            {
                if (($first['year'] % 100 == 0) && ($first['year'] % 400 == 0))
                {
                    $month_lengths[1] = 29;
                }
                else
                {
                    if ($first['year'] % 4 == 0)
                    {
                        $month_lengths[1] = 29;
                    }
                }
                
                $add_day++;
                $first['day']++;
                if ($first['day'] > $month_lengths[$first['month'] - 1])
                {
                    $first['month']++;
                    $first['day'] = 1;
                    
                    if ($first['month'] > 12)
                    {
                        $first['month'] = 1;
                    }
                }
                
            }
                                                                                                                                                                                                                                                        
            $retval = array ('years' => $add_year, 'months' => $add_month, 'days' => $add_day);
        }
    }
                                                                                                                                                                                                                                                                                
    return $retval;
}




?>