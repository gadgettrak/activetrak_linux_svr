<?php

$navData = array(
	"myDevices" => array(
		"label" => "Dashboard",
		"url"   => "/index.php"
	),
	
	"usbDevices" => array(
		"label" => "USB Control Panel",
		"url"   => "/usb.php"
	),
	
	"usbReports" => array(
		"label" => "USB Reports",
		"url"   => "/report.php"
	),
	
	"laptopDevices" => array(
		"label" => "Laptop Control Panel",
		"url"   => "/laptop.php"
	),
	
	"iPhoneDevices" => array(
		"label" => "iPhone Control Panel",
		"url"   => "/iphone2/"
	),
	
	"iPhoneReports" => array(
		"label" => "iPhone Reports",
		"url"   => "/iphone2/report/"
	),
	
	"labels" => array(
		"label" => "Lost &amp; Found Tags",
		"url"   => "/labels.php"
	)

);

function cookieCrumbNav($sections, $extra="") {
	global $navData;
	$index = 1;
	print("<div id='cookieCrumbs'>");
	foreach($sections as $section) {
		if($index != count($sections) || ($index == count($sections) && $extra != "")) {
			print("<a href='" . $navData[$section][url] . "'>" . $navData[$section][label] . "</a> &raquo; ");
		} else {
			print($navData[$section][label]);
		}
		$index++;
	}
	print($extra);
	print("</div>");
}

?>