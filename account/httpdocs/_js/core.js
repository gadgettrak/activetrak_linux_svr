;(function($) {

	// create custom tooltip effect for jQuery Tooltip
	$.tools.tooltip.addEffect("bouncy",
		function(done) {
			this.getTip().animate({top: '-=5px'}, 100, 'linear', done).show();
		},
		function(done) {
			this.getTip().animate({}, 100, 'linear', function()  {
				$(this).hide();
				done.call();
			});
		}
	);

	var TrackingLink = {

		_init: function() {

			this.element.append("<span></span>");

			var ref = this;
			this.element.click(function() {
				ref._callTrackingService();
				return false;
			});

			var params = $gt.getQueryParamObject(this.element.attr("href"));
			this.deviceId = params.id;
			this.state = params.s
			this._setState(this.state == this.options.onValue);
		},
		
		_callTrackingService: function() {
			$self = this;
			$("#track-confirm").remove();
			$("body").append($("<div/>").attr("id", "track-confirm").hide());
			$("#track-confirm").load(this.options.script, ("s=" + this.state + "&id=" + this.deviceId), function() {
				$d = $(this);
				$d.show().dialog({
					modal: true,
					title:$self.element.attr("title")
				});

				$(".services-link").button({
					icons: {
						primary:'ui-icon-signal-diag'
					}
				});
				
				//tracking
						
				/*$(".services-link", $d).click(function() {
					var $link = $(this);

					var saveData = $(":radio[name=saveData]:checked").val();
					$.getJSON($link.attr("href"), {saveTracking:saveData}, function(res) {
						if(res.success) {
							$self.state = res.tracking_status;
							$self._setState($self.state == $self.options.onValue);
							if($self.state == $self.options.onValue) {
								$("#dr_" + $self.deviceId.replace(/:/g,"_")).addClass("tracking-active");
								$(".si_" + $self.deviceId).addClass("tracking-active");
							} else {
								$("#dr_" + $self.deviceId.replace(/:/g,"_")).removeClass("tracking-active");
								$(".si_" + $self.deviceId).removeClass("tracking-active");
							}
							
							
							$d.dialog('destroy');
							$(".total-active-devices").text(res.total_active)
						}
					});
					return false;
				});*/
			});
		},
		_setState: function(active) {
			if(active) {
				this.element.addClass("active");
				this.element.find("span").text(this.options.labelOn);
				$(".si_" + this.deviceId).addClass("tracking-active");
			} else {
				this.element.removeClass("active");
				this.element.find("span").text(this.options.labelOff);
				$(".si_" + this.deviceId).removeClass("tracking-active");
			}
		}
	};

	$.widget("ui.trackingLink", TrackingLink);
	$.ui.trackingLink.getter = "";
	$.ui.trackingLink.prototype.options = {
		uiClass: "gt-toggle-button",
		labelOn:'Disable tracking',
		labelOff:'Enable tracking',
		script:'/status.php',
		onValue:'Y',
		offValue:'N'
	};

})(jQuery);

$gt = {
	getQueryParamObject: function(url) {
		var params = {};
		var kvps = url.split("?").pop().split("&");
		for(var i = 0; i < kvps.length; i++) {
			var kvp = kvps[i].split("=");
			params[kvp[0]] = kvp[1];
		}
		return params;
	}
}

