<?
 
session_start();
if (!$_SESSION['loggedin']){
header("Location: login.php?error=notloggedin");
}

$userid=$_SESSION['userid'];
include("database.php");

//check for number of current active devices
$dnum_q = "SELECT COUNT(devices) FROM devices, devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid";

$dnum= mysql_query($dnum_q ,$conn);

//count number of licenses
$lnum_q="SELECT SUM(licenses)";
$lnum_q="SELECT COUNT(devices) FROM devices, devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid";



//here we run a query for the users licenses and current devices registered
// everyone gets one free device
// for more devices they need to purchase them in blocks of 
//5 for $12, 10 for $20, 20 for $30
//5 day return policy


require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');

$form = new HTML_QuickForm('register');

$devicetypes= array(
					""=>" - Please Select -",
					"Cell Phone"=>"Cell Phone",
					"Digital Camera"=>"Digital Camera",
					"External Hard Drive"=>"External Hard Drive",
					"GPS System"=>"GPS System",
 					"iPod"=>"iPod",
					"MP3 Player"=>"MP3 Player",
                    "USB Flash Drive"=>"USB Flash Drive", 
                    
					"Sony PSP"=>"Sony PSP",
					
					"Other" => "Other");

function addDevice($data){
	global $conn;
	$userid=$_SESSION['userid'];
	$type=addslashes($data['type']);
	$manuf=addslashes($data['manufacturer']);
	$model=addslashes($data['model']);
	$serial=addslashes($data['serial']);
	$devicekey=sha1(uniqid($userid,true));
	
	//print $devicekey;
	
	//createPackage($devicekey, $data['type']);
	
	$q = "INSERT INTO devices (type, manufacturer, model, serial, devicekey) VALUES('$type','$manuf','$model','$serial','$devicekey')";
	$q2= "INSERT INTO devicemap (userid, devicekey) VALUES ('$userid','$devicekey')";
	
	mysql_query($q,$conn) or die(mysql_error()); 
	mysql_query($q2,$conn) or die(mysql_error());
}

$form->addElement('select', 'type', 'Device Type:', $devicetypes);
$form->addElement('text', 'manufacturer', 'Manufacturer:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'model', 'Model:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'serial', 'Serial Number:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('text', 'color', 'Color:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('textarea','other','Other Descriptions (engravings, customizations etc):','wrap="soft"');
$form->addElement('submit', null, 'Submit', array('class' => "submit"));

//validation
$form->addRule('type', 'Please select a device type', 'required', null, 'client');


function processdata($data){
	addDevice($data);
	header("Location: usb.php");
}

if ($form->validate()) {// if the form validates?
	$form->process('processdata');
} else {
?>

<div style="width:450px;margin:0 auto;">
	<? $form->display(); ?>
</div>

<? } ?>


