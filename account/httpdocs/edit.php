<?

require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/devices.php");
authenticateSession(false);

require_once ('HTML/QuickForm.php');

$form = new HTML_QuickForm('register');

$userid=$_SESSION['userid'];

function createForm() {
	global $userid, $form, $conn;
	$did=$_GET['did'];
	
	//get the device information and make sure the person logged in is the owner
	$devicesql="SELECT * FROM devices , devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid AND devices.devicekey='$did'";
	$deviceresult = mysql_query($devicesql,$conn);
	$devicedata=mysql_fetch_array($deviceresult);
	$devicedata = cleanOutput($devicedata);
	$devicetypes= array(
		$devicedata['type'] => $devicedata['type'],
		"#" => "----------",
		"Cell Phone"=>"Cell Phone",
		"Digital Camera"=>"Digital Camera",
		"External Hard Drive"=>"External Hard Drive",
		"GPS System"=>"GPS System",
		"iPod"=>"iPod",
		"MP3 Player"=>"MP3 Player",
		"Sony PSP"=>"Sony PSP",
		"USB Flash Drive"=>"USB Flash Drive", 	
		"Other" => "Other");
	
	if(
		$devicedata['productid'] != 8 &&
		$devicedata['productid'] != 12 &&
		$devicedata['productid'] != 13 &&
		$devicedata['productid'] != 14 &&
		$devicedata['productid'] != 15) {

		$form->addElement('select', 'type', 'Device Type:', $devicetypes);

	} else {

		$form->addElement('html', "<tr><td align='right'><b>Device Type:</b></td><td>$devicedata[type]</td></tr>");
		$form -> addElement ('hidden', 'type', $devicedata[type]);

	}
	
	if($devicedata['productid'] == 8) {
		
		$form->addElement('html', "<tr><td align='right'><b>Manufacturer</b></td><td>$devicedata[manufacturer]</td></tr>");
		$form -> addElement ('hidden', 'manufacturer', $devicedata[manufacturer]);
		
		$form->addElement('html', "<tr><td align='right'><b>Model:</b></td><td>$devicedata[model]</td></tr>");
		$form -> addElement ('hidden', 'model', $devicedata[model]);
		
		$form->addElement('html', "<tr><td align='right'><b>Serial Number:</b></td><td>$devicedata[serial]</td></tr>");
		$form -> addElement ('hidden', 'serial', $devicedata[serial]);

	} else {
		$form->addElement('text', 'manufacturer', 'Manufacturer:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['manufacturer']));
		$form->addElement('text', 'model', 'Model:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['model']));
		$form->addElement('text', 'serial', 'Serial Number:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['serial']));
	}
	

	$form->addElement('text', 'color', 'Color:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['color']));
	$test = $form->addElement('textarea','description','Description:', array('rows' => 5, 'cols' => 28));
	$test->setValue($devicedata['description']);
	$form->addElement('text', 'labelcode', 'GadgetTrak Label Code:', array('size' => 30, 'maxlength' => 255, 'value' => $devicedata['labelcode']));
	$form -> addElement ('hidden', 'did', $did);
	$form -> addElement ('hidden', 'src', $_REQUEST['src']);
	$form->addElement('submit', null, 'Save', array('class' => "submit"));
					
}				

				
function updateDevice($data){
	global $conn;
	$data = cleanInput($data);
	$userid=addslashes($_SESSION['userid']);
	$type=addslashes($data['type']);
	$manuf=addslashes($data['manufacturer']);
	$model=addslashes($data['model']);
	$color=addslashes($data['color']);
	$description=addslashes($data['description']);
	$serial=addslashes($data['serial']);
	$devicekey=addslashes($data['did']);
	$labelcode=addslashes($data['labelcode']);
	$q ="UPDATE devices SET type='$type', serial='$serial' , manufacturer='$manuf', model='$model', color='$color', description='$description', labelcode='$labelcode' WHERE devicekey='$devicekey'";
	mysql_query($q,$conn) or die(mysql_error()); 
}

function processdata($data){
	updateDevice($data);
	if($_REQUEST['src'] == 'dash') {
		header("Location: index.php");
	} else if($_REQUEST['src'] == 'iphone') {
		header("Location: iphone2/index.php");
	} else {
		header("Location: usb.php");
	}
}

if ($form->validate()) {
	$form->process('processdata');
} else { ?>
	<div style="width:450px;margin:0 auto;">
		<?php
			createForm();
			$form->display();
		?>
	</div>
<?php } ?>