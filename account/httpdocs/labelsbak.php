<?php

require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/devices.php");

require_once("$_SERVER[DOCUMENT_ROOT]/classes/Label.php");

authenticateSession();

$labels   = Label::getAllLabelsByUserId($_SESSION['user']->id);

$userid=$_SESSION['userid'];

$credits = mysql_evaluate("Select SUM(number_licenses) FROM licenses WHERE productid !=1 AND userid='$userid'");

$usedcredits = mysql_evaluate("Select COUNT(pro) FROM devices, devicemap, users WHERE devicemap.devicekey = devices.devicekey AND 
devices.productid != 1 AND users.userid=devicemap.userid AND users.userid='$userid' ");

$currentCredits=$credits - $usedcredits;

if ($credits <= 0){
	//header("Location: /buy/");
}

$title="GadgetTrak Labels for PhoneBAK &amp; Verey " . $_SESSION['name'];

include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");

?>

<?php cookieCrumbNav(array("myDevices", "labels"));?>
<h2>Lost &amp; Found Tag Registration</h2>

<div style="margin-bottom:10px;">
	<div style="float:right;">
		<?if ($currentCredits > 0 ) {?>
			Unregistered Labels: <?=$currentCredits?>
		<? }?>
	</div>
	<?if($usedcredits < $credits){?>
	<div style="float:left;">
		<a href="labeladd.php" class="lbOn icon-link icon-link-add" title="Add Label">Add Label</a>
	</div>
	<?}?>
	<div style="clear:both;"></div>
</div>

<table class="tabledata">
	<thead>
		<tr>
			<th>Label Code</th>
			<th>Type</th>
			<th>Manufacturer</th>
			<th>Model</th>
			<th>Platform</th>
			<th>Serial</th>			
			<th>Description</th>			
			<th>Date Created</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($labels as $label) {?>
		<tr>
			<td><?=$label->labelcode?></td>	
			<td><?=$label->type?></td>	
			<td><?=$label->manufacturer?></td>	
			<td><?=$label->model?></td>	
			<td><?=$label->platform?></td>		
			<td><?=$label->serial?></td>	
			<td><?=$label->description?></td>	
			<td><?=$label->datecreated?></td>
			<td><a href="label-edit.php?did=<?=$label->devicekey?>" class="lbOn" title="Edit Label">Edit</a></td>
		</tr>
	<?}?>
	</tbody>
</table>

<br /><br />
<p> GadgetTrak PhoneBAK and Verey products do not communicate with our server directly. However the label that is shipped to you can be added.   </p>

<br clear="both" />
<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>