    <?
    $form1 = new HTML_QuickForm('purchase');
	$usbtrakSelect['0']='0';
	$usbtrakSelect['1']='1 Device License - $12.95';
	$usbtrakSelect['5']='5 Device License Pack - $19.95';
	$usbtrakSelect['10']='10 Device License Pack - $34.95 ';
	$usbtrakSelect['20']='20 Device License Pack - $45.95 ';
	
	
	$form1->addElement('text', 'phonebak', 'PhoneBAK (Symbian)  $19.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('phonebak', 'Please enter a number for PhoneBAK licenses', 'numeric', null, 'client');	
	$form1->addRule('phonebak', 'Please enter a number for PhoneBAK licenses', 'required', null, 'client');
	
	$form1->addElement('text', 'pdabak', 'PhoneBAK PDA (Windows Mobile) $29.95:', array('size' => 2, 'maxlength' => 255));
	$form1->addRule('pdabak', 'Please enter a number for PhoneBAK PDA licenses', 'numeric', null, 'client');	
	$form1->addRule('pdabak', 'Please enter a number for PhoneBAK PDA licenses', 'required', null, 'client');
	

	$form1->addElement('text', 'verey', 'Verey $39.95: ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('verey', 'Please enter a number for Verey licenses', 'numeric', null, 'client');	
	$form1->addRule('verey', 'Please enter a number for Verey licenses', 'required', null, 'client');
	
	
	
	
	$form1->addElement('select', 'usblicenses', 'USBTrak Packages:', $usbtrakSelect);
	$form1->addRule('usblicenses', 'Number of licenses is required to process your request', 'required', null, 'client');
	
	
	$form1->addElement('html','<tr><td style="text-align:right;"><p> </p></td><td><p id="total"></p></td></tr>');
	$form1->addRule('licenses', 'Number of licenses is required to process your request', 'required', null, 'client');
	$form1->addRule('licenses', 'Please enter a number value', 'numeric', null, 'client');
	$form1->addElement('submit', null, 'Submit', array('class' => "submit"));
	?>