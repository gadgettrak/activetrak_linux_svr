<?

	session_start();
	

	if (sizeof($_SESSION['cart']) < 1){ // check to make sure the cart has been set--if not go to order page
		header("Location: index.php");
	} 

	$gtCart=($_SESSION['cart']);
	//var_dump($gtCart);
	
	if ($gtCart[sizeof($gtCart)-1]['grandTotal']  <= 0){
	
    header("Location: index.php");
	}
	
	include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");
	include($_SERVER['DOCUMENT_ROOT']. "/_functions/payment.php");
	include($_SERVER['DOCUMENT_ROOT']."/database.php");
	require_once('Mail.php');
	require_once('Mail/mime.php');
	require_once ('HTML/QuickForm.php');

	$userid=$_SESSION['userid'];
	$did=$_GET['did'];
	$usersql="SELECT * FROM users where userid=". $userid;
	$userresult = mysql_query($usersql,$conn);
	$userdata=mysql_fetch_array($userresult);
	
	$defaults=array('address'=>$userdata['address1']);
	
	$creditform = new HTML_QuickForm('credit');
	$creditform->setDefaults($defaults);
	$creditform->addElement('header', 'payment', 'Payment Information *');
	$creditform->addElement('text', 'firstname', 'First Name:', array('size' => 20, 'maxlength' => 255, 'value' => $userdata['fname']));
	$creditform->addElement('text', 'lastname', 'Last Name:', array('size' => 20, 'maxlength' => 255, 'value' => $userdata['lname']));
	$creditCardType= array("Visa"=>"Visa","MasterCard"=>"MasterCard", "Discover"=>"Discover",  "American Express"=>"American Express");			
	$creditform->addElement('select', 'ccType', 'Card Type:', $creditCardType);
	$creditform->addElement('text', 'creditCardNumber', 'Card Number:', array('size' => 20, 'maxlength' => 255));

	$creditform->addElement('date', 'ccexpire', 'Credit Card Expiration', array('format' => 'F-Y', 'minYear' => date('Y'), 'maxYear' => date('Y') + 10));
	
	$creditform->addElement('text', 'cvv', 'Card Verification Number:', array('size' => 4, 'maxlength' => 4)); 
	$creditform->addElement('header', 'billingadd', 'Shipping Address');
	$creditform->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));
	$creditform->addElement('text', 'city', 'City:', array('size' => 20, 'maxlength' => 255, 'value' => $userdata['city']));
	$creditform->addElement('text', 'state', 'State/Province/Region:', array('size' => 20, 'maxlength' => 255, 'value' => $userdata['state']));
	$creditform->addElement('text', 'postal', 'Postal Code:', array('size' => 20, 'maxlength' => 255, 'value' => $userdata['postal']));
	//$countries= countryList();
	//$creditform->addElement('select', 'country', 'Country:', $countries); 
	$creditform->addElement('static','country','Country:', $userdata['country']);
	
	  
	$creditform->addElement('submit', null,'Submit'); 
	
	//add validations for credit card form
	$creditform->addRule('creditCardNumber', 'Please enter your credit card number', 'required', null, 'client');	
	$creditform->addRule('creditCardNumber', 'Please enter a valid credit card number', 'numeric', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'required', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'numeric', null, 'client');
	
	$creditform->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
	$creditform->addRule('lastname', 'Please enter your last name', 'required', null, 'client');
	$creditform->addRule('address', 'Please enter your address', 'required', null, 'client');
	$creditform->addRule('city', 'Please enter your city', 'required', null, 'client');
	$creditform->addRule('state', 'Please enter your state', 'required', null, 'client');
	$creditform->addRule('postal', 'Please enter your postal code', 'required', null, 'client');
	
	// end of form creation
	
	function paymentProcess($data){
	global $gtCart, $userdata, $userid;
	//var_dump($data);
	require_once 'PayPal.php';
	require_once 'PayPal/Profile/Handler.php';
	require_once 'PayPal/Profile/Handler/Array.php';
	require_once 'PayPal/Profile/API.php';
	require_once 'PayPal/Type/DoDirectPaymentRequestType.php';
	require_once 'PayPal/Type/DoDirectPaymentRequestDetailsType.php';
	require_once 'PayPal/Type/DoDirectPaymentResponseType.php';
	// Add all of the types
	require_once 'PayPal/Type/BasicAmountType.php';
	require_once 'PayPal/Type/PaymentDetailsType.php';
	require_once 'PayPal/Type/AddressType.php';
	require_once 'PayPal/Type/CreditCardDetailsType.php';
	require_once 'PayPal/Type/PayerInfoType.php';
	require_once 'PayPal/Type/PersonNameType.php';
	require_once 'lib/constants.inc.php';
	require_once 'lib/functions.inc.php';
	//require_once 'SampleLogger.php';

	global $conn;
	$ccdata=$data['ccexpire']; // get date exp array
	$dp_request =& PayPal::getType('DoDirectPaymentRequestType');

	define('UNITED_STATES', 'US');

	// get data and assign
	$paymentType = $data['paymentType'];

	$firstName = $data['firstname'];
	$lastName = $data['lastname'];
	$creditCardType = $data['creditCardType'];
	$creditCardNumber = $data['creditCardNumber'];
	//ccexpire
	$expDateMonth = str_pad($ccdata['F'], 2, '0', STR_PAD_LEFT); //grab month and pad it with leading 0 if needed
	$expDateYear =$ccdata['Y']; // need to break month and year up?
	$cvv = $data['cvv'];
	$address = $data['address'];
	$city = $data['city'];
	$state = $data['state'];
	$postal = $data['postal'];
	// add country?
	$amount =$gtCart[sizeof($gtCart)-1]['grandTotal'];
	
	// Populate SOAP request information
	// Payment details
	$OrderTotal =& PayPal::getType('BasicAmountType');
	if (PayPal::isError($amount)) {
    var_dump($amount);
    exit;
	}
	$OrderTotal->setattr('currencyID', 'USD');
	$OrderTotal->setval($amount, 'iso-8859-1');

	$PaymentDetails =& PayPal::getType('PaymentDetailsType');
	$PaymentDetails->setOrderTotal(	$OrderTotal);

	$shipTo =& PayPal::getType('AddressType');
	$shipTo->setName($firstName.' '.$lastName);
	$shipTo->setStreet1($address);

	//$shipTo->setStreet2($address2);
	$shipTo->setCityName($city);
	$shipTo->setStateOrProvince($state);
	$shipTo->setCountry(UNITED_STATES); // fix this to international 
	$shipTo->setPostalCode($postal);
	$PaymentDetails->setShipToAddress($shipTo);

	$dp_details =& PayPal::getType('DoDirectPaymentRequestDetailsType');
	$dp_details->setPaymentDetails($PaymentDetails);

	// Credit Card info
	$card_details =& PayPal::getType('CreditCardDetailsType');
	$card_details->setCreditCardType($ccType);
	$card_details->setCreditCardNumber($creditCardNumber);
	$card_details->setExpMonth($expDateMonth);
	// $card_details->setExpMonth('01');
	$card_details->setExpYear($expDateYear);
	// $card_details->setExpYear('2010');
	$card_details->setCVV2($cvv);
	//$logger->_log('card_details: '. print_r($card_details, true));

	$payer =& PayPal::getType('PayerInfoType');
	$person_name =& PayPal::getType('PersonNameType');
	$person_name->setFirstName($firstName);
	$person_name->setLastName($lastName);
	$payer->setPayerName($person_name);

	$payer->setPayerCountry(UNITED_STATES);
	$payer->setAddress($shipTo);

	$card_details->setCardOwner($payer);

	$dp_details->setCreditCard($card_details);
	$dp_details->setIPAddress($_SERVER['SERVER_ADDR']);
	$dp_details->setPaymentAction('Sale');

	$dp_request->setDoDirectPaymentRequestDetails($dp_details);


    $dummy= @new APIProfile();
    $environments = $dummy->getValidEnvironments();
   
    $handler = & ProfileHandler_Array::getInstance(array(
            'username' => $_POST['apiUsername'],
            'certificateFile' => null,
            'subject' => null,
            'environment' => ENVIRONMENT ));
            
   $pid = ProfileHandler::generateID();
   
   $profile = & new APIProfile($pid, $handler);
   
   //$logger->_log('Profile: '. print_r($profile, true));
      
   $save_file;
   //sandbox
	//$profile->setAPIUsername('kwestin_api1.gmail.com');
	//$profile->setAPIPassword('C88GCRF7E9KHEDXV'); 
	//$profile->setSignature('A3oUv43c4Jz3xCOWypKmn418hFiXAr3sA5hMHLbccsC1MJSxgWFKe0MC');
	
	//live
	$profile->setAPIUsername('kwestin_api1.gmail.com');
	$profile->setAPIPassword('P5RP8TGMTRB2M5PC'); 
	$profile->setSignature('ADwVtsN50I4vGKH1pYGXvXbFG3sqAtekrOZuSAqPuEa2kzG7z26hnFOH');
	
	$profile->setCertificateFile($save_file); 
	$profile->setEnvironment(ENVIRONMENT);    
	    
	//$logger->_log('Profile: '. print_r($profile, true));

	$caller =& PayPal::getCallerServices($profile);

	// Execute SOAP request
	
	
	$response = $caller->DoDirectPayment($dp_request); 
	//var_dump($response);
	$ack = $response->getAck();
	//print  "Ack response " . $ack;
	

	if ($ack == 'Success' || $ack == 'SuccessWithWarning'){ // if the credit card transaction is successful 
	
	
	
	
	
	//if (true){ //for development 	
	$amt_obj = $response->getAmount();
	$transactid = $response->getTransactionID();
		
		
		
     // create a summary of order for email and database
	 $emailtitle = "GadgetTrak Order Details: " . $transactid . "\n" ;
	 $emailto = $userdata['email'] ;
	 $emailcontent = "Transaction ID:" . $transactid . "\n" ;
	 $emailcontent .= "User ID: " . $userid . "\n" ;
	 $emailcontent .= "Name: ". $data['firstname'] . " " . $data['lastname'] . "\n" ;
	 $emailcontent .= "Email: " . $data['email'] . "\n" ;
	 $emailcontent .= "Address: " . $data['address'] . "\n" ;
	 $emailcontent .= "City: " . $data['city'] . "\n" ;
	 $emailcontent .= "State: " . $data['state'] . "\n" ;
	 $emailcontent .= "Postal: " . $data['postal'] . "\n" ;
	 $emailcontent .= "Country: " .  $userdata['country'] . "\n\n" ;
	 $emailcontent .= "Promo: " .  $_SESSION['pid']. "\n\n" ;
	 $emailcontent .= "============= Order Summary ============= \n";
	
	 for($row = 0; $row < sizeof($gtCart) - 1; ){
		 $emailcontent.=$gtCart[$row]['title']. " \$". $gtCart[$row]['price'] . " " . $gtCart[$row]['number'] . "\n";
		 $row++;
	 }
	
	$emailcontent .=  "\n ----------------------------- \n";
	$emailcontent .= "Subtotal: $" . $gtCart[sizeof($gtCart)-1]['total'] . "\n";
	

	$emailcontent .= "Discount: $" . $gtCart[sizeof($gtCart)-1]['total'] * $gtCart[sizeof($gtCart)-1]['discount']['discountamt'] . "\n";
	
	
	$emailcontent .= "Total Charged: $" .$gtCart[sizeof($gtCart)-1]['grandTotal'] . "\n";
	
	
	$headers['From'] = "\"GadgetTrak\" <accounts@gadgettrak.com>";
	$headers['Subject'] = $emailtitle;
	$headers['Bcc'] = 'info@gadgettrak.com';
	
	
	$message = &Mail::factory('mail');

	$message->send($emailto, $headers, $emailcontent);
	
	
	
		// insert sql
		
		//loop through each product
		
		 for($row = 0; $row < sizeof($gtCart) - 1; ){
		
				  $q = "INSERT INTO licenses (
						userid, 
						productid,
						number_licenses,
						discountcode, 
						transactid, 
						address,
						city,
						state,
						postal,
						country) VALUES('"
					.addslashes($userid)."','"
					.addslashes($gtCart[$row]['pid'] )."','"
					.addslashes($gtCart[$row]['number'])."','"
					.addslashes($_SESSION['pid'])."','"
					.addslashes($transactid)."','"
					.addslashes($data['address'])."','"
					.addslashes($data['city'])."','"
					.addslashes($data['state'])."','"
					.addslashes($data['postal'])."','"
					.addslashes($data['country'])."')";
		   
  					mysql_query($q,$conn) or die(mysql_error()); 
			
            $row++;
          }
		
		// end of insert
		
		
		// need to run conditionals here -- check for what products they downloaded
		print "<p> Your payment has been processed. Below is your receipt please print for your records. You may download your software below.<br />
		<strong> Keys will be provided in a seperate email. </strong> If you have questions, please contact us at support@gadgettrak.com </p>";
		
		
		// set the appropriate downloads here
		
		//var_dump($gtCart);
		
		print "<h4> Order Details </h4>";
		print "<p>Order Date:"  . date("m/d/y") . "</p>"; 
   		print "<p>Transaction ID: " . $response->getTransactionID() . "</p>"; 
		
		print "<table class=\"tabledata\" width=\"500\"><tr><th>Product</th><th>Amount</th><th> Licenses </th><th> Download</th></tr>";
        
       for($row = 0; $row < sizeof($gtCart) - 1; ){
            print "<tr><td><p>" . $gtCart[$row]['title']. " </p></td><td><p>\$". $gtCart[$row]['price']."</p> </td><td><p> ".$gtCart[$row]['number'] . "</p></td><td>";
			
			if ($gtCart[$row]['title'] == 'GadgetTrak USB'){
			print "<a target=\"_blank\"  href=\"/usb.php\"> Download </a> ";
			
			}else if($gtCart[$row]['title'] == 'GadgetTrak Lost and Found'){
			
			print "<p> Tags and instructions will be mailed to you</p>";
			
			}else{
			print "<a target=\"_blank\"  href=\"http://www.gadgettrak.com/downloads/" .  $gtCart[$row]['download']  . " \"/> Download </a>";
			}
			
			print "</td></tr>";
			
            $row++;
        }
		
		if ($gtCart[sizeof($gtCart)-1]['discount']['valid']){
		print "<tr><td align=\"right\">Total:</td><td colspan=\"2\">$" . $gtCart[sizeof($gtCart)-1]['total'] . "</td></tr>";
        print "<tr><td align=\"right\"> Discount: </td><td colspan=\"2\"> - $" . ($gtCart[sizeof($gtCart)-1]['total'] * $gtCart[sizeof($gtCart)-1]['discount']['discountamt']) . "</td></tr>";       
       }
          	
		print "<tr><td align=\"right\"> Total Charged: </td><td colspan=\"2\">$". $amt_obj->_value . " " . $amt_obj->_attributeValues['currencyID'] . "</td></tr>";
		
		print "</table>";
		
		print "<h4> Shipping Address</h4>";
		print "<p> GadgetTrak labels will be shipped to the address below, if you need to change the address we ship to, please contact us at <a href=\"mailto:support@gadgetrrak.com\">support@gadgettrak.com</a></p>"; 
		print "<p>" .$data['firstname'] . " " . $data['lastname'] . "<br />" ;
		print $data['address'] . "<br />";
		print $data['city'] . " ". $data['state'] . " " . $data['postal'] . "<br />"; 
		print $userdata['country'];
		print "</p>";
		print "<script language=\"JavaScript\" type=\"text/javascript\">
				var google_conversion_id = 1059748133;
				var google_conversion_language = \"en_US\";
				var google_conversion_format = \"1\";
			var google_conversion_color = \"666666\";
			if (12.0) {
  			var google_conversion_value = 12.0;
			}
			var google_conversion_label = \"purchase\";
			</script>
			<script language=\"JavaScript\" src=\"https://www.googleadservices.com/pagead/conversion.js\">
			</script>";               
		
		//print $currency_cd.' '.$amt . "<br />"; 
		
		//print "</table>";
		//print "Ammount" . $response['Amount'] ."<br />"; 
		
		
 		  // add licenses to the database
   	//var_dump($response);
  		// case ACK_SUCCESS_WITH_WARNING:
      	    	//break;
   
   }else{// if there was a problem with payment
   
     $headers['From'] = "\"GadgetTrak\" <accounts@gadgettrak.com>";
	 $headers['Subject'] = 'Error in Order Process';
	 $headers['Bcc'] = 'info@gadgettrak.com';
	 $emailcontent = "Transaction ID:" . $transactid . "\n" ;
	 $emailcontent = "ERROR CODE:" . $ack . "\n" ;
	 $emailcontent .= "User ID: " . $data['userid'] . "\n" ;
	 $emailcontent .= "Name: ". $data['firstname'] . " " . $data['lastname'] . "\n" ;
	 $emailcontent .= "Address: " . $data['address'] . "\n" ;
	 $emailcontent .= "City: " . $data['city'] . "\n" ;
	 $emailcontent .= "State: " . $data['state'] . "\n" ;
	 $emailcontent .= "Postal: " . $data['postal'] . "\n" ;
	 $emailcontent .= "Country: " .  $userdata['country'] . "\n\n" ;
	 $emailcontent .= "============= Order Summary ============= \n";
	
	 for($row = 0; $row < sizeof($gtCart) - 1; ){
		 $emailcontent.=$gtCart[$row]['title']. " \$". $gtCart[$row]['price'] . " " . $gtCart[$row]['number'] . "\n";
		 $row++;
	 }
	
	$emailcontent .=  "\n ----------------------------- \n";
	$emailcontent .= "Subtotal: $" . $gtCart[sizeof($gtCart)-1]['total'] . "\n";
	$emailcontent .= "Discount: $" . $gtCart[sizeof($gtCart)-1]['total'] * $gtCart[sizeof($gtCart)-1]['discount']['discountamt'] . "\n";
	$emailcontent .= "Total Charged: $" .$gtCart[sizeof($gtCart)-1]['grandTotal'] . "\n";
	
	$message = &Mail::factory('mail');

	$message->send('info@gadgettrak.com', $headers, $emailcontent);
   
   print "<p> There was an error processing your credit card, please verify that the card information entered is correct and you are using a valid card. </p><FORM><INPUT TYPE=\"button\" VALUE=\"Back to order form\" onClick=\"history.go(-1)\"></FORM><p> If you continue to have problems with your order please contact as at support@gadgettrak.com, or <a href=\"http://www.gadgettrak.com/support/\">open a ticket</a>. </p>";
       
	} //end if statement for payment check

	
	
} // end of payment process function
include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");	
		
if ($creditform->validate()){ // run payment process functions
$creditform->process('paymentProcess');
}else{ 


?>
        <h2> Checkout</h2>
            <table class="tabledata" width="500" align="center" cellspacing="0">   
            <tr><th>Product</th><th>Amount</th><th> Licenses </th></tr>
                <?  
                for($row = 0; $row < sizeof($gtCart) - 1; ){
                print "<tr><td>" . $gtCart[$row]['title']. " </td><td>$". $gtCart[$row]['price']."</td><td> ".$gtCart[$row]['number'] . "</td></tr>";
                $row++;
                }
                ?>    
                         
                
             <? if ($gtCart[sizeof($gtCart)-1]['discount']['valid']){  ?>   
            <tr><td align="right">Total:</td><td colspan="2">$<?=$gtCart[sizeof($gtCart)-1]['total'] ?></td></tr> 
            
           
            <tr><td align="right">Discount:</td><td colspan="2">- 
			<?=number_format($gtCart[sizeof($gtCart)-1]['total'] * $gtCart[sizeof($gtCart)-1]['discount']['discountamt'], 2, '.', '')?></tr> 
            <? } ?> 
                   
            <tr><td align="right"><strong>Grand Total:</strong></td><td colspan="2"><strong>
            $<?=number_format($gtCart[sizeof($gtCart)-1]['grandTotal'], 2, '.', '') ?></strong></td></tr>
            </table>
           <!-- <p>Please choose your payment method. If you wish to pay with a credit card please complete the form below, or if you wish to pay using PayPal, 
             please click on the button below to checkout. If you have questions contact us at <a href="mailto:info@gadgettheft.com">info@gadgettheft.com</a>. </p> -->
                <h2>Payment Options</h2>
                
                <div style="width:370px; float:left; padding-right:20px;">
            <h4> Credit Card </h4>
            <img src="/_gfx/creditlogos.gif" />
           
          <? $creditform->display();?>
            </div>
            
            
            <div style="width:300px;float:left;border-left:1px solid #999;padding-left:20px; height:400px;">
            <h4> PayPal </h4>
            <p> To pay using your PayPal account, click the button below. </p>
            
            
           <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
           <input type="hidden" name="cmd" value="_cart">
           <input type="hidden" name="upload" value="1">
           <input type="hidden" name="business" value="info@gadgettrak.com">
           <?  for($row = 0; $row < sizeof($gtCart) - 1;) {?>
           <input type="hidden" name="item_name_<?=$row+1?>"  value="<?= $gtCart[$row]['title']?>">
           
           
           <input type="hidden" name="amount_<?=$row+1 ?>" 
           value="
		   <?= number_format(($gtCart[$row]['price'] -   ($gtCart[$row]['price'] * $gtCart[sizeof($gtCart)-1]['discount']['discountamt'])), 2, '.', '') ?>">
           <? 
           $row++;
           }
           ?>   
           <input type="hidden" name="custom" value="<?=$_SESSION['userid']?> - <?=$_SESSION['pid']?>" />
           <input type="hidden" name="currency_code" value="USD">
           <input type="image" value="PayPal" name="submit"  src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" >
        </form>
        </div>
        <br style="clear:both" />
<? 
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");

} ?>