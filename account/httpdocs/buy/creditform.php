<?	
    
    $creditform = new HTML_QuickForm('credit');
	$creditform->setDefaults($defaults);
	$creditform->addElement('header', 'payment', 'Payment Information *');
	$creditform->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['fname']));
	$creditform->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['lname']));
	$creditCardType= array("Visa"=>"Visa","MasterCard"=>"MasterCard", "Discover"=>"Discover",  "American Express"=>"American Express");			
	$creditform->addElement('select', 'ccType', 'Card Type:', $creditCardType);
	$creditform->addElement('text', 'creditCardNumber', 'Card Number:', array('size' => 30, 'maxlength' => 255));

	$creditform->addElement('date', 'ccexpire', 'Credit Card Expiration', array('format' => 'F-Y', 'minYear' => date('Y'), 'maxYear' => date('Y') + 10));
	//$creditform->addElement('select', 'expmonth', 'Card Month Expiration:', array('01,02,03,04,05,06,07,08,09,10,11,12'));
	//$creditform->addElement('select', 'expyear', 'Card  Year Expiration:', array('2007,2008,2009,2010,2011,2012,2013,2014,2015'));
	
	$creditform->addElement('text', 'cvv', 'Card Verification Number:', array('size' => 3, 'maxlength' => 3)); 
	$creditform->addElement('header', 'billingadd', 'Shipping Address');
	//$creditform>addElement('textarea', 'address', 'Address', array('value'=>'test','cols'=>21, 'rows'=>5, 'wrap'=>'virtual'));
	$creditform->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));
	$creditform->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['city']));
	$creditform->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['state']));
	$creditform->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['postal']));
	//$countries= countryList();
	//$creditform->addElement('select', 'country', 'Country:', $countries); 
	$creditform->addElement('static','country','Country:', $userdata['country']);
	//$creditform->addElement('static', 'Foobar');   
	
	$creditform->addElement('hidden', 'amount', $gtCart[sizeof($gtCart)-1]['grandTotal']); 
	$creditform->addElement('hidden', 'license', $totals['license']); 
	
	$creditform->addElement('hidden', 'discount', $totals['discount']); 
	
	$creditform->addElement('hidden', 'country', $userdata['country']);
	$creditform->addElement('hidden', 'userid', $userdata['userid']);  
	$creditform->addElement('submit', null,'Submit'); 
	
	//add validations for credit card form
	$creditform->addRule('creditCardNumber', 'Please enter your credit card number', 'required', null, 'client');	
	$creditform->addRule('creditCardNumber', 'Please enter a valid credit card number', 'numeric', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'required', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'numeric', null, 'client');
	
	$creditform->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
	$creditform->addRule('lastname', 'Please enter your last name', 'required', null, 'client');
	$creditform->addRule('address', 'Please enter your address', 'required', null, 'client');
	$creditform->addRule('city', 'Please enter your city', 'required', null, 'client');
	$creditform->addRule('state', 'Please enter your state', 'required', null, 'client');
	$creditform->addRule('postal', 'Please enter your postal code', 'required', null, 'client');
	?>