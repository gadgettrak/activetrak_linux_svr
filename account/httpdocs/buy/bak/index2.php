<?
ini_set( 
  'include_path', 
  ini_get( 'include_path' ) . PATH_SEPARATOR . "/home/kwestin/pear/php"
); 

session_start();

if (!$_SESSION['loggedin']){
header("Location: /login.php?error=notloggedin");
}

include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");
include($_SERVER['DOCUMENT_ROOT']."/database.php");

require_once ('HTML/QuickForm.php');
$creditform = new HTML_QuickForm('credit');
$totals;

function amount($licenses){


	//$subtotal ="$" . number_format($licenses. 2) * 12;
	if ($licenses == 5){
	$amount = 12;
	}else if($licenses == 10){
	$amount = 20;
	$discount=4;
	}else if($licenses == 20){
	$amount = 35;
	$discount=13;
	}else if($licenses >= 25 && $licenses <= 49){
	$amount = 50;
	$discount=22;
	}else if($licenses == 50){
	$amount = ($licenses * 12) * .80;
	$discount=35;
	}
	
	$returnarray['total']=$amount;
	$returnarray['license']=$licenses;
	$returnarray['discount']=$discount;

	return $returnarray;
}

// Process first form and build the order form for payment information
function processdata1($data){
	global $userid, $form, $conn, $totals, $creditform;

	$userid=$_SESSION['userid'];
	$did=$_GET['did'];
	$usersql="SELECT * FROM users where userid=". $userid;
	$userresult = mysql_query($usersql,$conn);
	$userdata=mysql_fetch_array($userresult);
	
	$defaults=array('address'=>$userdata['address1']);

	$totals=amount($data['licenses']);
	$creditform = new HTML_QuickForm('credit');
	$creditform->setDefaults($defaults);
	$creditform->addElement('header', 'payment', 'Payment Information *');
	$creditform->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['fname']));
	$creditform->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['lname']));
	$creditCardType= array("Visa"=>"Visa","MasterCard"=>"MasterCard", "Discover"=>"Discover",  "American Express"=>"American Express");			
	$creditform->addElement('select', 'ccType', 'Card Type:', $creditCardType);
	$creditform->addElement('text', 'creditCardNumber', 'Card Number:', array('size' => 30, 'maxlength' => 255));

	$creditform->addElement('date', 'ccexpire', 'Credit Card Expiration', array('format' => 'F-Y', 'minYear' => date('Y'), 'maxYear' => date('Y') + 10));
	//$creditform->addElement('select', 'expmonth', 'Card Month Expiration:', array('01,02,03,04,05,06,07,08,09,10,11,12'));
	//$creditform->addElement('select', 'expyear', 'Card  Year Expiration:', array('2007,2008,2009,2010,2011,2012,2013,2014,2015'));
	
	$creditform->addElement('text', 'cvv', 'Card Verification Number:', array('size' => 3, 'maxlength' => 3)); 
	$creditform->addElement('header', 'billingadd', 'Shipping Address');
	//$creditform>addElement('textarea', 'address', 'Address', array('value'=>'test','cols'=>21, 'rows'=>5, 'wrap'=>'virtual'));
	$creditform->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));
	$creditform->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['city']));
	$creditform->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['state']));
	$creditform->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['postal']));
	//$countries= countryList();
	//$creditform->addElement('select', 'country', 'Country:', $countries); 
	$creditform->addElement('static','country','Country:', $userdata['country']);
	//$creditform->addElement('static', 'Foobar');   
	$creditform->addElement('hidden', 'amount', $totals['total']); 
	$creditform->addElement('hidden', 'license', $totals['license']); 
	$creditform->addElement('hidden', 'discount', $totals['discount']); 
	$creditform->addElement('hidden', 'country', $userdata['country']);
	$creditform->addElement('hidden', 'userid', $userdata['userid']);  
	$creditform->addElement('submit', null,'Submit'); 
	
	//add validations for credit card form
	$creditform->addRule('creditCardNumber', 'Please enter your credit card number', 'required', null, 'client');	
	$creditform->addRule('creditCardNumber', 'Please enter a valid credit card number', 'numeric', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'required', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'numeric', null, 'client');
	
	$creditform->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
	$creditform->addRule('lastname', 'Please enter your last name', 'required', null, 'client');
	$creditform->addRule('address', 'Please enter your address', 'required', null, 'client');
	$creditform->addRule('city', 'Please enter your city', 'required', null, 'client');
	$creditform->addRule('state', 'Please enter your state', 'required', null, 'client');
	$creditform->addRule('postal', 'Please enter your postal code', 'required', null, 'client');
	

}

$form1 = new HTML_QuickForm('purchase');
$licensesSelect['5']='5 Device License Pack - $12';
$licensesSelect['10']='10 Device License Pack - $20 ';
$licensesSelect['20']='20 Device License Pack - $35 ';
$licensesSelect['30']='30 Device License Pack  - $50 ';
$licensesSelect['50']='50 Device License Pack  - $85 ';



//$form1->addElement('text', 'licenses', 'Number of GadgetTrak Pro Device Licenses', array('size' => 4, 'value' => 1, 'maxlength' => 4));
$form1->addElement('select', 'licenses', 'Select Number of Licenses:', $licensesSelect);
$form1->addElement('html','<tr><td style="text-align:right;"><p> </p></td><td><p id="total"></p></td></tr>');
$form1->addRule('licenses', 'Number of licenses is required to process your request', 'required', null, 'client');
$form1->addRule('licenses', 'Please enter a number value', 'numeric', null, 'client');
$form1->addElement('submit', null, 'Submit', array('class' => "submit"));



function paymentProcess($data){

	include 'ppsdk_include_path.inc';
	require_once 'PayPal.php';
	require_once 'PayPal/Profile/Handler.php';
	require_once 'PayPal/Profile/Handler/Array.php';
	require_once 'PayPal/Profile/API.php';
	require_once 'PayPal/Type/DoDirectPaymentRequestType.php';
	require_once 'PayPal/Type/DoDirectPaymentRequestDetailsType.php';
	require_once 'PayPal/Type/DoDirectPaymentResponseType.php';
	// Add all of the types
	require_once 'PayPal/Type/BasicAmountType.php';
	require_once 'PayPal/Type/PaymentDetailsType.php';
	require_once 'PayPal/Type/AddressType.php';
	require_once 'PayPal/Type/CreditCardDetailsType.php';
	require_once 'PayPal/Type/PayerInfoType.php';
	require_once 'PayPal/Type/PersonNameType.php';
	require_once 'lib/constants.inc.php';
	require_once 'lib/functions.inc.php';
	require_once 'SampleLogger.php';

	global $conn;
	$ccdata=$data['ccexpire']; // get date exp array
	$dp_request =& PayPal::getType('DoDirectPaymentRequestType');

	define('UNITED_STATES', 'US');

	// get data and assign
	$paymentType = $data['paymentType'];
	$firstName = $data['firstname'];
	$lastName = $data['lastname'];
	$creditCardType = $data['creditCardType'];
	$creditCardNumber = $data['creditCardNumber'];
	//ccexpire
	$expDateMonth = str_pad($ccdata['F'], 2, '0', STR_PAD_LEFT); //grab month and pad it with leading 0 if needed
	$expDateYear =$ccdata['Y']; // need to break month and year up?
	$cvv = $data['cvv'];
	$address = $data['address'];
	$city = $data['city'];
	$state = $data['state'];
	$postal = $data['postal'];
	// add country?
	$amount = $data['amount'];
	
	// Populate SOAP request information
	// Payment details
	$OrderTotal =& PayPal::getType('BasicAmountType');
	if (PayPal::isError($OrderTotal)) {
    var_dump($OrderTotal);
    exit;
	}
	$OrderTotal->setattr('currencyID', 'USD');
	$OrderTotal->setval($amount, 'iso-8859-1');

	$PaymentDetails =& PayPal::getType('PaymentDetailsType');
	$PaymentDetails->setOrderTotal($OrderTotal);

	$shipTo =& PayPal::getType('AddressType');
	$shipTo->setName($firstName.' '.$lastName);
	$shipTo->setStreet1($address);
	//$shipTo->setStreet2($address2);
	$shipTo->setCityName($city);
	$shipTo->setStateOrProvince($state);
	$shipTo->setCountry(UNITED_STATES); // fix this to international 
	$shipTo->setPostalCode($postal);
	$PaymentDetails->setShipToAddress($shipTo);

	$dp_details =& PayPal::getType('DoDirectPaymentRequestDetailsType');
	$dp_details->setPaymentDetails($PaymentDetails);

	// Credit Card info
	$card_details =& PayPal::getType('CreditCardDetailsType');
	$card_details->setCreditCardType($ccType);
	$card_details->setCreditCardNumber($creditCardNumber);
	$card_details->setExpMonth($expDateMonth);
	// $card_details->setExpMonth('01');
	$card_details->setExpYear($expDateYear);
	// $card_details->setExpYear('2010');
	$card_details->setCVV2($cvv);
	//$logger->_log('card_details: '. print_r($card_details, true));

	$payer =& PayPal::getType('PayerInfoType');
	$person_name =& PayPal::getType('PersonNameType');
	$person_name->setFirstName($firstName);
	$person_name->setLastName($lastName);
	$payer->setPayerName($person_name);

	$payer->setPayerCountry(UNITED_STATES);
	$payer->setAddress($shipTo);

	$card_details->setCardOwner($payer);

	$dp_details->setCreditCard($card_details);
	$dp_details->setIPAddress($_SERVER['SERVER_ADDR']);
	$dp_details->setPaymentAction('Sale');

	$dp_request->setDoDirectPaymentRequestDetails($dp_details);


  $dummy= @new APIProfile();
  $environments = $dummy->getValidEnvironments();
   
   $handler = & ProfileHandler_Array::getInstance(array(
            'username' => $_POST['apiUsername'],
            'certificateFile' => null,
            'subject' => null,
            'environment' => ENVIRONMENT ));
            
   $pid = ProfileHandler::generateID();
   
   $profile = & new APIProfile($pid, $handler);
   
   //$logger->_log('Profile: '. print_r($profile, true));
      
   $save_file;
   //sandbox
	//$profile->setAPIUsername('kwestin_api1.gmail.com');
	//$profile->setAPIPassword('C88GCRF7E9KHEDXV'); 
	//$profile->setSignature('A3oUv43c4Jz3xCOWypKmn418hFiXAr3sA5hMHLbccsC1MJSxgWFKe0MC');
	
	//live
	$profile->setAPIUsername('kwestin_api1.gmail.com');
	$profile->setAPIPassword('P5RP8TGMTRB2M5PC'); 
	$profile->setSignature('ADwVtsN50I4vGKH1pYGXvXbFG3sqAtekrOZuSAqPuEa2kzG7z26hnFOH');
	
	$profile->setCertificateFile($save_file); 
	$profile->setEnvironment(ENVIRONMENT);         
	//$logger->_log('Profile: '. print_r($profile, true));

	$caller =& PayPal::getCallerServices($profile);

	// Execute SOAP request
	$response = $caller->DoDirectPayment($dp_request); 
	//var_dump($response);
	$ack = $response->getAck();
	//print  "Ack response " . $ack;
	

	
	switch($ack) {
   		case ACK_SUCCESS: // if the credit card transaction is successful 
		
		$amt_obj = $response->getAmount();
		$transactid = $response->getTransactionID();
		
		// insert sql
		
		  $q = "INSERT INTO licenses (
   				userid, 
  				number_licenses, 
				transactid, 
				address,
				city,
				state,
				postal,
				country) VALUES('"
   			.addslashes($data['userid'])."','"
   			.addslashes($data['license'])."','"
			.addslashes($transactid)."','"
			.addslashes($data['address'])."','"
			.addslashes($data['city'])."','"
			.addslashes($data['state'])."','"
			.addslashes($data['postal'])."','"
			.addslashes($data['country'])."')";
   
  		mysql_query($q,$conn) or die(mysql_error()); 
		
		// end of insert
		
		
		print "<p>Transaction Complete! The license credits have been applied to your account. Please go to <a hre\"/my.php\">your account</a> and assign the license to an existing device, or add new device(s).</p>";
		
		print "<h4> Order Details </h4>";
		print "<p>Order Date:"  . date("m/d/y") . "</p>"; 
   		print "<p>Transaction ID: " . $response->getTransactionID() . "</p>"; 
		
		print "<p>GadgetTrak Pro Device License Qty: ". $data['license'] ;
	
		print "  - $" . $amt_obj->_value . " ". $amt_obj->_attributeValues['currencyID'] . "</p>";
		
		print "<h4> Shipping Address</h4>";
		print "<p> GadgetTrak labels will be shipped to the address below, if you need to change the address we ship to, please contact us at <a href=\"mailto:support@gadgettheft.com\">support@gadgettheft.com</a></p>"; 
		print "<p>" .$data['firstname'] . " " . $data['lastname'] . "<br />" ;
		print $data['address'] . "<br />";
		print $data['city'] . " ". $data['state'] . " " . $data['postal'] . "<br />"; 
		print $data['country'];
		print "</p>";
		
		//print $currency_cd.' '.$amt . "<br />"; 
		
		//print "</table>";
		//print "Ammount" . $response['Amount'] ."<br />"; 
		
		
 		  // add licenses to the database
   	//var_dump($response);
  		 case ACK_SUCCESS_WITH_WARNING:
      	// Good to break out;
    	break;
   
   default:
   print "<p> There was an error processing your credit card, please verify that the card information entered is correct and you are using a valid card. </p><FORM><INPUT TYPE=\"button\" VALUE=\"Back to order form\" onClick=\"history.go(-1)\"></FORM><p> If you continue to have problems with your order please contact as at support@gadgettheft.com, or you may order by phone at (503) 608-7310</p>";
      //$_SESSION['response'] =& $response;   
      //$logger->_log('DoDirectPayment failed: ' . print_r($response, true));
     // $location = "checkouterror.php";
     // header("Location: $location");  
	  }

	
	
} // end of payment process function


include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");

?>



<div style="width:500px;margin:0 auto;">

<? //IF THE FIRST ORDER FORM VALIDATES DISPLAY TOTAL AND PAYMENT OPTIONS

if ($form1->validate()) {// if the form validates?
$form1->process('processdata1');
?>
<h2>GadgetTrak&#8482; Checkout</h2>
	<table class="tabledata" width="375"> 
	<tr><th width="212">Description</th> 
	<th width="139">Amount</th>
	</tr>
	<tr><td class="grow"><p>GadgetTrak&#8482;  <?=$totals['license']?> Pack License:</p></td><td class="grow">
	<p>$<?=$totals['total']?>.00</p>
	</tr>
	<tr><td><p align=\"rignt\"><strong>Total:</strong></p></td><td><p><strong>$<?=$totals['total']?> .00</strong><br />
		<?
		if(!$totals['discount']== 0){
		print "<span style=\"font-size:10px;\"> ($" . $totals['discount']. ".00 discount included)";
		}
	?>
	</p>

	</tr>
	</table>

    <p>Please choose your payment method. If you wish to pay with a credit card please complete the form below, or if you wish to pay using PayPal, please click on the button below to checkout. If you have questions contact us at <a href="mailto:info@gadgettheft.com">info@gadgettheft.com</a>.  </p>
		<h2>Payment Options</h2>
		<h4> Credit Card </h4>
	<img src="/_gfx/creditlogos.gif" />
	<? $creditform->display( );?>
	
	<h4> PayPal </h4>
	<p> To pay with your PayPal account, click the button below. </p>
	<form action="https://www.paypal.com/us/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="info@gadgettheft.com">
<input type="hidden" name="item_name" value="<?=$totals['license']?> GadgetTrak Pro Licenses">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="custom" value="<?=$_SESSION['userid']?>" />
<input type="hidden" name="amount" value="<?=$totals['total']?> ">
<input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
</form>



<?
//if  $creditfrom is validated connect to paypal and process card data
}else if($creditform->validate()){ //&& !($form1->isSubmitted()

$creditform->process('paymentProcess');

?>
    <?}else{?>
	<h2>GadgetTrak&#8482; License Purchase</h2>
	<p>All prices are for a 1 year subscription. International orders are welcome, however all pricing is in US dollars.</p>
	<?$form1->display();?>
</div>
<p>&nbsp;</p>
<table width="350" border="0" align="center" class="tabledata">
 	<tr>
    <th colspan="2"><div align="left">Pricing and Discount Guide - 1 Year Subscription </div>
	<div id="div"></div>      <div align="right"></div>      <div id="div2"> </div></th>
	</tr>
	<tr>
    <td class="grow" width="187"><strong>5 Device </strong>Licence Pack  </td>
    <td class="grow"width="153">$12</td>
	</tr>
	<tr>
    <td><strong>10 Device</strong> License Pack </td>
    <td>$20 ($4 Discount) </td>
	</tr>
	<tr>
	  <td class="grow"><strong>20 Device</strong> License Pack </td>
	  <td class="grow">$35 ($13 Discount) </td>
  </tr>
	<tr>
	  <td><strong>30 Device</strong> License Pack </td>
	  <td>$50 ($22 Discount) </td>
  </tr>
	<tr>
	  <td class="grow"><strong>50 Device</strong> License Pack </td>
	  <td class="grow">$85 ($35 Discount) </td>
  </tr>
</table>
	<p>For larger volume pricing please contact 
	<a href="mailto:sales@gadgettheft.com">sales@gadgettheft.com </a> (503) 608-7310) for volume discounts.
<?}?>

    <?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>
