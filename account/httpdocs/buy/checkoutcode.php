<?

	$usersql="SELECT * FROM users where userid=". $userid;
	$userresult = mysql_query($usersql,$conn);
	$userdata=mysql_fetch_array($userresult);
	
	$defaults=array('address'=>$userdata['address1']);


function processdata1($data){
	global $userid, $form, $conn, $gtCart, $creditform;

	$userid=$_SESSION['userid'];
	$did=$_GET['did'];
	$usersql="SELECT * FROM users where userid=". $userid;
	$userresult = mysql_query($usersql,$conn);
	$userdata=mysql_fetch_array($userresult);
	
	$defaults=array('address'=>$userdata['address1']);

	$gtCart=shoppingCart($data);//create an cart  array with prices and volume 
	
	$creditform = new HTML_QuickForm('credit');
	$creditform->setDefaults($defaults);
	$creditform->addElement('header', 'payment', 'Payment Information *');
	$creditform->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['fname']));
	$creditform->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['lname']));
	$creditCardType= array("Visa"=>"Visa","MasterCard"=>"MasterCard", "Discover"=>"Discover",  "American Express"=>"American Express");			
	$creditform->addElement('select', 'ccType', 'Card Type:', $creditCardType);
	$creditform->addElement('text', 'creditCardNumber', 'Card Number:', array('size' => 30, 'maxlength' => 255));

	$creditform->addElement('date', 'ccexpire', 'Credit Card Expiration', array('format' => 'F-Y', 'minYear' => date('Y'), 'maxYear' => date('Y') + 10));
	//$creditform->addElement('select', 'expmonth', 'Card Month Expiration:', array('01,02,03,04,05,06,07,08,09,10,11,12'));
	//$creditform->addElement('select', 'expyear', 'Card  Year Expiration:', array('2007,2008,2009,2010,2011,2012,2013,2014,2015'));
	
	$creditform->addElement('text', 'cvv', 'Card Verification Number:', array('size' => 3, 'maxlength' => 3)); 
	$creditform->addElement('header', 'billingadd', 'Shipping Address');
	//$creditform>addElement('textarea', 'address', 'Address', array('value'=>'test','cols'=>21, 'rows'=>5, 'wrap'=>'virtual'));
	$creditform->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));
	$creditform->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['city']));
	$creditform->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['state']));
	$creditform->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255, 'value' => $userdata['postal']));
	//$countries= countryList();
	//$creditform->addElement('select', 'country', 'Country:', $countries); 
	$creditform->addElement('static','country','Country:', $userdata['country']);
	//$creditform->addElement('static', 'Foobar');   
	
	$creditform->addElement('hidden', 'amount', $gtCart[sizeof($gtCart)-1]['grandTotal']); 
	$creditform->addElement('hidden', 'license', $totals['license']); 
	
	$creditform->addElement('hidden', 'discount', $totals['discount']); 
	
	$creditform->addElement('hidden', 'country', $userdata['country']);
	$creditform->addElement('hidden', 'userid', $userdata['userid']);  
	$creditform->addElement('submit', null,'Submit'); 
	
	//add validations for credit card form
	$creditform->addRule('creditCardNumber', 'Please enter your credit card number', 'required', null, 'client');	
	$creditform->addRule('creditCardNumber', 'Please enter a valid credit card number', 'numeric', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'required', null, 'client');	
	$creditform->addRule('cvv', 'Please enter a valid code', 'numeric', null, 'client');
	
	$creditform->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
	$creditform->addRule('lastname', 'Please enter your last name', 'required', null, 'client');
	$creditform->addRule('address', 'Please enter your address', 'required', null, 'client');
	$creditform->addRule('city', 'Please enter your city', 'required', null, 'client');
	$creditform->addRule('state', 'Please enter your state', 'required', null, 'client');
	$creditform->addRule('postal', 'Please enter your postal code', 'required', null, 'client');

function paymentProcess($data){

	//include 'ppsdk_include_path.inc';
	require_once 'PayPal.php';
	require_once 'PayPal/Profile/Handler.php';
	require_once 'PayPal/Profile/Handler/Array.php';
	require_once 'PayPal/Profile/API.php';
	require_once 'PayPal/Type/DoDirectPaymentRequestType.php';
	require_once 'PayPal/Type/DoDirectPaymentRequestDetailsType.php';
	require_once 'PayPal/Type/DoDirectPaymentResponseType.php';
	// Add all of the types
	require_once 'PayPal/Type/BasicAmountType.php';
	require_once 'PayPal/Type/PaymentDetailsType.php';
	require_once 'PayPal/Type/AddressType.php';
	require_once 'PayPal/Type/CreditCardDetailsType.php';
	require_once 'PayPal/Type/PayerInfoType.php';
	require_once 'PayPal/Type/PersonNameType.php';
	require_once 'lib/constants.inc.php';
	require_once 'lib/functions.inc.php';
	//require_once 'SampleLogger.php';

	global $conn;
	$ccdata=$data['ccexpire']; // get date exp array
	$dp_request =& PayPal::getType('DoDirectPaymentRequestType');

	define('UNITED_STATES', 'US');

	// get data and assign
	$paymentType = $data['paymentType'];
	$firstName = $data['firstname'];
	$lastName = $data['lastname'];
	$creditCardType = $data['creditCardType'];
	$creditCardNumber = $data['creditCardNumber'];
	//ccexpire
	$expDateMonth = str_pad($ccdata['F'], 2, '0', STR_PAD_LEFT); //grab month and pad it with leading 0 if needed
	$expDateYear =$ccdata['Y']; // need to break month and year up?
	$cvv = $data['cvv'];
	$address = $data['address'];
	$city = $data['city'];
	$state = $data['state'];
	$postal = $data['postal'];
	// add country?
	$amount = $data['amount'];
	
	// Populate SOAP request information
	// Payment details
	$OrderTotal =& PayPal::getType('BasicAmountType');
	if (PayPal::isError($OrderTotal)) {
    var_dump($OrderTotal);
    exit;
	}
	$OrderTotal->setattr('currencyID', 'USD');
	$OrderTotal->setval($amount, 'iso-8859-1');

	$PaymentDetails =& PayPal::getType('PaymentDetailsType');
	$PaymentDetails->setOrderTotal($OrderTotal);

	$shipTo =& PayPal::getType('AddressType');
	$shipTo->setName($firstName.' '.$lastName);
	$shipTo->setStreet1($address);
	//$shipTo->setStreet2($address2);
	$shipTo->setCityName($city);
	$shipTo->setStateOrProvince($state);
	$shipTo->setCountry(UNITED_STATES); // fix this to international 
	$shipTo->setPostalCode($postal);
	$PaymentDetails->setShipToAddress($shipTo);

	$dp_details =& PayPal::getType('DoDirectPaymentRequestDetailsType');
	$dp_details->setPaymentDetails($PaymentDetails);

	// Credit Card info
	$card_details =& PayPal::getType('CreditCardDetailsType');
	$card_details->setCreditCardType($ccType);
	$card_details->setCreditCardNumber($creditCardNumber);
	$card_details->setExpMonth($expDateMonth);
	// $card_details->setExpMonth('01');
	$card_details->setExpYear($expDateYear);
	// $card_details->setExpYear('2010');
	$card_details->setCVV2($cvv);
	//$logger->_log('card_details: '. print_r($card_details, true));

	$payer =& PayPal::getType('PayerInfoType');
	$person_name =& PayPal::getType('PersonNameType');
	$person_name->setFirstName($firstName);
	$person_name->setLastName($lastName);
	$payer->setPayerName($person_name);

	$payer->setPayerCountry(UNITED_STATES);
	$payer->setAddress($shipTo);

	$card_details->setCardOwner($payer);

	$dp_details->setCreditCard($card_details);
	$dp_details->setIPAddress($_SERVER['SERVER_ADDR']);
	$dp_details->setPaymentAction('Sale');

	$dp_request->setDoDirectPaymentRequestDetails($dp_details);


    $dummy= @new APIProfile();
    $environments = $dummy->getValidEnvironments();
   
    $handler = & ProfileHandler_Array::getInstance(array(
            'username' => $_POST['apiUsername'],
            'certificateFile' => null,
            'subject' => null,
            'environment' => ENVIRONMENT ));
            
   $pid = ProfileHandler::generateID();
   
   $profile = & new APIProfile($pid, $handler);
   
   //$logger->_log('Profile: '. print_r($profile, true));
      
   $save_file;
   //sandbox
	//$profile->setAPIUsername('kwestin_api1.gmail.com');
	//$profile->setAPIPassword('C88GCRF7E9KHEDXV'); 
	//$profile->setSignature('A3oUv43c4Jz3xCOWypKmn418hFiXAr3sA5hMHLbccsC1MJSxgWFKe0MC');
	
	//live
	$profile->setAPIUsername('kwestin_api1.gmail.com');
	$profile->setAPIPassword('P5RP8TGMTRB2M5PC'); 
	$profile->setSignature('ADwVtsN50I4vGKH1pYGXvXbFG3sqAtekrOZuSAqPuEa2kzG7z26hnFOH');
	
	$profile->setCertificateFile($save_file); 
	$profile->setEnvironment(ENVIRONMENT);         
	//$logger->_log('Profile: '. print_r($profile, true));

	$caller =& PayPal::getCallerServices($profile);

	// Execute SOAP request
	$response = $caller->DoDirectPayment($dp_request); 
	//var_dump($response);
	$ack = $response->getAck();
	print  "Ack response " . $ack;
	

if ($ack == 'Success' || $ack == 'SuccessWithWarning'){ // if the credit card transaction is successful 
		
		$amt_obj = $response->getAmount();
		$transactid = $response->getTransactionID();
		
		// insert sql
		
		  $q = "INSERT INTO licenses (
   				userid, 
  				number_licenses, 
				transactid, 
				address,
				city,
				state,
				postal,
				country) VALUES('"
   			.addslashes($data['userid'])."','"
   			.addslashes($data['license'])."','"
			.addslashes($transactid)."','"
			.addslashes($data['address'])."','"
			.addslashes($data['city'])."','"
			.addslashes($data['state'])."','"
			.addslashes($data['postal'])."','"
			.addslashes($data['country'])."')";
   
  		mysql_query($q,$conn) or die(mysql_error()); 
		
		// end of insert
		
		
		print "<p>Transaction Complete! The license credits have been applied to your account. Please go to <a hre\"/my.php\">your account</a> and assign the license to an existing device, or add new device(s).</p>";
		
		
		// set the appropriate downloads here
		print "<h4> Order Details </h4>";
		print "<p>Order Date:"  . date("m/d/y") . "</p>"; 
   		print "<p>Transaction ID: " . $response->getTransactionID() . "</p>"; 
		
		print "<p>GadgetTrak Pro Device License Qty: ". $data['license'] ;
	
		print "  - $" . $amt_obj->_value . " ". $amt_obj->_attributeValues['currencyID'] . "</p>";
		
		print "<h4> Shipping Address</h4>";
		print "<p> GadgetTrak labels will be shipped to the address below, if you need to change the address we ship to, please contact us at <a href=\"mailto:support@gadgettheft.com\">support@gadgettheft.com</a></p>"; 
		print "<p>" .$data['firstname'] . " " . $data['lastname'] . "<br />" ;
		print $data['address'] . "<br />";
		print $data['city'] . " ". $data['state'] . " " . $data['postal'] . "<br />"; 
		print $data['country'];
		print "</p>";
		print "<script language=\"JavaScript\" type=\"text/javascript\">
				var google_conversion_id = 1059748133;
				var google_conversion_language = \"en_US\";
				var google_conversion_format = \"1\";
			var google_conversion_color = \"666666\";
			if (12.0) {
  			var google_conversion_value = 12.0;
			}
			var google_conversion_label = \"purchase\";
			</script>
			<script language=\"JavaScript\" src=\"https://www.googleadservices.com/pagead/conversion.js\">
			</script>";               
		
		//print $currency_cd.' '.$amt . "<br />"; 
		
		//print "</table>";
		//print "Ammount" . $response['Amount'] ."<br />"; 
		
		
 		  // add licenses to the database
   	//var_dump($response);
  		// case ACK_SUCCESS_WITH_WARNING:
      	    	//break;
   
   }else{// if there was a problem with payment
   print "<p> There was an error processing your credit card, please verify that the card information entered is correct and you are using a valid card. </p><FORM><INPUT TYPE=\"button\" VALUE=\"Back to order form\" onClick=\"history.go(-1)\"></FORM><p> If you continue to have problems with your order please contact as at support@gadgettheft.com, or you may order by phone at (503) 608-7310</p>";
       
} //end if statement for payment check

	
	
} // end of payment process function


?>


<h2>GadgetTrak&#8482; Checkout</h2>
	<table class="tabledata" width="500">   
	<tr><th>Product</th><th>Amount</th><th> Licenses </th></tr>
   		<?  
   		for($row = 0; $row < sizeof($gtCart) - 1; ){
   		print "<tr><td><p>" . $gtCart[$row]['title']. " </p></td><td><p>$". $gtCart[$row]['price']."</p> </td><td><p> ".$gtCart[$row]['number'] . "</p></td></tr>";
   		$row++;
   		}
   		?>    
	<tr><td><p align=\"rignt\"><strong>Total:</strong></p></td><td><p><strong>$<?=$gtCart[sizeof($gtCart)-1]['grandTotal'] ?></strong><br /></p></tr>
	</table>
    <p>Please choose your payment method. If you wish to pay with a credit card please complete the form below, or if you wish to pay using PayPal, 
     please click on the button below to checkout. If you have questions contact us at <a href="mailto:info@gadgettheft.com">info@gadgettheft.com</a>. </p>
		<h2>Payment Options</h2>
  	<h4> Credit Card </h4>
	<img src="/_gfx/creditlogos.gif" />
	<? $creditform->display();?>
	
	<h4> PayPal </h4>
	<p> To pay using your PayPal account, click the button below. </p>
    
    
   <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
   <input type="hidden" name="cmd" value="_cart">
   <input type="hidden" name="upload" value="1">
   <input type="hidden" name="business" value="info@gadgettheft.com">
 
   
   <?  for($row = 0; $row < sizeof($gtCart) - 1;) {?>
   <input type="hidden" name="item_name_<?=$row+1?>"  value="<?= $gtCart[$row]['title']?>">
   <input type="hidden" name="amount_<?=$row+1 ?>" value="<?=$gtCart[$row]['price'] ?>">
   <? 
   $row++;
   }
   ?>   
   <input type="hidden" name="custom" value="<?=$_SESSION['userid']?>" />
   <input type="hidden" name="currency_code" value="USD">
   <input type="image" value="PayPal" name="submit"  src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" >
</form>
   


<?
//if  $creditfrom is validated connect to paypal and process card data

}else if($creditform->validate()){ //&& !($form1->isSubmitted()

	$creditform->process('paymentProcess');

}else{
?>
   