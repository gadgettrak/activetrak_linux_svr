<?

session_start();

if (!$_SESSION['loggedin']){
header("Location: /store/login.php?error=notloggedin");
}

include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");
include($_SERVER['DOCUMENT_ROOT']. "/_functions/payment.php");
include($_SERVER['DOCUMENT_ROOT']."/database.php");
require_once ('HTML/QuickForm.php');


    $form1 = new HTML_QuickForm('purchase');
	
	$usbtrakSelect['0']='-- Please Select --';
	$usbtrakSelect['1']='1 Device License - $14.95';
	$usbtrakSelect['5']='5 Device License Pack - $29.95';
	//$usbtrakSelect['10']='10 Device License Pack - $39.95 ';
	//$usbtrakSelect['20']='20 Device License Pack - $64.95 ';
	
	$tagSelect['0']='-- Please Select --';
	$tagSelect['5']='Lost & Found Tag 5 Pack - $9.95';
	$tagSelect['10']='Lost & Found Tag 10 Pack - $15.95';
	$tagSelect['20']='Lost & Found Tag 20 Pack - $25.95';
	
	
	$form1->addElement('html','<tr><td colspan="2"> <h4>PC-Trak&trade;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="http://www.gadgettrak.com/_gfx/windows_logo.jpg" /></h4></td></tr>');
	$form1->addElement('text', 'pctrak1', 'One Year License $24.95 - Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('pctrak1', 'Please enter a number for PC-Trak 1 year licenses', 'numeric', null, 'client');	
	$form1->addRule('pctrak1', 'Please enter a number for PC-Trak 3 year licenses', 'required', null, 'client');

	$form1->addElement('text', 'pctrak3', 'Three Year License $59.95 - Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('pctrak3', 'Please enter a number for PC-Trak licenses', 'numeric', null, 'client');	
	$form1->addRule('pctrak3', 'Please enter a number for PC-Trak licenses', 'required', null, 'client');
	
	
	
	$form1->addElement('html','<tr><td colspan="2"> <span style="font-size:11px;">License can be transfered to a new device twice. </span></td></tr>');
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	
	$form1->addElement('html','<tr><td colspan="2"> <h4>MacTrak&trade; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<img width="75" height="48" src="http://www.gadgettrak.com/_gfx/osxlogo.png" /></h4></td></tr>');
	$form1->addElement('text', 'mac1', 'One Year License $24.95 - Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('mac1', 'Please enter a number for MacTrak 1 year licenses', 'numeric', null, 'client');	
	$form1->addRule('mac1', 'Please enter a number for MacTrak 1 year licenses', 'required', null, 'client');

	$form1->addElement('text', 'verey', 'Three Year License $59.95 - Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('verey', 'Please enter a number for MacTrak licenses', 'numeric', null, 'client');	
	$form1->addRule('verey', 'Please enter a number for MacTrak licenses', 'required', null, 'client');
	
	
	
	$form1->addElement('html','<tr><td colspan="2"> <span style="font-size:11px;">  License can be transfered to a new device twice. </span></td></tr>');
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	
	
	//$form1->addElement('html','<tr><td colspan="2"> <h4> GadgetTrak&reg; Search & Destroy&trade; for Windows  <img src="/_gfx/windows.gif" /></h4><p> Tracking &amp; Data Destruction</td></tr>');
	//$form1->addElement('text', 'sd1', '1 Year License $64.95 -  Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	//$form1->addElement('text', 'sd3', '3 Year License $89.95 -  Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	//$form1->addRule('sd1', 'Please enter a number for GadgetTrak for Windows licenses', 'numeric', null, 'client');	
	//$form1->addRule('sd1', 'Please enter a number for GadgetTrak for Windows licenses', 'required', null, 'client');
	//$form1->addRule('sd3', 'Please enter a number for GadgetTrak for Windows licenses', 'numeric', null, 'client');	
	//$form1->addRule('sd3', 'Please enter a number for GadgetTrak for Windows licenses', 'required', null, 'client');
	//$form1->addElement('html','<tr><td colspan="2"> <p> Temporarily unavailable as we upgrade to new version </p></td></tr>');
	//$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	
	/*$form1->addElement('html','<tr><td colspan="2"> <h4> GadgetTrak&reg; for Windows <img src="/_gfx/windows.gif" /></h4><p> Tracking Only </</td></tr>');
	$form1->addElement('text', 'pc1', '1 Year License $29.95 -  Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addElement('text', 'pc3', '3 Year License $59.95 -  Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('pc1', 'Please enter a number for GadgetTrak for Windows licenses', 'numeric', null, 'client');	
	$form1->addRule('pc1', 'Please enter a number for GadgetTrak for Windows licenses', 'required', null, 'client');
	$form1->addRule('pc3', 'Please enter a number for GadgetTrak for Windows licenses', 'numeric', null, 'client');	
	$form1->addRule('pc3', 'Please enter a number for GadgetTrak for Windows licenses', 'required', null, 'client');
	
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');*/
	
	$form1->addElement('html','<tr><td colspan="2"> <h4>GadgetTrak&reg; Mobile Security</h4></td></tr>');
	
	
	$form1->addElement('text', 'bakbb', 'Blackberry $24.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('bakbb', 'Please enter a number for Blackberry licenses', 'numeric', null, 'client');	
	$form1->addRule('bakbb', 'Please enter a number for Blackberry licenses', 'required', null, 'client');
	
	$form1->addElement('text', 'pdabak', 'Windows Mobile $24.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('pdabak', 'Please enter a number for Windows Mobile licenses', 'numeric', null, 'client');	
	$form1->addRule('pdabak', 'Please enter a number for WIndows Mobile licenses', 'required', null, 'client');
	
	//$form1->addElement('text', 'phonebak', 'Symbian (Nokia, LG etc)  $24.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	//$form1->addRule('phonebak', 'Please enter a number for PhoneBAK licenses', 'numeric', null, 'client');	
	//$form1->addRule('phonebak', 'Please enter a number for PhoneBAK licenses', 'required', null, 'client');
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	$form1->addElement('html','<tr><td colspan="2"> <h4>GadgetTra&reg; for USB Devices <img src="/_gfx/media_devices.gif" /></h4>
	<p style="margin:0px;padding:0px;font-size:11px;"> MP3 Players, iPods, flash drives, external hard drives, GPS devices </p></td></tr>');
	$form1->addElement('select', 'usblicenses', 'GadgetTrak USB Qty.', $usbtrakSelect);
	$form1->addRule('usblicenses', 'Number of licenses is required to process your request', 'required', null, 'client');
	
	
	
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	$form1->addElement('html','<tr><td colspan="2"> <h4>Lost &amp; Found Tags <img src="/_gfx/gt_mini.gif" /></h4></td></tr>');
	$form1->addElement('select', 'tags', 'GadgetTrak Lost & Found Tags:', $tagSelect);
	$form1->addRule('tags', 'Number of tags is required to process your request', 'required', null, 'client');
	
	
	$form1->addElement('html','<tr><td style="text-align:right;"><p> </p></td><td><p id="total"></p></td></tr>');
	//$form1->addRule('licenses', 'Number of licenses is required to process your request', 'required', null, 'client');
	//$form1->addRule('licenses', 'Please enter a number value', 'numeric', null, 'client');
	
	
	$form1->addElement('html','<tr><td style="text-align:right;"><p> </p></td><td><p></td></tr>');
	
	//check for partnerID 
	if(isset($_SESSION['pid'])){
	$form1->addElement('hidden', 'promo', $_SESSION['pid']);
	}else{
	
	$form1->addElement('text', 'promo', 'Promo Code: ', array('size' => 10, 'maxlength' => 255));
	}
	
	$form1->addElement('submit', null, 'Submit', array('class' => "submit"));

// Process first form and build the order form for payment information
function processdata1($data){
	

	$userid=$_SESSION['userid'];
	
	$_SESSION['cart'] = shoppingCart($data);
	
	//var_dump($_SESSION['cart']);
	
	header("Location: checkout.php");
	
	
	
}


 //IF THE FIRST ORDER FORM VALIDATES DISPLAY TOTAL AND PAYMENT OPTIONS
if ($form1->validate() ) {// if the order form validates?
$form1->process('processdata1');
}else{
include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
//START CHECKOUT
?>
	<!--
    
   <?= sizeof($_SESSION['cart'])?> 
    
    -->
    <div style="width:500px;margin:0 auto;">
    <h2> Step 2: Purchase GadgetTrak Software </h2>
    <p>To download the free iPhone and iPod Touch application please download from the<a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=288927565&amp;mt=8" target="_blank"> iTunes App Store</a>. Thank You</p>
    <p> Please enter the quantity of each product you wish to purchase. </p>
	<?$form1->display();?>
    

 
	<p>For volume pricing please contact 
	<a href="mailto:info@gadgettrak.com">info@gadgettrak.com </a> (503) 608-7310) for volume discounts.</p>
    
    
</div>
<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
 }
?>
