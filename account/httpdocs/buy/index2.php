<?

session_start();

if (!$_SESSION['loggedin']){
header("Location: /login.php?error=notloggedin");
}

include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");
include($_SERVER['DOCUMENT_ROOT']. "/_functions/payment.php");
include($_SERVER['DOCUMENT_ROOT']."/database.php");
require_once ('HTML/QuickForm.php');


    $form1 = new HTML_QuickForm('purchase');
	
	$usbtrakSelect['0']='-- Please Select --';
	$usbtrakSelect['1']='1 Device License - $12.95';
	$usbtrakSelect['5']='5 Device License Pack - $19.95';
	$usbtrakSelect['10']='10 Device License Pack - $34.95 ';
	$usbtrakSelect['20']='20 Device License Pack - $45.95 ';
	
	$tagSelect['0']='-- Please Select --';
	$tagSelect['5']='Lost & Found Tag 5 Pack - $9.95';
	$tagSelect['10']='Lost & Found Tag 10 Pack - $15.95';
	$tagSelect['20']='Lost & Found Tag 20 Pack - $25.95';
	
	
	
	$form1->addElement('html','<tr><td colspan="2"> <h4> GadgetTrak for Windows PC <img src="/_gfx/windows.gif" /></h4></td></tr>');
	$form1->addElement('text', 'pc1', '1 Year License $39.95 -  Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addElement('text', 'pc3', '3 Year License $69.95 -  Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('pc1', 'Please enter a number for GadgetTrak for Windows licenses', 'numeric', null, 'client');	
	$form1->addRule('pc1', 'Please enter a number for GadgetTrak for Windows licenses', 'required', null, 'client');
	$form1->addRule('pc3', 'Please enter a number for GadgetTrak for Windows licenses', 'numeric', null, 'client');	
	$form1->addRule('pc3', 'Please enter a number for GadgetTrak for Windows licenses', 'required', null, 'client');
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	
	
   	$form1->addElement('html','<tr><td colspan="2"> <h4>GadgetTrak for Mac - Verey <img src="/_gfx/osx.gif" /></h4></td></tr>');
	$form1->addElement('text', 'verey', 'Per device license $39.95 - Qty. ', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('verey', 'Please enter a number for Verey licenses', 'numeric', null, 'client');	
	$form1->addRule('verey', 'Please enter a number for Verey licenses', 'required', null, 'client');
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	
	
	$form1->addElement('html','<tr><td colspan="2"> <h4>GadgetTrak for USB Devices <img src="/_gfx/media_devices.gif" /></h4>
	<p style="margin:0px;padding:0px;font-size:11px;"> MP3 Players, iPods, flash drives, external hard drives, GPS devices </p></td></tr>');
	$form1->addElement('select', 'usblicenses', 'GadgetTrak USB Qty.', $usbtrakSelect);
	$form1->addRule('usblicenses', 'Number of licenses is required to process your request', 'required', null, 'client');
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	
	$form1->addElement('html','<tr><td colspan="2"> <h4>GadgetTrak PhoneBak for Mobile Phones <img src="/_gfx/mobile_logo.gif" /></h4></td></tr>');
	
	
	$form1->addElement('text', 'bakbb', 'Blackberry $24.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('bakbb', 'Please enter a number for PhoneBak Blackberry licenses', 'numeric', null, 'client');	
	$form1->addRule('bakbb', 'Please enter a number for PhoneBak Blackberry licenses', 'required', null, 'client');
	
	
	$form1->addElement('text', 'phonebak', 'Symbian (Nokia, LG etc)  $24.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('phonebak', 'Please enter a number for PhoneBAK licenses', 'numeric', null, 'client');	
	$form1->addRule('phonebak', 'Please enter a number for PhoneBAK licenses', 'required', null, 'client');
	
	$form1->addElement('text', 'pdabak', 'Windows Mobile $24.95:', array('size' => 2, 'maxlength' => 255, 'value' => 0));
	$form1->addRule('pdabak', 'Please enter a number for PhoneBak Windows Mobile licenses', 'numeric', null, 'client');	
	$form1->addRule('pdabak', 'Please enter a number for PhoneBak WIndows Mobile licenses', 'required', null, 'client');
	
	$form1->addElement('html','<tr><td colspan="2"> <hr /></td></tr>');
	$form1->addElement('html','<tr><td colspan="2"> <h4>Lost &amp; Found Tags <img src="/_gfx/gt_mini.gif" /></h4></td></tr>');
	$form1->addElement('select', 'tags', 'GadgetTrak Lost & Found Tags:', $tagSelect);
	$form1->addRule('tags', 'Number of tags is required to process your request', 'required', null, 'client');
	
	
	$form1->addElement('html','<tr><td style="text-align:right;"><p> </p></td><td><p id="total"></p></td></tr>');
	//$form1->addRule('licenses', 'Number of licenses is required to process your request', 'required', null, 'client');
	//$form1->addRule('licenses', 'Please enter a number value', 'numeric', null, 'client');
	
	
	$form1->addElement('html','<tr><td style="text-align:right;"><p> </p></td><td><p></td></tr>');
	
	$form1->addElement('text', 'promo', 'Promo Code: ', array('size' => 10, 'maxlength' => 255));
	
	$form1->addElement('submit', null, 'Submit', array('class' => "submit"));

// Process first form and build the order form for payment information
function processdata1($data){
	

	$userid=$_SESSION['userid'];
	
	$_SESSION['cart'] = shoppingCart($data);
	
	//var_dump($_SESSION['cart']);
	
	header("Location: checkout.php");
	
	
	
}


 //IF THE FIRST ORDER FORM VALIDATES DISPLAY TOTAL AND PAYMENT OPTIONS
if ($form1->validate() ) {// if the order form validates?
$form1->process('processdata1');
}else{
include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
//START CHECKOUT
?>
	<!--
    
   <?= sizeof($_SESSION['cart'])?> 
    
    -->
    <div style="width:500px;margin:0 auto;">
    <h2> Purchase GadgetTrak Software </h2>
    <p> Please enter the quantity of each product you wish to purchase. </p>
	<?$form1->display();?>
    

 
	<p>For larger volume pricing please contact 
	<a href="mailto:info@gadgettrak.com">info@gadgettrak.com </a> (503) 608-7310) for volume discounts.</p>
    
    
</div>
<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
 }
?>
