<?
$params = array();


function buildQuery ($args) {
  $query = '?';
  foreach ($args as $key => $val) {
    if ($query != '?') {
      $query .= '&';
    }
 
    $query .= $key . '=' . urlencode($val);
  }
  return $query;
}
 
$params['lat'] = '45.396452';
$params['lng'] = '-1.755630';
 
$query = buildQuery($params);
$url = 'http://ws.geonames.org/findNearestAddress';
$reqUrl = $url . $query;
 
$xml = simplexml_load_file($reqUrl);
$total =  (string)$xml['geonamse']['address'];  // Need to typecast, or it'll be an object

print $xml->address[0]->street;

print sizeof($xml);
//echo $xml->asXML();  // Look at the source code to see what you fetched

