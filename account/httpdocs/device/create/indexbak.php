<?

require_once ('HTML/QuickForm.php');
require_once("bootstrapper.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceMap.php");
require_once("classes/DeviceMapManager.php");

require_once("classes/DeviceConnectionManager.php");

Authentication::authenticateSession(false);

if(!isset($_REQUEST['p']) || ($_REQUEST['p'] != '1' && $_REQUEST['p'] != '0')) {
	die("<b>Incorrect product type</b>");
}

function processdata($data) {

	if($data['p'] != 0 && $data['p'] != 1) {
		die("<b>Incorrect product type</b>");
	}

	$deviceManager = new DeviceManager();
	$deviceMapManager = new DeviceMapManager();

	$device = new Device();
	$device->devicekey = sha1(uniqid($_SESSION['user']->userid,true));
	$device->type = $data['type'];
	$device->manufacturer = $data['manufacturer'];
	$device->model = $data['model'];
	$device->serial = $data['serial'];
	$device->color = $data['color'];
	$device->description = $data['description'];
	$device->theft_status = 'N';
	$device->saveTracking = 'N';
	$device->productid = $data['p'];
	$device->labelcode = $data['label'];
	$device->datecreated = date('Y-m-d H:i:s');

	$deviceMap = new DeviceMap(null, $_SESSION['user']->userid, $device->devicekey);

	$device = $deviceManager->addNewDevice($device);
	$deviceMap = $deviceMapManager->addNewDeviceMap($deviceMap);

	header("Location: /index.php");
}


$devicetypes = array(
	""=>" - Please Select -",
	"Cell Phone"=>"Cell Phone",
	"Digital Camera"=>"Digital Camera",
	"External Hard Drive"=>"External Hard Drive",
	"GPS System"=>"GPS System",
	"iPod"=>"iPod",
	"MP3 Player"=>"MP3 Player",
	"USB Flash Drive"=>"USB Flash Drive",
	"Sony PSP"=>"Sony PSP",
	"Other" => "Other"
);

$form = new HTML_QuickForm('device');

$form->addElement('text', 'label', 'GadgetTrak Label Code:', array('size' => 30, 'maxlength' => 7));
$form->addElement('select', 'type', 'Device Type:', $devicetypes);
$form->addElement('text', 'manufacturer', 'Manufacturer:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'model', 'Model:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'serial', 'Serial Number:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'color', 'Color:', array('size' => 30, 'maxlength' => 255));
$form->addElement('hidden', 'p', $_REQUEST['p']);
$form->addElement('textarea','description','Description:','wrap="soft"');
$form->addElement('submit', null, 'Submit', array('id' => 'createSubmit', 'class' => "submit"));

//validation
$form->addRule('type', 'Please select a device type', 'required', null, 'client');
$form->addRule('description', 'Please enter a description', 'required', null, 'client');
$form->addRule('label', 'Please enter a valid GadgetTrak label code', 'required', null, 'client');



if ($form->validate()) {// if the form validates?
	$form->process('processdata');
} else {
?>

<div style="width:450px;margin:0 auto;">
	<? $form->display(); ?>
</div>

<script type="text/javascript">
$("#createSubmit").button();
</script>

<? } ?>

