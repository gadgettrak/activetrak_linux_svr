<?

require_once ('HTML/QuickForm.php');
require_once("bootstrapper.php");

require_once("classes/Device.php");
require_once("classes/DeviceManager.php");

require_once("classes/DeviceMap.php");
require_once("classes/DeviceMapManager.php");

require_once("classes/LicenseManager.php");

Authentication::authenticateSession(false);

$confirm = $_GET['confirm'];
$deviceKey = $_GET['deviceKey'];

$deviceManager = new DeviceManager();
$deviceMapManager = new DeviceMapManager();
$licenseManager = new LicenseManager();

$device = $deviceManager->getDeviceByKey($deviceKey, $_SESSION['user']->userid);

if(!$device) {
	die("<strong>Invalid delete request</strong>");
}

if($confirm == 'y') {

	$deviceManager->deleteDeviceById($device->deviceid);
	$deviceMapManager->deleteDeviceMapByKey($device->devicekey);
	$licenseManager->resetLicenseByDeviceId($device->deviceid);
	header("Location: /index.php");

} else {
?>

<p><strong style="color:#c00;">Warning!</strong></p>

<p>you are about to delete the device <strong><?=$device->description?></strong> from our system.
This will remove all records pertaining to this device. This cannot be undone and all information
pertaining to this device will be lost. To continue with your deletion please click the "Delete" button.</p>

<a id="deleteButton" href="/device/delete?deviceKey=<?=$device->devicekey?>&confirm=y">Delete</a>

<?php } ?>

<script type="text/javascript">
$(function() {
	$("#deleteButton").button({
		icons: {
			primary: "ui-icon-circle-close"
		}
	});
});
</script>