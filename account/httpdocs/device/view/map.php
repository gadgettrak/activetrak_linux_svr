<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/_style/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/_js/gmap-label.js"></script>
<script type="text/javascript" src="/_js/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
var self = this;
$(function() {
	try {
		var connections = self.parent._getData();
		var startRow = self.parent._getStartRow();
		initialize(connections, startRow);
	} catch(e) {
		/*oh noes!*/
	}
	
});

function initialize(connections, startRow) {

	var myLatlng = new google.maps.LatLng(45.5240967,-122.6754679);
	var myOptions = {
		zoom: 18,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		streetViewControl: true
	}

	startRow = startRow || 1;

	var map = new google.maps.Map(document.getElementById("map_canvas_details"), myOptions);

	var latlngbounds = new google.maps.LatLngBounds();

	for(var i = 0; i < connections.length; i++) {

		var myLatlng = new google.maps.LatLng(connections[i].lat, connections[i].lon);
		latlngbounds.extend(myLatlng);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:connections[i].timestamp,
			icon: "/_gfx/gmap-icons/" + (i + startRow) + ".png"
		});

		window.parent.$gmapMarkers["mk_" + (i + startRow)] = marker;

		var label = new Label({
			map: map
		});

		google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
				$("#dr_" + (i + startRow), top.document).addClass("row-over");
				$("#ml_" + (i + startRow)).addClass("gmap-label-hover");
			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
				$("#dr_" + (i + startRow), top.document).removeClass("row-over");
				$("#ml_" + (i + startRow)).removeClass("gmap-label-hover");
			}
		})(marker, i));

		label.bindTo('position', marker, 'position');
		label.set('text', connections[i].timestamp);

		var labelSpan = $(label.span_);
		labelSpan.attr("id", "ml_" + (i + startRow));

	}

	if(connections.length) {
		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);
	}

}


</script>

<style type="text/css">
body {
	margin:0;
}
</style>

</head>

<body>
	<div id="map_canvas_details"></div>
</body>

</html>
