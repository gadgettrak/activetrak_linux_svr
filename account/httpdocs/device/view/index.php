<?php

require_once("bootstrapper.php");
require_once("classes/User.php");
require_once("classes/DeviceManager.php");
require_once("classes/Device.php");
require_once("classes/DeviceConnectionManager.php");
require_once("classes/Utils.php");
require_once 'classes/Paginator.php';

Authentication::authenticateSession();

$deviceKey = $_GET['key'];

$deviceManager = new DeviceManager();
$deviceConnectionManager = new DeviceConnectionManager();

/* @var $device Device */
$device = $deviceManager->getDeviceResultByKey($deviceKey, $_SESSION['user']->userid);

//Set up paging pariables
$pageNum = (isset($_GET['pageNum']) && ((int)$_GET['pageNum'] > 0))?(int)$_GET['pageNum']:1;
$pageSize = (isset($_GET['pageSize']) && ((int)$_GET['pageSize'] > 0))?(int)$_GET['pageSize']:10;
$paginator = new Paginator($pageNum, $pageSize);

/* @var $deviceConnections ArrayObject */
$deviceConnections = $deviceConnectionManager->getDeviceConnections($deviceKey, $paginator);
$paginator->numRecords = count($devices);

?>

<?php include("fragments/header.php"); ?>
<!-- PAGE CONTENT BEGIN -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/_js/gmap-label.js"></script>

<script type="text/javascript">

<?php
$connectionsJson = array();
foreach($deviceConnections as $connection) {
	if($connection->isValidLocation()) {
		array_push($connectionsJson, array(
			"timestamp" => $connection->timestamp,
			"lat" => $connection->ip_lat,
			"lon" => $connection->ip_lon
		));
	}
}
$connectionsJson = json_encode($connectionsJson);
?>

function createMap(connections, startRow) {

	var defaultLatlng = new google.maps.LatLng(0,0);
	//var defaultLatlng = new google.maps.LatLng(45.5240967,-122.6754679);
	//var defaultLatlng = new google.maps.LatLng(22.199803,-159.489963);
	var defaultLatlngbounds = new google.maps.LatLngBounds().extend(defaultLatlng);
	var myOptions = {
		zoom: 18,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		streetViewControl: true
	}

	startRow = 1;
	
	var map = new google.maps.Map(document.getElementById("map_canvas_details"), myOptions);

	var latlngbounds = new google.maps.LatLngBounds();

	for(var i = 0; i < connections.length; i++) {

		var myLatlng = new google.maps.LatLng(connections[i].lat, connections[i].lon);
		latlngbounds.extend(myLatlng);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:connections[i].timestamp,
			icon: "/_gfx/gmap-icons/" + (i + startRow) + ".png"
		});

		var label = new Label({
			map: map
		});

		google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
				$("#dr_" + (i + startRow), top.document).addClass("row-over");
				$("#ml_" + (i + startRow)).addClass("gmap-label-hover");
			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
				$("#dr_" + (i + startRow), top.document).removeClass("row-over");
				$("#ml_" + (i + startRow)).removeClass("gmap-label-hover");
			}
		})(marker, i));

		label.bindTo('position', marker, 'position');
		label.set('text', connections[i].timestamp);

		var labelSpan = $(label.span_);
		labelSpan.attr("id", "ml_" + (i + startRow));

	}

	if(connections.length) {
		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);
		return {map:map, bounds:latlngbounds};
	} else {

		var label = new Label({
			map: map,
			className:"gmap-message"
		});

		label.bindTo('position', map, 'center');
		label.set('text', "No location data is available for this device.");

		map.setCenter(defaultLatlngbounds.getCenter());
		map.fitBounds(defaultLatlngbounds);

		map.setOptions({
			disableDefaultUI:true,
			disableDoubleClickZoom:true,
			streetViewControl: false,
			scrollwheel: false,
			mapTypeControl: false,
			draggable: false
		});

		return {map:map, bounds:defaultLatlngbounds};
	}

}

$(function() {

	
	var mapObj;
	<?php if($device->productid != 0) {?>
	var connections = <?=$connectionsJson?>;

	//if(connections.length) {
		mapObj = createMap(connections, <?=$paginator->rowStartNum?>);
	//} else {
	//	$("#map_canvas_details").hide();
	//}
	<?php } ?>

	$('.tracking-link').trackingLink();

	$('.tracking-link').button({
		icons: {
			primary:'ui-icon-signal-diag'
		}
	});

	$("#tabs").tabs({
		selected:<?=($_GET['tab']=='reports')?1:0?>,
		show: function() {
			if(mapObj !== undefined) {
				google.maps.event.trigger(mapObj.map, 'resize')
				mapObj.map.setCenter(mapObj.bounds.getCenter());
				mapObj.map.fitBounds(mapObj.bounds);
			}
		}
	});

	$("#deviceActions a.editDevice").button({
		icons: {
			primary:'ui-icon-pencil'
		}
	});

	$("#deviceActions a.deleteDevice").button({
		icons: {
			primary:'ui-icon-trash'
		}
	});

	$("#connections li").click(function() {
		$this = $(this);
		var locs = $this.attr("class").replace("LOC_","").split("_");
		var src = "/map.php?lat=" + locs[0] + "&lon=" + locs[1];
		$("#trackingMap iframe").attr("src", src);
	});

	$(".tabledata tr").hover(
		function() {
			$this = $(this);
			$this.addClass("row-over");
			var temp = $("#ml_" + $this.attr("id").substring(3));
			temp.css({"color":"#fff", "background-color":"#000"});

		},
		function() {
			$this = $(this);
			$this.removeClass("row-over");
			var temp = $("#ml_" + $this.attr("id").substring(3));
			temp.css({"color":"#000", "background-color":"#fff"});
		}
	);

	$(".delete-connection-link").button({
		icons: {
			primary:'ui-icon-trash'
		}
	}).click(function() {
		$link = $(this);
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				'Delete tracking entry': function() {
					window.location = $link.attr("href");	
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			}
		});
		return false;
	});

	$(".delete-all-connections").button({
		icons: {
			primary:'ui-icon-trash'
		}
	}).click(function() {
		$link = $(this);
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				'Delete all': function() {
					window.location = $link.attr("href");
				},
				Cancel: function() {
					$(this).dialog('close');
					return false;
				}
			}
		});
		return false;
	});

	$("#prevButton").click(function() {
		$("#pageNum").val(parseInt($("#pageNum").val()) - 1);
		$("#devices").submit();
		return false;
	});

	$("#nextButton").click(function() {
		$("#pageNum").val(parseInt($("#pageNum").val()) + 1);
		$("#devices").submit();
		return false;
	});

	$("#pageSize").change(function() {
		$("#pageNum").val(1);
		$("#devices").submit();
	});

	$("pageNum").change(function() {
		$("#devices").submit();

	});

	$("a.ip-link").click(function() {
		var $link = $(this);
		var dialog = $('<div style="display:none;padding:20px;"></div>').appendTo('body');
		dialog.load($link.attr("href"), function (responseText, textStatus, XMLHttpRequest) {
			dialog.dialog({
				modal:true,
				title:$link.attr("title"),
				width:800,
				height:400
			});
		});
		return false;
	});

});
</script>

<h2><a href="/">Dashboard</a> &raquo; Device Details</h2>

<h3 style="margin-top:30px;"><?php if($device->productid == 0) {?>GadgetTrak Label : <?php } ?><?=$device->type?> : <?=$device->description?></h3>

<?php

if($device->productid == 0) {
	include("fragments/detailsPages/label.php");
} else if($device->productid == 1) {
	include("fragments/detailsPages/usb.php");
} else if($device->productid == 8) {
	include("fragments/detailsPages/iPhone.php");
} else if($device->productid == 4 || $device->productid == 7 || $device->productid == 10 || $device->productid == 12 || $device->productid == 13 || $device->productid == 14 || $device->productid == 15) {
	include("fragments/detailsPages/computer.php");
}

?>

<!-- PAGE CONTENT END -->

<div style="display:none;" id="dialog-confirm" title="Delete tracking data?">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<?php include("fragments/footer.php"); ?>