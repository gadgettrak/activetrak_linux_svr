<?

require_once ('HTML/QuickForm.php');
require_once("bootstrapper.php");
require_once("classes/User.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");

Authentication::authenticateSession(false);

$deviceKey = $_REQUEST['key'];
$deviceManager = new DeviceManager();
$device = $deviceManager->getDeviceByKey($deviceKey, $_SESSION['user']->userid);

$form = new HTML_QuickForm('register');

function createForm() {

	global $device, $form;

	$devicetypes= array(
		$device->type => $device->type,
		"#" => "----------",
		"Cell Phone"=>"Cell Phone",
		"Digital Camera"=>"Digital Camera",
		"External Hard Drive"=>"External Hard Drive",
		"GPS System"=>"GPS System",
		"iPod"=>"iPod",
		"MP3 Player"=>"MP3 Player",
		"Sony PSP"=>"Sony PSP",
		"USB Flash Drive"=>"USB Flash Drive",
		"Other" => "Other");

	if(
		$device->productid != 8 &&
		$device->productid != 12 &&
		$device->productid != 13 &&
		$device->productid != 14 &&
		$device->productid != 15) {

		$form->addElement('select', 'type', 'Device Type:', $devicetypes);

	} else {

		$form->addElement('html', "<tr><td align='right'><b>Device Type:</b></td><td>$device->type</td></tr>");
		$form -> addElement ('hidden', 'type', $device->type);
	}

	if($device->productid == 8 && (float) $device->version < 2) {

		$form->addElement('html', "<tr><td align='right'><b>Manufacturer</b></td><td>$device->manufacturer</td></tr>");
		$form -> addElement ('hidden', 'manufacturer', $device->manufacturer);

		$form->addElement('html', "<tr><td align='right'><b>Model:</b></td><td>$device->model</td></tr>");
		$form -> addElement ('hidden', 'model', $device->model);

		$form->addElement('html', "<tr><td align='right'><b>Serial Number:</b></td><td>$device->serial</td></tr>");
		$form -> addElement ('hidden', 'serial', $device->serial);

	} else {
		$form->addElement('text', 'manufacturer', 'Manufacturer:', array('size' => 30, 'maxlength' => 255, 'value' => $device->manufacturer));
		$form->addElement('text', 'model', 'Model:', array('size' => 30, 'maxlength' => 255, 'value' => $device->model));
		$form->addElement('text', 'serial', 'Serial Number:', array('size' => 30, 'maxlength' => 255, 'value' => $device->serial));
	}

	$form->addElement('text', 'color', 'Color:', array('size' => 30, 'maxlength' => 255, 'value' => $device->color));
	$test = $form->addElement('textarea','description','Description:', array('rows' => 5, 'cols' => 28));
	$test->setValue($device->description);
	$form->addElement('text', 'labelcode', 'GadgetTrak Label Code:', array('size' => 30, 'maxlength' => 255, 'value' => $device->labelcode));
	$form -> addElement ('hidden', 'key', $device->devicekey);
	$form -> addElement ('hidden', 'src', $_REQUEST['src']);
	$form->addElement('submit', null, 'Save', array('class' => "submit"));

}

function updateDevice($data) {

	global $device, $deviceManager;

	$device->type = $data['type'];
	$device->manufacturer = $data['manufacturer'];
	$device->model = $data['model'];
	$device->color = $data['color'];
	$device->description = $data['description'];
	$device->serial = $data['serial'];
	$device->labelcode = $data['labelcode'];
	$device->cleanAttributes();

	$deviceManager->updateDevice($device);
	
}

function processdata($data) {
	global $device;
	updateDevice($data);
	 if($_REQUEST['src'] == 'details') {
		header("Location: /device/view?key=$device->devicekey");
	} else {
		header("Location: /index.php");
	}
}

if ($form->validate()) {
	$form->process('processdata');
} else { ?>
	<div style="width:450px;margin:0 auto;">
		<?php
			createForm();
			$form->display();
		?>
	</div>
<?php } ?>