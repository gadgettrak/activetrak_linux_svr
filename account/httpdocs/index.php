<?php

require_once("bootstrapper.php");
require_once("classes/User.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceConnectionManager.php");
require_once("classes/LicenseManager.php");

Authentication::authenticateSession();

$deviceManager = new DeviceManager();
$deviceResults = $deviceManager->getUserDevices($_SESSION['user']->userid);

$licenseManager = new LicenseManager();
$remainingUSBLicenses = $licenseManager->getRemainingUSBLicenses($_SESSION['user']->userid);
$expiredLicenses = $licenseManager->getExpiredLicensesResults($_SESSION['user']->userid);

function getDeviceType($productId) {
	if($productId == 0) {
		return "label";
	} else if($productId == 1) {
		return "usb";
	} else if($productId == 8) {
		return "iphone";
	} else if($productId == 4 || $productId == 7 || $productId == 10 || $productId == 12 || $productId == 13 || $productId == 14 || $productId == 15) {
		return "laptop";
	}
}

$connectionsArray = array();
foreach($deviceResults->devices as $deviceResult) {
	if($deviceResult->lastConnection) {
		/* @var $deviceConnection DeviceConnection */
		$deviceConnection = $deviceResult->lastConnection;
		array_push($connectionsArray, array(
			"type" => getDeviceType($deviceResult->productid),
			"key" => $deviceResult->devicekey,
			"description" => $deviceResult->description,
			"timestamp" => $deviceConnection->timestamp,
			"lat" => $deviceConnection->ip_lat,
			"lon" => $deviceConnection->ip_lon
		));
	}
}
$connectionsJson = json_encode($connectionsArray);
?>

<?php include("fragments/header.php"); ?>
<!-- PAGE CONTENT BEGIN -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/_js/gmap-label.js"></script>
<script type="text/javascript" src="/_js/statusEver.js"></script>
<script type="text/javascript">
$(function() {

	
	$("#expiredLicenseAlert").effect("pulsate", { times:3 }, 500);

	$('.tracking-link').trackingLink();
	var connections = <?=$connectionsJson?>;

	if(connections.length) {
		initialize(connections);
	} else {
		$("#map_canvas").hide();
	}

	//$("*[style*=-webkit-transform]").css("webkit-transform","");

	$(".tabledata tr").hover(
		function() {
			$this = $(this);
			$this.addClass("row-over");
			var temp = $("#ml_" + $this.attr("id").substring(3));
			temp.css({"color":"#fff", "background-color":"#000"});
			
		},
		function() {
			$this = $(this);
			$this.removeClass("row-over");
			var temp = $("#ml_" + $this.attr("id").substring(3));
			temp.css({"color":"#000", "background-color":"#fff"});
		}
	);

	$('.tracking-link').button({
		icons: {
			primary:'ui-icon-signal-diag'
		}
	});

	$(".viewDetails").button({
		icons: {
			primary:'ui-icon-arrow-4-diag'
		}
	}).removeClass("ui-corner-all").addClass("ui-corner-left");

	$('.deviceMenu').button({
		icons: {
			primary:'ui-icon-triangle-1-e'
		},
		text:false
	}).click(function() {
		$self = $(this);
		$menu = $self.parent().find("ul");
		$(".deviceMenu").parent().find("ul").not($menu).hide();
		$menu.toggle(0);
		return false;
	}).removeClass("ui-corner-all");

	$(".subMenuButton a.viewDetails").removeClass("ui-corner-all").addClass("ui-corner-top").removeClass("ui-corner-left");

	$(".subMenuButton a.editDevice").button({
		icons: {
			primary:'ui-icon-pencil'
		}
	}).removeClass("ui-corner-all");

	$(".subMenuButton a.deleteDevice").button({
		icons: {
			primary:'ui-icon-trash'
		}
	}).removeClass("ui-corner-all").addClass("ui-corner-bottom");

	$('body').click(function() {
		$(".deviceMenu").parent().find("ul").hide();
	});

	$("#utils-menu a").button({
		icons: {
			primary:'ui-icon-document-b'
		}
	});
	
	$("#police-report").hover(function() {
		$(this).stop().animate({opacity:0.5}, 300);
	}, function() {
		$(this).stop().animate({opacity:1}, 300);
	});

});

function initialize(connections) {

	var myLatlng = new google.maps.LatLng(45.5240967,-122.6754679);
	var myOptions = {
		zoom: 18,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		streetViewControl: true
	}

	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	var latlngbounds = new google.maps.LatLngBounds();
	for(var i = 0; i < connections.length; i++) {

		var myLatlng = new google.maps.LatLng(connections[i].lat, connections[i].lon);
		latlngbounds.extend(myLatlng);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:connections[i].description,
			icon: "/_gfx/gmap-icons/" + connections[i].type + ".png",
			clickable:true
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				window.location = "device/view?key=" + connections[i].key;
			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
				$("#dr_" + connections[i].key).addClass("row-over");
				$("#ml_" + connections[i].key).addClass("gmap-label-hover");

			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
				$("#dr_" + connections[i].key).removeClass("row-over");
				$("#ml_" + connections[i].key).removeClass("gmap-label-hover");

			}
		})(marker, i));

		var label = new Label({
			map: map
		});
		label.bindTo('position', marker, 'position');
		label.set('text', (connections[i].description)?connections[i].description:"No Description Available");

		var labelSpan = $(label.span_);
		labelSpan.attr("id", "ml_" + connections[i].key);
	}

	if(connections.length) {
		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);
	}

}
</script>


<div>
	<div style="float:right;">
		<div id="active-devices">
			<p><span class="total-active-devices"><?=$deviceResults->activeDevices ?></span></p>
		</div>
	</div>
	<div style="float:right">
		<div id="police-report">
			<a href="http://www.gadgettrak.com/recovery">File police report</a>
		</div>
	</div>
	<div style="float:right;">
		<?php if(count($expiredLicenses)) { ?>
		<a href="/license/expired/" id="expiredLicenseAlert">Expired Licenses</a>
		<?php } ?>
	</div>
	<h2 style="float:left;">Dashboard</h2>
</div>

<div style="clear:both;"></div>

<div id="lastKnown">
<h3>Last known locations</h3>

<?php if(count($connectionsArray) > 0) { ?>
	<div id="map_canvas" style="width: 100%; height: 350px;"></div><br/>
<?php } else { ?>
	<p>None of your registered devices have made any tracking updates.</p>
<?php } ?>
</div>

<h3>
	<ul id="utils-menu" class="mobile_hidden">
		<li><a title="Add Lost and Found Tag" href="/device/create?p=0" class="lbOn">Add Lost and Found Tag</a></li>
		<?php if($remainingUSBLicenses > 0) {?>
			<li><a title="Add USB Device" href="/device/create?p=1" class="lbOn">Add USB Device</a></li>
		<?php } ?>
		<div class="clear"></div>
	</ul>
	<div style="float:left;">Your devices</div>
	<div class="clear"></div>
</h3>

<?php

if(count($deviceResults->devices) > 0) {

?>

<table class="tabledata" id="dash-devices">
	<thead>
	<tr>
		<th class="ui-corner-tl">&nbsp;</th>
		<th>Description</th>
		<th class="mobile_hidden">Manufacturer</th>
		<th class="mobile_hidden">Model</th>
		<!--
		<th>License Key</th>
		<th>Time until expiration</th>
		-->
		<th class="mobile_hidden">Tracking</th>
		<th class="ui-corner-tr">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($deviceResults->devices as $deviceResult) {?>
		<tr id="dr_<?=$deviceResult->devicekey?>" class="<?php if($deviceResult->theft_status=="Y"){ print("tracking-active");}?>">
			<td class="icon">

				<?if($deviceResult->productid == 0) {?>
					<img src="/_gfx/icon-label.gif" alt="GadgetTrak Label" title="GadgetTrak Label" />
				<?} else if($deviceResult->productid == 1) {?>
					<img src="/_gfx/icon-usb.png" alt="GadgetTrak USB" title="GadgetTrak USB"/>
				<?} else if($deviceResult->productid == 8) {?>
					<img src="/_gfx/icon-iphone.png" alt="GadgetTrak iPhone" title="GadgetTrak iPhone"/>
				<?} else if($deviceResult->productid == 4 || $deviceResult->productid == 7 || $deviceResult->productid == 10 || $deviceResult->productid == 12 || $deviceResult->productid == 13 || $deviceResult->productid == 14 || $deviceResult->productid == 15) {?>
					<img src="/_gfx/icon-laptop.png" alt="GadgetTrak for Laptops" title="GadgetTrak for Laptops"/>
				<?}?>
			</td>
			<td><?=$deviceResult->description?></td>
			<td class="mobile_hidden"><?=$deviceResult->manufacturer?></td>
			<td class="mobile_hidden"><?=$deviceResult->model?></td>
			<!--
			<td><?=$deviceResult->key?></td>
			<td>
				<? if($timeRemaining['years']){print("$timeRemaining[years] years");}?> <? if($timeRemaining['months']){print("$timeRemaining[months] months");}?> <? if($timeRemaining['days']){print("$timeRemaining[days] days");}?>
				<?php
				if(!$timeRemaining || (
					$timeRemaining['years'] == 0 &&
					$timeRemaining['months'] == 0 &&
					$timeRemaining['days'] == 0)) {
					print("<strong>This license is expired</strong>");
				}
				?>
			</td>
			-->
			<td class="mobile_hidden">
			<?php if($deviceResult->productid != 0) {?>
				<a title="Change Device Tracking Status" class="tracking-link" href="status.php?s=<?=$deviceResult->theft_status?>&id=<?=$deviceResult->devicekey?>"><?=$tracking?></a>
			<?php }?>
			</td>
			<td>
				<ul class="buttonSet">
					<li><a class="viewDetails" title="viewDetails" href="/device/view?key=<?=$deviceResult->devicekey?>">View</a></li>
					<li class="subMenuButton"><a class="deviceMenu" title="menu" href="#">Menu</a>
						<ul>
							<li><a class="viewDetails" title="viewDetails" href="/device/view?key=<?=$deviceResult->devicekey?>">View</a></li>
							<li><a class="lbOn editDevice" title="Edit Device" href="/device/edit?key=<?=$deviceResult->devicekey?>">Edit</a></li>
							<li><a class="lbOn deleteDevice" title="Delete Device" href="/device/delete?&deviceKey=<?=$deviceResult->devicekey?>">Delete</a></li>
						</ul>
					</li>
				</ul>
			</td>
		</tr>
	<?} ?>
	</tbody>
</table>

<?php } else { ?>

<p>There are no devices currently registered to this account.</p>

<?php } ?>

<!-- PAGE CONTENT END -->
<?php include("fragments/footer.php"); ?>
