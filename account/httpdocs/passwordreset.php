<?
ini_set( 
  'include_path', 
  ini_get( 'include_path' ) . PATH_SEPARATOR . "/home/kwestin/pear/php"
); 


require_once('HTML/QuickForm.php');
include("database.php");
require_once('Mail.php');
require_once('Mail/mime.php');
$form=new HTML_QuickForm('passwordsend');
$form2=new HTML_QuickForm('passwordreset');

function emailexists($element_name,$element_value) {
global $conn;
global $test;
if(!get_magic_quotes_gpc()){
      $usermail = addslashes($element_value);
   }
   $q = "SELECT email FROM users WHERE email = '$usermail'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) > 0){
   return true;
   }else{
   return false;
   }
}

function keycheck($key) {
global $conn;

if(!get_magic_quotes_gpc()){
      $key = addslashes($key);
   }
   $q = "SELECT email_key FROM users WHERE email_key = '$key'";
   $result = mysql_query($q,$conn) or die("query error");
   if (mysql_numrows($result) > 0){
   return true;
   }else{
   return false;;
   } 
}

function sendemail($data){
global $conn;
$q = "SELECT email_key FROM users WHERE email = '". $data['email']."'";

$result = mysql_query($q,$conn);
$keyrec=mysql_fetch_array($result);
$key=$keyrec['email_key'];

$to=$data['email'];
$bdy=<<<PTEXT
Hello, someone has requested that your password be reset at gadgettrak.com, probably you.  The next step to reset your password 
is to verify your email address by clicking on the link below. You will then be taken to a page where you can change your password. 

https://account.gadgettrak.com/passwordreset.php?key={$key} 


PTEXT;

$headers['From'] = 'accounts@gadgettrak.com';
$headers['Subject'] = 'gadgettrak.com Password Reset';
$message = &Mail::factory('mail');
$message->send($to, $headers, $bdy);

print "<p> An email has been sent to you to verify your password change request. Please click on the link in the email, you will then be able to enter a new password for your account. </p>";
}

//create form 1
$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');
$form->addElement('submit', null, 'Submit', array('class' => "submit"));

$form->registerRule('emailexists','function','emailexists');
$form->addRule('email','The email you entered does not exist in our database', 'emailexists'); 

//create form 2
$form2->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$form2->addRule('password', 'Please enter a password', 'required', null, 'client');
$form2->addElement ('hidden', 'key', $_GET['key']);
$form2->addElement('submit', null, 'Submit', array('class' => "submit"));



function processdata1($data){
sendemail($data);
}

function processdata2($data){
global $conn;
$password=sha1($data['password']); //encrypt password
$devicekey=$data['key'];
$updatesql="UPDATE users SET password='$password' WHERE email_key='$devicekey'";
mysql_query($updatesql, $conn) or die(mysql_error()); 
print "<p> Your password has been reset, <a href=\"login.php\">continue to login</a>. ";
//reset password
}

include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>

<div class="regform">
<?
if(keycheck($_GET['key'])) {
print "<h2> Password Reset</h2>";
print "<p> Please enter your new password</p>";
$form2->display();	
}else if (!($form->isSubmitted()) &&!($form2->isSubmitted()) ){
print "<h2> Change Password</h2>";
$form->display();
}
?>


<?

	if ($form->isSubmitted() && $form->validate()) {// if the form validates?
		$form->process('processdata1');
		}else if($form2->isSubmitted() && $form2->validate()){
		$form2->process('processdata2');
		}
		
?>
<p>If you have any problems with resetting your password please contact <a href=\"mailto:support@gadgettrak.com\">support@gadgettrak.com</a></p>
</div>
<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>

