<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "View License";
$pageHeader = "View License";
/***************************************************************/

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/LicenseManager.php");

$licenseManager = new LicenseManager();
$licenseKey = $_GET['key'];
$license = $licenseManager->getLicenseByKey($licenseKey);
?>

<?php if($license) {?>
<h3>License Data</h3>
<table class="data bordered">
	<tbody>
		<tr>
			<th>id</th>
			<td><?=$license->id?></td>
		</tr>
		<tr>
			<th>userId</th>
			<td><a href="<?=ADMIN_ROOT?>/users/view?id=<?=$license->userId?>"><?=$license->userId?></a></td>
		</tr>
		<tr>
			<th>deviceId</th>
			<td><a href="<?=ADMIN_ROOT?>/devices/view?id=<?=$license->deviceId?>"><?=$license->deviceId?></a></td>
		</tr>
		<tr>
			<th>productId</th>
			<td><?=$license->productId?></td>
		</tr>
		<tr>
			<th>resellerId</th>
			<td><?=$license->resellerId?></td>
		</tr>
		<tr>
			<th>length</th>
			<td><?=$license->length?></td>
		</tr>
		<tr>
			<th>renewals</th>
			<td><?=$license->renewals?></td>
		</tr>
		<tr>
			<th>licenses</th>
			<td><?=$license->licenses?></td>
		</tr>
		<tr>
			<th>key</th>
			<td><?=$license->key?></td>
		</tr>
		<tr>
			<th>used</th>
			<td><?=$license->used?></td>
		</tr>
		<tr>
			<th>activated</th>
			<td><?=$license->activated?></td>
		</tr>
		<tr>
			<th>nfr</th>
			<td><?=$license->nfr?></td>
		</tr>
		<tr>
			<th>os</th>
			<td><?=$license->os?></td>
		</tr>
	</tbody>
</table>

<div class="buttonBar"><a href="<?=ADMIN_ROOT?>/licenses/edit?key=<?=$license->key?>" class="modalEdit">Edit License</a></div>

<?php
} else {
	print("No license found");
}
?>

<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
$pagemaincontent = ob_get_contents();
ob_end_clean();
require_once("__admin/master.php");
/***************************************************************/
?>