<?php
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/LicenseManager.php");

$licenseManager = new LicenseManager();
$licenseKey = $_REQUEST['key'];
$license = $licenseManager->getLicenseByKey($licenseKey);

$form = new FormStyler('edit');
$form->form->addElement("hidden", "key", $license->key);
$form->form->addElement("text", "productId", "productId:", array("value" => "$license->productId", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "resellerId", "resellerId:", array("value" => "$license->resellerId", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "length", "length:", array("value" => "$license->length", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "renewals", "renewals:", array("value" => "$license->renewals", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "licenses", "licenses:", array("value" => "$license->licenses", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "used", "used:", array("value" => "$license->used", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "activated", "activated:", array("value" => "$license->activated", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "nfr", "nfr:", array("value" => "$license->nfr", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "os", "os:", array("value" => "$license->os", "size" => 30, "maxlength" => 255));
$form->form->addElement("submit", null, "Submit", array("class" => "submit"));

function processData($data) {

	global $license, $licenseManager;

	$license->productId = $data['productId'];
	$license->resellerId = $data['resellerId'];
	$license->length = $data['length'];
	$license->licenses = $data['licenses'];
	$license->used = $data['used'];
	$license->activated = $data['activated'];
	$license->nfr = $data['nfr'];
	$license->os = $data['os'];
	$license->renewals = $data['renewals'];

	$licenseManager->updateLicense($license);

	$redirect = ADMIN_ROOT . "/licenses/view?key=$license->key";
	header("Location: $redirect");

}

if ($form->form->validate()) {
	$form->form->process('processdata');
} else { ?>
	<div style="width:450px;margin:0 auto;">

		<?php
		if($license->length != -1) {
			$utc = new DateTimeZone("GMT");
			$datetime1 = new DateTime($license->activated, $utc);
			$datetime1->add(new DateInterval("P" . $license->length . "M"));
			$datetime2 = new DateTime(null, $utc);
			$interval = $datetime1->diff($datetime2);
		?>
			<p style="background-color:#f0f0f0;border:1px solid #ccc;padding:10px;margin:20px 0;border-radius:8px;"><strong>Date difference between expiration and today</strong><br/><?= $interval->format('%R%y Years %m Months %d Days')?></p>
		<?php } ?>
		<?php
			$form->form->display();
		?>
	</div>

<script type="text/javascript">


	$("input[name=activated]").datepicker({
		dateFormat: 'yy-mm-dd'
	});


</script>

<?php } ?>