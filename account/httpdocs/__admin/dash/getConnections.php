<?php

require_once("bootstrapper.php");
require_once("classes/AdminDeviceConnectionManager.php");

$connectionManager = new AdminDeviceConnectionManager();
$connections = $connectionManager->getAllConnections("archive", 4000);

print(json_encode($connections));

?>