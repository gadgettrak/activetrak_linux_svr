function ConnectionEarth(domId, options) {

	this.bars = [];
	this.plotNextTimeoutId = 0;
	this.dataGetInterval = 1;
	this.time = 60 * this.dataGetInterval;
	this.currentPoint = this.lastPoint = new Coordinate(0,0,"0","0");
	this.eventListeners = [];

	if(options) {
		this.options = $.extend({}, this.options, options);
	}

	var self = this;
	var initCallback = function(object)
	{
		self.earth = object;
		self.earth.getWindow().setVisibility(true);
		var options = self.earth.getOptions();
		options.setAtmosphereVisibility(true);
		self.rotate();
		self.getNextDataset();
	}
	var failureCallback = function(){}

	google.earth.createInstance(domId, initCallback, failureCallback);

}

ConnectionEarth.EARTH_RADIUS_EQUATOR = 6378140.0;
ConnectionEarth.RADIAN = 180 / Math.PI;

ConnectionEarth.prototype.options = {
	bottomBarColor: 'ff3222db',
	topBarColor:'ff00ffff',
	maxBars:400,
	cameraDistance:17600000,
	rotationAmount:0.3,
	rotationFrequency:50
};

ConnectionEarth.prototype.addEventListener = function(eventType, callback, scope) {
	this.eventListeners.push({type:eventType, callback:callback, scope:scope});
};

ConnectionEarth.prototype.triggerDataLoaded = function() {
	for(var i = 0; i < this.eventListeners.length; i++) {
		if(this.eventListeners[i].type == "dataLoaded") {
			this.eventListeners[i].callback.apply(this.eventListeners[i].scope,[this.data]);
		}
	}
};

ConnectionEarth.prototype.getNextDataset = function() {

	clearTimeout(this.plotNextTimeoutId);
	//console.log("getNextDataset called")
	var self = this;
	var d = new Date();

	$.getJSON("http://ip.gadgettrak.com/map/getConnections.php?callback=?", null, function(data) {
		self.data = data.reverse();
		self.plotPoints();
		self.triggerDataLoaded();
	});

	setTimeout(function() {
		self.time = 60 * self.dataGetInterval;
		//console.log("calling getNextDataset");
		self.getNextDataset();
	}, 1000 * 60 * self.dataGetInterval);

 };

ConnectionEarth.prototype.plotPoints = function()
{
	var self = this;
	var coord = this.getNextPoint();
	if(coord) {
		this.plotPoint(coord);
		var nextTime = this.getNextSpawnTime();
		this.plotNextTimeoutId = setTimeout(function()
		{
			self.plotPoints();
		}, nextTime);
	} else {
		//console.log("NO POINTS!");
	}
 };

ConnectionEarth.prototype.getNextPoint = function()
{
	var newCoord = this.data.pop();
	if(!newCoord) return;
	this.currentPoint = new Coordinate(parseFloat(newCoord.lat), parseFloat(newCoord.lon), newCoord.ip, newCoord.date);
	if(this.data.length && this.currentPoint.equals(this.lastPoint)) {
		this.currentPoint = this.getNextPoint();
	}
	return this.currentPoint;
};

ConnectionEarth.prototype.getNextSpawnTime = function()
{
	if(!this.data.length) return 0;
	var nextCoord = this.data[this.data.length -1];
	b = mysqlTimeStampToDate(nextCoord.date);
	return b-this.currentPoint.date;
}

ConnectionEarth.prototype.plotPoint = function(coord)
{

	var alt = (Math.random() * 1200000) + 300000;
	var wid = Math.random() * 40000 + 3000;

	var barBottom = this.createPolygonBar(coord.lon, coord.lat, 95000, 4, 159000, this.options.bottomBarColor);
	var barTop = this.createPolygonBar(coord.lon, coord.lat, 30000, 4, alt, this.options.topBarColor);

	if(this.bars.length > this.options.maxBars) {
		var bar = this.bars.shift();
		this.earth.getFeatures().removeChild(bar);
	}
	
	this.earth.getFeatures().appendChild(barBottom);
	this.earth.getFeatures().appendChild(barTop);
	this.bars.push(barBottom);
	this.bars.push(barTop);
	this.lastPoint = this.currentPoint
}

ConnectionEarth.prototype.rotate = function()
{
	this.earth.getOptions().setFlyToSpeed(this.earth.SPEED_TELEPORT);
	var lookAt = this.earth.getView().copyAsLookAt(this.earth.ALTITUDE_RELATIVE_TO_GROUND);
	lookAt.setRange(this.options.cameraDistance);
	var lat = lookAt.getLongitude() - this.options.rotationAmount;
	if(lat <= -180) lat = 180 + (180 + lat);
	lookAt.setLongitude(lat);
	this.earth.getView().setAbstractView(lookAt);
	setTimeout($.proxy( this.rotate, this ), this.options.rotationFrequency);
}

ConnectionEarth.prototype.createPolygonBar = function(longitude, latitude, distance, points, altitude, color)
{

	var f = 1/298.257;
	var e = 0.08181922;

	var lon = longitude / ConnectionEarth.RADIAN;
	var lat = latitude / ConnectionEarth.RADIAN;

	var r = ConnectionEarth.EARTH_RADIUS_EQUATOR * (1 - e * e) / Math.pow((1 - e * e * Math.pow(Math.sin(lat), 2)), 1.5);
	var psi = distance/r;
	var phi = Math.PI/2 - lat;

	var polygonPlacemark = this.earth.createPlacemark('');
	var polygon = this.earth.createPolygon('');

	polygon.setExtrude(true);
	polygon.setAltitudeMode(this.earth.ALTITUDE_ABSOLUTE);
	polygonPlacemark.setGeometry(polygon);

    if (!polygonPlacemark.getStyleSelector()) {
      polygonPlacemark.setStyleSelector(this.earth.createStyle(''));
    }

    var lineStyle = polygonPlacemark.getStyleSelector().getLineStyle();
    lineStyle.setWidth(.1);
    lineStyle.getColor().set('0000ffff');

	var polyColor = polygonPlacemark.getStyleSelector().getPolyStyle().getColor();
	polyColor.set(color)

	var outer = this.earth.createLinearRing('');
	polygon.setOuterBoundary(outer);

	for (var bearing = 45; bearing <= 405; bearing += 360 / points) {
		var b = bearing / ConnectionEarth.RADIAN;
		var arccos = Math.cos(psi) * Math.cos(phi) + Math.sin(psi) * Math.sin(phi) * Math.cos(b);
		var polyLat = (Math.PI/2 - Math.acos(arccos)) * ConnectionEarth.RADIAN;
		var arcsin = Math.sin(b) * Math.sin(psi) / Math.sin(phi);
		var polyLon = (lon - Math.asin(arcsin)) * ConnectionEarth.RADIAN;
		outer.getCoordinates().pushLatLngAlt(polyLat, polyLon, altitude);
	}
	return polygonPlacemark;

}

/**********************************************************************************
Coordinate
**********************************************************************************/

function Coordinate(lat, lon, ip, date)
{
	this.lat = lat;
	this.lon = lon;
	this.ip = ip;
	this.date = mysqlTimeStampToDate(date);
}

Coordinate.prototype.equals = function(coord) {
	return false;
	//return (this.ip == coord.ip);
}

/**********************************************************************************
Helper functions
**********************************************************************************/

function mysqlTimeStampToDate(timestamp)
{
	if(!timestamp) return null;
	var regex = /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
	var parts = timestamp.replace(regex,"$1 $2 $3 $4 $5 $6").split(' ');
	return new Date(parts[0],parts[1]-1,parts[2],parts[3],parts[4],parts[5]);
}