$(function() {

	var ge = null;
	var connectionEarth = null;
	var chart2Data = null;
	var chart2 = null;

	var addPoints = function(num) {

		if(!chart2Data.getNumberOfRows) return;

		if(chart2Data.getNumberOfRows() > 60) {
			chart2Data.removeRow(0);
		}

		var myDate = new Date();

		chart2Data.addRow([myDate.format("h:MM:ss TT"), num]);

		chart2.draw(chart2Data, {
			width: 1160,
			height: 280,
			title: 'Device Requests Per Minute',
			legend: 'none',
			backgroundColor: {
				stroke: '#000',
				strokeWidth:'0',
				fill:'none'
			},
			//curveType: "function",
			hAxis: {
				textStyle: {color:'white', fontSize:'12px'}
			},
			vAxis: {
				textStyle: {color:'white', fontSize:'12px'}
			},
			titleTextStyle: {color:'white', fontSize:'12px'}
		});
	}

	var drawChart2 = function () {
		chart2Data = new google.visualization.DataTable();
		chart2Data.addColumn('string', 'DateTime');
		chart2Data.addColumn('number', 'Requests');
		chart2 = new google.visualization.LineChart(document.getElementById('right-graph-container'));
		initEarth();
	}

	var chart1Data = null;
	var chart1 = null;

	var updateChart1 = function(num) {

		if(!chart1Data.getNumberOfRows) return;

		if(chart1Data.getNumberOfRows() > 60) {
			chart1Data.removeRow(0);
		}

		chart1Data.addRow(["", num]);

		chart1.draw(chart1Data, {
			width: 670,
			height: 280,
			title: 'gt1.gadgettrak.com Response Time (updated every 10 seconds)',
			legend: 'none',
			backgroundColor: {
				stroke: '#000',
				strokeWidth:'0',
				fill:'none'
			},
			//curveType: "function",
			hAxis: {
				textStyle: {color:'white', fontSize:'12px'}
			},
			vAxis: {
				textStyle: {color:'white', fontSize:'12px'}
			},
			titleTextStyle: {color:'white', fontSize:'12px'}
		});
	}

	var drawChart1 = function () {
		chart1Data = new google.visualization.DataTable();
		chart1Data.addColumn('string', 'DateTime');
		chart1Data.addColumn('number', 'ResponseTime');
		chart1 = new google.visualization.LineChart(document.getElementById('left-graph-container'));

		var updateStats = function()
		{
			$.getJSON("getStats.php", function(data) {
				$("#counter-1 p").text(data.laptopPhotos);
				$("#counter-2 p").text(data.iOsPhotos);
				$("#counter-3 p").text(data.devicesTracked);
				$("#counter-4 p").text(data.devicesTotal);
				updateChart1(data.responseTime);
				setTimeout(updateStats, 10000);
			});
		}
		setTimeout(updateStats, 10);
	}

	google.setOnLoadCallback(drawChart2);
	google.setOnLoadCallback(drawChart1);

	var initEarth = function()
	{
		connectionEarth = new ConnectionEarth("earth");
		connectionEarth.addEventListener("dataLoaded", function(data) {
			addPoints(data.length);
		}, window);
	}


});