<?php

$starttime = microtime(true);
$file      = fsockopen ("gt1.gadgettrak.com", 80, $errno, $errstr, 10);
$stoptime  = microtime(true);
$status    = 0;

if (!$file) $status = -1;  // Site is down
else {
	fclose($file);
	$status = ($stoptime - $starttime) * 1000;
	$status = floor($status);
}

require_once("bootstrapper.php");
require_once("__admin/Stats.php");
$stats = new Stats();
$statCounts = $stats->getStatCounts();
$statCounts->responseTime = $status;
print(json_encode($statCounts));
?>