<?php

require_once("bootstrapper.php");
require_once("classes/AdminDeviceConnectionManager.php");


$connectionManager = new AdminDeviceConnectionManager();
$connections = $connectionManager->getAllConnections("archive", 600);

$doc = new DomDocument('1.0');
$root = $doc->createElement('kml');
$root->setAttribute("xmlns", "http://www.opengis.net/kml/2.2");
$root = $doc->appendChild($root);
$document = $doc->createElement("Document");
$root->appendChild($document);

$Style = $doc->createElement("Style");
$LineStyle = $doc->createElement("LineStyle");
$color = $doc->createElement("color");

$PolyStyle = $doc->createElement("PolyStyle");
$outline = $doc->createElement("outline");
$fill = $doc->createElement("fill");

$Style->setAttribute("id", "defaultStyles");
$color->appendChild($doc->createTextNode("cc000000"));
$LineStyle->appendChild($color);
$Style->appendChild($LineStyle);

$outline->appendChild($doc->createTextNode("0"));
$PolyStyle->appendChild($outline);

$fill->appendChild($doc->createTextNode("1"));
$PolyStyle->appendChild($fill);

$Style->appendChild($PolyStyle);


$document->appendChild($Style);

function kmlSymbolCalculator( $longitude, $latitude, $distance, $points, $altitude )
{
	$EARTH_RADIUS_EQUATOR = 6378140.0;
	$RADIAN = 180 / pi();

	$long = $longitude;
	$lat = $latitude;

	$long = $long / $RADIAN;
	$lat = $lat / $RADIAN;
	$f = 1/298.257;
	$e = 0.08181922;

	$kml = '';

	//for ( $bearing = 0; $bearing <= 360; $bearing += 360/$points) {
	// Changed start bearing beacuse of square orientation
	for ( $bearing = 45; $bearing <= 405; $bearing += 360/$points) {

		$b = $bearing / $RADIAN;

		$R = $EARTH_RADIUS_EQUATOR * (1 - $e * $e) / pow( (1 - $e*$e * pow(sin($lat),2)), 1.5);
		$psi = $distance/$R;
		$phi = pi()/2 - $lat;
		$arccos = cos($psi) * cos($phi) + sin($psi) * sin($phi) * cos($b);
		$latA = (pi()/2 - acos($arccos)) * $RADIAN;

		$arcsin = sin($b) * sin($psi) / sin($phi);
		$longA = ($long - asin($arcsin)) * $RADIAN;

		$kml .= " ".round($longA, 2).",".round($latA, 2);
		if ($altitude) $kml .= ",".$altitude;
	}

	return $kml;
}


$index = 0;

foreach($connections as $connection) {
	
	
	$pm = $doc->createElement("Placemark");
	$document->appendChild($pm);

	/*
		<Style>
			<PolyStyle>
				<color>dc0155ff</color>
			</PolyStyle>
		</Style>
	*/

	$name = $doc->createElement("name");
	$name->appendChild($doc->createTextNode("Poop" .$index++));
	$pm->appendChild($name);

	$styleUrl = $doc->createElement("styleUrl");
	$styleUrl->appendChild($doc->createTextNode("#defaultStyles"));
	$pm->appendChild($styleUrl);




	$Style = $doc->createElement("Style");
	$PolyStyle = $doc->createElement("PolyStyle");
	$color = $doc->createElement("color");
	$color->appendChild($doc->createTextNode("8000CCFF"));
	$PolyStyle->appendChild($color);
	$Style->appendChild($PolyStyle);
	$pm->appendChild($Style);



	$Polygon = $doc->createElement("Polygon");
	$outerBoundaryIs = $doc->createElement("outerBoundaryIs");
	$LinearRing = $doc->createElement("LinearRing");
	$coordinates = $doc->createElement("coordinates");

	$extrude = $doc->createElement("extrude");
	$altitudeMode = $doc->createElement("altitudeMode");
	$tessellate = $doc->createElement("tessellate");


	$extrude->appendChild($doc->createTextNode("1"));
	$Polygon->appendChild($extrude);

	$tessellate->appendChild($doc->createTextNode("1"));
	$Polygon->appendChild($tessellate);

	$altitudeMode->appendChild($doc->createTextNode("absolute"));
	$Polygon->appendChild($altitudeMode);

	//$test = "-98.61000000000001,40.07,1740157 -98.79000000000001,40.05,1740157 -98.95,39.99,1740157 -99.08,39.89,1740157 -99.16,39.76,1740157 -99.19,39.62000000000001,1740157 -99.16,39.48,1740157 -99.08,39.36,1740157 -98.95,39.26,1740157 -98.79000000000001,39.19,1740157 -98.61000000000001,39.17,1740157 -98.43000000000001,39.19,1740157 -98.26000000000002,39.26,1740157 -98.13000000000001,39.36,1740157 -98.05,39.48,1740157 -98.02,39.62000000000001,1740157 -98.05,39.76,1740157 -98.13000000000001,39.89,1740157 -98.26000000000002,39.99,1740157 -98.43000000000001,40.05,1740157 -98.61000000000001,40.07,1740157";
	//$coordsValue = $doc->createTextNode($test);

	$coordsValue = $doc->createTextNode(kmlSymbolCalculator($connection->ip_lon, $connection->ip_lat, 20500, 4, 1195000));


	$coordinates->appendChild($coordsValue);

	$LinearRing->appendChild($coordinates);
	$outerBoundaryIs->appendChild($LinearRing);
	$Polygon->appendChild($outerBoundaryIs);



	$pm->appendChild($Polygon);



}

$xml_string = $doc->saveXML();
print(trim($xml_string));

?>