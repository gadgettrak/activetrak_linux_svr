<html>

<head>
	<title>GadgetTrak Dashboard</title>
	<link rel="stylesheet" href="main.css" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	<!--<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAgtrBY2_35_P3L1TwtIQMMxT9aTZRuB7deHJ97o99wMfv0M08VRSy4vHcTCw4TD9wSFIJb9iRts_AXA"></script>-->
	<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAQj2F7lcM1iuqjkDEfYQfbRQNnckFG7x0eLqsu9AqSFqpS2zinhRRKZOQ5KUyIdYgyAOplhIwNnf0Dg"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript" src="connectionEarth.js"></script>
	<script type="text/javascript" src="date.format.js"></script>
	<script type="text/javascript">
		google.load("earth", "1");
		google.load("visualization", "1", {packages:["corechart"]});
	</script>
	<script type="text/javascript" src="earth.js"></script>
	<script src="http://widgets.twimg.com/j/2/widget.js"></script>
</head>

<body>

	<div id="header" class="rounded-8"></div>
	<div id="main" class="clearfix">
		<div id="left-column">
			<div id="tweet-container" class="rounded-8">

				<div id="tweets">
					<script type="text/javascript">
					new TWTR.Widget({
							version: 2,
							type: 'search',
							search: 'gadgettrak OR activetrak',
							interval: 6000,
							title: 'GadgetTrak',
							subject: 'Twitter feed',
							width: 670,
							height: 230,
							theme: {
								shell: {
									background: 'none',
									color: '#ffffff'
								},
									tweets: {
									background: '#111',
									color: '#ffffff',
									links: '#cccccc'
								}
							},
							features: {
								scrollbar: false,
								loop: true,
								live: true,
								hashtags: true,
								timestamp: true,
								avatars: true,
								toptweets: true,
								behavior: 'default'
							}
						}).render().start();
					</script>
				</div>
			</div>

			<div id="counter-container" class="clearfix">
				<div id="counter-1" class="rounded-8 counter-box">
					<p></p>
					<h4>Laptop Photos Taken</h4>
					<h5>as of April 1st, 2011</h5>
				</div>
				<div id="counter-2" class="rounded-8 counter-box">
					<p></p>
					<h4>iOS Photos Taken</h4>
				</div>
				<div id="counter-3" class="rounded-8 counter-box">
					<p></p>
					<h4>Devices Tracked</h4>
					<h5>as of April 1st, 2011</h5>
				</div>
				<div id="counter-4" class="rounded-8 counter-box">
					<p></p>
					<h4>Devices Protected</h4>
				</div>
			</div>
			<div id="left-graph-container" class="rounded-8"></div>
		</div>

		<div id="right-column" class="clearfix">
			<div id="earth-container">
				<div id="earth"></div>
			</div>
			<div id="right-graph-container" class="rounded-8"></div>
		</div>

	</div>

</body>

</html>