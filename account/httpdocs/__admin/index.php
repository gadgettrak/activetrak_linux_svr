<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
require_once("__admin/Stats.php");

Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "";
$pageHeader = "Greetings earthlings";


$stats = new Stats();

$fromDate = "2010-07-29";
$newDevices = $stats->getNumRegisteredDevices($fromDate);
$newLicenses = $stats->getActivatedLicenses($fromDate);

$devicesData = $stats->getRegisteredDevicesByMonthSummary();
//$devicesData = $stats->getRegisteredDevicesByDaySummary();

$laptopsData = $stats->getRegisteredLaptopsByMonthSummary();

$licensesData = $stats->getActivatedLicensesByMonthSummary();

$dailyDeviceData = $stats->getLatestDeviceStats();
$dailyUserData = $stats->getLatestUserStats();
/***************************************************************/
?>

<style type="text/css">
	.highcharts-container {
		border-top:1px solid #ccc !important;
	}
</style>

<script type="text/javascript">


	$(document).ready(function() {

		var chart1 = new Highcharts.Chart({
			chart: {
				renderTo: 'chart-container-1',
				defaultSeriesType: 'area'
				
			},
			title: {
				text: 'Devices registrations by month',
				align: 'left'
			},
			xAxis: {
				categories: <? print(json_encode($devicesData['labels']))?>,
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						font: 'normal 8px Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				title: {
					text: null
				},
				style: {
					width:'1px'
				}

			},
			tooltip: {
				enabled: true,
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+
						this.x +' | '+ this.y;
				}
			},
			plotOptions: {
				spline: {
					lineWidth: 1,
					marker: {
						enabled: false,
						states: {
							hover: {
								enabled: true,
								radius: 5
							}
						}
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1
						}
					}
				},
				area: {
					lineWidth: 1,
					marker: {
						enabled: false,
						states: {
							hover: {
								enabled: true,
								radius: 5
							}
						}
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1
						}
					}
				}
			},
			legend: {
				enabled:false
			},
			series: [{
					name: 'Devices',
					data: <? print(json_encode($devicesData['values']))?>
				}]
		});

		var chart2 = new Highcharts.Chart({
			chart: {
				renderTo: 'chart-container-2',
				defaultSeriesType: 'area'
			},
			title: {
				text: 'License activations by month',
				align: 'left'
			},
			xAxis: {
				categories: <? print(json_encode($licensesData['labels']))?>,
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						font: 'normal 8px Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				title: {
					text: null
				}
			},
			tooltip: {
				enabled: true,
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+
						this.x +' | '+ this.y;
				}
			},
			plotOptions: {
				spline: {
					lineWidth: 1,
					marker: {
						enabled: false,
						states: {
							hover: {
								enabled: true,
								radius: 5
							}
						}
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1
						}
					}
				},
				area: {
					lineWidth: 1,
					marker: {
						enabled: false,
						states: {
							hover: {
								enabled: true,
								radius: 5
							}
						}
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1
						}
					}
				}
			},
			legend: {
				enabled:false
			},
			series: [{
					name: 'Licenses',
					data: <? print(json_encode($licensesData['values']))?>
				}]
		});

		var chart3 = new Highcharts.Chart({
			chart: {
				renderTo: 'chart-container-3',
				defaultSeriesType: 'area'
			},
			title: {
				text: 'Laptop registrations by month',
				align: 'left'
			},
			xAxis: {
				categories: <? print(json_encode($laptopsData['labels']))?>,
				labels: {
					rotation: -90,
					align: 'right',
					style: {
						font: 'normal 8px Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				title: {
					text: null
				}
			},
			tooltip: {
				enabled: true,
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+
						this.x +' | '+ this.y;
				}
			},
			plotOptions: {
				spline: {
					lineWidth: 1,
					marker: {
						enabled: false,
						states: {
							hover: {
								enabled: true,
								radius: 5
							}
						}
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1
						}
					}
				},
				area: {
					lineWidth: 1,
					marker: {
						enabled: false,
						states: {
							hover: {
								enabled: true,
								radius: 5
							}
						}
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1
						}
					}
				}
			},
			legend: {
				enabled:false
			},
			series: [{
					name: 'Laptops',
					data: <? print(json_encode($laptopsData['values']))?>
				}]
		});


	var sDate = new Date ((new Date("<?= $dailyDeviceData['startDate']?>")).toUTCString());

	var chart4 = new Highcharts.Chart({
      chart: {
         renderTo: 'chart-container-4',
         zoomType: 'x',
         spacingRight: 20,
		height:300
      },
       title: {
		align: "l",
         text: 'Daily iPhone Registrations'
      },
	subtitle: {
         text: document.ontouchstart === undefined ?
            'Click and drag in the plot area to zoom in' :
            'Drag your finger over the plot to zoom in'
      },
      xAxis: {
         type: 'datetime',
         maxZoom: 14 * 24 * 3600000, // fourteen days
         title: {
            text: null
         }
      },
      yAxis: {
         title: {
            text: null
         },
         min: 0.0,
         startOnTick: false,
         showFirstLabel: false
      },
      tooltip: {
         shared: true
      },
      legend: {
         enabled: false
      },
      plotOptions: {
         area: {
            lineWidth: 1,
            marker: {
               enabled: false,
               states: {
                  hover: {
                     enabled: true,
                     radius: 5
                  }
               }
            },
            shadow: false,
            states: {
               hover: {
                  lineWidth: 1
               }
            }
         }
      },

      series: [{
         type: 'area',
         name: 'iPhones',
         pointInterval: 24 * 3600 * 1000,
         pointStart: Date.UTC(sDate.getUTCFullYear(), sDate.getUTCMonth(), sDate.getUTCDate()),
         data: <? print(json_encode($dailyDeviceData['iPhoneData']))?>
      }]
	  
   });

	var chart5 = new Highcharts.Chart({
      chart: {
         renderTo: 'chart-container-5',
         zoomType: 'x',
         spacingRight: 20,
		height:300
      },
       title: {
		align: "l",
         text: 'Daily Laptop Registrations'
      },
	subtitle: {
         text: document.ontouchstart === undefined ?
            'Click and drag in the plot area to zoom in' :
            'Drag your finger over the plot to zoom in'
      },
      xAxis: {
         type: 'datetime',
         maxZoom: 14 * 24 * 3600000, // fourteen days
         title: {
            text: null
         }
      },
      yAxis: {
         title: {
            text: null
         },
         min: 0.0,
         startOnTick: false,
         showFirstLabel: false
      },
      tooltip: {
         shared: true
      },
      legend: {
         enabled: false
      },
      plotOptions: {
         area: {
            lineWidth: 1,
            marker: {
               enabled: false,
               states: {
                  hover: {
                     enabled: true,
                     radius: 5
                  }
               }
            },
            shadow: false,
            states: {
               hover: {
                  lineWidth: 1
               }
            }
         }
      },

      series: [{
         type: 'area',
         name: 'Laptops',
         pointInterval: 24 * 3600 * 1000,
         pointStart: Date.UTC(sDate.getUTCFullYear(), sDate.getUTCMonth(), sDate.getUTCDate()),
         data: <? print(json_encode($dailyDeviceData['laptopData']))?>
      }]

   });

	var sDate2 = new Date ((new Date("<?= $dailyUserData['startDate']?>")).toUTCString());


	var chart6 = new Highcharts.Chart({
      chart: {
         renderTo: 'chart-container-6',
         zoomType: 'x',
         spacingRight: 20,
		height:300
      },
       title: {
		align: "l",
         text: 'Daily User Registrations'
      },
	subtitle: {
         text: document.ontouchstart === undefined ?
            'Click and drag in the plot area to zoom in' :
            'Drag your finger over the plot to zoom in'
      },
      xAxis: {
         type: 'datetime',
         maxZoom: 14 * 24 * 3600000, // fourteen days
         title: {
            text: null
         }
      },
      yAxis: {
         title: {
            text: null
         },
         min: 0.0,
         startOnTick: false,
         showFirstLabel: false
      },
      tooltip: {
         shared: true
      },
      legend: {
         enabled: false
      },
      plotOptions: {
         area: {
            lineWidth: 1,
            marker: {
               enabled: false,
               states: {
                  hover: {
                     enabled: true,
                     radius: 5
                  }
               }
            },
            shadow: false,
            states: {
               hover: {
                  lineWidth: 1
               }
            }
         }
      },

      series: [{
         type: 'area',
         name: 'Users',
         pointInterval: 24 * 3600 * 1000,
         pointStart: Date.UTC(sDate2.getUTCFullYear(), sDate2.getUTCMonth(), sDate2.getUTCDate()),
         data: <? print(json_encode($dailyUserData['data']))?>
      }]

   });

	});

</script>
 
<table class="bordered">
	<tbody>
		<tr>
			<th>Registered devices since <?=$fromDate?></th>
			<td><?=$newDevices?></td>
		</tr>
		<tr>
			<th>Activated licenses since <?=$fromDate?></th>
			<td><?=$newLicenses?></td>
		</tr>
	</tbody>
</table>

<div id="chart-container-6" style="width:100%; margin-top:20px;"></div>
<div id="chart-container-5" style="width:100%; margin-top:20px;"></div>
<div id="chart-container-4" style="width:100%; margin-top:20px;"></div>
<div id="chart-container-3" style="width:100%; margin-top:20px;"></div>
<div id="chart-container-2" style="width:100%; margin-top:20px;"></div>
<div id="chart-container-1" style="width:100%; margin-top:20px;"></div>


<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>