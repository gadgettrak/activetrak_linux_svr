<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
require_once("classes/Utils.php");

Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Errors";
$pageHeader = "Errors";
/***************************************************************/

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/ErrorManager.php");
require_once("classes/Error.php");
require_once("classes/QueryFilterList.php");
require_once("classes/Paginator.php");
require_once("classes/HttpUtils.php");

$queryFilterList = new QueryFilterList();
$filterVars = array("email", "deviceKey", "password");
foreach($filterVars as $filterVar) {
	if(isset($_GET[$filterVar . "_a"]) && isset($_GET[$filterVar . "_v"]) && isset($_GET[$filterVar . "_o"])) {
		$varName = $filterVar . "_v";
		$$varName = $_GET[$filterVar . "_v"];
		$$varName = HttpUtils::cleanOutput($$varName);
		HttpUtils::cleanInput($fv = $_GET[$filterVar . "_v"]);
		HttpUtils::cleanInput($fo = $_GET[$filterVar . "_o"]);
		$queryFilterList->addFilter($filterVar, $fv, $fo);
	}
}

//Set up query sorting
HttpUtils::cleanInput($sortField = trim($_GET['sortField']));
HttpUtils::cleanInput($sortDirection = trim($_GET['sortDirection']));
if(!is_null($sortField) && $sortField != "" && !is_null($sortDirection) && $sortDirection != "") {
	$sort = array("field"=>$sortField, "direction"=>$sortDirection);
}

//Set up paging pariables
$pageNum = (isset($_GET['pageNum']) && ((int)$_GET['pageNum'] > 0))?(int)$_GET['pageNum']:1;
$pageSize = (isset($_GET['pageSize']) && ((int)$_GET['pageSize'] > 0))?(int)$_GET['pageSize']:10;
$paginator = new Paginator($pageNum, $pageSize);


$errorManager = new ErrorManager();
$errors = $errorManager->getAllErrors($queryFilterList, $sort, $paginator);

?>

<form action="." method="GET" id="searchForm">

<!-- FILTERS FORM -->
<div id="filtersForm">
	<h3 id="filter-header"><i></i>Filters</h3>
	<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>"/>
	<input type="hidden" name="sortDirection" id="sortDirection" value="<?=$sortDirection?>"/>
	<div id="filters">
		<?php include '__admin/errorFilterForm.php';?>
	</div>
</div>

<h3>Results
	<span>
		Showing <?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?> Matches
	</span>
</h3>

<?php if($paginator->totalRecords < 1) {?>

	<p><strong>No results were found.</strong></p>

<?php } else { ?>

<table class="data sortable bordered">
	<thead>
		<tr>
			<th id="id_head">ID</th>
			<th id="date_head">Date</th>
			<th id="count_head">Count</th>
			<th id="email_head">Email</th>
			<th id="deviceKey_head">Device Key</th>
			<th id="password_head">Password</th>
		</tr>
	</thead>
	<tbody>
<?php
/* @var $error Error */
foreach($errors as $error) {
?>
		<tr>
			<td><a href="<?=ADMIN_ROOT?>/error/view?id=<?=$error->id?>"><?=$error->id?></a></td>
			<td><?=$error->date?></td>
			<td><?=$error->count?></td>
			<td><a href="<?=ADMIN_ROOT?>/users/search?<?= Utils::UrlSearchParam("email", $error->email)?>"><?=$error->email?></a></td>
			<td><a href="<?=ADMIN_ROOT?>/devices/search?<?= Utils::UrlSearchParam("devicekey", $error->deviceKey)?>"><?=$error->deviceKey?></a></td>
			<td><?=$error->password?></td>
		</tr>

<?php } ?>
	</tbody>
</table>

			<!-- PAGING CONTROLS -->
			<div class="pagingControls">
				<span>
					Goto: <input type="text" name="pageNum" id="pageNum" value="<?=$paginator->pageNum?>" size="3"/> of <?=$paginator->numPages?> pages
				</span>
				<span>
					Show rows:
					<select name="pageSize" id="pageSize">
						<option value="10" <?php if($paginator->pageSize == 10) {print(" selected=\"selected\" ");}?>>10</option>
						<option value="25" <?php if($paginator->pageSize == 25) {print(" selected=\"selected\" ");}?>>25</option>
						<option value="50" <?php if($paginator->pageSize == 50) {print(" selected=\"selected\" ");}?>>50</option>
						<option value="100" <?php if($paginator->pageSize == 100) {print(" selected=\"selected\" ");}?>>100</option>
						<option value="200" <?php if($paginator->pageSize == 200) {print(" selected=\"selected\" ");}?>>200</option>
						<option value="500" <?php if($paginator->pageSize == 500) {print(" selected=\"selected\" ");}?>>500</option>
					</select>
				</span>
				<span>
					<?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?>
				</span>
				<span>
					<button id="prevButton" <?php if($paginator->pageNum <= 1) {print("disabled=\"disabled\"");}?>>&laquo;&laquo;</button>
					<button id="nextButton" <?php if($paginator->pageNum >= $paginator->numPages) {print("disabled=\"disabled\"");}?>>&raquo;&raquo;</button>
				</span>
			</div>

			<?php } ?>

		
		</form>


<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
$pagemaincontent = ob_get_contents();
ob_end_clean();
require_once("__admin/master.php");
/***************************************************************/
?>