<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "View Error";
$pageHeader = "View Error";
/***************************************************************/

require_once("classes/DeviceManager.php");
require_once("classes/AdminUserManager.php");
require_once("classes/ErrorManager.php");
require_once("classes/Error.php");
require_once("classes/DeviceMapManager.php");
require_once("classes/DeviceMap.php");

$deviceManager = new DeviceManager();
$errorManager = new ErrorManager();
$userManager = new AdminUserManager();
$deviceMapManager = new DeviceMapManager();

/* @var $error Error */
/* @var $deviceMap DeviceMap */

$errorId = $_GET['id'];
$error = $errorManager->get($errorId);

$deviceResult = $deviceManager->getDeviceByKey($error->deviceKey);
$deviceMap = $deviceMapManager->getDeviceMapByKey($error->deviceKey);
$deviceUser = $userManager->getUserById($deviceMap->userid);
$errorUser = $userManager->getUserByEmail($error->email);

?>

<h3>Error</h3>
<?php if($error) {?>

<table class="data bordered">
	<thead>
		<tr>
			<th>deviceid</th>
			<th>errorType</th>
			<th>date</th>
			<th>deviceKey</th>
			<th>email</th>
			<th>fname</th>
			<th>lname</th>
			<th>password sent</th>
			<th>product</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?=$error->id?></td>
			<td><?=$error->errorType?></td>
			<td><?=$error->date?></td>
			<td><?=$error->deviceKey?></td>
			<td><?=$error->email?></td>
			<td><?=$error->fname?></td>
			<td><?=$error->lname?></td>
			<td><?=$error->password?></td>
			<td><?=$error->product?></td>
		</tr>
	</tbody>
</table>

<?php
} else {
	print("No error found");
}
?>

<h3>User Data (from email passed in by the device request)</h3>
<?php if($errorUser) {?>
	<table class="data bordered">
		<thead>
			<tr>
				<th>userid</th>
				<th>fname</th>
				<th>lname</th>
				<th>city</th>
				<th>state</th>
				<th>postal</th>
				<th>country</th>
				<th>email</th>
				<th>company</th>
				<th>phone</th>
				<th>ipaddress</th>
				<th>datecreated</th>
				<th>suspended</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="<?=ADMIN_ROOT?>/users/view?id=<?=$errorUser->userid?>"><?=$errorUser->userid?></a></td>
				<td><?=$errorUser->fname?></td>
				<td><?=$errorUser->lname?></td>
				<td><?=$errorUser->city?></td>
				<td><?=$errorUser->state?></td>
				<td><?=$errorUser->postal?></td>
				<td><?=$errorUser->country?></td>
				<td><?=$errorUser->email?></td>
				<td><?=$errorUser->company?></td>
				<td><?=$errorUser->phone?></td>
				<td><?=$errorUser->ipaddress?></td>
				<td><?=$errorUser->datecreated?></td>
				<td><?=$errorUser->suspended?></td>
			</tr>
		</tbody>
	</table>

<?php
} else {
	print("No users found");
}
?>



<h3>Device Data</h3>
<?php if($deviceResult) {?>

	<table class="data bordered">
		<thead>
			<tr>
				<th>deviceid</th>
				<th>devicekey</th>
				<th>type</th>
				<th>model</th>
				<th>manufacturer</th>
				<th>os</th>
				<th>version</th>
				<th>theft_status</th>
				<th id="serial">serial</th>
				<th>color</th>
				<th>description</th>
				<th>macAddress</th>
				<th>labelcode</th>
				<th>datecreated</th>
				<th>saveTracking</th>
				<th>devicePassword</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="<?=ADMIN_ROOT?>/devices/view?id=<?=$deviceResult->deviceid?>"><?=$deviceResult->deviceid?></a></td>
				<td><?=$deviceResult->devicekey?></td>
				<td><?=$deviceResult->type?></td>
				<td><?=$deviceResult->model?></td>
				<td><?=$deviceResult->manufacturer?></td>
				<td><?=$deviceResult->os?></td>
				<td><?=$deviceResult->version?></td>
				<td><?=$deviceResult->theft_status?></td>
				<td><?=$deviceResult->serial?></td>
				<td><?=$deviceResult->color?></td>
				<td><?=$deviceResult->description?></td>
				<td><?=$deviceResult->macAddress?></td>
				<td><?=$deviceResult->labelcode?></td>
				<td><?=$deviceResult->datecreated?></td>
				<td><?=$deviceResult->saveTracking?></td>
				<td><?=$deviceResult->devicePassword?></td>
			</tr>
		</tbody>
	</table>

<?php
} else {
	print("No devices found");
}
?>

<h3>User Data (mapped via devicemap from the device key passed in)</h3>
<?php if($deviceUser) {?>
	<table class="data bordered">
		<thead>
			<tr>
				<th>userid</th>
				<th>fname</th>
				<th>lname</th>
				<th>city</th>
				<th>state</th>
				<th>postal</th>
				<th>country</th>
				<th>email</th>
				<th>company</th>
				<th>phone</th>
				<th>ipaddress</th>
				<th>datecreated</th>
				<th>suspended</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="<?=ADMIN_ROOT?>/users/view?id=<?=$deviceUser->userid?>"><?=$deviceUser->userid?></a></td>
				<td><?=$deviceUser->fname?></td>
				<td><?=$deviceUser->lname?></td>
				<td><?=$deviceUser->city?></td>
				<td><?=$deviceUser->state?></td>
				<td><?=$deviceUser->postal?></td>
				<td><?=$deviceUser->country?></td>
				<td><?=$deviceUser->email?></td>
				<td><?=$deviceUser->company?></td>
				<td><?=$deviceUser->phone?></td>
				<td><?=$deviceUser->ipaddress?></td>
				<td><?=$deviceUser->datecreated?></td>
				<td><?=$deviceUser->suspended?></td>
			</tr>
		</tbody>
	</table>
<?php
} else {
	print("No users found");
}
?>

<br/><br/><br/>

<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
$pagemaincontent = ob_get_contents();
ob_end_clean();
require_once("__admin/master.php");
/***************************************************************/
?>