<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Find Device Connections";
$pageHeader = "Find Device Connections";
/***************************************************************/

require_once("classes/AdminDeviceConnectionManager.php");
require_once("classes/QueryFilterList.php");
require_once("classes/Paginator.php");
require_once("classes/HttpUtils.php");

$deviceConnectionManager = new AdminDeviceConnectionManager();
$queryFilterList = new QueryFilterList();

$filterVars = array("connectionid", "devicekey", "timestamp", "public_ip", "internal_ip");
foreach($filterVars as $filterVar) {
	if(isset($_GET[$filterVar . "_a"]) && isset($_GET[$filterVar . "_v"]) && isset($_GET[$filterVar . "_o"])) {
		$varName = $filterVar . "_v";
		$$varName = $_GET[$filterVar . "_v"];
		$$varName = HttpUtils::cleanOutput($$varName);
		HttpUtils::cleanInput($fv = $_GET[$filterVar . "_v"]);
		HttpUtils::cleanInput($fo = $_GET[$filterVar . "_o"]);
		$queryFilterList->addFilter($filterVar, $fv, $fo);
	}
}

//Set up query sorting
HttpUtils::cleanInput($sortField = trim($_GET['sortField']));
HttpUtils::cleanInput($sortDirection = trim($_GET['sortDirection']));
if(!is_null($sortField) && $sortField != "" && !is_null($sortDirection) && $sortDirection != "") {
	$sort = array("field"=>$sortField, "direction"=>$sortDirection);
}

//Set up paging pariables
$pageNum = (isset($_GET['pageNum']) && ((int)$_GET['pageNum'] > 0))?(int)$_GET['pageNum']:1;
$pageSize = (isset($_GET['pageSize']) && ((int)$_GET['pageSize'] > 0))?(int)$_GET['pageSize']:10;
$paginator = new Paginator($pageNum, $pageSize);

$connections = $deviceConnectionManager->findDeviceConnections($queryFilterList, $sort, $paginator, $_GET['connectionsTable']);

?>

<script type="text/javascript">
	$(function() {
		$("#connectionsTable").change(function() {
			$("#searchForm").submit();
		});
	});
</script>

<form action="." method="GET" id="searchForm">

<!-- FILTERS FORM -->
<div id="filtersForm">

	<h3><i></i>Lookup Table</h3>
	<select name="connectionsTable" id="connectionsTable">
		<option value="connections" <?=($_GET['connectionsTable'] != "connectionsArchive")?"selected=\"selected\"":""?>">connections</option>
		<option value="connectionsArchive"<?=($_GET['connectionsTable'] != "connections" && isset($_GET['connectionsTable']))?"selected=\"selected\"":""?>">connections archive</option>
	</select>

	<h3 id="filter-header"><i></i>Filters</h3>
	<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>"/>
	<input type="hidden" name="sortDirection" id="sortDirection" value="<?=$sortDirection?>"/>
	<div id="filters">
		<?php include '__admin/userFilterForm.php';?>
	</div>
</div>

<h3>Results
	<span>
		Showing <?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?> Matches
	</span>
</h3>

<?php if($paginator->totalRecords < 1) {?>

	<p><strong>No results were found.</strong></p>

<?php } else { ?>

	<table class="data sortable bordered">
		<thead>
			<tr>
				<th id="connectionid_head">connectionid</th>
				<th id="devicekey_head">devicekey</th>
				<th id="timestamp_head">timestamp</th>
				<th id="public_ip_head">public_ip</th>
				<th id="internal_ip_head">internal_ip</th>
				<th id="stolen_head">stolen</th>
				<th id="ip_lon_head">ip_lon</th>
				<th id="ip_lat_head">ip_lat</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($connections as $connection) { ?>
			<tr>
				<td><?=$connection->connectionid?></td>
				<td><a href="<?=ADMIN_ROOT?>/devices/search?<?= Utils::UrlSearchParam("devicekey", $connection->devicekey)?>"><?=$connection->devicekey?></a></td>
				<td><?=$connection->timestamp?></td>
				<td><?=$connection->public_ip?></td>
				<td><?=$connection->internal_ip?></td>
				<td><?=$connection->stolen?></td>
				<td><?=$connection->ip_lon?></td>
				<td><?=$connection->ip_lat?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

		<!-- PAGING CONTROLS -->
		<div class="pagingControls">
			<span>
				Goto: <input type="text" name="pageNum" id="pageNum" value="<?=$paginator->pageNum?>" size="3"/> of <?=$paginator->numPages?> pages
			</span>
			<span>
				Show rows:
				<select name="pageSize" id="pageSize">
					<option value="10" <?php if($paginator->pageSize == 10) {print(" selected=\"selected\" ");}?>>10</option>
					<option value="25" <?php if($paginator->pageSize == 25) {print(" selected=\"selected\" ");}?>>25</option>
					<option value="50" <?php if($paginator->pageSize == 50) {print(" selected=\"selected\" ");}?>>50</option>
					<option value="100" <?php if($paginator->pageSize == 100) {print(" selected=\"selected\" ");}?>>100</option>
					<option value="200" <?php if($paginator->pageSize == 200) {print(" selected=\"selected\" ");}?>>200</option>
					<option value="500" <?php if($paginator->pageSize == 500) {print(" selected=\"selected\" ");}?>>500</option>
				</select>
			</span>
			<span>
				<?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?>
			</span>
			<span>
				<button id="prevButton" <?php if($paginator->pageNum <= 1) {print("disabled=\"disabled\"");}?>>&laquo;&laquo;</button>
				<button id="nextButton" <?php if($paginator->pageNum >= $paginator->numPages) {print("disabled=\"disabled\"");}?>>&raquo;&raquo;</button>
			</span>
		</div>

		<?php } ?>


	</form>


<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>