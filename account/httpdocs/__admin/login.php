<?php
require_once("bootstrapper.php");
require_once("classes/FormStyler.php");
require_once("classes/UserCredentials.php");
require_once ('HTML/QuickForm.php');

$prettyForm = new FormStyler('login');

$prettyForm->form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$prettyForm->form->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$prettyForm->form->addElement('submit', null, 'Submit', array('class' => "submit"));

$prettyForm->form->addRule('password', 'Please enter your password', 'required', null, 'client');
$prettyForm->form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$prettyForm->form->addRule('email', 'Please enter a valid email', 'email', null, 'client');

function processdata($data) {
	$credentials = new UserCredentials($data['email'], sha1($data['password']));
	Authentication::loginAdmin($credentials, "index.php");
}

if ($prettyForm->form->validate()) {
	$prettyForm->form->process('processdata');
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en-us" />
	<title>GadgetTrak Web Admin</title>

	<link rel="stylesheet" href="/__admin/css/reset.css" type="text/css" media="screen, projection" />
	<link rel="stylesheet" href="/__admin/css/layout.css" type="text/css" media="screen, projection" />
	<link rel="stylesheet" href="/__admin/css/main.css" type="text/css" media="screen, projection" />

</head>

<body>
	<div id="wrapper">
		<div id="header">
			<?php require_once("__admin/admin-header.php");?>
		</div>
		<div id="middle">
			<div id="container">
				<div id="content">
					<?php if($_REQUEST['error'] == "UserNotFound") {?>
					<p class="error">The username or password you entered is incorrect.</p>
					<?php }?>
					<? $prettyForm->display(); ?>
				</div>
			</div>
			<div class="sidebar" id="sideLeft">

			</div>
		</div>
	</div>
	<div id="footer">
		<?php require_once("__admin/admin-footer.php");?>
	</div>
</body>	

</html>