<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Find Users";
$pageHeader = "Find Users";
/***************************************************************/

require_once("classes/AdminUserManager.php");
require_once("classes/QueryFilterList.php");
require_once("classes/Paginator.php");
require_once("classes/HttpUtils.php");

$userManager = new AdminUserManager();
$queryFilterList = new QueryFilterList();

$filterVars = array("fname", "lname", "email", "datecreated", "userid", "city", "state", "postal", "country", "company", "phone", "ipaddress");
foreach($filterVars as $filterVar) {
	if(isset($_GET[$filterVar . "_a"]) && isset($_GET[$filterVar . "_v"]) && isset($_GET[$filterVar . "_o"])) {
		$varName = $filterVar . "_v";
		$$varName = $_GET[$filterVar . "_v"];
		$$varName = HttpUtils::cleanOutput($$varName);
		HttpUtils::cleanInput($fv = $_GET[$filterVar . "_v"]);
		HttpUtils::cleanInput($fo = $_GET[$filterVar . "_o"]);
		$queryFilterList->addFilter($filterVar, $fv, $fo);
	}
}

//Set up query sorting
HttpUtils::cleanInput($sortField = trim($_GET['sortField']));
HttpUtils::cleanInput($sortDirection = trim($_GET['sortDirection']));
if(!is_null($sortField) && $sortField != "" && !is_null($sortDirection) && $sortDirection != "") {
	$sort = array("field"=>$sortField, "direction"=>$sortDirection);
}

//Set up paging pariables
$pageNum = (isset($_GET['pageNum']) && ((int)$_GET['pageNum'] > 0))?(int)$_GET['pageNum']:1;
$pageSize = (isset($_GET['pageSize']) && ((int)$_GET['pageSize'] > 0))?(int)$_GET['pageSize']:10;
$paginator = new Paginator($pageNum, $pageSize);

$users = $userManager->findUsers($queryFilterList, $sort, $paginator);

?>


<form action="." method="GET" id="searchForm">

<!-- FILTERS FORM -->
<div id="filtersForm">
	<h3 id="filter-header"><i></i>Filters</h3>
	<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>"/>
	<input type="hidden" name="sortDirection" id="sortDirection" value="<?=$sortDirection?>"/>
	<div id="filters">
		<?php include '__admin/userFilterForm.php';?>
	</div>
</div>

<h3>Results
	<span>
		Showing <?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?> Matches
	</span>
</h3>

<?php if($paginator->totalRecords < 1) {?>

	<p><strong>No results were found.</strong></p>

<?php } else { ?>

	<table class="data sortable bordered">
		<thead>
			<tr>
				<th id="userid_head">userid</th>
				<th id="fname_head">fname</th>
				<th id="lname_head">lname</th>
				<th id="city_head">city</th>
				<th id="state_head">state</th>
				<th id="postal_head">postal</th>
				<th id="country_head">country</th>
				<th id="email_head">email</th>
				<th id="company_head">company</th>
				<th id="phone_head">phone</th>
				<th id="ipaddress_head">ipaddress</th>
				<th id="datecreated_head">datecreated</th>
				<th id="suspended_head">suspended</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($users as $user) { ?>
			<tr>
				<td><a href="<?=ADMIN_ROOT?>/users/view?id=<?=$user->userid?>"><?=$user->userid?></a></td>
				<td><?=$user->fname?></td>
				<td><?=$user->lname?></td>
				<td><?=$user->city?></td>
				<td><?=$user->state?></td>
				<td><?=$user->postal?></td>
				<td><?=$user->country?></td>
				<td><?=$user->email?></td>
				<td><?=$user->company?></td>
				<td><?=$user->phone?></td>
				<td><?=$user->ipaddress?></td>
				<td><?=$user->datecreated?></td>
				<td><?=$user->suspended?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

		<!-- PAGING CONTROLS -->
		<div class="pagingControls">
			<span>
				Goto: <input type="text" name="pageNum" id="pageNum" value="<?=$paginator->pageNum?>" size="3"/> of <?=$paginator->numPages?> pages
			</span>
			<span>
				Show rows:
				<select name="pageSize" id="pageSize">
					<option value="10" <?php if($paginator->pageSize == 10) {print(" selected=\"selected\" ");}?>>10</option>
					<option value="25" <?php if($paginator->pageSize == 25) {print(" selected=\"selected\" ");}?>>25</option>
					<option value="50" <?php if($paginator->pageSize == 50) {print(" selected=\"selected\" ");}?>>50</option>
					<option value="100" <?php if($paginator->pageSize == 100) {print(" selected=\"selected\" ");}?>>100</option>
					<option value="200" <?php if($paginator->pageSize == 200) {print(" selected=\"selected\" ");}?>>200</option>
					<option value="500" <?php if($paginator->pageSize == 500) {print(" selected=\"selected\" ");}?>>500</option>
				</select>
			</span>
			<span>
				<?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?>
			</span>
			<span>
				<button id="prevButton" <?php if($paginator->pageNum <= 1) {print("disabled=\"disabled\"");}?>>&laquo;&laquo;</button>
				<button id="nextButton" <?php if($paginator->pageNum >= $paginator->numPages) {print("disabled=\"disabled\"");}?>>&raquo;&raquo;</button>
			</span>
		</div>

		<?php } ?>


	</form>
	

<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>