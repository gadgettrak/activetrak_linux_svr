<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
require_once("classes/Utils.php");

Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "View User";
$pageHeader = "View User";
/***************************************************************/

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/AdminUserManager.php");
require_once("classes/DeviceManager.php");

$userManager = new AdminUserManager();
$deviceManager = new DeviceManager();

$userId = $_GET['id'];

$user = $userManager->getUserById($userId);

$deviceResults = $deviceManager->getUserDevices($user->userid);

?>

<?php if($user) {?>
<h3>User Data</h3>
<table class="data bordered">
	<tbody>
		<tr>
			<th>userid</th>
			<td><?=$user->userid?></td>
		</tr>
		<tr>
			<th>fname</th>
			<td><?=$user->fname?></td>
		</tr>
		<tr>
			<th>lname</th>
			<td><?=$user->lname?></td>
		</tr>
		<tr>
			<th>city</th>
			<td><?=$user->city?></td>
		</tr>
		<tr>
			<th>state</th>
			<td><?=$user->state?></td>
		</tr>
		<tr>
			<th>postal</th>
			<td><?=$user->postal?></td>
		</tr>
		<tr>
			<th>country</th>
			<td><?=$user->country?></td>
		</tr>
		<tr>
			<th>email</th>
			<td><?=$user->email?></td>
		</tr>
		<tr>
			<th>company</th>
			<td><?=$user->company?></td>
		</tr>
		<tr>
			<th>phone</th>
			<td><?=$user->phone?></td>
		</tr>
		<tr>
			<th>ipaddress</th>
			<td><?=$user->ipaddress?></td>
		</tr>
		<tr>
			<th>datecreated</th>
			<td><?=$user->datecreated?></td>
		</tr>
		<tr>
			<th>suspended</th>
			<td><?=$user->suspended?></td>
		</tr>
		<tr>
			<th>email_key</th>
			<td><?=$user->email_key?></td>
		</tr>
		<tr>
			<th>email validated</th>
			<td><?=$user->email_val?></td>
		</tr>
	</tbody>
</table>

<div class="buttonBar"><a href="<?=ADMIN_ROOT?>/users/edit?userid=<?=$user->userid?>" class="modalEdit">Edit User</a></div>

<?php
} else {
	print("No users found");
}
?>

<h3>Devices</h3>

<?php if(count($deviceResults->devices)) {?>
<table class="data bordered">
	<thead>
	<tr>
		<th class="ui-corner-tl">&nbsp;</th>
		<th>id</th>
		<th>description</th>
		<th>manufacturer</th>
		<th>model</th>
		<th>key</th>
		<th>activated</th>
		<th>length</th>
		<th>Time until expiration</th>
		<th>Tracking</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($deviceResults->devices as $deviceResult) {?>
		<tr>
			<td class="icon">

				<?if($deviceResult->productid == 0) {?>
					<img src="/_gfx/icon-label.gif" alt="GadgetTrak Label" title="GadgetTrak Label" />
				<?} else if($deviceResult->productid == 1) {?>
					<img src="/_gfx/icon-usb.png" alt="GadgetTrak USB" title="GadgetTrak USB"/>
				<?} else if($deviceResult->productid == 8) {?>
					<img src="/_gfx/icon-iphone.png" alt="GadgetTrak iPhone" title="GadgetTrak iPhone"/>
				<?} else if($deviceResult->productid == 4 || $deviceResult->productid == 7 || $deviceResult->productid == 10 || $deviceResult->productid == 12 || $deviceResult->productid == 13 || $deviceResult->productid == 14 || $deviceResult->productid == 15) {?>
					<img src="/_gfx/icon-laptop.png" alt="GadgetTrak for Laptops" title="GadgetTrak for Laptops"/>
				<?}?>

			</td>
			<td><a href="<?=ADMIN_ROOT?>/devices/view?id=<?=$deviceResult->deviceid?>"><?=$deviceResult->deviceid?></a></td>
			<td><?=$deviceResult->description?></td>
			<td><?=$deviceResult->manufacturer?></td>
			<td><?=$deviceResult->model?></td>
			<td><a href="<?=ADMIN_ROOT?>/licenses/view?key=<?=$deviceResult->key?>"><?=$deviceResult->key?></a></td>
			<td><?=$deviceResult->activated?></td>
			<td><?=$deviceResult->length?></td>
			<td>
				<?php if($deviceResult->length != -1) {?>
				<?php
					$activatedDate = strtotime($deviceResult->activated);
					$expireDate = getdate(strtotime("+" . $deviceResult->length . " months", $activatedDate));
					$nowDate = getdate(strtotime("now"));
					$begin = array ('year' => $nowDate['year'], 'month' => $nowDate['mon'], 'day' => $nowDate['mday']);
					$end = array ('year' => $expireDate['year'], 'month' => $expireDate['mon'], 'day' => $expireDate['mday']);
					$timeRemaining = Utils::dateDifference($begin, $end);
				?>
				<? if($timeRemaining['years']){print("$timeRemaining[years] years");}?> <? if($timeRemaining['months']){print("$timeRemaining[months] months");}?> <? if($timeRemaining['days']){print("$timeRemaining[days] days");}?>
				<?php
				if(!$timeRemaining || (
					$timeRemaining['years'] == 0 &&
					$timeRemaining['months'] == 0 &&
					$timeRemaining['days'] == 0)) {
					print("<strong>This license is expired</strong>");
				}
				?>
				<?php } else {?>
				Lifetime license
				<?php } ?>
			</td>

			<td style="width:150px;">
				<?=$deviceResult->theft_status?>
			</td>
		</tr>


	<?} ?>
	</tbody>
</table>
<?php } else { ?>
	<p>No devices found</p>
<?php } ?>

<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
$pagemaincontent = ob_get_contents();
ob_end_clean();
require_once("__admin/master.php");
/***************************************************************/
?>