<?php
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/AdminUserManager.php");

$userManager = new AdminUserManager();

$userId = $_REQUEST['userid'];
$user = $userManager->getUserById($userId);

$form = new FormStyler('edit');
$form->form->addElement("hidden", "userid", $user->userid);
$form->form->addElement("text", "fname", "First Name:", array("value" => "$user->fname", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "lname", "Last Name:", array("value" => "$user->lname", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "address1", "Address 1:", array("value" => "$user->address1", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "address2", "Address 2:", array("value" => "$user->address2", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "city", "City:", array("value" => "$user->city", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "state", "State:", array("value" => "$user->state", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "postal", "Postal:", array("value" => "$user->postal", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "country", "Country:", array("value" => "$user->country", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "email", "Email:", array("value" => "$user->email", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "company", "company:", array("value" => "$user->company", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "phone", "phone:", array("value" => "$user->phone", "size" => 30, "maxlength" => 255));
$form->form->addElement("text", "suspended", "suspended:", array("value" => "$user->suspended", "size" => 30, "maxlength" => 255));

$form->form->addElement("submit", null, "Submit", array("class" => "submit"));

function processData($data) {

	global $user, $userManager;

	$user->fname = $data['fname'];
	$user->lname = $data['lname'];
	$user->address1 = $data['address1'];
	$user->address2 = $data['address2'];
	$user->city = $data['city'];
	$user->state = $data['state'];
	$user->postal = $data['postal'];
	$user->country = $data['country'];
	$user->email = $data['email'];
	$user->company = $data['company'];
	$user->phone = $data['phone'];
	$user->suspended = $data['suspended'];

	$userManager->updateUser($user);

	$redirect = ADMIN_ROOT . "/users/view?id=$user->userid";
	header("Location: $redirect");

}

if ($form->form->validate()) {
	$form->form->process('processdata');
} else { ?>
	<div style="width:450px;margin:0 auto;">
		<?php
			$form->form->display();
		?>
	</div>

<?php } ?>