<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Generate Keys";
$pageHeader = "Generate Keys";
/***************************************************************/

require_once("classes/LicenseManager.php");
require_once("classes/License.php");

$licenseManager = new LicenseManager();

$fields = array("prefix", "length", "keyLength", "productId", "nfr");

$count = (isset($_GET['count']))?$_GET['count']:0;

$prefix = (isset($_GET['prefix']))?$_GET['prefix']:"GT-";
$length = (isset($_GET['length']))?$_GET['length']:"12";
$keyLength = (isset($_GET['keyLength']))?$_GET['keyLength']:"10";
$productId = (isset($_GET['productId']))?$_GET['productId']:"14";
$nfr = (isset($_GET['nfr']))?$_GET['nfr']:"0";

$import = (isset($_GET['import']))?true:false;
?>

<p>This page generates keys for use in the system.</p><br/>

<script type="text/javascript">
	$(function() {
		$("#resetForm").click(function() {
			window.location.href = "/__admin/utils/keyGen.php";
			return false;
		});

		$("#keyForm").submit(function() {

			if($("#import").is(":checked")) {
				return confirm("Are you sure you want to import keys into the database?");
			} else {
				return true;
			}
		});
	});
</script>

<form action="<?php echo $PHP_SELF;?>" method="get" id="keyForm">
<table class="form">
	<tbody>

		<tr>
			<td><label for="count">Number of Keys</label></td>
			<td><input type="text" name="count" id="count" value="<?=$count?>"/></td>
		</tr>
		<tr>
			<td>Import Keys?</td>
			<td><input type="checkbox" name="import" id="import" value="1"/><label for="import">Yes</label></td>
		</tr>
		
		<?php foreach($fields as $field) {?>
		<tr>
			<td><label for="<?= $field?>"><?= $field?></label></td>
			<td><input type="text" name="<?= $field?>" id="<?= $field?>" value="<?=$$field?>"/></td>
		</tr>
		<?php } ?>
		
	</tbody>
</table>
<p><input type="submit" value="submit"/> <button id="resetForm">Reset</button></p>
</form>
<br/>
<?php if($count > 0) {?>
<h3>Generated Keys</h3>
<p>Below are the the generated keys. If you have chosen to automatically import these into the database, you'll need to copy and paste these into another document for distribution.</p>
<br/>

<textarea style="font-size:13px;font-family:monospace;padding:10px;width:200px;min-height:200px;max-height:600px;">
<?php
$errors = 0;
for($i = 0; $i < $count; $i++) {
	$key = $licenseManager->createRandomKey($prefix, $length, $productId, $nfr, $keyLength, $import);if(!$key) $errors++;?>
<?=($key)?$key->key:""?>
<?if($i != $count -1) { print("\n");}?>
<?php } ?>
</textarea>

<br/>

<?php if($errors > 0) {?>
<!-- OH HELL YES I USED THE MARQUEE AND THE BLINK TAG, BITCHEZ!!! -->
<marquee scrollamount="2" scrolldelay="0" width="369px;" direction="right" behavior="Alternate"><blink><span style="font-size:15px;display:inline-block;font-weight:bold;color:#f00;padding:20px 0;"><?=$errors?> keys could not be generated due to conflict errors!</span></blink></marquee>
<?php } ?>

<?php }?>
<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>