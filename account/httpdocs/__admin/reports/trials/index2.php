<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Trial Users";
$pageHeader = "Trial Users";
/***************************************************************/

require_once("classes/TrialsManager.php");
require_once("classes/QueryFilterList.php");
require_once("classes/DbFieldList.php");
require_once("classes/Paginator.php");
require_once("classes/HttpUtils.php");

$trialsManager = new TrialsManager();

$dBFieldList = new DbFieldList();
$dBFieldList->addField("Trials Date", "trials", "date");
$dBFieldList->addField("User Id", "users", "userid");
$dBFieldList->addField("Email", "users", "email");
$dBFieldList->addField("User Created", "users", "datecreated");
$dBFieldList->addField("License Id", "license", "id");
$dBFieldList->addField("License Key", "license", "key");
$dBFieldList->addField("License Activated", "license", "activated");
$dBFieldList->addField("Device Key", "devices", "devicekey");
$dBFieldList->addField("Device Created", "devices", "datecreated");

$filterVars = array("trials___date", "userid", "email", "users___datecreated", "licenseId", "key", "activated", "devicekey", "devices___datecreated");
$queryFilterList = new QueryFilterList2();


foreach($filterVars as $filterVar) {
	if(isset($_GET[$filterVar . "_a"]) && isset($_GET[$filterVar . "_v"]) && isset($_GET[$filterVar . "_o"])) {
		$varName = $filterVar . "_v";
		$$varName = $_GET[$filterVar . "_v"];
		$$varName = HttpUtils::cleanOutput($$varName);
		HttpUtils::cleanInput($fv = $_GET[$filterVar . "_v"]);
		HttpUtils::cleanInput($fo = $_GET[$filterVar . "_o"]);
		$queryFilterList->addFilter($filterVar, $fv, $fo);
	}
}

//Set up query sorting
HttpUtils::cleanInput($sortField = trim($_GET['sortField']));
HttpUtils::cleanInput($sortDirection = trim($_GET['sortDirection']));
if(!is_null($sortField) && $sortField != "" && !is_null($sortDirection) && $sortDirection != "") {
	$sort = array("field"=>$sortField, "direction"=>$sortDirection);
}

//Set up paging pariables
$pageNum = (isset($_GET['pageNum']) && ((int)$_GET['pageNum'] > 0))?(int)$_GET['pageNum']:1;
$pageSize = (isset($_GET['pageSize']) && ((int)$_GET['pageSize'] > 0))?(int)$_GET['pageSize']:10;
$paginator = new Paginator($pageNum, $pageSize);

$trailUserResults = $trialsManager->getTrialUserResults($queryFilterList, $sort, $paginator);

/* @var $trialUserResult TrialUserResult */

?>

<form action="." method="GET" id="searchForm">

<!-- FILTERS FORM -->
<div id="filtersForm">
	<h3 id="filter-header"><i></i>Filters</h3>
	<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>"/>
	<input type="hidden" name="sortDirection" id="sortDirection" value="<?=$sortDirection?>"/>
	<div id="filters">
		<?php include '__admin/userFilterForm.php';?>
	</div>
</div>

<h3>Results
	<span>
		Showing <?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?> Matches
	</span>
</h3>

<?php if($paginator->totalRecords < 1) {?>

	<p><strong>No results were found.</strong></p>

<?php } else { ?>

<!--

public $trialsDate;
public $userid;
public $email;
public $userCreateDate;
public $licenseId;
public $key;
public $activated;
public $devicekey;
public $deviceCreateDate;

-->

	<table class="data sortable bordered">
		<thead>
			<tr>
				<?php foreach($filterVars as $filterVar) {?>
				<th id="<?=$filterVar?>_head"><?=$filterVar?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<?php foreach($trailUserResults as $trialUserResult) { ?>
			<tr>
				<td><?=$trialUserResult->trialsDate?></td>
				<td><a href="<?=ADMIN_ROOT?>/users/search?<?= Utils::UrlSearchParam("userid", $trialUserResult->userid)?>"><?=$trialUserResult->userid?></a></td>
				<td><a href="<?=ADMIN_ROOT?>/users/search?<?= Utils::UrlSearchParam("email", $trialUserResult->email)?>"><?=$trialUserResult->email?></a></td>
				<td><?=$trialUserResult->userCreateDate?></td>
				<td><a href="<?=ADMIN_ROOT?>/licenses/search?<?= Utils::UrlSearchParam("id", $trialUserResult->licenseId)?>"><?=$trialUserResult->licenseId?></a></td>
				<td><a href="<?=ADMIN_ROOT?>/licenses/search?<?= Utils::UrlSearchParam("key", $trialUserResult->key)?>"><?=$trialUserResult->key?></a></td>
				<td><?=$trialUserResult->activated?></td>
				<td><a href="<?=ADMIN_ROOT?>/devices/search?<?= Utils::UrlSearchParam("devicekey", $trialUserResult->devicekey)?>"><?=$trialUserResult->devicekey?></a></td>
				<td><?=$trialUserResult->deviceCreateDate?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

		<!-- PAGING CONTROLS -->
		<div class="pagingControls">
			<span>
				Goto: <input type="text" name="pageNum" id="pageNum" value="<?=$paginator->pageNum?>" size="3"/> of <?=$paginator->numPages?> pages
			</span>
			<span>
				Show rows:
				<select name="pageSize" id="pageSize">
					<option value="10" <?php if($paginator->pageSize == 10) {print(" selected=\"selected\" ");}?>>10</option>
					<option value="25" <?php if($paginator->pageSize == 25) {print(" selected=\"selected\" ");}?>>25</option>
					<option value="50" <?php if($paginator->pageSize == 50) {print(" selected=\"selected\" ");}?>>50</option>
					<option value="100" <?php if($paginator->pageSize == 100) {print(" selected=\"selected\" ");}?>>100</option>
					<option value="200" <?php if($paginator->pageSize == 200) {print(" selected=\"selected\" ");}?>>200</option>
					<option value="500" <?php if($paginator->pageSize == 500) {print(" selected=\"selected\" ");}?>>500</option>
				</select>
			</span>
			<span>
				<?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?>
			</span>
			<span>
				<button id="prevButton" <?php if($paginator->pageNum <= 1) {print("disabled=\"disabled\"");}?>>&laquo;&laquo;</button>
				<button id="nextButton" <?php if($paginator->pageNum >= $paginator->numPages) {print("disabled=\"disabled\"");}?>>&raquo;&raquo;</button>
			</span>
		</div>

		<?php } ?>

	</form>


<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>