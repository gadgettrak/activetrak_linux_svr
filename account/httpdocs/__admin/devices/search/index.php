<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Find Devices";
$pageHeader = "Find Devices";
/***************************************************************/

require_once("classes/AdminDeviceManager.php");
require_once("classes/QueryFilterList.php");
require_once("classes/Paginator.php");
require_once("classes/HttpUtils.php");

$deviceManager = new AdminDeviceManager();
$queryFilterList = new QueryFilterList();

$filterVars = array("deviceid", "devicekey", "serial", "description", "macAddress", "labelcode", "datecreated", "postal", "type", "version", "devicePassword");
foreach($filterVars as $filterVar) {
	if(isset($_GET[$filterVar . "_a"]) && isset($_GET[$filterVar . "_v"]) && isset($_GET[$filterVar . "_o"])) {
		$varName = $filterVar . "_v";
		$$varName = $_GET[$filterVar . "_v"];
		$$varName = HttpUtils::cleanOutput($$varName);
		HttpUtils::cleanInput($fv = $_GET[$filterVar . "_v"]);
		HttpUtils::cleanInput($fo = $_GET[$filterVar . "_o"]);
		$queryFilterList->addFilter($filterVar, $fv, $fo);
	}
}

//Set up query sorting
HttpUtils::cleanInput($sortField = trim($_GET['sortField']));
HttpUtils::cleanInput($sortDirection = trim($_GET['sortDirection']));
if(!is_null($sortField) && $sortField != "" && !is_null($sortDirection) && $sortDirection != "") {
	$sort = array("field"=>$sortField, "direction"=>$sortDirection);
}

//Set up paging pariables
$pageNum = (isset($_GET['pageNum']) && ((int)$_GET['pageNum'] > 0))?(int)$_GET['pageNum']:1;
$pageSize = (isset($_GET['pageSize']) && ((int)$_GET['pageSize'] > 0))?(int)$_GET['pageSize']:10;
$paginator = new Paginator($pageNum, $pageSize);

$devices = $deviceManager->findDevices($queryFilterList, $sort, $paginator);

?>


<form action="." method="GET" id="searchForm">

<!-- FILTERS FORM -->
<div id="filtersForm">
	<h3 id="filter-header"><i></i>Filters</h3>
	<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>"/>
	<input type="hidden" name="sortDirection" id="sortDirection" value="<?=$sortDirection?>"/>
	<div id="filters">
		<?php include '__admin/userFilterForm.php';?>
	</div>
</div>

<h3>Results
	<span>
		Showing <?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?> Matches
	</span>
</h3>

<?php if($paginator->totalRecords < 1) {?>

	<p><strong>No results were found.</strong></p>

<?php } else { ?>

	<table class="data sortable bordered">
		<thead>
			<tr>
				<th id="deviceid_head">deviceid</th>
				<th id="devicekey_head">devicekey</th>
				<th id="type_head">type</th>
				<th id="model_head">model</th>
				<th id="manufacturer_head">manufacturer</th>
				<th id="os_head">os</th>
				<th id="version_head">version</th>
				<th id="theft_status_head">theft_status</th>
				<th id="serial">serial</th>
				<th id="color_head">color</th>
				<th id="description_head">description</th>
				<th id="macAddress_head">macAddress</th>
				<th id="labelcode_head">labelcode</th>
				<th id="datecreated_head">datecreated</th>
				<th id="saveTracking_head">saveTracking</th>
				<th id="devicePassword_head">devicePassword</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($devices as $device) { ?>
			<tr>
				<td><a href="<?=ADMIN_ROOT?>/devices/view?id=<?=$device->deviceid?>"><?=$device->deviceid?></a></td>
				<td><?=$device->devicekey?></td>
				<td><?=$device->type?></td>
				<td><?=$device->model?></td>
				<td><?=$device->manufacturer?></td>
				<td><?=$device->os?></td>
				<td><?=$device->version?></td>
				<td><?=$device->theft_status?></td>
				<td><?=$device->serial?></td>
				<td><?=$device->color?></td>
				<td><?=$device->description?></td>
				<td><?=$device->macAddress?></td>
				<td><?=$device->labelcode?></td>
				<td><?=$device->datecreated?></td>
				<td><?=$device->saveTracking?></td>
				<td><?=$device->devicePassword?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

		<!-- PAGING CONTROLS -->
		<div class="pagingControls">
			<span>
				Goto: <input type="text" name="pageNum" id="pageNum" value="<?=$paginator->pageNum?>" size="3"/> of <?=$paginator->numPages?> pages
			</span>
			<span>
				Show rows:
				<select name="pageSize" id="pageSize">
					<option value="10" <?php if($paginator->pageSize == 10) {print(" selected=\"selected\" ");}?>>10</option>
					<option value="25" <?php if($paginator->pageSize == 25) {print(" selected=\"selected\" ");}?>>25</option>
					<option value="50" <?php if($paginator->pageSize == 50) {print(" selected=\"selected\" ");}?>>50</option>
					<option value="100" <?php if($paginator->pageSize == 100) {print(" selected=\"selected\" ");}?>>100</option>
					<option value="200" <?php if($paginator->pageSize == 200) {print(" selected=\"selected\" ");}?>>200</option>
					<option value="500" <?php if($paginator->pageSize == 500) {print(" selected=\"selected\" ");}?>>500</option>
				</select>
			</span>
			<span>
				<?=$paginator->rowStartNum?> - <?=$paginator->rowEndNum?> of <?=$paginator->totalRecords?>
			</span>
			<span>
				<button id="prevButton" <?php if($paginator->pageNum <= 1) {print("disabled=\"disabled\"");}?>>&laquo;&laquo;</button>
				<button id="nextButton" <?php if($paginator->pageNum >= $paginator->numPages) {print("disabled=\"disabled\"");}?>>&raquo;&raquo;</button>
			</span>
		</div>

		<?php } ?>


	</form>


<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>