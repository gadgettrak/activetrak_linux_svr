<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Find Devices";
$pageHeader = "Find Devices";
/***************************************************************/

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/AdminDeviceManager.php");

$deviceManager = new AdminDeviceManager();

$prettyForm = new FormStyler('search');

$prettyForm->form->addElement('text', 'deviceKey', 'Device Key:', array('size' => 30, 'maxlength' => 255));
$prettyForm->form->addElement('text', 'macAddress', 'MAC Address:', array('size' => 30, 'maxlength' => 255));
$prettyForm->form->addElement('submit', null, 'Submit', array('class' => "submit"));

if($prettyForm->form->isSubmitted()) {
	$devices = $deviceManager->findDevices($_POST['deviceKey'], $_POST['macAddress']);
}

?>

<? $prettyForm->display(); ?>

<?php
if($prettyForm->form->isSubmitted()) {
	if(count($devices)) {
?>

	<table class="data sortable bordered">
		<thead>
			<tr>
				<th>deviceid</th>
				<th>devicekey</th>
				<th>type</th>
				<th>model</th>
				<th>manufacturer</th>
				<th>os</th>
				<th>theft_status</th>
				<th>serial</th>
				<th>color</th>
				<th>description</th>
				<th>macAddress</th>
				<th>labelcode</th>
				<th>datecreated</th>
				<th>saveTracking</th>
				<th>devicePassword</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($devices as $device) { ?>
			<tr>
				<td><a href="<?=ADMIN_ROOT?>/devices/view?id=<?=$device->deviceid?>"><?=$device->deviceid?></a></td>
				<td><?=$device->devicekey?></td>
				<td><?=$device->type?></td>
				<td><?=$device->model?></td>
				<td><?=$device->manufacturer?></td>
				<td><?=$device->os?></td>
				<td><?=$device->theft_status?></td>
				<td><?=$device->serial?></td>
				<td><?=$device->color?></td>
				<td><?=$device->description?></td>
				<td><?=$device->macAddress?></td>
				<td><?=$device->labelcode?></td>
				<td><?=$device->datecreated?></td>
				<td><?=$device->saveTracking?></td>
				<td><?=$device->devicePassword?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

<?php
	} else {
		print("No devices found");
	}
}
?>


<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
	$pagemaincontent = ob_get_contents();
	ob_end_clean();
	require_once("__admin/master.php");
/***************************************************************/
?>