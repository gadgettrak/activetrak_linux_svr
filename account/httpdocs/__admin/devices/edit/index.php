<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
require_once("bootstrapper.php");
Authentication::authenticateAdminSession();
ob_start();

$pagetitle = "Edit Device";
$pageHeader = "Edit Device";
/***************************************************************/

require_once("classes/FormStyler.php");
require_once("HTML/QuickForm.php");
require_once("classes/DeviceManager.php");

$deviceManager = new DeviceManager();

$deviceId = $_GET['id'];

$deviceResult = $deviceManager->getDeviceResultById($deviceId)
?>

<?php if($deviceResult) {?>
<h3>Device Data</h3>
<table class="data">
	<tbody>
		<tr>
			<th>deviceid</th>
			<td><?=$deviceResult->deviceid?></td>
		</tr>
		<tr>
			<th>devicekey</th>
			<td><?=$deviceResult->devicekey?></td>
		</tr>
		<tr>
			<th>description</th>
			<td><?=$deviceResult->description?></td>
		</tr>
		<tr>
			<th>license key</th>
			<td><a href="<?=ADMIN_ROOT?>/licenses/edit?key=<?=$deviceResult->key?>"><?=$deviceResult->key?></a></td>
		</tr>
		<tr>
			<th>type</th>
			<td><?=$deviceResult->type?></td>
		</tr>
		<tr>
			<th>model</th>
			<td><?=$deviceResult->model?></td>
		</tr>
		<tr>
			<th>manufacturer</th>
			<td><?=$deviceResult->manufacturer?></td>
		</tr>
		<tr>
			<th>os</th>
			<td><?=$deviceResult->os?></td>
		</tr>
		<tr>
			<th>theft_status</th>
			<td><?=$deviceResult->theft_status?></td>
		</tr>
		<tr>
			<th>serial</th>
			<td><?=$deviceResult->serial?></td>
		</tr>
		<tr>
			<th>color</th>
			<td><?=$deviceResult->color?></td>
		</tr>
		<tr>
			<th>macAddress</th>
			<td><?=$deviceResult->macAddress?></td>
		</tr>
		<tr>
			<th>labelcode</th>
			<td><?=$deviceResult->labelcode?></td>
		</tr>
		<tr>
			<th>datecreated</th>
			<td><?=$deviceResult->datecreated?></td>
		</tr>
		<tr>
			<th>pro</th>
			<td><?=$deviceResult->pro?></td>
		</tr>
		<tr>
			<th>productid</th>
			<td><?=$deviceResult->productid?></td>
		</tr>
		<tr>
			<th>version</th>
			<td><?=$deviceResult->version?></td>
		</tr>
		<tr>
			<th>saveTracking</th>
			<td><?=$deviceResult->saveTracking?></td>
		</tr>
	</tbody>
</table>

<?php
} else {
	print("No devices found");
}
?>

<?php
/****************************************************************
* REQUIRED FOR EVERY PAGE
****************************************************************/
$pagemaincontent = ob_get_contents();
ob_end_clean();
require_once("__admin/master.php");
/***************************************************************/
?>