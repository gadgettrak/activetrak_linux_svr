<?
ini_set( 
  'include_path', 
  ini_get( 'include_path' ) . PATH_SEPARATOR . "/home/kwestin/pear/php"
); 

$userid;
$form;
$conn;
$description;
$did=$_GET['did'];

session_start();

if (!$_SESSION['loggedin']){
	header("Location: login.php?error=notloggedin");
}

$userid=$_SESSION['userid'];
include("database.php");
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');

$form = new HTML_QuickForm('register');
$form->addElement('submit', null, 'Delete', array('class' => "submit"));
$form -> addElement ('hidden', 'did', $did);

function processdata($data){
	global $conn;
	global $userid;
	$devicekey=$data['did'];
	//delete the records
	$q = "DELETE FROM devices WHERE devicekey='$devicekey'";
	$q2= "DELETE FROM devicemap WHERE devicekey= '$devicekey'";
	mysql_query($q,$conn) or die(mysql_error()); 
	mysql_query($q2,$conn) or die(mysql_error());
	header("Location: usb.php");
}

function content() {
	global $userid, $form, $conn, $description, $did;
	//make sure this device belongs to this owner
	$devicesql="SELECT * FROM devices ,devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid='$userid' AND devices.devicekey='$did'";
	$deviceresult = mysql_query($devicesql,$conn);
	$devicedata=mysql_fetch_array($deviceresult);
	if (count($devicedata) <= 0){
		header("Location: error.php?error=notyourdevice");
	}
	//description
	$description= $devicedata['manufacturer'] . " " . $devicedata['model'] . " ". $devicedata['type'];
}

if ($form->validate()) {// if the form validates?
	$form->process('processdata');
}else{
content();
?>
<div style="width:450px;margin:0 auto;">
<p> 
<strong style="color:#c00;">Warning!</strong> you are about to delete your <strong><?=$description?> </strong>from our system. This will remove all records pertaining to this device. This cannot be undone, all information pertaining to this device will be lost.   To continue with your deletion please click the "Delete" button.
<? 
$form->display() ;
?>

<p>If you do not wish to delete this device please click the "close window" link below to return to your account page.</p>

</div>
<?} ?>