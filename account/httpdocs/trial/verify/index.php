<?php

require_once("bootstrapper.php");
require_once("classes/UserManager.php");
require_once("classes/LicenseManager.php");
require_once("classes/Mailer.php");
require_once("classes/User.php");
require_once("classes/Trials.php");
require_once("classes/TrialsManager.php");
require_once("classes/User.php");
require_once("classes/HttpUtils.php");

/* @var $user User */
HttpUtils::cleanRequestParamsArray($_GET);
$emailKey = $_GET['key'];
$email = $_GET['email'];

$licenseManager = new LicenseManager();
$userManager = new UserManager();
$trialsManager = new TrialsManager();

$user = $userManager->getUserByEmail($email);
$error = false;

if(!$user) {
	$error = "UserNotFound";
} else if($user->email_key != $emailKey) {
	$error = "IncorrectKey";
}

if(!$error) {
	$userManager->validateUser($user);
	$trial = $trialsManager->get($user->userid);

	if($trial) {
		$error = "TrialAlreadyUsed";
	}
}

if(!$error) {

	/* @var $license License */
	$license = $licenseManager->createTrialKey($user);
	$trial = new Trials($user->userid, gmdate("Y-m-d H:i:s"), $license->id);
	$trialsManager->create($trial);

	Mailer::sendNewTrialKey($user, $license->key);
	header("HTTP/1.1 303 See Other");
	header("Location: /trial/key/?key=" . $license->key);
}

?>

<?php include("fragments/header.php"); ?>

<h2>GadgetTrak User Verification</h2>

<?php if($error == "UserNotFound") { ?>

	<p class="error">Sorry, that was an invalid user!</p>

<?php } else if ($error == "IncorrectKey") { ?>

	<p class="error">Sorry, that was an invalid verification key!</p>

<?php } else if ($error == "TrialAlreadyUsed") { ?>

	<p class="error">Sorry, there's only one trial allowed per user.</p>

<?php } else { ?>
	
	<p class="error">Sorry, an error occurred.</p>

<?php } ?>

<?php include("fragments/footer.php");?>