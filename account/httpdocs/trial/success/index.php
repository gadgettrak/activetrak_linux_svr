<?php include("fragments/header.php"); ?>

<h2>GadgetTrak free trial registration</h2>

<p>Thank you for registering for a trial license!</p>
<p>You will soon receive an email with instructions on how to verify your account. After verifying your account you will receive your trial license key.</p>

<?php include("fragments/footer.php");?>