<?php

require_once("bootstrapper.php");
require_once("classes/TabelessFormRenderer.php");
require_once("classes/UserCredentials.php");
require_once ('HTML/QuickForm.php');
require_once('classes/UserManager.php');
require_once('classes/Mailer.php');

function usernameTaken($element_name, $element_value) {
	$userManager = new UserManager();
	return $userManager->userNameIsAvailable($element_value);
}

function createUser($data) {

	//Clean inputs
	HttpUtils::cleanRequestParamsArray($data);

	$userManager = new UserManager();

	$user = new User();
	$user->email = $data["email"];
	$user->email_key = sha1($_POST['email'] . rand());
	$user->datecreated = gmdate("Y-m-d H:i:s");
	$user->ipaddress = $_SERVER["REMOTE_ADDRESS"];
	$user->password = sha1($data["password"]);
	$user->email_val = 0;

	$user = $userManager->saveNewUser($user);
	$success = Mailer::sendNewTrialUserVerifyNotice($user);

	if($success) {
		header("HTTP/1.1 303 See Other");
		header("Location: success/");
	} else {
		die("OOOPS. ERROR SENDING MAIL!");
	}

}

$renderer =& new HTML_QuickForm_Renderer_Tableless();

/*
 * CREATE USER FORM
 */

$createForm = new HTML_QuickForm();

$createForm->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$createForm->addElement('text', 'emailConfirm', 'Confirm Email:', array('size' => 30, 'maxlength' => 255));
$createForm->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$createForm->addElement('password', 'passwordConfirm', 'Confirm Password:', array('size' => 30, 'maxlength' => 255));
$createForm->addElement('hidden', 'action', 'create');
$createForm->addElement('advcheckbox','terms','', 'I&nbsp;agree&nbsp;to&nbsp;the&nbsp;<a target="_blank" href="http://www.gadgettrak.com/legal/terms.php">terms&nbsp;and&nbsp;conditions</a>', array('class'=>'poop'),'yes');

$createForm->addElement('submit', null, 'Create Account', array('class' => "submit"));

$createForm->addRule('email', 'Please enter your email address', 'required', null, 'client');
$createForm->addRule('email', 'Please enter a valid email', 'email', null, 'client');
$createForm->addRule(array('email','emailConfirm'), 'Emails do not match', 'compare', null, 'client');
$createForm->addRule('password', 'Please enter a password', 'required', null, 'client');
$createForm->addRule(array('password','passwordConfirm'), 'Passwords do not match', 'compare', null, 'client');
$createForm->registerRule('usernameTaken','function','usernameTaken');
$createForm->addRule('email','The email you have selected already exists', 'usernameTaken');
$createForm->addRule('terms', 'In order to use this service you must agree the terms and conditions', 'required', null, 'client');

$createForm->setRequiredNote('');
$createForm->accept($renderer);

?>

<?php include("fragments/header.php"); ?>

<h2>GadgetTrak Laptop Security Trial Registration</h2>
<p class="form-switch">Already have an account? <a href="/trial/login">Sign In</a></p>

<div id="trialForm" class="user-forms">

<?php if ($createForm->validate()) {?>
	<?php $user = $createForm->process('createUser'); ?>
<?php } else { ?>
	<?php
		$createForm->accept($renderer);
		echo $renderer->toHtml();
	?>
<?php } ?>

</div>

<?php include("fragments/footer.php");?>