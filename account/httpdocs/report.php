<?php require_once("$_SERVER[DOCUMENT_ROOT]/_functions/navigation.php");?>
<?
session_start();
if (!$_SESSION['loggedin']){
header("Location: login.php?error=notloggedin");
}
$coordinates;
include("database.php");

$did=$_GET['did'];
$userid=$_SESSION['userid'];
$devicesql="SELECT * FROM devices , devicemap WHERE devices.devicekey=devicemap.devicekey AND devicemap.userid=$userid AND devices.devicekey='$did'";

$deviceresult = mysql_query($devicesql,$conn);
$devicedata=mysql_fetch_array($deviceresult);


$datasql = "SELECT
`connections`.`connectionid`,
`connections`.`devicekey`,
`connections`.`timestamp`,
`connections`.`public_ip`,
`connections`.`internal_ip`,
`connections`.`stolen`,
`connections`.`username`,
`connections`.`computer_name`,
`connections`.`ip_country`,
`connections`.`ip_state`,
`connections`.`ip_city`,
`connections`.`postal_code`,
`connections`.`ip_lon`,
`connections`.`ip_lat`,
`connections`.`agent`,
`connections`.`os`,
`devicemap`. `userid`
FROM
`connections` , `devices`, `devicemap`
WHERE
connections.devicekey='" . $did . "'
AND
 connections.stolen = 'Y'
AND 
connections.devicekey = devicemap.devicekey
AND 
devicemap.devicekey = devices.devicekey ORDER BY timestamp DESC


";

//print $datasql;

$connectiondata = mysql_query($datasql ,$conn);

if($devicedata['theft_status'] =='Y'){
$status="Active";
}else{
$status="Disabled";
}


$title="Gadget Theft Tracking System - " . $_SESSION['name'];

?>
<?php include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");?>
<script type="text/javascript" src="http://maps.yahooapis.com/v3.5/fl/javascript/apiloader.js?appid=6GtLLnXV34ECUR7Y4Rt232qN0FyW6gTMhdLFHYH93jsWr84FzCyIKQhsMxtwwvZL">
</script>
<style type="text/css">
#mapContainer {
margin-top:10px; 
  height:300px; 
  width: 930px; 
} 
</style> 

<?php cookieCrumbNav(array("myDevices", "usbDevices"), "$devicedata[manufacturer] $devicedata[model]");?>
<h2>Device Report</h2>

<table class="tabledata">
  <tr>
    <td><strong>Device Type: </strong></td>
    <td><?=$devicedata['type']; ?></td>
    <td><strong>Device Model: </strong></td>
    <td><?=$devicedata['model']; ?></td>
    <td><strong>Tracking:  </strong></td>
    <td><?=$status; ?></td>
  </tr>
  <tbody>
  <tr>
    <td><strong>Device Manufacturer: </strong></td>
    <td><?=$devicedata['manufacturer']; ?></td>
    <td><strong>Device Serial: </strong></td>
    <td><?=$devicedata['serial']; ?></td>
    <td><strong>Label Tracking Code: </strong></td>
    <td><?=$devicedata['labelcode']; ?></td>
  </tr>
  </tbody>
</table>
<div id="mapContainer" style="margin-bottom:10px;"></div>



<!-- 6GtLLnXV34ECUR7Y4Rt232qN0FyW6gTMhdLFHYH93jsWr84FzCyIKQhsMxtwwvZL -->
<table class="tabledata">
<thead>
<tr><th>Connection Time</th><th>Public IP</th><th>Internal IP</th><th> Username </th>
<th>Computer Name</th><th>Location</th>
</tr>
</thead>
<tbody>
<?
$coorjs;
$rownum=1;


while ($row = mysql_fetch_array($connectiondata)){

	if($rownum == 1){
	$startloc ="var startloc=new LatLon(" . $row['ip_lat'] . ", " .$row['ip_lon'].");\n";
	}
if($row['ip_lon'] != '' && $row['ip_lat'] != '' ){
	//coordinates
	$coorjs.="var latlon" . $rownum . " = new LatLon(" . $row['ip_lat'] . ", " .$row['ip_lon'].");\n";
	//coordinates

	$description="Computer Name: " . $row['computer_name'] . "<br />Username: ". $row['username'] ."Lat: ". $row['ip_lat'] . " Lon:" . $row['ip_lon'];
			
			
	$coorjs  .= "marker" . $rownum ."= new CustomPOIMarker('" .  $row['timestamp']. "', ' -- " .$row['public_ip']."', '". $description ."', '0xCC0000', '0xFFFFFF');\n";
	$description='';
	$coorjs .="map.addMarkerByLatLon(marker". $rownum .",latlon". $rownum.");\n";
}
//addMarkerByLatLon(marker, latlon)

	if($rownum%2 != 0) {
	$td="<td class=\"grow\">";
	}else {
	$td="<td>";
	}

	print "<tr>";
	print $td . $row['timestamp'] . "</td>";
	print $td . $row['public_ip'] . "</td>";
	print $td . $row['internal_ip'] ."</td>";
	print $td . $row['username'] .  "</td>" ;
	print $td . $row['computer_name']. "</td>"; 
	print $td . $row['ip_city'] . ", " . $row['ip_state'] . ", " . $row['ip_country']. "</td></tr>";
	$rownum++;
	}

?>
</tbody>
</table>
<script type="text/javascript">
// Create and display Map object 
<?=$startloc ?>
var map = new Map("mapContainer", "6GtLLnXV34ECUR7Y4Rt232qN0FyW6gTMhdLFHYH93jsWr84FzCyIKQhsMxtwwvZL", startloc, 12);
// Make the map draggable 
map.addTool( new PanTool(), true ); 
//map.addWidget(new SatelliteControlWidget());
map.addWidget(new NavigatorWidget());  

<?=print $coorjs;?>
</script>
<br clear="both" />
<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>
