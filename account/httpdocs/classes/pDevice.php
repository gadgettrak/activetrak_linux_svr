<?php

require_once("$_SERVER[DOCUMENT_ROOT]/database.php");
require_once("$_SERVER[DOCUMENT_ROOT]/classes/BaseDAO.php");


class Device extends BaseDAO
{
	public static function updateTracking($devicekey, $status) {
		//only allow Y/N values
		$theft_status = ($status == 'Y')?'Y':'N';
		$sql = "UPDATE devices SET theft_status='$status' WHERE devicekey='$devicekey'";
		$result = mysql_query($sql) or die(mysql_error());
		if($result) {
			return array("success" => true, "tracking_status" => $theft_status);
		}
	}
	
	
	public static function getIPhoneDevicesByUserId($uid) {
		$sql = "SELECT devices.type, devices.model, devices.manufacturer, devices.os, devices.theft_status, devices.serial, devices.color, devices.deviceid, devices.description, devices.devicekey, devices.labelcode, devices.macAddress, devices.datecreated, devices.productid, products.productname FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN products on products.productid=devices.productid WHERE devicemap.userid='$uid' AND devices.productid=8";
		
		$results = mysql_query($sql);
		if($results) {
			$devices = self::getResultsAsObjectList($results);
		} else {
			$devices = array();
		}
		return $devices;
	}
	
	public static function getLaptopDevicesByUserId($uid) {
		$sql = "SELECT devices.type, devices.model, devices.manufacturer, devices.os, devices.theft_status, devices.serial, devices.color, devices.deviceid, devices.description, devices.devicekey, devices.labelcode, devices.macAddress, devices.datecreated, devices.productid, products.productname, license.key, license.activated, license.length FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN license on license.deviceId=devices.deviceid LEFT JOIN products on products.productid=devices.productid WHERE devicemap.userid='$uid' AND devices.productid in (4,7,10,12,13,14,15)";
		
		$results = mysql_query($sql);
		if($results) {
			$devices = self::getResultsAsObjectList($results);
		} else {
			$devices = array();
		}
		return $devices;
	}
	
	public static function getTotalActiveDevices($uid) {
		$sql = "SELECT devices.devicekey FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN products on products.productid=devices.productid WHERE devicemap.userid='$uid' AND devices.theft_status='Y'";
		$results = mysql_query($sql) or die(mysql_error());
		return mysql_num_rows($results);
	}
	
	public static function getDevicesByUserId($uid) {
		$sql = "SELECT devices.type, devices.model, devices.manufacturer, devices.os, devices.theft_status, devices.serial, devices.color, devices.deviceid, devices.description, devices.devicekey, devices.labelcode, devices.datecreated, devices.productid, products.productname, license.key FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN products on products.productid=devices.productid LEFT JOIN license on license.deviceId=devices.deviceid WHERE devicemap.userid='$uid' ORDER BY devices.productid";
		$results = mysql_query($sql);
		if($results) {
			$devices = self::getResultsAsObjectList($results);
		} else {
			$devices = array();
		}
		return $devices;
	}

	public static function getUSBDevicesByUserId($uid) {
		$sql = "SELECT devices.type, devices.model, devices.manufacturer, devices.os, devices.theft_status, devices.serial, devices.color, devices.deviceid, devices.description, devices.devicekey, devices.labelcode, devices.datecreated, devices.productid, products.productname FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN products on products.productid=devices.productid WHERE devicemap.userid='$uid' AND devices.productid=1";
		$results = mysql_query($sql);
		if($results) {
			$devices = self::getResultsAsObjectList($results);
		} else {
			$devices = array();
		}
		return $devices;
	}

}