<?php

require_once("$_SERVER[DOCUMENT_ROOT]/database.php");
require_once("$_SERVER[DOCUMENT_ROOT]/classes/BaseDAO.php");

class User
{
	
	public $id;
	public $email;
	public $fname;
	public $lname;
	
	function __construct($id, $email, $fname, $lname) {
		$this->id    = $id;
		$this->email = $email;
		$this->fname = $fname;
		$this->lname = $lname;
		$this->licenseCount = $this->getLicenseCount();
	}

	function getLicenseCount() {

		//Get licenses from licenses table
		$sql = "Select SUM(number_licenses) AS licensecount FROM licenses WHERE userid='$this->id'";
		$count1 = mysql_result(mysql_query($sql), "licensecount");

		//Get licenses from license_key table (old gt_mac database)
		$sql = "SELECT license_keys.keyid, license_keys.license_key, license_keys.length_of_license, license_keys.initial_activation, device_user.email, device_user.licenseid, device_user.license FROM license_keys INNER JOIN device_user ON license_keys.keyid = device_user.licenseid WHERE device_user.email='$this->email'";

		$count2 = mysql_num_rows(mysql_query($sql));

		return $count1 + $count2;
	}
	
	function getDevices() {
		$sql = "SELECT devices.type, devices.model, devices.manufacturer, devices.os, devices.theft_status, devices.serial, devices.color, devices.deviceid, devices.description, devices.devicekey, devices.labelcode, devices.datecreated, devices.productid, products.productname FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN products on products.productid=devices.productid WHERE devicemap.userid='$this->id' AND devices.productid<>'0'";
		$results = mysql_query($sql);
		if($results) {
			$devices = BaseDAO::getResultsAsObjectList($results);
		} else {
			$devices = array();
		}
		return $devices;
	}
	
	function getLicenses() {
		$sql = "SELECT * FROM licenses, products WHERE licenses.userid= '$this->id' AND products.productid = licenses.productid";
		$results = mysql_query($sql);
		if($results) {
			$licenses = BaseDAO::getResultsAsObjectList($results);
		} else {
			$licenses = array();
		}
		return $licenses;
	}
	
	function getLabels() {
		$sql = "SELECT devices.type, devices.model, devices.manufacturer, devices.os, devices.theft_status, devices.serial, devices.color, devices.deviceid, devices.description, devices.devicekey, devices.labelcode, devices.datecreated, devices.productid, products.productname FROM devicemap INNER JOIN devices ON devices.devicekey=devicemap.devicekey LEFT JOIN products on products.productid=devices.productid WHERE devicemap.userid='$this->id' AND devices.productid=0";
		$results = mysql_query($sql);
		if($results) {
			$labels = BaseDAO::getResultsAsObjectList($results);
		} else {
			$labels = array();
		}
		return $labels;
	}

}


?>