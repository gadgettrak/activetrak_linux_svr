<?php

class BaseDAO
{

	/**
	 * Returns a list of objects from a mysql result. Each row is first fetched as an Array, so that
	 * we can do some easy cleaning of the output (encode HTML chars, and unescape quotes)
	 */
	public static function getResultsAsObjectList(/*mysql resource*/ $results) {
		$values = array();
		while ($row = mysql_fetch_assoc($results)) {
		
			//clean values
			$row = self::cleanOutput($row);
			
			//Cast array to object
			array_push($values, (object) $row);
			
		}
		return $values;
	}
	
	/**
	 * Converts HTML entities for output which should always happen to prevent XSS attacks
	 */
	public static function cleanHTML($data) {
		return htmlentities($data, ENT_QUOTES, 'UTF-8');
	}
	
	/**
	 * Convert HTML entites and strips quotes for output to page.
	 */
	public static function cleanOutput($data) {
		$data = array_map('cleanHTML', $data);
		return array_map('stripslashes', $data);
	}

}