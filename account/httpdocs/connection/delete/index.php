<?

require_once("bootstrapper.php");
require_once("classes/DeviceConnectionManager.php");

Authentication::authenticateSession(false);

if(!isset($_GET['id'])) { die(); }

$deviceKey = $_GET['deviceKey'];
$connectionId = $_GET['id'];
$connectionManager = new DeviceConnectionManager();
$success = $connectionManager->deleteDeviceConnection($connectionId, $_SESSION['user']->userid);

header("Location: /device/view/?key=" . $deviceKey . "&tab=reports");


?>