<?

require_once("bootstrapper.php");
require_once("classes/DeviceConnectionManager.php");

Authentication::authenticateSession(false);

if(!isset($_GET['deviceKey'])) { die(); }

$deviceKey = $_GET['deviceKey'];
$connectionManager = new DeviceConnectionManager();
$success = $connectionManager->deleteDeviceConnections($deviceKey, $_SESSION['user']->userid);

header("Location: /device/view/?key=" . $deviceKey . "&tab=reports");

?>