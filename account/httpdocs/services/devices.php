<?php
require_once("$_SERVER[DOCUMENT_ROOT]/_functions/authentication.php");
require_once("$_SERVER[DOCUMENT_ROOT]/classes/Device.php");

authenticateSession(false);

$event = $_GET['event'];

if(!$_GET['event']) {
	die();
}

if($event == "tracking") {
	
	if(isset($_GET['track']) && isset($_GET['d'])) {
		$result = Device::updateTracking($_GET['d'], $_GET['track']);
		if($result) {
			$result['total_active'] = Device::getTotalActiveDevices($_SESSION['user']->id);
			print(json_encode($result));
			//prevent further output;
			exit();
		}
	}

}



?>