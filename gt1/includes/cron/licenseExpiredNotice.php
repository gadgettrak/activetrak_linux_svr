<?php

require_once("bootstrapper.php");
require_once("classes/LicenseManager.php");
require_once("classes/UserLicenseResult.php");
require_once('Mail.php');
require_once('Mail/mime.php');

$licenseManager = new LicenseManager();
$userLicenseResults = $licenseManager->getExpiringLicenses('0', 'DAY');

/* @var $userLicenseResult UserLicenseResult */

foreach($userLicenseResults as $userLicenseResult) {

	ob_start();
	include("templates/email/license-expired-html.php");
	$text = ob_get_contents();
	ob_end_clean();

	$from = "support@gadgettrak.com";
	$subject = "[Expiration Notice] Your GadgetTrak Subscription Has Expired";

	$message = new Mail_mime();
	$message->setHTMLBody($text);

	$headers['From'] = $from;
	$headers['Subject'] = $subject;

	$body = $message->get();

	$to = $userLicenseResult->email;
	//$to = "fugged@gmail.com";

	$extraheaders = array("To"=>$to, "From"=>$from, "Subject"=>$subject);
	$headers = $message->headers($extraheaders);

	$smtp = Mail::factory('smtp', array("host"=>"intGTmail"));
	//$smtp = Mail::factory('mail');

	$smtp->send($to, $headers, $body);

}

 

?>