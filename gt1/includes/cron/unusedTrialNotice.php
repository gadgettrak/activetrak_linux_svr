<?php

require_once("bootstrapper.php");
require_once("classes/LicenseManager.php");
require_once("classes/UserLicenseResult.php");
require_once('Mail.php');
require_once('Mail/mime.php');

$licenseManager = new LicenseManager();
$userLicenseResults = $licenseManager->getUnactivatedTrials();

/* @var $userLicenseResult UserLicenseResult */

foreach($userLicenseResults as $userLicenseResult) {

	ob_start();
	include("templates/email/unused-trial-html.php");
	$text = ob_get_contents();
	ob_end_clean();

	$subject = "GadgetTrak Trial Support";
	$from = "support@gadgettrak.com";

	$message = new Mail_mime();
	$message->setHTMLBody($text);

	$headers['From'] = $from;
	$headers['Subject'] = $subject;

	$body = $message->get();

	$to = $userLicenseResult->email;
	//$to = "msweet@alien109.com";

	$extraheaders = array("To"=>$to, "From"=>$from, "Subject"=>$subject);

	$headers = $message->headers($extraheaders);

	$smtp = Mail::factory('smtp', array("host"=>"intGTmail"));
	//$smtp = Mail::factory('mail');
	$smtp->send($to, $headers, $body);

}

?>