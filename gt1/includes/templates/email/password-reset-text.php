Hello,

Someone has requested that your password be reset at GadgetTrak.com.

The next step to reset your password is to verify your email address by clicking
on the link below. You will then be taken to a page where you can create a new
password.

https://account.gadgettrak.com/passwordreset.php?key=<?=$user->email_key?>

--------------------------------------------------------------------------------
If you have any problems with resetting your password please contact technical
support at: support@gadgettrak.com
--------------------------------------------------------------------------------