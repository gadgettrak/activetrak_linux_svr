The following device has attempted to check in but your license has expired.

License Key
-------------------------------------------------------------------
$license->key

Device Details
-------------------------------------------------------------------
ID: <?=$device->deviceid?>
Type: <?=$device->type?>
Model: <?=$device->model?>
Manufacturer: <?=$device->manufacturer?>
OS: <?=$device->os?>
Serial: <?=$device->serial?>
Color: <?=$device->color?>
Description: <?=$device->description?>
MACAddress: <?=$device->macAddress?>
Label Code: <?=$device->labelcode?>
Date Registered: <?=$device->datecreated?>


If you need assistance, please contact us at: support@gadgettrak.com