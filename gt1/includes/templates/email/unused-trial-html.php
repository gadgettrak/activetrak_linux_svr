<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">

			<h3>GadgetTrak Trial Support</h3>

			<p>Hi there,</p>

			<p>Thanks for signing up for the Free trial of GadgetTrak!</p>

			<p>Our system indicates that you have not yet registered a device to your GadgetTrak account. I wanted to quickly reach out and see if you were having any trouble getting GadgetTrak set up or if you had any questions.</p>

			<p>Let me know or refer to our <a href="http://www.gadgettrak.com/downloads/manual.pdf">User Manual</a>. We're here to help!</p>

			<p>Regards,</p>

			<p>Justin</p>

			<br/>

			<p>
				<b>Justin Thiele</b><br/>
				Community Manager<br/>
				<a href="mailto:support@gadgettrak.com">support@gadgettrak.com</a><br/>
				<a href="http://www.gadgettrak.com">http://www.gadgettrak.com</a>
			</p>

			<br/>

		</td>
	</tr>
</table>
</body>
</html>