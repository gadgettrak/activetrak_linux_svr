<?php $mapUrl = "http://maps.google.com/maps?q=" . $deviceConnection->ip_lat . ",+" . $deviceConnection->ip_lon . "(Approximate+Device+Location)&iwloc=A&hl=en"; ?>
--------------------------------------------------------------------------------
GadgetTrak Tracking report
--------------------------------------------------------------------------------
The following details have been collected from your missing device

--------------------------------------------------------------------------------
System Information
--------------------------------------------------------------------------------
Device Description: <?=$device->description?> 

--------------------------------------------------------------------------------
Location Information
--------------------------------------------------------------------------------
<?php if((float)$deviceConnection->ip_lat != 0 && (float)$deviceConnection->ip_lon != 0) {?>
Latitude: <?=$deviceConnection->ip_lat?> 
Longitude: <?=$deviceConnection->ip_lon?> 
Map: <?=$mapUrl?> 
<?} else {?>
Location information not available
<?php }?>

To disable tracking for this device, please log into your account at:
https://account.gadgettrak.com