<?php $mapUrl = "http://maps.google.com/maps?q=" . $deviceConnection->ip_lat . ",+" . $deviceConnection->ip_lon . "(Approximate+Device+Location)&iwloc=A&hl=en"; ?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>GadgetTrak Tracking Report</h3>
			<!--
			<p>
				<?=$xml->UTCTime?> UTC*<br/>
			</p><br/><br/>
			-->
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse:collapse;" align="center">
				<tr>
					<th colspan="2" style="font:13px Arial;background-color:#333;color:#fff;padding:3px;border:1px solid #aaa;">System Information</th>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Device Description</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$device->description?></td>
				</tr>
				<tr>
					<th colspan="2" style="font:13px Arial;background-color:#333;color:#fff;padding:3px;border:1px solid #aaa;">Location</th>
				</tr>

				<?php if((float)$deviceConnection->ip_lat != 0 && (float)$deviceConnection->ip_lon != 0) {?>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Latitude</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$deviceConnection->ip_lat?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Longitude</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$deviceConnection->ip_lon?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Map</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;">
						<a href="<?=$mapUrl?>"><?=$mapUrl?></a></td>
				</tr>
				<?} else {?>
				<tr>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;" colspan="2">Location information not available</td>
				</tr>
				<?php }?>
			</table>
			<br/>
			<p>To disable tracking for this device, please log into your account at: <a href="https://account.gadgettrak.com">https://account.gadgettrak.com</a></p>
		</td>
	</tr>
</table>
</body>
</html>