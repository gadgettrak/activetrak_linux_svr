<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>Expired License Notice</h3>
			<p>The following device has attempted to check in but your license has expired.</p>

			<table>
				<tr>
					<td>License Key</td>
					<td><?=$license->key?></td>
				</tr>
				<tr>
					<td>ID</td>
					<td><?=$device->deviceid?></td>
				</tr>
				<tr>
					<td>Type</td>
					<td><?=$device->type?></td>
				</tr>
				<tr>
					<td>Model</td>
					<td><?=$device->model?></td>
				</tr>
				<tr>
					<td>Manufacturer</td>
					<td><?=$device->manufacturer?></td>
				</tr>
				<tr>
					<td>OS</td>
					<td><?=$device->os?></td>
				</tr>
				<tr>
					<td>Serial</td>
					<td><?=$device->serial?></td>
				</tr>
				<tr>
					<td>Color</td>
					<td><?=$device->color?></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><?=$device->description?></td>
				</tr>
				<tr>
					<td>MACAddress</td>
					<td><?=$device->macAddress?></td>
				</tr>
				<tr>
					<td>Label Code</td>
					<td><?=$device->labelcode?></td>
				</tr>
				<tr>
					<td>Date Registered</td>
					<td><?=$device->datecreated?></td>
				</tr>
			</table>
			<br/><br/>
			<p>If you need assistance, please contact us at: support@gadgettrak.com</p>
		</td>
	</tr>
</table>
</body>
</html>

