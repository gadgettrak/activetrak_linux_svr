Expired License Notice
-----------------------------------------------------

The following license key has expired.
Please contact support@gadgettrak.com to nenew.

License Key: <?=$license->key ?>
License Length: <?=$license->length ?>
Expiration Date: <?=gmdate("F j, Y, g:i a", $license->getExpirationDate());?>