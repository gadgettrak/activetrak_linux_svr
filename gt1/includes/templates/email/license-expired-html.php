<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
<tr>
<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
</tr>
<tr>
<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">

<h1>Your GadgetTrak Coverage Has Expired</h1>

<p>Protecting a laptop with GadgetTrak greatly increases your chances of
recovering it if it's stolen. Renew your subscription now to reinstate your coverage.</p>

<center>
<p style="margin:10px 0 30px 0;border-radius:10px;border:1px solid #bbb;background-color:#eee; width:300px;color:#000;font-size:12px;padding:10px;font-weight:bold">
<table>
<tr>
<td style="font-weight: normal; font-size: 12px; line-height: 18px; font-family: Arial, Helvetica, sans-serif; color: #575757;">License Key: </td>
<td style="font-weight: bold; font-size: 12px; line-height: 18px; font-family: Arial, Helvetica, sans-serif; color: #000;"><?= $userLicenseResult->key?></td>
</tr>
<tr>
<td style="font-weight: normal; font-size: 12px; line-height: 18px; font-family: Arial, Helvetica, sans-serif; color: #575757;">Expiration Date: </td>
<td style="font-weight: bold; font-size: 12px; line-height: 18px; font-family: Arial, Helvetica, sans-serif; color: #000;"><?= $userLicenseResult->expiredDate->format("Y-m-d")?></td>
</tr>
</table>
</p>
</center>

<h2>Reinstate GadgetTrak</h2>

<p><center>
<a href="https://secure.esellerate.net/secure/prefill.aspx?s=STR2046114780&Cmd=Buy&_CartItem0.SkuRefNum=SKU07270055655&Coupon=RENEW10OFF&Options=PREVALIDATECOUPON&_Custom.Data0=<?=$userLicenseResult->key?>&page=MultiCart.htm"><img src="http://www.gadgettrak.com/emails/renew/images/renewsave10.png"></a><br>
</center></p>

<p>Coverage will be reinstated within 24 hours.</p>

<p>We also encourage you to install the latest version of the GadgetTrak
application to make sure you are getting the most accurate and reliable
security. This is optional and can be done following
<a href="https://gadgettrak.tenderapp.com/kb/laptop/how-to-update-your-license-key">
these instructions</a>.</p>

<hr>

<p>Did you know that GadgetTrak also can protect your iPad, iPhone, iPod touch,
Android, and BlackBerry? Check out our <a href="http://www.gadgettrak.com/products">
Products Page</a> for more details.</p>

</td>
</tr>
</table>
</body>
</html>