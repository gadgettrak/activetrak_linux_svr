<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>Device Registration Notice</h3>
			<p>Congratulations,</p>
			<p>You have successfully registered your device. Below you will find the details for the device that has been registered as well as a link to the online control panel where you can activate tracking.</p>

			<table>
				<tr>
					<td>License Key</td>
					<td><?=$license->key?></td>
				</tr>
				<tr>
					<td>ID</td>
					<td><?=$device->deviceid?></td>
				</tr>
				<tr>
					<td>Key</td>
					<td><?=$device->devicekey?></td>
				</tr>
				<tr>
					<td>Type</td>
					<td><?=$device->type?></td>
				</tr>
				<tr>
					<td>OS</td>
					<td><?=$device->os?></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><?=$device->description?></td>
				</tr>
				<tr>
					<td>MACAddress</td>
					<td><?=$device->macAddress?></td>
				</tr>
				<tr>
					<td>Date Registered</td>
					<td><?=$device->datecreated?></td>
				</tr>
			</table>

			<p>To enable tracking or to make changes to information pertaining to this device, please log into <a href="https://account.gadgettrak.com">https://account.gadgettrak.com.</a></p>
		</td>
	</tr>
</table>
</body>
</html>