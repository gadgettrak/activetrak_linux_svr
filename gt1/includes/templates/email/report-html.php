<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>GadgetTrak Tracking Report</h3>
			<p>
				<?=$xml->UTCTime?> UTC*<br/>
			</p><br/><br/>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse:collapse;" align="center">
				<tr>
					<th colspan="2" style="font:13px Arial;background-color:#333;color:#fff;padding:3px;border:1px solid #aaa;">System Information</th>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Device Description</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$device->description?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">User Name</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->UserName?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Host Name</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->HostName?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">MACAddress</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->MACAddress?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Memory</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->Memory?></td>
				</tr>
				<?php try {?>
					<?php
						foreach($xml->HardDrives->Drive as $drive) {
					?>
					<tr>
						<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;" rowspan="2">Hard drive</td>
						<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;">Serial: <?=$drive->Serial?>
					</tr>
					<tr>
						<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;">Size:<?=$drive->Size?></td>
					</tr>
					<?php } ?>
				<?php } catch (Exception $e) {} ?>
				<?php if (count($xml->WIFIs->Name)) {?>
				<tr>
					<th colspan="2" style="font:13px Arial;background-color:#333;color:#fff;padding:3px;border:1px solid #aaa;">WiFi Networks Detected</th>
				</tr>
				<?php
					foreach($xml->WIFIs->Name as $WiFi) {?>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Network name</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$WiFi?></td>
				</tr>
				<?php }?>
				<?php }?>
				<tr>
					<th colspan="2" style="font:13px Arial;background-color:#333;color:#fff;padding:3px;border:1px solid #aaa;">Network Information</th>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">External IP Address</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->ExternalIP?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Internal IP Address</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->InternalIP?></td>
				</tr>
				
				<tr>
					<th colspan="2" style="font:13px Arial;background-color:#333;color:#fff;padding:3px;border:1px solid #aaa;"><?=$xml->TrackSource?> based location</th>
				</tr>
				<?php if((float)$xml->Latitude != 0 && (float)$xml->Longitude != 0) {?>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Latitude</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->Latitude?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Longitude</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;"><?=$xml->Longitude?></td>
				</tr>
				<tr>
					<td style="width:120px;font-weight:bold;font:12px Arial; color:#000; padding:4px;border:1px solid #aaa;">Map</td>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;">http://maps.google.com/maps?q=<?=$xml->Latitude?>,+<?=$xml->Longitude?>(Approximate+Device+Location)&iwloc=A&hl=en</td>
				</tr>
				<?} else {?>
				<tr>
					<td style="font:12px Arial; color:#666; padding:4px;border:1px solid #aaa;" colspan="2">Location information not available</td>
				</tr>
				<?php }?>
			</table>
			<br/>
			<p>To disable tracking for this device, please log into your account at: <a href="https://account.gadgettrak.com">https://account.gadgettrak.com</a></p>
			<p>*<small>Time as reported by the device that is being tracked</small></p>
		</td>
	</tr>
</table>
</body>
</html>

<br/><br/><br/><br/>