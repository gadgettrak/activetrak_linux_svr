--------------------------------------------------------------------------------
Tracking report:<?=$xml->UTCTime?> UTC*
--------------------------------------------------------------------------------
The following details have been collected from your missing computer

--------------------------------------------------------------------------------
System Information
--------------------------------------------------------------------------------
User Name: <?=$xml->UserName?> 
Host Name: <?=$xml->HostName?> 
MACAddress: <?=$xml->MACAddress?> 
Memory: <?=$xml->Memory?> 
Hard Drives
<?php try {?>
<?php
	foreach($xml->HardDrives->Drive as $drive) {
?>
	Serial: <?=$drive->Serial?> 
	Size: <?=$drive->Size?> 
<?}?>
<?php } catch (Exception $e) {} ?>
<?php if (count($xml->WIFIs->Name)) {?>
--------------------------------------------------------------------------------
WiFi Networks Detected
--------------------------------------------------------------------------------
<?php	foreach($xml->WIFIs->Name as $WiFi) {?>
Network name: <?=$WiFi?> 
<?}?>
<?}?>

--------------------------------------------------------------------------------
Network Information
--------------------------------------------------------------------------------
External IP Address:<?=$xml->ExternalIP?> 
Internal IP Address:<?=$xml->InternalIP?> 

--------------------------------------------------------------------------------
<?=$xml->TrackSource?> based location
--------------------------------------------------------------------------------
<?php if((float)$xml->Latitude != 0 && (float)$xml->Longitude != 0) {?>
Latitude: <?=$xml->Latitude?> 
Longitude: <?=$xml->Longitude?>
Map: http://maps.google.com/maps?q=<?=$xml->Latitude?>,+<?=$xml->Longitude?>(Approximate+Device+Location)&iwloc=A&hl=en
<?} else {?>
Location information not available
<?php }?>


To disable tracking for this device, please log into your account at:
https://account.gadgettrak.com

*Time as reported by the device that is being tracked



