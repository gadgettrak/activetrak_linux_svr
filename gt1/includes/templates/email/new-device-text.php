Device Registration Notice 
----------------------------------------------------- 

Congratulations, 

You have successfully registered your device. Below you will find the details
for the device that has been registered as well as a link to the online control
panel where you can activate tracking. 
 
License Key: <?=$license->key?> 
ID: <?=$device->deviceid?> 
Key: <?=$device->devicekey?> 
Type: <?=$device->type?> 
OS: <?=$device->os?> 
Description: <?=$device->description?> 
MACAddress: <?=$device->macAddress?> 
Date Registered: <?=$device->datecreated?> 
 
To enable tracking or to make changes to information pertaining to this device, please log into https://account.gadgettrak.com.