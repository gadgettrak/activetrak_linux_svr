<html>
<body style="background-color:#fff;" bgcolor="#ffffff">
<table width="800" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td style="padding:0;"><img src="http://www.gadgettrak.com/_gfx/email-support/account_notifications/mail_header.gif"/></td>
	</tr>
	<tr>
		<td style="font-family: Helvetica, Arial, sans-serif;font-size:12px;border-bottom:1px solid #999;border-right:1px solid #999;border-left:1px solid #999;padding:20px;">
			<h3>Device Registration Notice</h3>
			<p>Congratulations,</p>
			<p>You have successfully registered your device!</p>
			<p>To enable tracking or to make changes to information pertaining to this device, please log into <a href="https://account.gadgettrak.com">https://account.gadgettrak.com.</a></p>
		</td>
	</tr>
</table>
</body>
</html>