New User Registration
--------------------------------------------------------------------------------

Thank you for registering at GadgetTrak.com!

The next step to enable your account is to verify your email address. Please
follow the link below:
https://account.gadgettrak.com/verify.php?key=<?=$user->email_key?> 

If you have any problems verifying your email address,
please contact support@gadgettrak.com
