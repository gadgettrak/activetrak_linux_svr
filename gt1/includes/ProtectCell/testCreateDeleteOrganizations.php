<?php

require_once("inc/OrganizationManager.php");
require_once("inc/core.php");

$organizationTemplate="<StoreData><StoreID></StoreID><Description></Description></StoreData>";

$xml = simplexml_load_string($organizationTemplate);

$organzationsXML = array();

Utils::dumpHeader("Creating Organzations");

for($i = 0; $i < $count; $i++) {
	$xml->StoreID = uniqid();
	$xml->Description = uniqid();
	print("Creating Organzation: ($xml->StoreID) $xml->Description -- ");
	$output = OrganizationManager::create($seedUserId, $seedPassword, $xml->asXML());
	array_push($organzationsXML, simplexml_load_string($xml->asXML()));
	print("Result: " . $output->headers['http_code'] . "\n");
}

Utils::dumpHeader("Deleting Organzations");

foreach($organzationsXML as $organization) {
	print("Deleting Organzation: ($organization->StoreID) $organization->Description -- ");
	$output = OrganizationManager::delete($seedUserId, $seedPassword, $organization->asXML());
	print("Result: " . $output->headers['http_code'] . "\n");
}

?>