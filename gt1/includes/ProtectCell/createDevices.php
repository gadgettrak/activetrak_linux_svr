<?php

require_once("inc/DeviceManager.php");
require_once("inc/core.php");

$xml = simplexml_load_file($filePath);

if($action == "create" || $action == "test") {
	$index = 0;

	Utils::dumpHeader("Creating Devices");

	foreach($xml->DeviceData as $deviceData) {
		$index++;
		print("($index) Creating device: $deviceData->Policy / $deviceData->PhoneNo) -- ");
		$output = DeviceManager::create($seedUserId, $seedPassword, $deviceData->asXML());
		print("Result: " . $output->headers['http_code'] . "\n");
	}
}

if($action == "delete" || $action == "test") {
	$index = 0;

	Utils::dumpHeader("Deleting Devices");

	foreach($xml->DeviceData as $deviceData) {
		$index++;
		print("($index) Deleting device: $deviceData->Policy / $deviceData->PhoneNo) -- ");
		$output = DeviceManager::delete($seedUserId, $seedPassword, $deviceData->asXML());
		print("Result: " . $output->headers['http_code'] . "\n");
	}
}

?>