<?php

class AuthHeaders {

	public static function getAuthorizationHeader($userId, $password)
	{
		$passwordBase = hash("sha256", (utf8_encode(strtolower($userId) . $password)), true);
		$userPassword = base64_encode($passwordBase);
		$userCredentials = base64_encode("$userId:$userPassword");
		$authHeader = "Authorization: Basic $userCredentials";
		$header = array(
			"POST HTTP/1.0",
			"Content-type: text/xml; charset=utf-8",
			"Content-transfer-encoding: text",
			$authHeader
		);
		array_push($header, "Connection: close");
		return $header;
	}

}

?>