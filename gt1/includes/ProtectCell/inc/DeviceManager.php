<?php

require_once("inc/AuthHeaders.php");
require_once("inc/CurlRequest.php");

class DeviceManager {

	public static function create($userId, $password, $postBody)
	{

		if(DEBUG_MODE) {
			$postUrl = "http://protectcell.activetrak.com/protectcell/devices/create";
		} else {
			$postUrl = "https://protectcell.activetrak.com/devices/create";
		}

		$header = AuthHeaders::getAuthorizationHeader($userId, $password);
		$curlRequest = new CurlRequest($postUrl, $postBody, $header);
		return $curlRequest->exec();
	}

	public static function delete($userId, $password, $postBody)
	{

		if(DEBUG_MODE) {
			$postUrl = "http://protectcell.activetrak.com/protectcell/devices/delete";
		} else {
			$postUrl = "https://protectcell.activetrak.com/devices/delete";
		}

		$header = AuthHeaders::getAuthorizationHeader($userId, $password);
		$curlRequest = new CurlRequest($postUrl, $postBody, $header, "DELETE");
		return $curlRequest->exec();
	}

}

?>
