<?php

class Utils {

	public static function dumpHeader($str, $pad = true)
	{

		if($pad) {
			print("\n");
		}
		print(str_repeat("*", 80) . "\n");
		print($str . "\n");
		print(str_repeat("*", 80) . "\n");
		if($pad) {
			print("\n");
		}

	}

}

?>