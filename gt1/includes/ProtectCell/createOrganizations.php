<?php

require_once("inc/OrganizationManager.php");
require_once("inc/core.php");

$xml = simplexml_load_file($filePath);

if($action == "create" || $action == "test") {
	$index = 0;

	Utils::dumpHeader("Creating Organizations");

	foreach($xml->StoreData as $storeData) {
		$index++;
		print("($index) Creating store: $storeData->StoreID ($storeData->Description) -- ");
		$output = OrganizationManager::create($seedUserId, $seedPassword, $storeData->asXML());
		print("Result: " . $output->headers['http_code'] . "\n");
	}
}

if($action == "delete" || $action == "test") {
	$index = 0;

	Utils::dumpHeader("Deleting Organizations");

	foreach($xml->StoreData as $storeData) {
		$index++;
		print("($index) Deleting store: $storeData->StoreID ($storeData->Description) -- ");
		$output = OrganizationManager::delete($seedUserId, $seedPassword, $storeData->asXML());
		print("Result: " . $output->headers['http_code'] . "\n");
	}
}

?>