<?php

require_once("inc/DeviceManager.php");
require_once("inc/core.php");

$deviceTemplate="<DeviceData>
	<Policy></Policy>
	<PhoneNo></PhoneNo>
	<UserID>msweet@alien109.com</UserID>
	<DeviceType>iPhone</DeviceType>
	<ExpirationDate>10/28/2050</ExpirationDate>
</DeviceData>";

$xml = simplexml_load_string($deviceTemplate);

$startNumber = 1111111111;
$format = "%3s\n";

$devicesXML = array();

Utils::dumpHeader("Creating Devices");

for($i = 0; $i < $count; $i++) {
	$phoneNumber = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $startNumber + $i);
	$guid = uniqid();
	$xml->Policy = $guid;
	$xml->PhoneNo = $phoneNumber;
	print("Creating Device: $xml->Policy -- ");
	$output = DeviceManager::create($seedUserId, $seedPassword, $xml->asXML());
	array_push($devicesXML, simplexml_load_string($xml->asXML()));
	print("Result: " . $output->headers['http_code'] . "\n");
}

Utils::dumpHeader("Deleting Devices");

foreach($devicesXML as $device) {
	print("Deleting Device: $device->Policy -- ");
	$output = DeviceManager::delete($seedUserId, $seedPassword, $device->asXML());
	print("Result: " . $output->headers['http_code'] . "\n");
}

?>