<?php

require_once("test/inc/AuthHeaders.php");
require_once("test/inc/CurlRequest.php");

class OrganizationManager {

	public static function create($userId, $password, $postBody)
	{

		if(DEBUG_MODE) {
			$postUrl = "http://protectcell.activetrak.com/stores/create";
		} else {
			$postUrl = "https://protectcell.activetrak.com/stores/create";
		}

		$header = AuthHeaders::getAuthorizationHeader($userId, $password);
		$curlRequest = new CurlRequest($postUrl, $postBody, $header);
		return $curlRequest->exec();
	}

	public static function delete($userId, $password, $postBody)
	{

		if(DEBUG_MODE) {
			$postUrl = "http://protectcell.activetrak.com/stores/delete";
		} else {
			$postUrl = "https://protectcell.activetrak.com/stores/delete";
		}

		$header = AuthHeaders::getAuthorizationHeader($userId, $password);
		$curlRequest = new CurlRequest($postUrl, $postBody, $header, "DELETE");
		return $curlRequest->exec();
	}

}

?>
