<?php

class AuthHeaders {

	public static function getAuthorizationHeader($userId, $password)
	{

		$userCredentials = base64_encode("$userId:$password");
		$authHeader = "Authorization: Basic $userCredentials";
		$header = array(
			"POST HTTP/1.0",
			"Content-type: text/xml; charset=utf-8",
			"Content-transfer-encoding: text",
			$authHeader
		);
		array_push($header, "Connection: close");
		return $header;
	}

}

?>