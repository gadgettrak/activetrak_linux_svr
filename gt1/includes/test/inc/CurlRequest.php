<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("test/inc/CurlResponse.php");;
require_once("test/inc/Utils.php");;

class CurlRequest
{
    private $ch;
    private $callback;

	public function __construct($postUrl, $postBody, $header, $method = null) {
		$this->ch = curl_init();

		array_push($header, "Expect:");

		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($this->ch, CURLOPT_URL, $postUrl);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, 99);
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postBody);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);

		if($method) {
			curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);
		}
	}

	public function exec($callback = null) {
		$this->callback = $callback;
		$data = curl_exec($this->ch);

		if (!curl_error($this->ch)) {
			$curlResponse = new CurlResponse($data, curl_getinfo($this->ch));
			if ($this->callback)
			{
				return call_user_func($callback, $curlResponse);
			} else {
				return $curlResponse;
			}
		} else {
			return curl_error($this->ch);
		}
		curl_close($this->ch);
	}
}

?>
