<?php

class Utils {

	public static function dumpHeader($str, $pad = true)
	{
		if($pad) {
			print("<p>");
		}
		print(str_repeat("*", 80) . "\n");
		print($str . "\n");
		print(str_repeat("*", 80) . "\n");
		if($pad) {
			print("</p>");
		}
	}

	public static function debug($str)
	{
		print("<p>$str</p>");
	}

}

?>