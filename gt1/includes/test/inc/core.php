<?php

require_once("test/inc/Utils.php");;

Utils::dumpHeader("Params");

$arguments = getopt("a:d:f:c:");

$action = $arguments['a'];
if(!$action) {
	$action = "create";
}

Utils::debug("Action: $action\n");

if($arguments['f']) {
	$filePath = $arguments['f'];
	if(!is_file($filePath)) {
		die("Not a valid file for source.\n");
	}
	Utils::debug("Input file: $filePath");
}

if($arguments['c']) {
	$count = $arguments['c'];
	Utils::debug("Create count: $count");
} else {
	$count = 10;
}

define("DEBUG_MODE", ($arguments['d'] == 'true'));
Utils::debug("Debug mode: " . DEBUG_MODE . "");

?>