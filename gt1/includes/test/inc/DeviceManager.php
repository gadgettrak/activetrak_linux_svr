<?php

require_once("test/inc/AuthHeaders.php");
require_once("test/inc/CurlRequest.php");
require_once("test/inc/Utils.php");

class DeviceManager {

	public static function create($userId, $password, $postBody)
	{
		$postUrl = "https://gt1.gadgettrak.com/iphone/device/create/index.php";
		Utils::debug("Post URL: $postUrl");
		Utils::debug("Post Body: " . var_export($postBody, true));

		$header = AuthHeaders::getAuthorizationHeader($userId, $password);
		$curlRequest = new CurlRequest($postUrl, $postBody, $header);
		return $curlRequest->exec();
	}

}

?>
