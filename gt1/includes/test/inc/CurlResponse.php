<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class CurlResponse
{

    public $data;
    public $headers;

	public function __construct($data, $headers) {
		$this->data = $data;
		$this->headers = $headers;
	}

}

?>