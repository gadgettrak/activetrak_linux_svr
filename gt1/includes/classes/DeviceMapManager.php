<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceMapDAO.php");

class DeviceMapManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new DeviceMapDAO();
	}

	/**
	 *
	 * @param <type> $deviceMap
	 * @return <type>
	 */
	public function addNewDeviceMap($deviceMap)
	{
		return $this->dao->saveNewDeviceMap($deviceMap);
	}

	/**
	 *
	 * @param <type> $key
	 * @return <type> 
	 */
	public function deleteDeviceMapByKey($key)
	{
		return $this->dao->deleteDeviceMapByKey($key);
	}
	
	
	

}


?>