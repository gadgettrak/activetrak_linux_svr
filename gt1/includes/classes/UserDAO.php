<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/User.php");
require_once("classes/UserCredentials.php");
require_once("classes/HttpUtils.php");

class UserDAO extends BaseDAO
{

	/**
	 * Gets a User by from the database
	 * @param string $id
	 * @return Device
	 */
	public function getUserId($id)
	{
		HttpUtils::cleanInput($id);
		$result = $this->queryUniqueObject("SELECT * FROM uesrs WHERE id=$id");
		if($result) {
			return User::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 * Saves a new User to the database
	 * @param User $user
	 * @return User
	 */
	public function saveNewUser($user)
	{
		$q = "INSERT INTO users (
			`fname`,
			`lname`,
			`address1`,
			`address2`,
			`city`,
			`state`,
			`postal`,
			`country`,
			`email`,
			`company`,
			`phone`,
			`email_val`,
			`email_key`,
			`ipaddress`,
			`datecreated`,
			`suspended`,
			`password`,
			`tester`,
			`partnerid`
		) VALUES (
			'$user->fname',
			'$user->lname',
			'$user->address1',
			'$user->address2',
			'$user->city',
			'$user->state',
			'$user->postal',
			'$user->country',
			'$user->email',
			'$user->company',
			'$user->phone',
			'$user->email_val',
			'$user->email_key',
			'$user->ipaddress',
			'$user->datecreated',
			'$user->suspended',
			'$user->password',
			'$user->tester',
			'$user->partnerid'
		)";
		$this->execute($q);
		$user->userid = mysql_insert_id();
		return $user;
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @return User
	 */
	public function getUserByCredentials($credentials)
	{
		HttpUtils::cleanInput($credentials->email);
		HttpUtils::cleanInput($credentials->password);

		$result = $this->queryUniqueObject("SELECT * FROM users WHERE email='$credentials->email' AND password='$credentials->password'");
		if($result) {
			return User::newFromDatabase($result);
		} else {
			return null;
		}
	}

	public function getUserByDeviceCredentials($userCredentials, $deviceKey) {

		HttpUtils::cleanInput($deviceKey);
		HttpUtils::cleanInput($userCredentials->password);
		HttpUtils::cleanInput($userCredentials->email);

		$q = "SELECT users.* FROM devicemap INNER JOIN users ON users.userid = devicemap.userid INNER JOIN devices ON devices.devicekey = devicemap.devicekey WHERE users.email='$userCredentials->email' AND devices.devicePassword='$userCredentials->password' AND devices.devicekey = '$deviceKey'";

		$result = $this->queryUniqueObject($q);

		if($result) {
			return User::newFromDatabase($result);
		} else {
			return null;
		}

	}

	/**
	 *
	 * @param string $email
	 * @return User
	 */
	public function getUserByEmail($email) {
		HttpUtils::cleanInput($email);
		$q = "SELECT * FROM users WHERE email='$email'";
		$result = $this->queryUniqueObject($q);
		if($result) {
			return User::newFromDatabase($result);
		} else {
			return null;
		}
	}

	public function userNameIsAvailable($userName)
	{
		HttpUtils::cleanInput($userName);
		$q = "SELECT email FROM users WHERE email = '$userName'";
		$result = $this->query($q);
		return (mysql_numrows($result) < 1);
	}

	public function isValidEmailKey($key)
	{
		HttpUtils::cleanInput($key);
		$q = "SELECT email_key FROM users WHERE email_key = '$key'";
		$result = $this->query($q);
		return ($this->numRows($result) > 0);
	}

	public function isValidEmail($email)
	{
		HttpUtils::cleanInput($email);
		$q = "SELECT email FROM users WHERE email = '$email'";
		$result = $this->query($q);
		return ($this->numRows($result) > 0);
	}


}

?>