<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceDAO.php");

class DeviceManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new DeviceDAO();
	}

	/**
	 *
	 * @param <type> $macAddress
	 * @return <type>
	 */
	public function isDuplicateDevice($macAddress)
	{
		return $this->dao->isDuplicateDevice($macAddress);
	}

	/**
	 *
	 * @param <type> $macAddress
	 * @return <type>
	 */
	public function isDuplicateDeviceByGuid($devicekey)
	{
		return $this->dao->isDuplicateDeviceByGuid($devicekey);
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @return <type>
	 */
	public function getDeviceByKey($deviceKey)
	{
		return $this->dao->getDeviceByKey($deviceKey);
	}

	/**
	 *
	 * @param <type> $device
	 * @return <type>
	 */
	public function addNewDevice($device)
	{
		return $this->dao->saveNewDevice($device);
	}

	/**
	 *
	 * @param <type> $device
	 * @return <type>
	 */
	public function updateDevice($device)
	{
		return $this->dao->updateDevice($device);
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @param <type> $licenseKey
	 * @param <type> $macAddress
	 * @param <type> $userId
	 * @return <type>
	 */
	public function getDeviceForDeleteRequest($deviceKey, $licenseKey, $macAddress, $userId)
	{
		return $this->dao->getDeviceForDeleteRequest($deviceKey, $licenseKey, $macAddress, $userId);
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @param <type> $userId
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function isValidDeviceRequest($deviceKey, $userId, $licenseKey = null)
	{
		return $this->dao->isValidDeviceRequest($deviceKey, $userId, $licenseKey);
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @param <type> $userId
	 * @return <type>
	 */
	public function isValidIPhoneRequest($deviceKey, $userId)
	{
		return $this->dao->isValidIPhoneRequest($deviceKey, $userId, $licenseKey);
	}

	/**
	 *
	 * @param <type> $id
	 * @return <type>
	 */
	public function deleteDeviceById($id)
	{
		return $this->dao->deleteDeviceById($id);
	}

	public function deviceCredentialsMatch($deviceKey, $userCredentials) {
		return $this->dao->deviceCredentialsMatch($deviceKey, $userCredentials);
	}


}


?>