<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/HttpUtils.php");
require_once("classes/UserManager.php");
require_once("classes/UserCredentials.php");
require_once("classes/Logger.php");
require_once("classes/DeviceManager.php");
require_once("classes/ErrorManager.php");
require_once("classes/Error.php");

class Authorization {

	/**
	 * Validates the current user based on authorization headers with device credential fallback
	 * @param $deviceKey string 
	 * @return User
	 */
	public static function validateUser($deviceKey = null)
	{
		$user = self::getCurrentUser($deviceKey);
		if($user) {
			return $user;
		} else {
			$errorManager = new ErrorManager();
			$userCredentials = self::getCurrentUserCredentials();
			$error = new Error();
			$error->date = gmdate("Y-m-d H:i:s");
			$error->deviceKey = $deviceKey;
			$error->email = $userCredentials->email;
			$error->errorType = "authorization";
			$error->password = $userCredentials->password;
			$errorManager->createIfDeviceKeyUnique($error);
			HttpUtils::setHttpStatus(401);
			die();
		}
	}

	/**
	 *
	 * @return <type>
	 */
	public static function getCurrentUser($deviceKey = null)
	{
		$user = null;
		$userManager = new UserManager();
		$userCredentials = self::getCurrentUserCredentials();

		if(!self::isValidUserCredentials($userCredentials)) {
			return null;
		}

		$user = $userManager->getUserByUserCredentials($userCredentials);

		if(!$user && $deviceKey != null) {
			$user = $userManager->getUserByDeviceCredentials($userCredentials, $deviceKey);
		}

		return $user;
	}

	/**
	 *
	 * @return UserCredentials
	 */
	public static function getCurrentUserCredentials()
	{
		return new UserCredentials($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
	}

	 /* @var $userCredentials UserCredentials */

	public static function isValidUserCredentials($userCredentials) {

		if(isset($userCredentials->email) && isset($userCredentials->password)) {
			return true;
		} else {
			return false;
		}
	}

}

?>