<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/UserLicenseResult.php");
require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class LicenseDAO extends BaseDAO
{

	/**
	 *
	 * @param <type> $licenseKey
	 * @return License
	 */
	public function getLicenseByKey($licenseKey)
	{

		HttpUtils::cleanInput($licenseKey);
		$result = $this->queryUniqueObject("SELECT * FROM license WHERE `key`='$licenseKey'");
		//if(CONFIG_APP_LOGGING) Logger::logToFile("License query: SELECT * FROM license WHERE `key`='$licenseKey'");

		if($result) {
			return new License(
				$result->id,
				$result->userId,
				$result->deviceId,
				$result->productId,
				$result->resellerId,
				$result->length,
				$result->licenses,
				$result->key,
				$result->used,
				$result->activated,
				$result->nfr,
				$result->os
			);
		} else {
			return null;
		}

	}

	/**
	 *
	 * @param <type> $deviceId
	 * @return License
	 */
	public function getLicenseByDeviceId($deviceId)
	{
		HttpUtils::cleanInput($deviceId);
		$result = $this->queryUniqueObject("SELECT * FROM license WHERE `deviceId`='$deviceId'");
		if($result) {
			return new License(
				$result->id,
				$result->userId,
				$result->deviceId,
				$result->productId,
				$result->resellerId,
				$result->length,
				$result->licenses,
				$result->key,
				$result->used,
				$result->activated,
				$result->nfr,
				$result->os
			);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param <type> $license
	 */
	public function updateLicense($license)
	{
		$q = "UPDATE license SET
				userId = '$license->userId',
				deviceId = '$license->deviceId',
				productId = '$license->productId',
				resellerId = '$license->resellerId',
				length = '$license->length',
				licenses = '$license->licenses',
				`key` = '$license->key',
				used = '$license->used',
				activated = '$license->activated',
				nfr = '$license->nfr',
				os = '$license->os'
			WHERE
				id=$license->id";

		$this->execute($q);
	}

	/**
	 *
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function isValidKey($licenseKey)
	{
		$result = $this->query("SELECT id FROM license WHERE `key`='$licenseKey'");
		if($this->numRows($result)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param <type> $userId
	 * @param <type> $deviceId
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function mapLicenseToUserAndDevice($userId, $deviceId, $licenseKey)
	{
		$q = "UPDATE license SET userId='$userId', deviceId='$deviceId', activated=NOW() WHERE `key`='$licenseKey'";
		return $this->execute($q);
	}

	/**
	 *
	 * @param <type> $licenseKey
	 * @return <type> 
	 */
	public function resetLicense($licenseKey)
	{
		$q = "UPDATE license SET userId=null, deviceId=null, used='N' WHERE `key`='$licenseKey'";
		return $this->execute($q);
	}

	public function getExpiringLicenses($interval = 0, $intervalUnit = "MONTH")
	{
		
		$operator = ($offset <= 0)?"DATE_ADD":"DATE_SUB";
		
		$q = "
		SELECT
			license.length as length,
			license.deviceId as deviceId,
			license.key as `key`,
			license.activated as activated,
			users.email as email,
			DATE_ADD(DATE(license.activated), INTERVAL license.length MONTH) as expiredDate
		FROM
			license
		INNER JOIN
			users
		ON
			users.userid=license.userId
		WHERE
			DATE_ADD(DATE(license.activated), INTERVAL license.length MONTH) = $operator(CURDATE(), INTERVAL '$interval' $intervalUnit)
		AND
			license.activated != '0000-00-00 00:00:00'
		AND
			license.length<>'-1'
		ORDER BY
			expiredDate ASC
		";

		$results = $this->query($q);
		$values = array();
		
		while ($row = mysql_fetch_object($results)) {
			array_push($values, UserLicenseResult::newFromDatabase($row));
		}
		return $values;
	}


	public function getUnactivatedTrials()
	{
		$query = "SELECT
			trials.date,
			license.`key`,
			users.email
		FROM
			users
		INNER JOIN
			trials ON users.userid = trials.userId
		INNER JOIN
			license ON license.id = trials.licenseId
		WHERE
			license.activated IS NULL
		AND
			DATE_ADD(DATE(trials.date), INTERVAL 4 DAY) = CURDATE()";

		$results = $this->query($query);
		$values = array();
		while ($row = mysql_fetch_object($results)) {
			new UserLicenseResult($row->email, $row->key, null, null, null, null);
			array_push($values, UserLicenseResult::newFromDatabase($row));
		}

		return $values;
	}


}