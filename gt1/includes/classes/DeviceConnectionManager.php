<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/DeviceConnectionDAO.php");

class DeviceConnectionManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new DeviceConnectionDAO();
	}

	public function getDeviceConnections($devicekey)
	{
		return $this->dao->getDeviceConnections($devicekey);
	}

	public function saveNewDeviceConnection($deviceConnection)
	{
		return $this->dao->saveNewDeviceConnection($deviceConnection);
	}

}


?>