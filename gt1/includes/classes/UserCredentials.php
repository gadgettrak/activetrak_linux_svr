<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class UserCredentials {

	public $email;
	public $password;
	
	public function __construct($email, $passwordHash)
	{
		$this->email = $email;
		$this->password = $passwordHash;
	}

	/**
	 *
	 * @return <type>
	 */
	public function toString()
	{
		return ("email: $this->email, password: $this->password");
	}

}

?>