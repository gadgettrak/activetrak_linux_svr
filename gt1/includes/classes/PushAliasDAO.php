<?php
/**
 * PushAliasDAO
 *
 * A DAO class for this model implementing methods called
 * from the manager class
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/BaseDAO.php");
require_once("classes/PushAlias.php");

class PushAliasDAO extends BaseDAO
{

	/**
	 * Saves a new PushAlias to the database
	 * @param PushAlias $pushAlias
	 * @return PushAlias
	 */
	public function create($pushAlias)
	{
		$q = "INSERT INTO pushAlias (
			`deviceKey`,
			`deviceToken`
		) VALUES (
			'$pushAlias->deviceKey',
			'$pushAlias->deviceToken'
		)";
		$this->execute($q);
		$pushAlias->deviceKey = mysql_insert_id();
		return $pushAlias;
	}

	/**
	 * Saves a new PushAlias to the database
	 * @param PushAlias $pushAlias
	 * @return PushAlias
	 */
	public function createUpdate($pushAlias)
	{

		$q = "INSERT INTO pushAlias (
			`deviceKey`,
			`deviceToken`
		) VALUES (
			'$pushAlias->deviceKey',
			'$pushAlias->deviceToken'
		) ON DUPLICATE KEY UPDATE `deviceToken` = '$pushAlias->deviceToken'";

		$this->execute($q);
		$pushAlias->deviceKey = mysql_insert_id();
		return $pushAlias;

	}

	/**
	 * Updates a PushAlias in the database
	 * @param PushAlias $pushAlias 
	 * @return PushAlias 
	 */
	public function update($pushAlias)
	{
		$q = "UPDATE pushAlias SET
			`deviceToken` = '$pushAlias->deviceToken' 
		WHERE
			`deviceKey` = '$pushAlias->deviceKey'";
		$this->execute($q);
		return mysql_affected_rows();
	}

	/**
	 * Deletes a PushAlias in the database
	 * @param PushAlias $pushAlias 
	 */
	public function delete($pushAlias)
	{
		$q = "DELETE from pushAlias WHERE `deviceKey` = '$pushAlias->deviceKey'";
		return $this->execute($q);
	}

	/**
	 * Retrieves a PushAlias in the database
	 * @param String deviceKey 
	 */
	public function get($deviceKey)
	{
		$q = "SELECT * from pushAlias WHERE `deviceKey` = '$deviceKey'";
		$result = $this->queryUniqueObject($q);
		if($result) {
			return PushAlias::newFromDatabase($result);
		} else {
			return null;
		}
	}
}

?>