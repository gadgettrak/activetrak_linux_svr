<?php

class UserLicenseResult
{
	public $email;
	public $key;
	public $deviceId;
	public $length;
	public $activated;
	public $expiredDate;

	public function __construct($email = null, $key = null, $deviceId = null, $length = null, $activated = null, $expiredDate = null)
	{
		$this->email = $email;
		$this->key = $key;
		$this->deviceId = $deviceId;
		$this->length = $length;
		$this->activated = new DateTime($activated);
		$this->expiredDate = new DateTime($expiredDate);
	}

	public static function newFromDatabase($result)
	{
		return new UserLicenseResult(
			$result->email,
			$result->key,
			$result->deviceId,
			$result->length,
			$result->activated,
			$result->expiredDate
		);
	}

}

?>