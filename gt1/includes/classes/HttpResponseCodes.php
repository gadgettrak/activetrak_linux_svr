<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class HttpResponseCodes {

	public static $CODES = array(
		"STATUS_100" => "100 Continue",
		"STATUS_101" => "101 Switching Protocols",
		"STATUS_200" => "200 OK",
		"STATUS_201" => "201 Created",
		"STATUS_202" => "202 Accepted",
		"STATUS_203" => "203 Non-Authoritative Information",
		"STATUS_204" => "204 No Content",
		"STATUS_205" => "205 Reset Content",
		"STATUS_206" => "206 Partial Content",
		"STATUS_300" => "300 Multiple Choices",
		"STATUS_301" => "301 Moved Permanently",
		"STATUS_302" => "302 Found",
		"STATUS_303" => "303 See Other",
		"STATUS_304" => "304 Not Modified",
		"STATUS_305" => "305 Use Proxy",
		"STATUS_307" => "307 Temporary Redirect",
		"STATUS_400" => "400 Bad Request",
		"STATUS_401" => "401 Unauthorized",
		"STATUS_402" => "402 Payment Required",
		"STATUS_403" => "403 Forbidden",
		"STATUS_404" => "404 Not Found",
		"STATUS_405" => "405 Method Not Allowed",
		"STATUS_406" => "406 Not Acceptable",
		"STATUS_407" => "407 Proxy Authentication Required",
		"STATUS_408" => "408 Request Timeout",
		"STATUS_409" => "409 Conflict",
		"STATUS_410" => "410 Gone",
		"STATUS_411" => "411 Length Required",
		"STATUS_412" => "412 Precondition Failed",
		"STATUS_413" => "413 Request Entity Too Large",
		"STATUS_414" => "414 Request-URI Too Long",
		"STATUS_415" => "415 Unsupported Media Type",
		"STATUS_416" => "416 Requested Range Not Satisfiable",
		"STATUS_417" => "417 Expectation Failed",
		"STATUS_500" => "500 Internal Server Error",
		"STATUS_501" => "501 Not Implemented",
		"STATUS_502" => "502 Bad Gateway",
		"STATUS_503" => "503 Service Unavailable",
		"STATUS_504" => "504 Gateway Timeout",
		"STATUS_505" => "505 HTTP Version Not Supported"
	);

}

?>