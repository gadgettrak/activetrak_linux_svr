<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class DeviceMap
{
	public $mapid;
	public $userid;
	public $devicekey;
	public $deviceid; //This field is not used, best I can tell (?) User devicekey for unique id.
	
	public function __construct($mapid = null, $userid = null, $devicekey = null) {
		$this->mapid = $mapid;
		$this->userid = $userid;
		$this->devicekey = $devicekey;
	}

	/**
	 *
	 * @return <type>
	 */
	public function toString()
	{
		return "mapid: $this->mapid, userid: $this->userid, devicekey: $this->devicekey";
	}

}