<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class License
{
	public $id;
	public $userId;
	public $deviceId;
	public $productId;
	public $resellerId;
	public $length;
	public $licenses;
	public $key;
	public $used;
	public $activated;
	public $nfr;
	public $os;
	
	public function __construct($id = null, $userId = null, $deviceId = null, $productId = null, $resellerId = null, $length = null, $licenses = null, $key = null, $used = null, $activated = null, $nfr = null, $os = null)
	{
		$this->id = $id;
		$this->userId = $userId;
		$this->deviceId = $deviceId;
		$this->productId = $productId;
		$this->resellerId = $resellerId;
		$this->length = $length;
		$this->licenses = $licenses;
		$this->key = $key;
		$this->used = $used;
		$this->activated = $activated;
		$this->nfr = $nfr;
		$this->os = $os;
	}

	/**
	 * determines if the license has expired
	 * @return bool
	 */
	public function isExpired($today = null)
	{
		if($this->length == -1 || !$this->hasValidDate()) {
			return false;
		}
		$today = ($today)?$today:strtotime(gmdate("Y-m-d H:i:s"));
		$expiresDate = $this->getExpirationDate();
		if ($today <= $expiresDate || !strtotime($this->activated) || strtotime($this->activated) < 0) {
			 return false;
		} else {
			 return true;
		}
	}

	/**
	 * Gets the expiration date of the license
	 * @return string unix timestamp
	 */
	public function getExpirationDate()
	{
		return strtotime("+$this->length months", strtotime($this->activated));
	}

	public function hasValidDate()
	{
		return ($this->activated != null && $this->activated != "0000-00-00 00:00:00" && $this->activated != "");
	}

}