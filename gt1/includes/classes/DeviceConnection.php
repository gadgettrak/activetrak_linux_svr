<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class DeviceConnection {

	public $connectionid;
	public $devicekey;
	public $timestamp;
	public $public_ip;
	public $internal_ip;
	public $stolen;
	public $username;
	public $computer_name;
	public $ip_country;
	public $ip_state;
	public $ip_city;
	public $postal_code;
	public $ip_lon;
	public $ip_lat;
	public $agent;
	public $os;
	public $serial;
	public $loc_state;
	public $loc_city;
	public $loc_street;
	public $did;

	public function __construct($deviceid = null, $type = null, $model = null, $manufacturer = null, $os = null, $theft_status = null, $serial = null, $color = null, $description = null, $devicekey = null, $macAddress = null, $labelcode = null, $datecreated = null, $pro = null, $productid = null)
	{
		$this->connectionid = $connectionid;
		$this->devicekey = $devicekey;
		$this->timestamp = $timestamp;
		$this->public_ip = $public_ip;
		$this->internal_ip = $internal_ip;
		$this->stolen = $stolen;
		$this->username = $username;
		$this->computer_name = $computer_name;
		$this->ip_country = $ip_country;
		$this->ip_state = $ip_state;
		$this->ip_city = $ip_city;
		$this->postal_code = $postal_code;
		$this->ip_lon = $ip_lon;
		$this->ip_lat = $ip_lat;
		$this->agent = $agent;
		$this->os = $os;
		$this->serial = $serial;
		$this->loc_state = $loc_state;
		$this->loc_city = $loc_city;
		$this->loc_street = $loc_street;
		$this->did = $did;
	}

	/**
	 *
	 * @param MySQLResource $result
	 * @return DeviceConnection
	 */
	public static function newFromDatabase($result)
	{
		return new DeviceConnection(
			$result->connectionid,
			$result->devicekey,
			$result->timestamp,
			$result->public_ip,
			$result->internal_ip,
			$result->stolen,
			$result->username,
			$result->computer_name,
			$result->ip_country,
			$result->ip_state,
			$result->ip_city,
			$result->postal_code,
			$result->ip_lon,
			$result->ip_lat,
			$result->agent,
			$result->os,
			$result->serial,
			$result->loc_state,
			$result->loc_city,
			$result->loc_street,
			$result->did
		);
	}

}
	
?>
