<?php
/**
 * PushAlias 
 *
 * An object model, representing a table in the database
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

class PushAlias 
{

	public $deviceKey;
	public $deviceToken;

	public function __construct($deviceKey = null, $deviceToken = null)
	{
		$this->deviceKey = $deviceKey;
		$this->deviceToken = $deviceToken;
	}

	/**
	 * Creates a new instance of a PushAlias from a database result
	 * @param MySQLResource $result
	 * @return PushAlias 
	 */
	public static function newFromDatabase($result)
	{
		return new PushAlias(
			$result->deviceKey,
			$result->deviceToken 
		);
	}

	public function toString()
	{
		return "deviceKey=" . $this->deviceKey . ", " . "deviceToken=" . $this->deviceToken;
	}

}

?>