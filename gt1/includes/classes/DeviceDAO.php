<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/Device.php");
require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");
require_once("classes/UserCredentials.php");

class DeviceDAO extends BaseDAO
{
	/**
	 *
	 * @param <type> $macAddress
	 * @return <type>
	 */
	public function isDuplicateDevice($macAddress)
	{
		HttpUtils::cleanInput($macAddress);
		$result = $this->query("SELECT deviceid FROM devices WHERE macAddress='$macAddress'");
		if($this->numRows($result)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @return <type>
	 */
	public function isDuplicateDeviceByGuid($devicekey)
	{
		HttpUtils::cleanInput($devicekey);
		$query = "SELECT deviceid FROM devices WHERE devicekey='$devicekey'";
		$result = $this->query($query);
		if($this->numRows($result)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @return <type>
	 */
	public function getDeviceByKey($deviceKey)
	{
		HttpUtils::cleanInput($deviceKey);
		$result = $this->queryUniqueObject("SELECT * FROM devices WHERE devicekey='$deviceKey'");
		if($result) {
			return Device::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param <type> $device
	 * @return <type>
	 */
	public function saveNewDevice($device)
	{
		$device->cleanAttributes();
		$newDeviceQuery = "INSERT INTO devices (type, model, manufacturer, os, theft_status, serial, color, description, devicekey, macAddress, labelcode, datecreated, pro, productid, saveTracking, version, devicePassword) VALUES ('$device->type', '$device->model', '$device->manufacturer', '$device->os', '$device->theft_status', '$device->serial', '$device->color', '$device->description', '$device->devicekey', '$device->macAddress', '$device->labelcode', '$device->datecreated', '$device->pro', '$device->productid', '$device->saveTracking', '$device->version', '$device->devicePassword')";
		$this->executeNoDie($newDeviceQuery);
		$device->deviceid = mysql_insert_id();
		return $device;
	}

	/**
	 *
	 * @param <type> $device
	 * @return <type>
	 */
	public function updateDevice($device)
	{
		$device->cleanAttributes();
		
		$q = "UPDATE devices SET
			`type` = '$device->type',
			`model` = '$device->model',
			`manufacturer` = '$device->manufacturer',
			`os` = '$device->os',
			`theft_status` = '$device->theft_status',
			`serial` = '$device->serial',
			`color` = '$device->color',
			`description` = '$device->description',
			`devicekey` = '$device->devicekey',
			`macAddress` = '$device->macAddress',
			`labelcode` = '$device->labelcode',
			`datecreated` = '$device->datecreated',
			`pro` = '$device->pro',
			`productid` = '$device->productid',
			`saveTracking` = '$device->saveTracking',
			`version` = '$device->version',
			`devicePassword` = '$device->devicePassword'
		WHERE
			deviceid='$device->deviceid'";
		return $this->execute($q);
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @param <type> $licenseKey
	 * @param <type> $macAddress
	 * @param <type> $userId
	 * @return <type>
	 */
	public function getDeviceForDeleteRequest($deviceKey, $licenseKey, $macAddress, $userId)
	{
		HttpUtils::cleanInput($deviceKey);
		HttpUtils::cleanInput($licenseKey);
		HttpUtils::cleanInput($macAddress);
		HttpUtils::cleanInput($userId);
		$q = "select * from devices join license on devices.deviceid=license.deviceid where license.userId='$userId' AND devices.devicekey='$deviceKey' AND devices.macAddress='$macAddress' AND license.key='$licenseKey'";
		$result = $this->queryUniqueObject($q);
		return $result;
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @param <type> $userId
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function isValidDeviceRequest($deviceKey, $userId, $licenseKey = null)
	{
		HttpUtils::cleanInput($deviceKey);
		HttpUtils::cleanInput($userId);
		HttpUtils::cleanInput($licenseKey);

		if($licenseKey) {
			$q = "select devices.* from devices join license on devices.deviceid=license.deviceid where license.userId='$userId' AND devices.devicekey='$deviceKey' AND license.key='$licenseKey'";
		} else {
			$q = "select devices.* from devices join license on devices.deviceid=license.deviceid where license.userId='$userId' AND devices.devicekey='$deviceKey'";
		}

		$result = $this->queryUniqueObject($q);

		if($result) {
			return Device::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param <type> $deviceKey
	 * @param <type> $userId
	 * @param <type> $licenseKey
	 * @return <type>
	 */
	public function isValidIPhoneRequest($deviceKey, $userId)
	{
		HttpUtils::cleanInput($deviceKey);
		HttpUtils::cleanInput($userId);

		$q = "SELECT devices.* FROM devicemap INNER JOIN devices ON devicemap.devicekey = devices.devicekey INNER JOIN users ON devicemap.userid = users.userid where users.userId='$userId' AND devices.devicekey='$deviceKey'";
		$result = $this->queryUniqueObject($q);

		if($result) {
			return Device::newFromDatabase($result);
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param <type> $id
	 * @return <type> 
	 */
	public function deleteDeviceById($id)
	{
		HttpUtils::cleanInput($id);
		return $this->execute("DELETE FROM devices WHERE deviceid='$id'");
	}


}