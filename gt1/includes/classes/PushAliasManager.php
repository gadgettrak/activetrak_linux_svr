<?php
/**
 * PushAlias Manager
 *
 * A manager class used to interact with this Model's DAO
 *
 * Example usage:
 * $pushAliasManager = new PushAliasManager();
 * $pushAlias = new PushAlias();
 * $pushAliasManager->save($pushAlias);
 *
 * @author   Matthew Sweet <msweet@alien109.com>
 * @author   ModelMaker
 * @version  $Revision: 1.0 $
 * @access   public
 */

require_once("classes/PushAliasDAO.php");

class PushAliasManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new PushAliasDAO();
	}

	/**
	 * Creates a new PushAlias
	 * @param PushAlias $pushAlias
	 * @return PushAlias
	 */
	public function create($pushAlias)
	{
		return $this->dao->create($pushAlias);
	}

	/**
	 * Creates a new PushAlias
	 * @param PushAlias $pushAlias
	 * @return PushAlias
	 */
	public function createUpdate($pushAlias)
	{
		return $this->dao->createUpdate($pushAlias);
	}

	/**
	 * Updates a PushAlias 
	 * @param PushAlias $pushAlias 
	 * @return PushAlias 
	 */
	public function update($pushAlias)
	{
		return $this->dao->update($pushAlias);
	}

	/**
	 * Deletes a PushAlias	 * @param PushAlias $pushAlias 
	 * @return bool
	 */
	public function delete($pushAlias)
	{
		return $this->dao->delete($pushAlias);
	}

	/**
	 * Retrieves a PushAlias in the database
	 * @param String deviceKey 
	 * @return PushAlias 
	 */
	public function get($deviceKey)
	{
		return $this->dao->get($deviceKey);
	}

}

?>

