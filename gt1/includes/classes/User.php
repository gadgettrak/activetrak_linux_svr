<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

class User
{
	public $userid;
	public $fname;
	public $lname;
	public $address1;
	public $address2;
	public $city;
	public $state;
	public $postal;
	public $country;
	public $email;
	public $company;
	public $phone;
	public $email_val;
	public $email_key;
	public $ipaddress;
	public $datecreated;
	public $suspended;
	public $password;
	public $tester;
	public $partnerid;

	public function __construct($userid = null, $fname = null, $lname = null, $address1 = null, $address2 = null, $city = null, $state = null, $postal = null, $country = null, $email = null, $company = null, $phone = null, $email_val = null, $email_key = null, $ipaddress = null, $datecreated = null, $suspended = null, $password = null, $tester = null, $partnerid = null)
	{
		$this->userid = $userid;
		$this->fname = $fname;
		$this->lname = $lname;
		$this->address1 = $address1;
		$this->address2 = $address2;
		$this->city = $city;
		$this->state = $state;
		$this->postal = $postal;
		$this->country = $country;
		$this->email = $email;
		$this->company = $company;
		$this->phone = $phone;
		$this->email_val = $email_val;
		$this->email_key = $email_key;
		$this->ipaddress = $ipaddress;
		$this->datecreated = $datecreated;
		$this->suspended = $suspended;
		$this->password = $password;
		$this->tester = $tester;
		$this->partnerid = $partnerid;
	}

	public function toString() {
		return ("email: " . $this->email . ", userid: " . $this->userid . ", password: " . $this->password);
	}

	/**
	 *
	 * @param <type> $result
	 * @return User
	 */
	public static function newFromDatabase($result)
	{
		return new User(
			$result->userid,
			$result->fname,
			$result->lname,
			$result->address1,
			$result->address2,
			$result->city,
			$result->state,
			$result->postal,
			$result->country,
			$result->email,
			$result->company,
			$result->phone,
			$result->email_val,
			$result->email_key,
			$result->ipaddress,
			$result->datecreated,
			$result->suspended,
			$result->password,
			$result->tester,
			$result->partnerid
		);
	}

}

?>