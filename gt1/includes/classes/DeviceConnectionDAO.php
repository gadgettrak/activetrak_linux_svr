<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/BaseDAO.php");
require_once("classes/DeviceConnection.php");
require_once("classes/HttpUtils.php");
require_once("classes/Logger.php");

class DeviceConnectionDAO extends BaseDAO
{
	public function getDeviceConnections($devicekey)
	{
		HttpUtils::cleanInput($deviceKey);
		$result = $this->query("SELECT * FROM connections WHERE devicekey='$devicekey'");
		$values = array();
		while ($row = mysql_fetch_object($results)) {
			array_push($values, DeviceConnection::newFromDatabase($row));
		}
		return $values;
	}

	/**
	 *
	 * @param <type> $device
	 * @return <type>
	 */
	public function saveNewDeviceConnection($deviceConnection)
	{
		//TODO should sanitize objects attributes here...

		$newDeviceConnectionQuery = "
			INSERT INTO connections (
				`devicekey`,
				`timestamp`,
				`public_ip`,
				`internal_ip`,
				`stolen`,
				`username`,
				`computer_name`,
				`ip_country`,
				`ip_state`,
				`ip_city`,
				`postal_code`,
				`ip_lon`,
				`ip_lat`,
				`agent`,
				`os`,
				`serial`,
				`loc_state`,
				`loc_city`,
				`loc_street`,
				`did`
			) VALUES (
				'$deviceConnection->devicekey',
				'$deviceConnection->timestamp',
				'$deviceConnection->public_ip',
				'$deviceConnection->internal_ip',
				'$deviceConnection->stolen',
				'$deviceConnection->username',
				'$deviceConnection->computer_name',
				'$deviceConnection->ip_country',
				'$deviceConnection->ip_state',
				'$deviceConnection->ip_city',
				'$deviceConnection->postal_code',
				'$deviceConnection->ip_lon',
				'$deviceConnection->ip_lat',
				'$deviceConnection->agent',
				'$deviceConnection->os',
				'$deviceConnection->serial',
				'$deviceConnection->loc_state',
				'$deviceConnection->loc_city',
				'$deviceConnection->loc_street',
				'$deviceConnection->did'
		)";

		$this->executeNoDie($newDeviceConnectionQuery);
		$deviceConnection->connectionid = mysql_insert_id();
		return $deviceConnection;
	}

}