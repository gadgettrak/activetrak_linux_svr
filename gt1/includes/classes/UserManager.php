<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/UserDAO.php");

class UserManager {

	private $dao;

	public function __construct()
	{
		$this->dao = new UserDAO();
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @return User
	 */
	public function getUserByUserCredentials($userCredentials) {
		return $this->dao->getUserByCredentials($userCredentials);
	}

	/**
	 *
	 * @param UserCredentials $credentials
	 * @return User
	 */
	public function getUserByDeviceCredentials($userCredentials, $deviceKey) {
		return $this->dao->getUserByDeviceCredentials($userCredentials, $deviceKey);
	}


	/**
	 *
	 * @param <type> $email
	 * @return <type>
	 */
	public function getUserByEmail($email) {
		return $this->dao->getUserByEmail($email);
	}

	/**
	 * Saves a new User to the database
	 * @param User $user
	 * @return User
	 */
	public function saveNewUser($user)
	{
		return $this->dao->saveNewUser($user);
	}

	/**
	 *
	 * @param <type> $userName
	 * @return bool
	 */
	public function userNameIsAvailable($userName)
	{
		return $this->dao->userNameIsAvailable($userName);
	}

	/**
	 *
	 * @param <type> $key
	 * @return <type>
	 */
	public function isValidEmailKey($key)
	{
		return $this->dao->isValidEmailKey($key);
	}

	/**
	 *
	 * @param <type> $email
	 * @return <type>
	 */
	public function isValidEmail($email)
	{
		return $this->dao->isValidEmail($email);
	}


}


?>