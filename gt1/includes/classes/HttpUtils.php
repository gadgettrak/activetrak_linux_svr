<?php

/**
 * Copyright 2010 GadgetTrak, Inc.
 *
 * @author Matthew Sweet
 */

require_once("classes/HttpResponseCodes.php");
require_once("classes/Logger.php");

class HttpUtils {

	/**
	 *
	 * @param <type> $code 
	 */
	public static function setHttpStatus($code)
	{
		header('HTTP/1.0 ' . HttpResponseCodes::$CODES["STATUS_$code"]);
		//if(CONFIG_APP_DEBUG) {
		//	print('HTTP/1.0 ' . HttpResponseCodes::$CODES["STATUS_$code"]);
		//}
	}

	/**
	 *
	 * @return <type>
	 */
	public static function getXMLPayload($doEscapeHack = false)
	{

		$xml;
		$raw_data = "";

		try {
			$raw_data = file_get_contents('php://input');
		} catch (Exception $e) {
			if(CONFIG_APP_LOGGING) Logger::logToFile("Caught exception getting input:" . $e->getMessage());
		}

		$raw_data = str_replace("&", "&amp;", $raw_data);

		try {
			$xml = @new SimpleXMLElement($raw_data);
		} catch (Exception $e) {
			if(CONFIG_APP_LOGGING) Logger::logToFile("Error parsing XML:" . $e->getMessage());
		}
		
		return $xml;
	}

	/**
	 *
	 */
	public static function requirePostRequest()
	{
		if ( $_SERVER['REQUEST_METHOD'] !== 'POST')
		{
			self::setHttpStatus(400);
			die();
		}
	}

	/**
	 *
	 * @param <type> $var
	 */
	public static function cleanInput(&$var)
	{
		//first strip slashes to prevent double escaping
		$var = stripslashes($var);
		$var = mysql_escape_string($var);
	}

	/**
	 *
	 * @param <type> $array
	 */
	public static function cleanRequestParamsArray(&$array)
	{
		$array = array_map('mysql_escape_string', $array);
	}

	/**
	 * Fires off a non blocking request
	 * @param <type> $url
	 * @param <type> $params
	 */
	public static function fireAndForget($url, $params)
	{
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}
		$post_string = implode('&', $post_params);

		$parts=parse_url($url);

		$fp = fsockopen($parts['host'],
			isset($parts['port'])?$parts['port']:80,
			$errno, $errstr, 30);

		$out = "POST ".$parts['path']." HTTP/1.1\r\n";
		$out.= "Host: ".$parts['host']."\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: ".strlen($post_string)."\r\n";
		$out.= "Connection: Close\r\n\r\n";
		if (isset($post_string)) $out.= $post_string;

		fwrite($fp, $out);
		fclose($fp);
	}

	public function isValidIp($ip)
	{
		return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);
	}

}