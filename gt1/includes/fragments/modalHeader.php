<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<?php
$isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
?>

<?php if(!$isiPad) {?>
<meta name="viewport" content="width=device-width"/>
<?php } ?>

<link href="/v3/style/modal.css" rel="stylesheet" type="text/css" />
<link href="/v3/style/iphone.css" rel="stylesheet" type="text/css" media="only screen and (max-device-width: 320px)"/>

</head>

<body>

	<div id="header">
		<div class="container">
			<div class="logo"><a href="/"><img src="/v3/images/gt-logo.gif" alt="GadgetTrak" /></a></div>
		</div>
	</div>

	<div class="container">
		<div id="body"><div id="content">