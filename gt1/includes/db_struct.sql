-- MySQL dump 10.11
--
-- Host: localhost    Database: gt_app
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `admins` (
  `id` mediumint(9) NOT NULL auto_increment,
  `userId` mediumint(9) NOT NULL,
  `level` mediumint(9) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `connections`
--

DROP TABLE IF EXISTS `connections`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `connections` (
  `connectionid` int(255) NOT NULL auto_increment,
  `devicekey` varchar(40) default NULL,
  `timestamp` timestamp NULL default CURRENT_TIMESTAMP,
  `public_ip` varchar(255) default NULL,
  `internal_ip` varchar(255) default NULL,
  `stolen` enum('N','Y') default NULL,
  `username` varchar(255) default NULL,
  `computer_name` varchar(255) default NULL,
  `ip_country` text,
  `ip_state` text,
  `ip_city` text,
  `postal_code` varchar(255) default NULL,
  `ip_lon` varchar(255) default NULL,
  `ip_lat` varchar(255) default NULL,
  `agent` text,
  `os` text,
  `serial` text,
  `loc_state` varchar(255) default '',
  `loc_city` varchar(255) default '',
  `loc_street` varchar(255) default '',
  `did` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`connectionid`),
  KEY `devicekey` (`devicekey`)
) ENGINE=MyISAM AUTO_INCREMENT=4019846 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `connectionsArchive`
--

DROP TABLE IF EXISTS `connectionsArchive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `connectionsArchive` (
  `connectionid` int(255) NOT NULL auto_increment,
  `devicekey` varchar(40) default NULL,
  `timestamp` timestamp NULL default CURRENT_TIMESTAMP,
  `public_ip` varchar(255) default NULL,
  `internal_ip` varchar(255) default NULL,
  `stolen` enum('N','Y') default NULL,
  `username` varchar(255) default NULL,
  `computer_name` varchar(255) default NULL,
  `ip_country` text,
  `ip_state` text,
  `ip_city` text,
  `postal_code` varchar(255) default NULL,
  `ip_lon` varchar(255) default NULL,
  `ip_lat` varchar(255) default NULL,
  `agent` text,
  `os` text,
  `serial` text,
  `loc_state` varchar(255) default '',
  `loc_city` varchar(255) default '',
  `loc_street` varchar(255) default '',
  `did` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`connectionid`)
) ENGINE=MyISAM AUTO_INCREMENT=3906628 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `devicemap`
--

DROP TABLE IF EXISTS `devicemap`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `devicemap` (
  `mapid` mediumint(9) NOT NULL auto_increment,
  `userid` mediumint(9) NOT NULL default '0',
  `devicekey` varchar(40) NOT NULL default '',
  `deviceid` mediumint(9) NOT NULL default '0',
  PRIMARY KEY  (`mapid`),
  KEY `userdevice` (`userid`,`devicekey`)
) ENGINE=MyISAM AUTO_INCREMENT=140943 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `devices` (
  `deviceid` mediumint(9) NOT NULL auto_increment,
  `type` varchar(255) default NULL,
  `model` varchar(255) default NULL,
  `manufacturer` varchar(255) default NULL,
  `os` enum('mac','pc') default NULL,
  `theft_status` enum('N','Y') NOT NULL default 'N',
  `serial` varchar(255) default NULL,
  `color` varchar(255) default NULL,
  `description` mediumtext,
  `devicekey` varchar(40) NOT NULL default '',
  `macAddress` varchar(40) default NULL,
  `labelcode` varchar(255) default NULL,
  `datecreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `pro` enum('N','Y') NOT NULL default 'Y',
  `productid` mediumint(9) NOT NULL default '1',
  `version` varchar(10) default NULL,
  `saveTracking` enum('N','Y') NOT NULL default 'N',
  `devicePassword` varchar(40) default NULL,
  PRIMARY KEY  (`deviceid`),
  KEY `gtLabel` (`labelcode`),
  KEY `macAddressIndex` (`macAddress`),
  KEY `productid` (`devicekey`,`productid`)
) ENGINE=MyISAM AUTO_INCREMENT=141774 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `discountcodes`
--

DROP TABLE IF EXISTS `discountcodes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `discountcodes` (
  `discountcodeid` mediumint(9) NOT NULL auto_increment,
  `code` varchar(255) NOT NULL default '',
  `partnerid` mediumint(9) default NULL,
  `discountamt` float NOT NULL default '0',
  `notes` mediumtext NOT NULL,
  PRIMARY KEY  (`discountcodeid`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `error`
--

DROP TABLE IF EXISTS `error`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `error` (
  `id` mediumint(10) NOT NULL auto_increment,
  `date` datetime default NULL,
  `errorType` varchar(30) default NULL,
  `count` mediumint(9) default NULL,
  `fname` varchar(255) default '',
  `lname` varchar(255) default '',
  `email` varchar(255) default '',
  `deviceKey` varchar(255) default NULL,
  `product` varchar(255) default NULL,
  `password` varchar(40) default NULL,
  PRIMARY KEY  (`id`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=41031 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `keycodes`
--

DROP TABLE IF EXISTS `keycodes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `keycodes` (
  `keyid` mediumint(9) NOT NULL auto_increment,
  `paymentcode` mediumtext NOT NULL,
  `serialcode` mediumtext character set utf8 NOT NULL,
  `resellerid` mediumint(9) NOT NULL default '0',
  `licenses` smallint(10) NOT NULL default '0',
  `used` enum('yes','no') NOT NULL default 'no',
  `notes` varchar(255) NOT NULL default '',
  `length` int(11) NOT NULL default '0',
  PRIMARY KEY  (`keyid`)
) ENGINE=MyISAM AUTO_INCREMENT=14900 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `license` (
  `id` int(11) NOT NULL auto_increment,
  `userId` int(11) default NULL,
  `deviceId` int(11) default NULL,
  `productId` int(11) default NULL,
  `resellerId` int(11) default NULL,
  `length` smallint(3) NOT NULL COMMENT 'length of license in months',
  `licenses` tinyint(4) NOT NULL default '1' COMMENT 'number of devices license can be used on',
  `key` char(30) NOT NULL,
  `used` enum('Y','N') NOT NULL default 'N' COMMENT 'determines if the license has been used (installed/activated)',
  `activated` datetime default NULL COMMENT 'date the key was first used (installed)',
  `nfr` tinyint(4) NOT NULL,
  `os` enum('n/a','mac','pc') NOT NULL default 'n/a',
  `renewals` smallint(4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `licenseKey` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=78370 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `licenses`
--

DROP TABLE IF EXISTS `licenses`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `licenses` (
  `purchaseid` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `deviceKey` int(11) default NULL,
  `number_licenses` int(11) NOT NULL default '0',
  `date_activated` timestamp NULL default CURRENT_TIMESTAMP,
  `version` varchar(255) NOT NULL default 'pro',
  `postal` varchar(255) default NULL,
  `country` text,
  `city` varchar(255) default NULL,
  `state` varchar(255) default NULL,
  `address` text,
  `transactid` varchar(255) NOT NULL default '',
  `shipped` enum('','No','Yes','Hold') NOT NULL default '',
  `notes` varchar(255) NOT NULL default '',
  `paymentcode` mediumint(9) NOT NULL default '0',
  `discountcode` varchar(255) NOT NULL default '',
  `productid` mediumint(9) NOT NULL default '1',
  PRIMARY KEY  (`purchaseid`)
) ENGINE=MyISAM AUTO_INCREMENT=13516032 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `newsletter` (
  `newsletterid` int(11) NOT NULL auto_increment,
  `fname` varchar(255) NOT NULL default '',
  `lname` varchar(255) NOT NULL default '',
  `city` varchar(255) NOT NULL default '',
  `state` varchar(255) NOT NULL default '',
  `country` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `company` varchar(255) NOT NULL default '',
  `ipaddress` varchar(255) NOT NULL default '',
  `source` varchar(255) NOT NULL default '',
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`newsletterid`)
) ENGINE=MyISAM AUTO_INCREMENT=3899 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `products` (
  `productid` mediumint(9) NOT NULL auto_increment,
  `productname` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`productid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pushAlias`
--

DROP TABLE IF EXISTS `pushAlias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pushAlias` (
  `deviceKey` varchar(255) NOT NULL default '',
  `deviceToken` varchar(255) default NULL,
  PRIMARY KEY  (`deviceKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `resellers`
--

DROP TABLE IF EXISTS `resellers`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `resellers` (
  `resellerid` mediumint(9) NOT NULL auto_increment,
  `companyname` mediumtext NOT NULL,
  `companyaddress` mediumtext NOT NULL,
  `companyemail` mediumtext NOT NULL,
  `companycontact` mediumtext NOT NULL,
  PRIMARY KEY  (`resellerid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `stats` (
  `type` varchar(40) NOT NULL,
  `count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `transactions` (
  `id` mediumint(9) NOT NULL default '0',
  `transactionid` mediumtext NOT NULL,
  `customerid` mediumint(9) NOT NULL default '0',
  `details` mediumtext NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `trials`
--

DROP TABLE IF EXISTS `trials`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `trials` (
  `userId` mediumint(9) NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `licenseId` char(30) NOT NULL,
  PRIMARY KEY  (`userId`,`licenseId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `users` (
  `userid` mediumint(9) NOT NULL auto_increment,
  `fname` varchar(255) NOT NULL default '',
  `lname` varchar(255) NOT NULL default '',
  `address1` text,
  `address2` text,
  `city` text,
  `state` varchar(255) default NULL,
  `postal` varchar(255) default NULL,
  `country` varchar(255) default NULL,
  `email` varchar(255) NOT NULL default '',
  `company` varchar(255) default NULL,
  `phone` varchar(50) default NULL,
  `email_val` binary(1) default '0',
  `email_key` varchar(40) default NULL,
  `ipaddress` varchar(255) default NULL,
  `datecreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `suspended` int(11) default '0',
  `password` varchar(40) default NULL,
  `tester` binary(1) default '0',
  `partnerid` mediumint(9) NOT NULL default '0',
  PRIMARY KEY  (`userid`,`email`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=128579 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-20  3:29:56
