<?
ini_set("display_errors", 0);
 
require_once ('HTML/QuickForm.php');
include("database.php");
$form = new HTML_QuickForm('login');
$data;


function passwordCheck($email, $password) {
global $conn;
if(!get_magic_quotes_gpc()){
      $usermail = addslashes($email);
	  $userpass = sha1(addslashes($password));
   }
   
   
  $q = "SELECT * FROM users WHERE email = '$usermail' and password='$userpass'";
  $result = mysql_query($q,$conn);

   if (mysql_numrows($result) == 1){
   $dbarray = mysql_fetch_array($result);
   
   
   		if (!$dbarray['email_val']){
		$redirecturl="Location: error.php?id=emailverification&email=" .$dbarray['email'];
  		header($redirecturl);
  		}else{
		
		//check to see how many valid licenses they have - turn this into a reusable function
		//$license_sql="Select SUM(number_licenses) AS licensecount FROM licenses WHERE DATE_SUB(date_activated, interval 1 YEAR) AND  userid='".$dbarray['userid'] ."'";
		$license_sql="Select SUM(number_licenses) AS licensecount FROM licenses WHERE  userid='".$dbarray['userid'] ."'";
		$licensecount = mysql_result(mysql_query($license_sql), "licensecount");
		
		
		session_start(); 
		$_SESSION['userid']=$dbarray['userid'];
		$_SESSION['name']=$dbarray['fname'] . " " . $dbarray['lname'] ;
		$_SESSION['email']=$dbarray['email'];
		$_SESSION['loggedin'] = true;
		$_SESSION['licensecount'] =$licensecount;
		if ($licensecount > 0){
		header("Location: my.php");
		
		//print "<!--" . $_SESSION['email'] ."-->";
		}else{
   		header("Location: index.php");
		}
   	}
   
   //redirect them to their devices page
   }else{
   print "Invalid login attempt , <a href=\"login.php\" />try again</a>";
   }
   
 
}


$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));
$form->addElement('submit', null, 'Submit', array('class' => "submit"));
//$form->addElement('link', 'new_reg', 'New Customer?','register.php','Register Here');

$form->addRule('password', 'Please enter your password', 'required', null, 'client');
$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');

//$form->registerRule('passwordcheck','function','passwordCheck');
//$form->addRule('email','Invalid login or password', 'passwordCheck'); 


function processdata($data){

passwordCheck($data['email'], $data['password']);

}


if ($form->validate()) {// if the form validates?

$form->process('processdata');

}else{
include("header.php");
?>

<div style="width:875px;margin:25px auto">
  <div style="width:400px;float:left;padding-right:25px;border-right:1px solid #666;">
    <h2> New User </h2>
     <p> If you are a new ThermaTrak user and are registering your first FLIR camera and have a certificate from FLIR please <a href="registration.php">click here to begin registration.</a></p>
    <p> <br /><br /><br /><br />
  </div>
  <div style="width:400px;float:left;padding-left:25px;">
    <h2>Existing User Login</h2>
   <p> If you already have a device registered with ThermaTrak and wish to activate tracking, view reports or add another device please login with your rmail address and password. </p>
   <?
   
   $form->display();
   ?>
  </div>
</div>
<?
include("footer.php");
}
?>
