<?
ini_set("display_errors", 0);
 
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');
include($_SERVER['DOCUMENT_ROOT']. "/db2.php");

function serialExists($element_name,$element_value) {
global $conn;
global $test;
	if(!get_magic_quotes_gpc()){
      $serial = addslashes($element_value);
   	}
   $q = "SELECT * FROM devices WHERE manufacturer='Flir' AND serial='$serial'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) == 1){
   return true;
   }else{
   return false;
   }
}

$form= new HTML_QuickForm('flirreg');
$form->addElement('text', 'flirreg', 'Serial Number: ', array('size' => 30, 'maxlength' => 255));
$form->addElement('submit', null, 'Submit', array('class' => "submit"));

$form->registerRule('serialExists','function','serialExists');
$form->addRule('flirreg','The serial number you entered is not registered with ThermaTrak, please contact a customer service representative at<br /> (866) FLIR-911 or by email at thermatrak@flir.com ', 'serialExists'); 
$form->addRule('flirreg', 'Please enter your device serial number', 'required', null, 'client');

function processdata($data){

	if(serialExists($data['flirreg'])){
		session_start();
		$_SESSION['serial'] = $data['flirreg'];
		header("Location: step2.php");	
		//create session
		//store serial in session
		//go to second form
	
	}
}



if ($form->validate()) {// if the form validates?

$form->process('processdata');

}else{ 
include("header.php");
?>
<h1>Flir ThermaTrak Registration</h1>

<h2> Step 1: Enter Device Serial Number </h2>
<p> Please enter the serial number of your device. You will find this number on the ThermaTrak certificate provided by Flir.</p> 
<?
$form->display();
}
include("footer.php");
?>
