<?
session_start();
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');
include($_SERVER['DOCUMENT_ROOT']. "/db2.php");
include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");

$countries= countryList();
$form = new HTML_QuickForm('register');
$insertid;
$pid;

$form->setDefaults($defaults);



function usernameTaken($element_name,$element_value) {
global $conn;

if(!get_magic_quotes_gpc()){
      $usermail = addslashes($element_value);
   }
   $q = "SELECT email FROM users WHERE email = '$usermail'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) > 0){
   return false;
   }else{
   return true;
   }
   
 
}


function emailver($key, $email){

$to=$email;
$bdy=<<<PTEXT
Thank you for registering with GadgetTrak, the final step to enable your ThermaTrakaccount
is to verify your email address, all you need to do is click on the link below:
https://account.gadgettheft.com/verify.php?key={$key}


PTEXT;



$headers['From'] = 'accounts@gadgettrak.com';
$headers['Subject'] = 'GadgetTrak.com.com Email Verification';
$message = &Mail::factory('mail');
$message->send($to, $headers, $bdy);

}


function mapData($userid,$data){
global $conn;
$deviceq = "SELECT devicekey FROM devices WHERE manufacturer='Flir' AND serial='". $data['serialnum']."'";
$result=mysql_fetch_assoc(mysql_query($deviceq,$conn));
$devicekey=$result['devicekey'];

$insertq="INSERT INTO devicemap (userid, devicekey) VALUES ('$userid','$devicekey')";
mysql_query($insertq,$conn) or die(mysql_error());

$licenseq = "INSERT INTO licenses (
   				userid, 
  				number_licenses, 
				productid) VALUES('"
   			.addslashes($userid)."','"
   			.'1'."','"
			.'1'."')";

mysql_query($licenseq,$conn) or die(mysql_error()); 

}

function createAccount($data){

//create the account
 global $conn;
 $password=sha1($data['password']); //encrypt password
 $emailkey=sha1($data['email']); //create encyrpted mail key
   $q = "INSERT INTO users (
   				fname, 
  				lname, 
				address1, 
				city,
				state,
				postal,
				country,
   				email,
				email_val,
				company,	 
   				phone, 
				ipaddress,
   				email_key, 
  			 	password) 
   
   VALUES('"
   			.addslashes($data['firstname'])."','"
   			.addslashes($data['lastname'])."','"
			.addslashes($data['address'])."','"
			.addslashes($data['city'])."','"
			.addslashes($data['state'])."','"
			.addslashes($data['postal'])."','"
			.addslashes($data['country'])."','"
   			.addslashes($data['email'])."','"
			.'1'."','"
			.addslashes($data['company'])."','"
   			.addslashes($data['phone'])."','"
			.$_SERVER['REMOTE_ADDR']."','"
			.$emailkey."','"
			.$password."')";
  
  
  emailver($emailkey, $data['email']);
   
  mysql_query($q,$conn) or die(mysql_error()); 
   
  return mysql_insert_id();
}

$form->addElement('static','serial','Device Serial:',$_SESSION['serial']);
$form->addElement('hidden','serialnum',$_SESSION['serial']);
$form->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
//$form->addElement('html', '<tr><td></td><td><p style=\"color:#fff;font-size:10px;\">Hotmail accounts cannot be used. If you use a free email provider such as Yahoo, please check your spam box as an email verification link will be sent. We recommend using a Gmail account.</p></td></tr>');
$form->addElement('text', 'phone', 'Phone:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'company', 'Company:', array('size' => 30, 'maxlength' => 255));
$form->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));


$form->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255));
$form->addElement('select', 'country', 'Country:', $countries);
$form->addElement('password', 'password', 'Create Password:', array('size' => 30, 'maxlength' => 255));

$form->addElement('html','<tr><td style="margin:0;padding;0"></td><td style="margin:0;padding;0"> <p><a target="_blank" href="http://www.gadgettheft.com/legal/terms.php"/>Terms &amp; Conditions</a> </p></td></tr>');
$form->addElement('advcheckbox','terms','I agree to terms and conditions', '',null,'yes');

$form->addElement('submit', null, 'Submit', array('class' => "submit"));

$form->addElement('html', '<tr><td></td><td><p><a href="http://www.gadgettrak.com/company/privacy.php"> Privacy Policy</a></p></td></tr>');

//validation
$form->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
$form->addRule('lastname', 'Please enter your last name', 'required', null, 'client');

$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');
$form->addRule('phone', 'Please enter your phone number', 'required', null, 'client');
$form->addRule('address', 'Please enter your address', 'required', null, 'client');
$form->addRule('city', 'Please enter your city', 'required', null, 'client');
$form->addRule('state', 'Please enter your state', 'required', null, 'client');
$form->addRule('postal', 'Please enter your postal code', 'required', null, 'client');
$form->addRule('country', '', 'required', null, 'client');
$form->addRule('country', 'Please enter select a country', 'minlength', 2, 'client');
$form->addRule('password', 'Please enter a password', 'required', null, 'client');

$form->registerRule('usernameTaken','function','usernameTaken');
$form->addRule('email','The email you have selected already exists', 'usernameTaken'); 
$form->addRule('terms', 'In order to use this service you must agree to the terms and conditions', 'required', null, 'client');

if(isset($_GET['pid'])){

$parternid=$_GET['pid'];
$form->addElement('hidden', 'pid', $parternid);
}


function processdata($data){

global $insertid;
global $pid;

//create account
$insertid=createAccount($data);
//get the device info
mapData($insertid,$data);

$pid = $data['pid'];

}

include("header.php");
if ($form->validate()) {// if the form validates?

$form->process('processdata');


?>
<h2> Registration Complete </h2>
Thank you, your device is now registered with ThermaTrak, you can now log into your account to <a href="https://account.gadgettheft.com"> activate tracking for your device</a>. 

<?
}else{


?>

<h2> Step 2: ThermaTrak Account Registration </h2>
<div class="regform">
<?
$form->display();
?>
<!-- <p> After you register you will need to verify your email address, once verified you can add devices to your account and download the agent files. To verify
that the service is not being misused all accounts are screened by our staff, any information submitted that is not factual or complete will result in the account being removed and the user banned.</p>
-->
<? }?>



<?
include("footer.php");
?>








