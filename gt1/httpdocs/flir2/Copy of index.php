<?
include($_SERVER['DOCUMENT_ROOT']. "/database.php");
include($_SERVER['DOCUMENT_ROOT']. "/_functions/countries.php");
require_once ('HTML/QuickForm.php');
require_once('Mail.php');
require_once('Mail/mime.php');

$countries= countryList();

function serialExists($element_name,$element_value) {
global $conn;
global $test;
if(!get_magic_quotes_gpc()){
      $keycode = addslashes($element_value);
   }
   $q = "SELECT * FROM devices WHERE serial='$dserial'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) > 0){
   return true;
   }else{
   return false;
   }
   
 
}

function emailver($key, $email){

$to=$email;
$bdy=<<<PTEXT
Thank you for registering with ThermaTrak, your device has been registered. The next step to enable your account and verify your email address, all you need to do is click on the link below:
http://account.gadgettheft.com/verify.php?key={$key}

PTEXT;

$headers['From'] = 'accounts@gadgettrak.com';
$headers['Subject'] = 'ThermaTrak Email Verification';
$message = &Mail::factory('mail');
$message->send($to, $headers, $bdy);

}


function serialLookup($element_name,$element_value) {
global $conn;
global $test;
if(!get_magic_quotes_gpc()){
      $usermail = addslashes($element_value);
   }
   $q = "SELECT serial FROM devices WHERE serial = '$serial'";
   $result = mysql_query($q,$conn);
   if (mysql_numrows($result) > 0){
   return false;
   }else{
   return true;
   }
   
 
}


$form= new HTML_QuickForm('flirreg');
$form->addElement('text', 'flirreg', 'Flir Serial Number:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'firstname', 'First Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'lastname', 'Last Name:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'email', 'Email:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'phone', 'Phone:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'company', 'Company:', array('size' => 30, 'maxlength' => 255));
$form->addElement('textarea', 'address', 'Address:', array('cols'=>21, 'rows'=>2, 'wrap'=>'soft'));


$form->addElement('text', 'city', 'City:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'state', 'State/Province/Region:', array('size' => 30, 'maxlength' => 255));
$form->addElement('text', 'postal', 'Postal Code:', array('size' => 30, 'maxlength' => 255));
$form->addElement('select', 'country', 'Country:', $countries);
$form->addElement('password', 'password', 'Password:', array('size' => 30, 'maxlength' => 255));

$form->addElement('html','<tr><td style="margin:0;padding;0"></td><td style="margin:0;padding;0"> <p><a target="_blank" href="http://www.gadgettheft.com/legal/terms.php"/>Terms &amp; Conditions</a> </p></td></tr>');
$form->addElement('advcheckbox','terms','I agree to terms and conditions', '',null,'yes');

$form->addElement('submit', null, 'Submit', array('class' => "submit"));

$form->addElement('html', '<tr><td></td><td><p><a href=\"http://www.gadgettheft.com/legal/privacy.php\"> Privacy Policy</a></p></td></tr>');

//validation
$form->addRule('keycode', 'The payment code you entered is not valid', 'required', null, 'client');

$form->registerRule('keycodeExists','function','keycodeExists');
$form->addRule('keycode','The payment code you entered is not valid. ', 'keycodeExists'); 

$form->addRule('firstname', 'Please enter your first name', 'required', null, 'client');
$form->addRule('lastname', 'Please enter your last name', 'required', null, 'client');

$form->addRule('email', 'Please enter your email address', 'required', null, 'client');
$form->addRule('email', 'Please enter a valid email', 'email', null, 'client');
$form->addRule('phone', 'Please enter your phone number', 'required', null, 'client');
$form->addRule('address', 'Please enter your address', 'required', null, 'client');
$form->addRule('city', 'Please enter your city', 'required', null, 'client');
$form->addRule('state', 'Please enter your state', 'required', null, 'client');
$form->addRule('postal', 'Please enter your postal code', 'required', null, 'client');
$form->addRule('country', '', 'required', null, 'client');
$form->addRule('country', 'Please enter select a country', 'minlength', 2, 'client');
$form->addRule('password', 'Please enter a password', 'required', null, 'client');

$form->registerRule('usernameTaken','function','usernameTaken');
$form->addRule('email','The email you have selected already exists', 'usernameTaken'); 
$form->addRule('terms', 'In order to use this service you must agree the terms and conditions', 'required', null, 'client');


function processdata($data){
global $conn;

if(!get_magic_quotes_gpc()){
 $dserial= addslashes($data['serial']);
}
$q_device_serial = "SELECT * FROM devices WHERE serial='$dserial' AND LIMIT 1";
$keyquery = mysql_query($pcodeq,$conn);
$keydata= mysql_fetch_array($keyquery);
//$keyid = $keycodedata['keyid'];
//$partnerid= $keycodedata['resellerid'];


$password=sha1($data['password']); //encrypt password
$emailkey=sha1($data['email']); //create encyrpted mail key
$accountq = "INSERT INTO users (
   				fname, 
  				lname, 
				partnerid,
				address1, 
				city,
				state,
				postal,
				country,
   				email,
				company,	 
   				phone, 
				ipaddress,
   				email_key, 
  			 	password) 
   
   VALUES('"
   			.addslashes($data['firstname'])."','"
   			.addslashes($data['lastname'])."','"
			.addslashes($partnerid)."','"
			.addslashes($data['address'])."','"
			.addslashes($data['city'])."','"
			.addslashes($data['state'])."','"
			.addslashes($data['postal'])."','"
			.addslashes($data['country'])."','"
   			.addslashes($data['email'])."','"
			.addslashes($data['company'])."','"
   			.addslashes($data['phone'])."','"
			.$_SERVER['REMOTE_ADDR']."','"
			.$emailkey."','"
			.$password."')";
  
  
emailver($emailkey, $data['email']);
   
mysql_query($accountq,$conn) or die(mysql_error()); 

$accountid = mysql_insert_id();

$licenseq = "INSERT INTO licenses (
   				userid, 
  				number_licenses, 
				paymentcode, 
				address,
				city,
				state,
				postal,
				country) VALUES('"
   			.addslashes($accountid)."','"
   			.addslashes($keydata['licenses'])."','"
			.addslashes($keydata['keyid'])."','"
			.addslashes($data['address'])."','"
			.addslashes($data['city'])."','"
			.addslashes($data['state'])."','"
			.addslashes($data['postal'])."','"
			.addslashes($data['country'])."')";
  mysql_query($licenseq,$conn) or die(mysql_error()); 

$keystatusq = "UPDATE keycodes SET used = 'yes' WHERE keyid='". $keydata['keyid']. "'";

mysql_query($keystatusq ,$conn) or die(mysql_error()); 

}

$title="Flir ThermaTrak Registration";
include($_SERVER['DOCUMENT_ROOT']."/_includes/header.php");
?>
<style type="text/css">
#gtlabel{
	text-align:center;
	background-image: url(/_gfx/gadgettraklbl.png);
	background-repeat: no-repeat;
	width:200px;height:100px;margin:0 auto;
}
#gtlbl_code{position:relative;top:65px;font-weight:bold;}
#lblcontainer{margin:0 auto;width:200px;text-align:center;}
</style>

<h2>Flir ThermaTrak Registration</h2>

<?
if ($form->validate()) {// if the form validates?

$form->process('processdata');
?>

<p>Thank you, your account has been created and your payment code applied to your subscription. The last step is to confirm your email address, we have sent an email to you with a link to click on to verify your email address. If you have questions or require any assistance please contact us at <a href="mailto:support@gadgettrak.com">support@gadgettrak.com</a>. 
</p>

<?}else{ ?>
<p> Please enter the payment code provided to you by your reseller, or the payment code on the card provided to you at the time of purchase.  

<?$form->display();}?>

<?
include($_SERVER['DOCUMENT_ROOT']."/_includes/footer.php");
?>
