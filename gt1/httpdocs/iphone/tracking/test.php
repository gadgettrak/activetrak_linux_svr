<?php

require_once("XML/Serializer.php");
require_once('Mail.php');
require_once('Mail/mime.php');
require_once("bootstrapper.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceConnection.php");
require_once("classes/DeviceConnectionManager.php");
require_once("classes/HttpUtils.php");

try {
	HttpUtils::fireAndForget("http://ip.gadgettrak.com/createConnection.php", array("src" => "IPHONEV2", "ip" => $_SERVER['REMOTE_ADDR'], "pass" => "415f202e5615778c16768d9d4fa417ed71cfe69f"));
} catch (Exception $e) {
	// hit me, you can't hurt me... suck my kiss.
}

$deviceKey = $_POST['devicekey'];
$currentUser = Authorization::validateUser($deviceKey);

HttpUtils::requirePostRequest();
$serializer = new XML_Serializer(array('rootName' => 'DeviceData'));
$deviceManager = new DeviceManager();
$deviceConnectionManager = new DeviceConnectionManager();
$device = $deviceManager->isValidIPhoneRequest($deviceKey, $currentUser->userid);

if($device) {

	$url = 'http://ws.geonames.org/findNearestAddress?lng=' . urlencode($_POST['longitude']) . '&lat='. urlencode($_POST['latitude']);
	$geo= simplexml_load_file($url); // Need to typecast, or it'll be an object

	// Save device connection
	if($device->theft_status == "Y" && $device->saveTracking == "Y") {
		$now = gmdate("Y-m-d H:i:s");
		$deviceConnection = new DeviceConnection();
		$deviceConnection->timestamp = $now;
		$deviceConnection->devicekey = $deviceKey;
		$deviceConnection->public_ip = $_SERVER['REMOTE_ADDR'];
		$deviceConnection->ip_lat = $_POST['latitude'];
		$deviceConnection->ip_lon = $_POST['longitude'];
		$deviceConnection->loc_street = addslashes($geo->address[0]->streetNumber) . " " . addslashes($geo->address[0]->street);
		$deviceConnection->loc_city = addslashes($geo->address[0]->placename);
		$deviceConnection->loc_state = addslashes($geo->address[0]->adminName1);
		$deviceConnection->agent = addslashes($_POST['phonemodel']);
		$deviceConnection->serial = addslashes($_POST['serialnumber']);
		$deviceConnection->did = addslashes($device->deviceid);
		$deviceConnection->os = addslashes($_POST['os']);
		$deviceConnection->stolen = 'Y';
		$deviceConnectionManager->saveNewDeviceConnection($deviceConnection);
	}

	// SEND MAIL
	$message = new Mail_mime();

	/*
	try {
		$fname = $device->devicekey . "_" . time() . "_back.txt";
		$handle = fopen("/var/www/vhosts/gadgettrak.com/subdomains/gt1/includes/images/" . $fname, "w") or die("Couldn't create new file");
		fwrite($handle, $_POST['backImage']);
		fclose($handle);

		$fname = $device->devicekey . "_" . time() . "_front.txt";
		$handle = fopen("/var/www/vhosts/gadgettrak.com/subdomains/gt1/includes/images/" . $fname, "w") or die("Couldn't create new file");
		fwrite($handle, $_POST['frontImage']);
		fclose($handle);

	} catch (Exception $e) {
		//
	}
	*/

	if(isset($_POST['frontImage']) || isset($_POST['backImage'])) {

		if(isset($_POST['backImage'])) {

			$fileName = $device->devicekey . "_" . time() . "_back.jpg";
			$directoryPath = "/var/www/vhosts/gadgettrak.com/subdomains/gt1/includes/images";

			if(!is_dir($directoryPath)) {
				mkdir($directoryPath , 0777);
			}

			$backFilePath = $directoryPath . "/" . $fileName;
			$imageFileHandle = fopen($backFilePath, "w");
			fwrite($imageFileHandle, base64_decode($_POST['backImage']));
			fclose($imageFileHandle);
			$message->addAttachment($backFilePath, 'quoted-printable', 'BackCameraCapture.jpg', true, 'base64');
		}

		if(isset($_POST['frontImage'])) {

			$fileName = $device->devicekey . "_" . time() . "_front.jpg";
			$directoryPath = "/var/www/vhosts/gadgettrak.com/subdomains/gt1/includes/images";

			if(!is_dir($directoryPath)) {
				mkdir($directoryPath , 0777);
			}

			$frontFilePath = $directoryPath . "/" . $fileName;
			$imageFileHandle = fopen($frontFilePath, "w");
			fwrite($imageFileHandle, base64_decode($_POST['frontImage']));
			fclose($imageFileHandle);
			$message->addAttachment($frontFilePath, 'quoted-printable', 'FrontCameraCapture.jpg', true, 'base64');
		}
	}

	ob_start();
	include("templates/email/iphone-report-text.php");
	$text = ob_get_contents();
	ob_end_clean();

	ob_start();
	include("templates/email/iphone-report-html.php");
	$html = ob_get_contents();
	ob_end_clean();

	$message->setTXTBody($text);
	$message->setHTMLBody($html);

	$extraheaders = array("To"=>$currentUser->email, "From"=>"info@gadgettrak.com", "Subject"=>"GadgetTrak Tracking Info", "text_charset" => "utf-8");

	$param['text_charset'] = 'utf-8';
	$param['html_charset'] = 'utf-8';
	$param['head_charset'] = 'utf-8';

	$body = $message->get($param);
	$headers = $message->headers($extraheaders);

	$smtp = Mail::factory('smtp');
	$success = $smtp->send($currentUser->email, $headers, $body);

	if($success && isset($_POST['backImage'])) {
		unlink($backFilePath);
	}

	if($success && isset($_POST['frontImage'])) {
		unlink($frontFilePath);
	}

	if(CONFIG_APP_LOGGING) Logger::logToFile("IPHONE:: Device tracked: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString());

	// Send response
	$temp = array(
		"Status" => ($device->theft_status == "Y")?"3":"1",
		"Description" => stripslashes($device->description)
	);
	
	$serializer->serialize($temp);
	HttpUtils::setHttpStatus(201);
	print($serializer->getSerializedData());

} else {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Not valid device request: (deviceKey: $deviceKey)" . Authorization::getCurrentUserCredentials()->toString());
	HttpUtils::setHttpStatus(403);
}

?>