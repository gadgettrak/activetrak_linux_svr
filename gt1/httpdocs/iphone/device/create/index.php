<?php


require_once("XML/Serializer.php");
require_once("bootstrapper.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceMap.php");
require_once("classes/DeviceMapManager.php");
require_once("classes/UserManager.php");
require_once("classes/License.php");
require_once("classes/LicenseManager.php");
require_once("classes/Mailer.php");

/*
$t = "";
foreach($_SERVER as $k=>$v) {
	$t .= "[" . $k . "=" . $v . "] & ";
}
Logger::logToFile("IPHONE:: " . $t . "|| " . Authorization::getCurrentUserCredentials()->toString());
 */

$deviceKey = $_POST['devicekey'];
$currentUser = Authorization::validateUser($deviceKey);
HttpUtils::requirePostRequest();
$userManager = new userManager();
$deviceManager = new DeviceManager();
$deviceMapManager = new DeviceMapManager();

//Check to see if device exists
if($deviceManager->isDuplicateDeviceByGuid($deviceKey)) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("IPHONE:: iPhone device already exists: " . Authorization::getCurrentUserCredentials()->toString());
	HttpUtils::setHttpStatus(303);
	die();
}

//Create new device and devicemap objects

$device = new Device();
$device->type = $_POST['type'];
$device->model = $_POST['model'];
$device->manufacturer = $_POST['manufacturer'];
$device->os = $_POST['os'];
$device->theft_status = 'N';
$device->serial = $_POST['serial'];
$device->color = $_POST['color'];
$device->description = str_replace("â€™", "'", $_POST['description']);
$device->devicekey = $deviceKey;
$device->macAddress = $_POST['macAddress'];
$device->datecreated = gmdate("Y-m-d H:i:s");
$device->productid = '8';
$device->version = $_POST['version'];

$device->devicePassword = $currentUser->password;

$deviceMap = new DeviceMap(null, $currentUser->userid, $device->devicekey);

$device = $deviceManager->addNewDevice($device);
$deviceMap = $deviceMapManager->addNewDeviceMap($deviceMap);

if($device->deviceid != null && $deviceMap->mapid != null) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("IPHONE:: Created device: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString());

	HttpUtils::setHttpStatus(201);
	Mailer::sendiPhoneRegistrationNotice($currentUser);
} else {
	if(CONFIG_APP_LOGGING) Logger::logToFile("IPHONE:: Couldn't create device: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(500);
}

?>