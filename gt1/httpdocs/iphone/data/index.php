<?php

require_once("XML/Serializer.php");
require_once("bootstrapper.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/PushAlias.php");
require_once("classes/PushAliasManager.php");
require_once("classes/HttpUtils.php");

try {
	HttpUtils::fireAndForget("http://ip.gadgettrak.com/createConnection.php", array("src" => "iOSDATA", "ip" => $_SERVER['REMOTE_ADDR'], "pass" => "415f202e5615778c16768d9d4fa417ed71cfe69f"));
} catch (Exception $e) {
	// hit me, you can't hurt me... suck my kiss.
}

$deviceKey = $_POST['devicekey'];
$currentUser = Authorization::validateUser($deviceKey);

HttpUtils::requirePostRequest();
$deviceManager = new DeviceManager();
$pushAliasManager = new PushAliasManager();
$serializer = new XML_Serializer(array('rootName' => 'DeviceData'));
$device = $deviceManager->isValidIPhoneRequest($deviceKey, $currentUser->userid);

if($device) {

	if(isset($_POST['deviceToken'])) {
		$pushAlias = new PushAlias($deviceKey, $_POST['deviceToken']);
		$pushAliasManager->createUpdate($pushAlias);
	}

	if(isset($_POST['description'])) {
		$device->description = $_POST['description'];
		$deviceManager->updateDevice($device);
	}

	if(isset($_POST['version'])) {
		$device->version = $_POST['version'];
		$deviceManager->updateDevice($device);
	}


	$temp = array(
		"Status" => ($device->theft_status == "Y")?"3":"1",
		"Description" => stripslashes($device->description)
	);

	$serializer->serialize($temp);
	HttpUtils::setHttpStatus(201);
	print($serializer->getSerializedData());

	//if(CONFIG_APP_LOGGING) Logger::logToFile("IPHONE:: Device updated: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString());

} else {
	//if(CONFIG_APP_LOGGING) Logger::logToFile("IPHONE:: Not valid device update request:  (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString());
	HttpUtils::setHttpStatus(403);
}
?>