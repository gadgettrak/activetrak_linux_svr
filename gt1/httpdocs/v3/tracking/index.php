<?php
require_once('Mail.php');
require_once('Mail/mime.php');
require_once("XML/Serializer.php");

require_once("bootstrapper.php");
require_once("classes/BaseDAO.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceMap.php");
require_once("classes/DeviceMapManager.php");
require_once("classes/DeviceConnection.php");
require_once("classes/DeviceConnectionManager.php");

require_once("classes/License.php");
require_once("classes/LicenseManager.php");
require_once("classes/Mailer.php");
require_once("classes/HttpUtils.php");

try {
	HttpUtils::fireAndForget("http://ip.gadgettrak.com/createConnection.php", array("src" => "V3TRACKING", "ip" => $_SERVER['REMOTE_ADDR'], "pass" => "415f202e5615778c16768d9d4fa417ed71cfe69f"));
} catch (Exception $e) {
	// hit me, you can't hurt me... suck my kiss.
}

$deviceKey = $_SERVER["HTTP_DEVICEID"];
$currentUser = Authorization::validateUser($deviceKey);

HttpUtils::requirePostRequest();

$serializer = new XML_Serializer(array('rootName' => 'DeviceData'));
$deviceManager = new DeviceManager();
$deviceConnectionManager = new DeviceConnectionManager();
$licenseManager = new LicenseManager();

try {
	$xml = HttpUtils::getXMLPayload(true);
} catch (Exception $e) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Error calling getXMLPayload :" . $e->getMessage());
}

if(!$xml) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("NO XML!: " . file_get_contents('php://input'));

	//This saves the bad XML to file for debugging. This should not be left on for long and data saved to the server
	//should be removed ASAP.

	try {
		$fname = $deviceKey . "_" . time() . ".txt";
		$handle = fopen("/var/www/gadgettrak.com/gt1/httpdocs/v3/error_dump/" . $fname, "w") or die("Couldn't create new file");
		fwrite($handle, file_get_contents('php://input'));
		fclose($handle);
	} catch (Exception $e) {
		if(CONFIG_APP_LOGGING) Logger::logToFile("Error saving bad XML");
	}

}

$device = $deviceManager->isValidDeviceRequest($deviceKey, $currentUser->userid);

//if(CONFIG_APP_LOGGING) Logger::logToFile("Tracking Called: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());

if($device) {

	$license = $licenseManager->getLicenseByDeviceId($device->deviceid);

	//Check to see if key is expired
	if(!$license || $license->isExpired()) {
		//if(CONFIG_APP_LOGGING) Logger::logToFile("License key expired: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
		HttpUtils::setHttpStatus(403);
		die();
	}

	// SEND MAIL

	/* In the event we need to receive emails as well for assisting in tracking stolen device,
	 * add a device key into the list here to enable bcc to emails.
	 */

	//ea6497223442676a30c6bda5c03d1c4e2d6999bc : soggymonkey laptop
	//1285997ecffda2478fa83aada859f473d2d20976 : david douglas laptop
	//e8d51090a2830384bb08b0dffcee4398468fe895 : 
        //c97ae79e03cb9bfeb9983af1d1300aacb0d65668 : Ty Dopham lequitymd@yahoo.com
	//dd582fefd012cd3fbd7df1a4924fbbb464c7ef04 : pam communicatewithclass 
        //f5d64cda27ef5476cedb9af565dcb7af96ecaff8 : Benjamin Yao
	//77d2e23798f1c10645e433a63fb9333f6c37ca13 : pobluck@loanmanaz.com
        //3aa0e52063ff387e294b2701cdee256b333bea54 : scott davis
	$bccDevices = array("3aa0e52063ff387e294b2701cdee256b333bea54","77d2e23798f1c10645e433a63fb9333f6c37ca13","6c93843e8fd603e1a4b9c891bf5e85df7a1c22e8","dd582fefd012cd3fbd7df1a4924fbbb464c7ef04", "349a3ac74e2e36a7f06a191b83b679649b960c63","01349b5a6c3ade4dc66e9c6bad48fc059af14a02");

	$message = new Mail_mime();

	ob_start();
	include("templates/email/report-text.php");
	$text = ob_get_contents();
	ob_end_clean();

	ob_start();
	include("templates/email/report-html.php");
	$html = ob_get_contents();
	ob_end_clean();

	if(isset($xml->Image)) {

		$fileName = $device->devicekey . "_" . time() . ".jpg";
		
		$directoryPath = "/var/www/gadgettrak.com/gt1/includes/images";
		
		if(!is_dir($directoryPath)) {
			mkdir($directoryPath , 0777);
		}

		$filePath = $directoryPath . "/" . $fileName;
		$imageFileHandle = fopen($filePath, "w");
		fwrite($imageFileHandle, base64_decode((string)$xml->Image));
		fclose($imageFileHandle);
		$message->addAttachment($filePath, 'quoted-printable', 'CamCapture.jpg', true, 'base64');

	}

	//$bccDevices

	$message->setTXTBody($text);
	$message->setHTMLBody($html);
	$body = $message->get();

	$extraheaders = array("To"=>$currentUser->email, "From"=>"info@gadgettrak.com", "Subject"=>"GadgetTrak Tracking Info");

	$headers = $message->headers($extraheaders);


	$smtp = Mail::factory('smtp', array("host"=>"intGTmail"));

	$success = $smtp->send($currentUser->email, $headers, $body);

	if(in_array($deviceKey, $bccDevices)) {
		//$smtp->send("msweet@alien109.com", $headers, $body);
		$smtp->send("kwestin@activetrak.com", $headers, $body);
	}

	// Send response
	$temp = array(
		"Status" => ($device->theft_status == "Y")?"3":"1",
		"Description" => stripslashes($device->description)
	);

	if($success && isset($xml->Image)) {
		unlink($filePath);
	}

	if($device->theft_status == "Y" && $device->saveTracking == "Y") {
		$now = gmdate("Y-m-d H:i:s");
		$deviceConnection = new DeviceConnection();
		$deviceConnection->ip_lat = $xml->Latitude;
		$deviceConnection->ip_lon = $xml->Longitude;
		$deviceConnection->username = $xml->UserName;
		$deviceConnection->devicekey = $deviceKey;
		$deviceConnection->internal_ip = $xml->InternalIP;
		$deviceConnection->public_ip = $xml->ExternalIP;
		$deviceConnection->timestamp = $now;
		$deviceConnection->agent = "Computer";
		$deviceConnection->os = $xml->OS;
		$deviceConnection->stolen = 'Y';
		$deviceConnectionManager->saveNewDeviceConnection($deviceConnection);
	}

	$serializer->serialize($temp);
	HttpUtils::setHttpStatus(201);
	print($serializer->getSerializedData());

	if(isset($xml->Image)) {
		try {
			$tempDao = new BaseDAO();
			$q = "UPDATE stats set count = (count + 1) where `type`='laptopPhotos'";
			$tempDao->execute($q);

		} catch (Exception $e) {
			error_log($e->getMessage());
		}
	}

	try {
		$tempDao = new BaseDAO();
		$q = "UPDATE stats set count = (count + 1) where `type`='trackingRequests'";
		$tempDao->execute($q);

	} catch (Exception $e) {
		error_log($e->getMessage());
	}

	if(CONFIG_APP_LOGGING) Logger::logToFile("Device tracked: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $serializer->getSerializedData());

} else {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Not valid device request: (deviceKey: $deviceKey)" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(403);
}

?>
