<?php

require_once("XML/Serializer.php");
require_once("bootstrapper.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/License.php");
require_once("classes/LicenseManager.php");
require_once("classes/HttpUtils.php");

try {
	HttpUtils::fireAndForget("http://ip.gadgettrak.com/createConnection.php", array("src" => "V3DATA", "ip" => $_SERVER['REMOTE_ADDR'], "pass" => "415f202e5615778c16768d9d4fa417ed71cfe69f"));
} catch (Exception $e) {
	// hit me, you can't hurt me... suck my kiss.
}

// Temp hax to fix issue where client is flooding the server with update requests.
// Most of these requests are being sent with a blank email/password so  if we respond
// with everthing is fine, the client will maintain normal update cycles.
if($_SERVER['PHP_AUTH_PW'] == "da39a3ee5e6b4b0d3255bfef95601890afd80709" || $_SERVER['PHP_AUTH_PW'] == "DA39A3EE5E6B4B0D3255BFEF95601890AFD80709") {
	$xml = HttpUtils::getXMLPayload();
	//Logger::logToFile("Blank password: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	$temp = array(
		"Status" => "1",
		"Description" => "description"
	);
	$serializer = new XML_Serializer(array('rootName' => 'DeviceData'));
	$serializer->serialize($temp);
	HttpUtils::setHttpStatus(200);
	print($serializer->getSerializedData());
	die();
}

$deviceKey = $_SERVER["HTTP_DEVICEID"];
$currentUser = Authorization::validateUser($deviceKey);

$serializer = new XML_Serializer(array('rootName' => 'DeviceData'));
$deviceManager = new DeviceManager();
$licenseManager = new LicenseManager();

$xml = HttpUtils::getXMLPayload();

$license = $licenseManager->getLicenseByKey((string)$xml->LicenseKey);

$device = $deviceManager->isValidDeviceRequest($deviceKey, $currentUser->userid, (string)$xml->LicenseKey);

//Check to see if key is expired
if(!$license || $license->isExpired()) {
	//if(CONFIG_APP_LOGGING) Logger::logToFile("License key expired: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(403);
	die();
}

if($device) {

	if(isset($xml->Description) || isset($xml->MACAddress)) {
		if(isset($xml->Description)) {
			$device->description = (string)$xml->Description;
		}
		if(isset($xml->MACAddress)) {
			$device->macAddress = (string)$xml->MACAddress;
		}
		$deviceManager->updateDevice($device);
	}

	$temp = array(
		"Status" => ($device->theft_status == "Y")?"3":"1",
		"Description" => stripslashes($device->description)
	);

	$serializer->serialize($temp);
	HttpUtils::setHttpStatus(201);
	print($serializer->getSerializedData());

	if(CONFIG_APP_LOGGING) Logger::logToFile("Device updated: (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $serializer->getSerializedData());

} else {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Not valid device update request:  (" . $deviceKey . ")" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(403);
}
?>