<?php

require_once("XML/Serializer.php");
require_once("bootstrapper.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceMap.php");
require_once("classes/DeviceMapManager.php");
require_once("classes/UserManager.php");
require_once("classes/License.php");
require_once("classes/LicenseManager.php");
require_once("classes/Mailer.php");
require_once("classes/User.php");

// @var $currentUser User
$currentUser = Authorization::validateUser();

HttpUtils::requirePostRequest();

$serializer = new XML_Serializer(array('rootName' => 'DeviceData'));
$userManager = new userManager();
$deviceManager = new DeviceManager();
$deviceMapManager = new DeviceMapManager();
$licenseManager = new LicenseManager();

$xml = HttpUtils::getXMLPayload();
$license = $licenseManager->getLicenseByKey((string)$xml->LicenseKey);

//Check to see if key is valid
if(!$license) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Not valid license key: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(403);
	die();
}

//Check to see if key is expired
if($license->isExpired()) {
	//if(CONFIG_APP_LOGGING) Logger::logToFile("License key expired: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(403);
	die();
}

//Check to see if key is already associated with a device
if(isset($license->deviceId)) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("License key already used: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(409);
	die();
}

//Check to see if device exists
if($deviceManager->isDuplicateDevice($xml->MACAddress[0])) {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Device already exists: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(303);
	die();
}

//Create new device and devicemap objects
$device = Device::newFromSimpleXML($xml, $currentUser);
$deviceMap = new DeviceMap(null, $currentUser->userid, $device->devicekey);

if(CONFIG_APP_LOGGING) Logger::logToFile("Instantiated new devicemap: " . $deviceMap->toString());

$device->productid = $license->productId;
$device->version = '3';
$device = $deviceManager->addNewDevice($device);

$deviceMap = $deviceMapManager->addNewDeviceMap($deviceMap);

if(CONFIG_APP_LOGGING) Logger::logToFile("DeviceMap after save: " . $deviceMap->toString());
if(CONFIG_APP_LOGGING) Logger::logToFile("Device after save: " . $device->toString());


if($device->deviceid != null && $deviceMap->mapid != null) {

	$license->userId = $currentUser->userid;
	$license->deviceId = $device->deviceid;

	if(!$license->activated || $license->activated == "0000-00-00 00:00:00") {
		$license->activated = gmdate("Y-m-d H:i:s");
	}

	$license->used = "Y";
	$licenseManager->updateLicense($license);

	HttpUtils::setHttpStatus(201);

	//Create a response object with the new GUID of the device to serialize as XML
	$serializer->serialize(array("IDGUID" => $device->devicekey));
	print($serializer->getSerializedData());

	Mailer::sendDeviceRegistrationNotice($currentUser, $license, $device);

	if(CONFIG_APP_LOGGING) Logger::logToFile("Serialized device data: " . $serializer->getSerializedData() . " :: deviceKey=" . $device->devicekey);
	if(CONFIG_APP_LOGGING) Logger::logToFile("Device created with guid: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $device->devicekey);
	if(CONFIG_APP_LOGGING) Logger::logToFile("DeviceMap created with guid: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $deviceMap->toString());

} else {

	if(CONFIG_APP_LOGGING) Logger::logToFile("Couldn't create device: " . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(500);

}

?>