<?php

require_once("bootstrapper.php");
require_once("classes/Logger.php");
require_once("classes/Device.php");
require_once("classes/DeviceManager.php");
require_once("classes/DeviceMap.php");
require_once("classes/DeviceMapManager.php");
require_once("classes/License.php");
require_once("classes/LicenseManager.php");

$deviceKey = $_SERVER["HTTP_DEVICEID"];
$currentUser = Authorization::validateUser($deviceKey);

HttpUtils::requirePostRequest();

$deviceManager    = new DeviceManager();
$deviceMapManager = new DeviceMapManager();
$licenseManager   = new LicenseManager();


$xml = HttpUtils::getXMLPayload();

$device = $deviceManager->getDeviceForDeleteRequest($deviceKey, (string)$xml->LicenseKey, (string)$xml->MACAddress, $currentUser->userid);

if($device) {
	$a = $deviceManager->deleteDeviceById($device->deviceid);
	$b = $deviceMapManager->deleteDeviceMapByKey($device->devicekey);
	$c = $licenseManager->resetLicense((string)$xml->LicenseKey);
	HttpUtils::setHttpStatus(204);
	if(CONFIG_APP_LOGGING) Logger::logToFile("Deleted device: ($deviceKey - $device->deviceid)" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
} else {
	if(CONFIG_APP_LOGGING) Logger::logToFile("Not valid device delete request: ($deviceKey)" . Authorization::getCurrentUserCredentials()->toString() . " :: " . $xml->asXML());
	HttpUtils::setHttpStatus(403);
}

?>