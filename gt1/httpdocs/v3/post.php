<?php

$header = array(
	"POST HTTP/1.0",
	"Content-type: text/xml",
	"Content-transfer-encoding: text",
	$authHeader
);

$action = $_GET['action'];

if(strpos($_SERVER['HTTP_HOST'], "localhost") === false) {
	$URLPrefix = "https://gt1.gadgettrak.com/v3";
} else {
	$URLPrefix = "http://localhost:8085";
}

switch($action) {
	case "createDevice":
		$url = "$URLPrefix/licenses/create/";
		$post_string = file_get_contents("xml/device_create.xml");
		break;
	case "updateDevice":
		$url = "$URLPrefix/tracking/";
		$post_string = file_get_contents("/xml/device_update.xml");
		array_push($header, "DeviceID: AAAAAAAAAA");
		break;
	case "trackDevice":
		$url = "$URLPrefix/data/";
		$post_string = file_get_contents("xml/device_tracking.xml");
		array_push($header, "DeviceID: AAAAAAAAAA");
		break;
	case "deleteDevice":
		$url = "$URLPrefix/licenses/delete/";
		$post_string = file_get_contents("xml/device_delete.xml");
		array_push($header, "DeviceID: AAAAAAAAAA");
		break;
	default:
		die("All your actions are belong to us!");
}

array_push($header, "Connection: close");

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 4);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string); 
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

$data = curl_exec($ch); 

if(curl_errno($ch)) {
	print curl_error($ch);
} else {
	curl_close($ch);
	print_r($data);
}

?>