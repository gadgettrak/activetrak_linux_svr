<?php

require_once("test/inc/DeviceManager.php");

$deviceData = array(
	"type" => "iPad",
	"model" => "3G",
	"manufacturer" => "apple",
	"serial" => "X9X9",
	"description" => "Matts iPad",
	"devicekey" => "123456789XXXXXXXXX",
	"version" => "iPhoneTestScript1.0",
);

$deviceData = http_build_query($deviceData);

Utils::debug("Device Data: $deviceData");

Utils::dumpHeader("Creating Devices");

$output = DeviceManager::create("fugged@gmail.com", "9405879011d5cfe6fc869f115cafda789cd2f69c", $deviceData);
Utils::debug("Result: " . $output->headers['http_code']);

?>